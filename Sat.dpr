program SAT;

{$R *.dres}

uses
  System.StartUpCopy,
  FMX.Forms,
  Winapi.Windows,
  TratandoErro,
  uAtualizador,
  Model.LibUtil,
  uCliente.CFe in 'Model\CFe\Dados\uCliente.CFe.pas',
  uImposto.CFe in 'Model\CFe\Dados\uImposto.CFe.pas',
  uImposto.COFINS.CFe in 'Model\CFe\Dados\uImposto.COFINS.CFe.pas',
  uImposto.COFINSST.CFe in 'Model\CFe\Dados\uImposto.COFINSST.CFe.pas',
  uImposto.ICMS.CFe in 'Model\CFe\Dados\uImposto.ICMS.CFe.pas',
  uImposto.ISSQN.CFe in 'Model\CFe\Dados\uImposto.ISSQN.CFe.pas',
  uImposto.PIS.CFe in 'Model\CFe\Dados\uImposto.PIS.CFe.pas',
  uImposto.PISST.CFe in 'Model\CFe\Dados\uImposto.PISST.CFe.pas',
  uProduto.CFe in 'Model\CFe\Dados\uProduto.CFe.pas',
  uIdentificacao.CFe in 'Model\CFe\Dados\uIdentificacao.CFe.pas',
  uItem.CFe in 'Model\CFe\Dados\uItem.CFe.pas',
  uPagamento.CFe in 'Model\CFe\Dados\uPagamento.CFe.pas',
  uTotal.CFe in 'Model\CFe\Dados\uTotal.CFe.pas',
  uCFe in 'Model\CFe\uCFe.pas',
  uCFe.Txt in 'Model\CFe\uCFe.Txt.pas',
  Form.Principal in 'View\Form.Principal.pas' {FormHome},
  uVm.Principal in 'ViewModel\uVm.Principal.pas' {dmVmPrincipal: TDataModule},
  uValidaIE in 'Model\Utils\uValidaIE.pas',
  uDBTributacaoAuxiliarContexto in 'Repository\DbsAuxiliares\uDBTributacaoAuxiliarContexto.pas',
  uDmCons in 'DMs\uDmCons.pas' {dmCons: TDataModule},
  uListaNCMRepository in 'Repository\DbsAuxiliares\uListaNCMRepository.pas',
  ListaCEST in 'Model\Entidades\ListaCEST.pas',
  uVendaRepository in 'Repository\uVendaRepository.pas',
  ListaNCM in 'Model\Entidades\ListaNCM.pas',
  uListaCESTRepository in 'Repository\DbsAuxiliares\uListaCESTRepository.pas',
  uVm.SoftHouse in 'ViewModel\uVm.SoftHouse.pas',
  uVm.Contabilidade in 'ViewModel\uVm.Contabilidade.pas',
  uVm.Estabelecimento in 'ViewModel\uVm.Estabelecimento.pas',
  uVm.Pasta in 'ViewModel\uVm.Pasta.pas',
  uVm.Impressora in 'ViewModel\uVm.Impressora.pas',
  uVm.Sat in 'ViewModel\uVm.Sat.pas',
  uCategoria in 'Model\Produto\uCategoria.pas',
  uProduto in 'Model\Produto\uProduto.pas',
  uContabilidade in 'Model\Sistema\uContabilidade.pas',
  uEstabelecimento in 'Model\Sistema\uEstabelecimento.pas',
  uImpressora in 'Model\Sistema\uImpressora.pas',
  uPastas in 'Model\Sistema\uPastas.pas',
  uSAT in 'Model\Sistema\uSAT.pas',
  uSoftwareHouse in 'Model\Sistema\uSoftwareHouse.pas',
  uUsuario in 'Model\Sistema\uUsuario.pas',
  uValidacoes in 'Model\Utils\uValidacoes.pas',
  uCliente in 'Model\Venda\uCliente.pas',
  uCupom in 'Model\Venda\uCupom.pas',
  uVenda in 'Model\Venda\uVenda.pas',
  uVendaProduto in 'Model\Venda\uVendaProduto.pas',
  uSAT.Equipamento in 'ACBr\uSAT.Equipamento.pas',
  uSAT.Impressora in 'ACBr\uSAT.Impressora.pas',
  uSistema in 'Model\Sistema\uSistema.pas',
  uDBControleContext in 'Repository\DbControle\uDBControleContext.pas',
  uCategoriaRepository in 'Repository\uCategoriaRepository.pas',
  uVm.Categoria in 'ViewModel\uVm.Categoria.pas',
  uDadosDbContext in 'Repository\uDadosDbContext.pas',
  uProdutoRepository in 'Repository\uProdutoRepository.pas',
  uVm.Produto in 'ViewModel\uVm.Produto.pas',
  uUsuarioReporitory in 'Repository\uUsuarioReporitory.pas',
  uVm.Usuario in 'ViewModel\uVm.Usuario.pas',
  uCriador.Cupom in 'Model\Venda\uCriador.Cupom.pas',
  uVm.CriadorCupom in 'ViewModel\uVm.CriadorCupom.pas',
  uCFe.CriadorVenda in 'Model\CFe\uCFe.CriadorVenda.pas',
  uVm.Sistema in 'ViewModel\uVm.Sistema.pas',
  uConverteParaDadosDb in 'Repository\DbControle\uConverteParaDadosDb.pas',
  uVendaRelatorios in 'Model\Venda\uVendaRelatorios.pas',
  uFrmLogin in 'View\uFrmLogin.pas' {frmLogin},
  uAuditoriaContexto in 'Repository\Auditoria\uAuditoriaContexto.pas',
  uAuRepository in 'Repository\Auditoria\uAuRepository.pas',
  uAuditoria in 'Model\Auditoria\uAuditoria.pas',
  uFrmAuditoria in 'View\uFrmAuditoria.pas' {FrmAuditoria},
  uVm.Auditoria in 'ViewModel\uVm.Auditoria.pas',
  uScriptInsercaoCategorias in 'Repository\uScriptInsercaoCategorias.pas';

{$R *.res}


var
  programa, atualizador: THandle;
  titulo, atualizadorTitulo: string;
begin
  Tratamento.MostraMensagem:= true;
  ReportMemoryLeaksOnShutdown:= false;
  titulo:= 'SICLOP SAT';
  atualizadorTitulo:= 'Atualizador SICLOP';
  Application.Title:=titulo;
  programa := FindWindow(nil, PWideChar(titulo));
  atualizador:= FindWindow(nil, PWideChar(atualizadorTitulo));
  if (programa = 0) and (atualizador = 0) then
  begin
    iniciaComWindows('SAT', ParamStr(0));
    Application.Initialize;
    Application.CreateForm(TdmCons, dmCons);
  Application.CreateForm(TfrmLogin, frmLogin);
  Application.Run;
  end
  else if (programa = 0) then
    SetForegroundWindow(programa)
  else if (atualizador = 0) then
    SetForegroundWindow(atualizador);

end.
