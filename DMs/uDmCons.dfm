object dmCons: TdmCons
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 249
  Width = 414
  object ConAuxTributacao: TFDConnection
    Params.Strings = (
      
        'Database=C:\Users\Renan Lucilio\Documents\Projetos\Sat\SAT\Win32' +
        '\Debug\AuxTributacao.db'
      'OpenMode=ReadOnly'
      'DriverID=SQLite')
    LoginPrompt = False
    Left = 32
    Top = 8
  end
  object conDados: TFDConnection
    Params.Strings = (
      
        'Database=C:\Users\Renan Lucilio\Documents\Projetos\Sat\SAT\Win32' +
        '\Debug\dados.db'
      'OpenMode=ReadWrite'
      'LockingMode=Normal'
      'DriverID=SQLite')
    LoginPrompt = False
    Left = 112
    Top = 8
  end
  object scrCriacaoDb: TFDScript
    SQLScripts = <
      item
        Name = 'criaDb'
        SQL.Strings = (
          '-- Table: Categoria'
          'DROP TABLE IF EXISTS Categoria;'
          
            'CREATE TABLE Categoria (Id INTEGER NOT NULL CONSTRAINT PK_Catego' +
            'ria PRIMARY KEY AUTOINCREMENT, Descricao TEXT NOT NULL, Ncm TEXT' +
            ' NOT NULL, Cest TEXT NOT NULL, Cfop TEXT NOT NULL, Cofins_CST IN' +
            'TEGER NOT NULL, Cofins_BaseCalculo REAL NOT NULL, Cofins_Porcent' +
            'o REAL NOT NULL, Cofins_Valor REAL NOT NULL, Cofins_BaseCalculoP' +
            'orProduto REAL NOT NULL, Cofins_Aliquota REAL NOT NULL, CofinsSt' +
            '_BaseCalculo REAL NOT NULL, CofinsSt_Porcento REAL NOT NULL, Cof' +
            'insSt_Valor REAL NOT NULL, CofinsSt_BaseCalculoPorProduto REAL N' +
            'OT NULL, CofinsSt_Aliquota REAL NOT NULL, Pis_CST INTEGER NOT NU' +
            'LL, Pis_BaseCalculo REAL NOT NULL, Pis_Porcento REAL NOT NULL, P' +
            'is_Valor REAL NOT NULL, Pis_BaseCalculoPorProduto REAL NOT NULL,' +
            ' Pis_Aliquota REAL NOT NULL, PisSt_BaseCalculo REAL NOT NULL, Pi' +
            'sSt_Porcento REAL NOT NULL, PisSt_Valor REAL NOT NULL, PisSt_Bas' +
            'eCalculoPorProduto REAL NOT NULL, PisSt_Aliquota REAL NOT NULL, ' +
            'Icms_Origem INTEGER NOT NULL, Icms_Cst INTEGER NOT NULL, Icms_Po' +
            'rcento REAL NOT NULL, Icms_Valor REAL NOT NULL, Issqn_BaseCalcul' +
            'o REAL NOT NULL, Issqn_Deducao REAL NOT NULL, Issqn_CodigoIbge T' +
            'EXT NOT NULL, Issqn_BaseCalculoProduto REAL NOT NULL, Issqn_Item' +
            'DaListaServico TEXT NOT NULL, Issqn_CodigoTributarioIssqn TEXT N' +
            'OT NULL, Issqn_Natureza INTEGER NOT NULL, TaxaEstadual REAL NOT ' +
            'NULL, TaxaFederal REAL NOT NULL);'
          ''
          '-- Table: Cliente'
          'DROP TABLE IF EXISTS Cliente;'
          
            'CREATE TABLE Cliente (Id INTEGER NOT NULL CONSTRAINT PK_Cliente ' +
            'PRIMARY KEY AUTOINCREMENT, Nome TEXT DEFAULT ('#39'Desconhecido'#39'), D' +
            'ocumento TEXT DEFAULT ('#39'Desconhecido'#39'));'
          ''
          '-- Table: Cupom'
          'DROP TABLE IF EXISTS Cupom;'
          
            'CREATE TABLE Cupom (Id INTEGER NOT NULL CONSTRAINT PK_Cupom PRIM' +
            'ARY KEY AUTOINCREMENT, Nome TEXT NOT NULL, ArquivoXml TEXT NOT N' +
            'ULL, ArquivoPdf TEXT NOT NULL, Cancelado INTEGER DEFAULT (0), Ge' +
            'rado INTEGER DEFAULT (0));'
          ''
          '-- Table: Produto'
          'DROP TABLE IF EXISTS Produto;'
          
            'CREATE TABLE Produto (Id INTEGER NOT NULL CONSTRAINT PK_Produto ' +
            'PRIMARY KEY AUTOINCREMENT, Descricao TEXT NOT NULL, Preco REAL N' +
            'OT NULL, UnidadeMedida TEXT NOT NULL, CategoriaId INTEGER NOT NU' +
            'LL, Ncm TEXT NOT NULL, Cest TEXT NOT NULL, Cfop TEXT NOT NULL, C' +
            'ofins_CST INTEGER NOT NULL, Cofins_BaseCalculo REAL NOT NULL, Co' +
            'fins_Porcento REAL NOT NULL, Cofins_Valor REAL NOT NULL, Cofins_' +
            'BaseCalculoPorProduto REAL NOT NULL, Cofins_Aliquota REAL NOT NU' +
            'LL, CofinsSt_BaseCalculo REAL NOT NULL, CofinsSt_Porcento REAL N' +
            'OT NULL, CofinsSt_Valor REAL NOT NULL, CofinsSt_BaseCalculoPorPr' +
            'oduto REAL NOT NULL, CofinsSt_Aliquota REAL NOT NULL, Pis_CST IN' +
            'TEGER NOT NULL, Pis_BaseCalculo REAL NOT NULL, Pis_Porcento REAL' +
            ' NOT NULL, Pis_Valor REAL NOT NULL, Pis_BaseCalculoPorProduto RE' +
            'AL NOT NULL, Pis_Aliquota REAL NOT NULL, PisSt_BaseCalculo REAL ' +
            'NOT NULL, PisSt_Porcento REAL NOT NULL, PisSt_Valor REAL NOT NUL' +
            'L, PisSt_BaseCalculoPorProduto REAL NOT NULL, PisSt_Aliquota REA' +
            'L NOT NULL, Icms_Origem INTEGER NOT NULL, Icms_Cst INTEGER NOT N' +
            'ULL, Icms_Porcento REAL NOT NULL, Icms_Valor REAL NOT NULL, Issq' +
            'n_BaseCalculo REAL NOT NULL, Issqn_Deducao REAL NOT NULL, Issqn_' +
            'CodigoIbge TEXT NOT NULL, Issqn_BaseCalculoProduto REAL NOT NULL' +
            ', Issqn_ItemDaListaServico TEXT NOT NULL, Issqn_CodigoTributario' +
            'Issqn TEXT NOT NULL, Issqn_Natureza INTEGER NOT NULL, TaxaEstadu' +
            'al REAL NOT NULL, TaxaFederal REAL NOT NULL);'
          ''
          '-- Table: Usuario'
          'DROP TABLE IF EXISTS Usuario;'
          
            'CREATE TABLE Usuario (Id INTEGER NOT NULL CONSTRAINT PK_Usuario ' +
            'PRIMARY KEY AUTOINCREMENT, Nome TEXT NOT NULL, Senha TEXT NOT NU' +
            'LL, Nivel INTEGER DEFAULT (0));'
          ''
          '-- Table: Venda'
          'DROP TABLE IF EXISTS Venda;'
          
            'CREATE TABLE Venda (Id INTEGER NOT NULL CONSTRAINT PK_Venda PRIM' +
            'ARY KEY AUTOINCREMENT, DataHora TEXT NOT NULL, Valor REAL NOT NU' +
            'LL, ValorTributacao REAL DEFAULT (0), Acrescimo REAL DEFAULT (0)' +
            ', Desconto REAL DEFAULT (0), CupomId INTEGER, FormaPagamento TEX' +
            'T DEFAULT ('#39'Dinheiro'#39'), OperadoraCartao TEXT DEFAULT ('#39#39'), Gerad' +
            'a INTEGER DEFAULT (0), ClienteId INTEGER REFERENCES Cliente (Id)' +
            ');'
          ''
          '-- Table: VendaProduto'
          'DROP TABLE IF EXISTS VendaProduto;'
          
            'CREATE TABLE VendaProduto (Id INTEGER NOT NULL CONSTRAINT PK_Ven' +
            'daProduto PRIMARY KEY AUTOINCREMENT, VendaId INTEGER NOT NULL, P' +
            'rodutoId INTEGER, Quantidade REAL NOT NULL, DescricaoProdutoAtua' +
            'l TEXT NOT NULL, ValorProdutoAtual REAL NOT NULL, Pis_CST INTEGE' +
            'R NOT NULL, Cofins_CST INTEGER NOT NULL, CFOP TEXT NOT NULL, NCM' +
            ' TEXT NOT NULL, CONSTRAINT FK_VendaProduto_Produto_ProdutoId FOR' +
            'EIGN KEY (ProdutoId) REFERENCES Produto (Id) ON DELETE RESTRICT)' +
            ';'
          ''
          '-- Index: IX_Categoria_Descricao'
          'DROP INDEX IF EXISTS IX_Categoria_Descricao;'
          
            'CREATE UNIQUE INDEX IX_Categoria_Descricao ON Categoria ("Descri' +
            'cao");'
          ''
          '-- Index: IX_Produto_CategoriaId'
          'DROP INDEX IF EXISTS IX_Produto_CategoriaId;'
          'CREATE INDEX IX_Produto_CategoriaId ON Produto ("CategoriaId");'
          ''
          '-- Index: IX_Venda_CupomId'
          'DROP INDEX IF EXISTS IX_Venda_CupomId;'
          'CREATE INDEX IX_Venda_CupomId ON Venda ("CupomId");'
          ''
          '-- Index: IX_VendaProduto_ProdutoId'
          'DROP INDEX IF EXISTS IX_VendaProduto_ProdutoId;'
          
            'CREATE INDEX IX_VendaProduto_ProdutoId ON VendaProduto ("Produto' +
            'Id");'
          ''
          '-- Index: IX_VendaProduto_VendaId'
          'DROP INDEX IF EXISTS IX_VendaProduto_VendaId;'
          
            'CREATE INDEX IX_VendaProduto_VendaId ON VendaProduto ("VendaId")' +
            ';'
          '')
      end>
    Connection = ConAuxTributacao
    Params = <>
    Macros = <>
    FetchOptions.AssignedValues = [evItems, evAutoClose, evAutoFetchAll]
    FetchOptions.AutoClose = False
    FetchOptions.Items = [fiBlobs, fiDetails]
    ResourceOptions.AssignedValues = [rvMacroCreate, rvMacroExpand, rvDirectExecute, rvPersistent]
    ResourceOptions.MacroCreate = False
    ResourceOptions.DirectExecute = True
    Left = 320
    Top = 24
  end
  object tableUsuario: TFDTable
    IndexFieldNames = 'Id'
    Connection = conDados
    UpdateOptions.UpdateTableName = 'Usuario'
    TableName = 'Usuario'
    Left = 32
    Top = 200
    object tableUsuarioId: TFDAutoIncField
      FieldName = 'Id'
      Origin = 'Id'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object tableUsuarioNome: TWideMemoField
      FieldName = 'Nome'
      Origin = 'Nome'
      Required = True
      BlobType = ftWideMemo
    end
    object tableUsuarioSenha: TWideMemoField
      FieldName = 'Senha'
      Origin = 'Senha'
      Required = True
      BlobType = ftWideMemo
    end
    object tableUsuarioNivel: TIntegerField
      FieldName = 'Nivel'
      Origin = 'Nivel'
      Required = True
    end
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    ScreenCursor = gcrNone
    Left = 336
    Top = 192
  end
  object conControle: TFDConnection
    Params.Strings = (
      
        'Database=C:\Users\Renan Lucilio\Documents\Projetos\Sat\SAT\Win32' +
        '\Debug\dados.db'
      'OpenMode=ReadWrite'
      'DriverID=SQLite')
    LoginPrompt = False
    Left = 176
    Top = 8
  end
  object tableCategoria: TFDTable
    IndexFieldNames = 'Id'
    Connection = conDados
    UpdateOptions.UpdateTableName = 'Categoria'
    TableName = 'Categoria'
    Left = 32
    Top = 152
    object tableCategoriaId: TFDAutoIncField
      FieldName = 'Id'
      Origin = 'Id'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object tableCategoriaDescricao: TWideMemoField
      FieldName = 'Descricao'
      Origin = 'Descricao'
      Required = True
      BlobType = ftWideMemo
    end
    object tableCategoriaNcm: TWideMemoField
      FieldName = 'Ncm'
      Origin = 'Ncm'
      BlobType = ftWideMemo
    end
    object tableCategoriaCest: TWideMemoField
      FieldName = 'Cest'
      Origin = 'Cest'
      BlobType = ftWideMemo
    end
    object tableCategoriaCfop: TWideMemoField
      FieldName = 'Cfop'
      Origin = 'Cfop'
      BlobType = ftWideMemo
    end
    object tableCategoriaCofins_CST: TIntegerField
      FieldName = 'Cofins_CST'
      Origin = 'Cofins_CST'
      Required = True
    end
    object tableCategoriaCofins_BaseCalculo: TFloatField
      FieldName = 'Cofins_BaseCalculo'
      Origin = 'Cofins_BaseCalculo'
      Required = True
    end
    object tableCategoriaCofins_Porcento: TFloatField
      FieldName = 'Cofins_Porcento'
      Origin = 'Cofins_Porcento'
      Required = True
    end
    object tableCategoriaCofins_Valor: TFloatField
      FieldName = 'Cofins_Valor'
      Origin = 'Cofins_Valor'
      Required = True
    end
    object tableCategoriaCofins_BaseCalculoPorProduto: TFloatField
      FieldName = 'Cofins_BaseCalculoPorProduto'
      Origin = 'Cofins_BaseCalculoPorProduto'
      Required = True
    end
    object tableCategoriaCofins_Aliquota: TFloatField
      FieldName = 'Cofins_Aliquota'
      Origin = 'Cofins_Aliquota'
      Required = True
    end
    object tableCategoriaCofinsSt_BaseCalculo: TFloatField
      FieldName = 'CofinsSt_BaseCalculo'
      Origin = 'CofinsSt_BaseCalculo'
      Required = True
    end
    object tableCategoriaCofinsSt_Porcento: TFloatField
      FieldName = 'CofinsSt_Porcento'
      Origin = 'CofinsSt_Porcento'
      Required = True
    end
    object tableCategoriaCofinsSt_Valor: TFloatField
      FieldName = 'CofinsSt_Valor'
      Origin = 'CofinsSt_Valor'
      Required = True
    end
    object tableCategoriaCofinsSt_BaseCalculoPorProduto: TFloatField
      FieldName = 'CofinsSt_BaseCalculoPorProduto'
      Origin = 'CofinsSt_BaseCalculoPorProduto'
      Required = True
    end
    object tableCategoriaCofinsSt_Aliquota: TFloatField
      FieldName = 'CofinsSt_Aliquota'
      Origin = 'CofinsSt_Aliquota'
      Required = True
    end
    object tableCategoriaPis_CST: TIntegerField
      FieldName = 'Pis_CST'
      Origin = 'Pis_CST'
      Required = True
    end
    object tableCategoriaPis_BaseCalculo: TFloatField
      FieldName = 'Pis_BaseCalculo'
      Origin = 'Pis_BaseCalculo'
      Required = True
    end
    object tableCategoriaPis_Porcento: TFloatField
      FieldName = 'Pis_Porcento'
      Origin = 'Pis_Porcento'
      Required = True
    end
    object tableCategoriaPis_Valor: TFloatField
      FieldName = 'Pis_Valor'
      Origin = 'Pis_Valor'
      Required = True
    end
    object tableCategoriaPis_BaseCalculoPorProduto: TFloatField
      FieldName = 'Pis_BaseCalculoPorProduto'
      Origin = 'Pis_BaseCalculoPorProduto'
      Required = True
    end
    object tableCategoriaPis_Aliquota: TFloatField
      FieldName = 'Pis_Aliquota'
      Origin = 'Pis_Aliquota'
      Required = True
    end
    object tableCategoriaPisSt_BaseCalculo: TFloatField
      FieldName = 'PisSt_BaseCalculo'
      Origin = 'PisSt_BaseCalculo'
      Required = True
    end
    object tableCategoriaPisSt_Porcento: TFloatField
      FieldName = 'PisSt_Porcento'
      Origin = 'PisSt_Porcento'
      Required = True
    end
    object tableCategoriaPisSt_Valor: TFloatField
      FieldName = 'PisSt_Valor'
      Origin = 'PisSt_Valor'
      Required = True
    end
    object tableCategoriaPisSt_BaseCalculoPorProduto: TFloatField
      FieldName = 'PisSt_BaseCalculoPorProduto'
      Origin = 'PisSt_BaseCalculoPorProduto'
      Required = True
    end
    object tableCategoriaPisSt_Aliquota: TFloatField
      FieldName = 'PisSt_Aliquota'
      Origin = 'PisSt_Aliquota'
      Required = True
    end
    object tableCategoriaIcms_Origem: TIntegerField
      FieldName = 'Icms_Origem'
      Origin = 'Icms_Origem'
      Required = True
    end
    object tableCategoriaIcms_Cst: TIntegerField
      FieldName = 'Icms_Cst'
      Origin = 'Icms_Cst'
      Required = True
    end
    object tableCategoriaIcms_Porcento: TFloatField
      FieldName = 'Icms_Porcento'
      Origin = 'Icms_Porcento'
      Required = True
    end
    object tableCategoriaIcms_Valor: TFloatField
      FieldName = 'Icms_Valor'
      Origin = 'Icms_Valor'
      Required = True
    end
    object tableCategoriaIssqn_BaseCalculo: TFloatField
      FieldName = 'Issqn_BaseCalculo'
      Origin = 'Issqn_BaseCalculo'
      Required = True
    end
    object tableCategoriaIssqn_Deducao: TFloatField
      FieldName = 'Issqn_Deducao'
      Origin = 'Issqn_Deducao'
      Required = True
    end
    object tableCategoriaIssqn_CodigoIbge: TWideMemoField
      FieldName = 'Issqn_CodigoIbge'
      Origin = 'Issqn_CodigoIbge'
      BlobType = ftWideMemo
    end
    object tableCategoriaIssqn_BaseCalculoProduto: TFloatField
      FieldName = 'Issqn_BaseCalculoProduto'
      Origin = 'Issqn_BaseCalculoProduto'
      Required = True
    end
    object tableCategoriaIssqn_ItemDaListaServico: TWideMemoField
      FieldName = 'Issqn_ItemDaListaServico'
      Origin = 'Issqn_ItemDaListaServico'
      BlobType = ftWideMemo
    end
    object tableCategoriaIssqn_CodigoTributarioIssqn: TWideMemoField
      FieldName = 'Issqn_CodigoTributarioIssqn'
      Origin = 'Issqn_CodigoTributarioIssqn'
      BlobType = ftWideMemo
    end
    object tableCategoriaIssqn_Natureza: TIntegerField
      FieldName = 'Issqn_Natureza'
      Origin = 'Issqn_Natureza'
      Required = True
    end
    object tableCategoriaTaxaEstadual: TFloatField
      FieldName = 'TaxaEstadual'
      Origin = 'TaxaEstadual'
      Required = True
    end
    object tableCategoriaTaxaFederal: TFloatField
      FieldName = 'TaxaFederal'
      Origin = 'TaxaFederal'
      Required = True
    end
  end
  object tableProduto: TFDQuery
    Connection = conDados
    FormatOptions.AssignedValues = [fvMapRules]
    FormatOptions.OwnMapRules = True
    FormatOptions.MapRules = <
      item
        SourceDataType = dtDouble
        TargetDataType = dtCurrency
      end>
    SQL.Strings = (
      'SELECT '
      ' P.ID AS ID,'
      ' P.DESCRICAO AS DESCRICAO, '
      ' C.DESCRICAO AS CATEGORIA, '
      ' P.PRECO AS VALOR '
      'FROM PRODUTO P'
      ' JOIN CATEGORIA C ON'
      '  P.CATEGORIAID = C.ID')
    Left = 176
    Top = 152
    object tableProdutoID: TFDAutoIncField
      FieldName = 'ID'
      Origin = 'Id'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object tableProdutoDESCRICAO: TWideMemoField
      FieldName = 'DESCRICAO'
      Origin = 'Descricao'
      Required = True
      BlobType = ftWideMemo
    end
    object tableProdutoCATEGORIA: TWideMemoField
      AutoGenerateValue = arDefault
      FieldName = 'CATEGORIA'
      Origin = 'Descricao'
      ProviderFlags = []
      ReadOnly = True
      BlobType = ftWideMemo
    end
    object tableProdutoVALOR: TCurrencyField
      FieldName = 'VALOR'
      Origin = 'Preco'
      Required = True
    end
  end
  object conAuditoria: TFDConnection
    Params.Strings = (
      
        'Database=C:\Users\Renan Lucilio\Documents\Projetos\Sat\SAT\Win32' +
        '\Debug\aud.db'
      'OpenMode=ReadWrite'
      'DriverID=SQLite')
    LoginPrompt = False
    Left = 32
    Top = 64
  end
  object scrAuditoriaDb: TFDScript
    SQLScripts = <
      item
        Name = 'criaDb'
        SQL.Strings = (
          ''
          '-- Table: Auditoria'
          'DROP TABLE IF EXISTS Auditoria;'
          
            'CREATE TABLE Auditoria (Id INTEGER PRIMARY KEY AUTOINCREMENT UNI' +
            'QUE NOT NULL, Resumo TEXT NOT NULL, Descricao NOT NULL, Categori' +
            'a CHAR NOT NULL, DataHora TEXT NOT NULL);')
      end>
    Connection = ConAuxTributacao
    Params = <>
    Macros = <>
    FetchOptions.AssignedValues = [evItems, evAutoClose, evAutoFetchAll]
    FetchOptions.AutoClose = False
    FetchOptions.Items = [fiBlobs, fiDetails]
    ResourceOptions.AssignedValues = [rvMacroCreate, rvMacroExpand, rvDirectExecute, rvPersistent]
    ResourceOptions.MacroCreate = False
    ResourceOptions.DirectExecute = True
    Left = 320
    Top = 88
  end
  object srcAuxTributacaoDb: TFDScript
    SQLScripts = <>
    Connection = ConAuxTributacao
    Params = <>
    Macros = <>
    FetchOptions.AssignedValues = [evItems, evAutoClose, evAutoFetchAll]
    FetchOptions.AutoClose = False
    FetchOptions.Items = [fiBlobs, fiDetails]
    ResourceOptions.AssignedValues = [rvMacroCreate, rvMacroExpand, rvDirectExecute, rvPersistent]
    ResourceOptions.MacroCreate = False
    ResourceOptions.DirectExecute = True
    Left = 320
    Top = 144
  end
end
