/// <summary>
///   DataModule com todas as conex�es suas defini��es
/// </summary>
unit uDmCons;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.FMXUI.Wait,
  FireDAC.Comp.ScriptCommands, FireDAC.Stan.Util, FireDAC.Comp.Script,
  Data.DB, FireDAC.Comp.Client, FireDAC.Phys.SQLite,
  FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs, FireDAC.Stan.Param,
  FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.UI;

type
  TdmCons = class(TDataModule)
    ConAuxTributacao: TFDConnection;
    conDados: TFDConnection;
    scrCriacaoDb: TFDScript;
    tableUsuario: TFDTable;
    tableUsuarioId: TFDAutoIncField;
    tableUsuarioNome: TWideMemoField;
    tableUsuarioSenha: TWideMemoField;
    tableUsuarioNivel: TIntegerField;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    conControle: TFDConnection;
    tableCategoria: TFDTable;
    tableCategoriaId: TFDAutoIncField;
    tableCategoriaDescricao: TWideMemoField;
    tableCategoriaNcm: TWideMemoField;
    tableCategoriaCest: TWideMemoField;
    tableCategoriaCfop: TWideMemoField;
    tableCategoriaCofins_CST: TIntegerField;
    tableCategoriaCofins_BaseCalculo: TFloatField;
    tableCategoriaCofins_Porcento: TFloatField;
    tableCategoriaCofins_Valor: TFloatField;
    tableCategoriaCofins_BaseCalculoPorProduto: TFloatField;
    tableCategoriaCofins_Aliquota: TFloatField;
    tableCategoriaCofinsSt_BaseCalculo: TFloatField;
    tableCategoriaCofinsSt_Porcento: TFloatField;
    tableCategoriaCofinsSt_Valor: TFloatField;
    tableCategoriaCofinsSt_BaseCalculoPorProduto: TFloatField;
    tableCategoriaCofinsSt_Aliquota: TFloatField;
    tableCategoriaPis_CST: TIntegerField;
    tableCategoriaPis_BaseCalculo: TFloatField;
    tableCategoriaPis_Porcento: TFloatField;
    tableCategoriaPis_Valor: TFloatField;
    tableCategoriaPis_BaseCalculoPorProduto: TFloatField;
    tableCategoriaPis_Aliquota: TFloatField;
    tableCategoriaPisSt_BaseCalculo: TFloatField;
    tableCategoriaPisSt_Porcento: TFloatField;
    tableCategoriaPisSt_Valor: TFloatField;
    tableCategoriaPisSt_BaseCalculoPorProduto: TFloatField;
    tableCategoriaPisSt_Aliquota: TFloatField;
    tableCategoriaIcms_Origem: TIntegerField;
    tableCategoriaIcms_Cst: TIntegerField;
    tableCategoriaIcms_Porcento: TFloatField;
    tableCategoriaIcms_Valor: TFloatField;
    tableCategoriaIssqn_BaseCalculo: TFloatField;
    tableCategoriaIssqn_Deducao: TFloatField;
    tableCategoriaIssqn_CodigoIbge: TWideMemoField;
    tableCategoriaIssqn_BaseCalculoProduto: TFloatField;
    tableCategoriaIssqn_ItemDaListaServico: TWideMemoField;
    tableCategoriaIssqn_CodigoTributarioIssqn: TWideMemoField;
    tableCategoriaIssqn_Natureza: TIntegerField;
    tableCategoriaTaxaEstadual: TFloatField;
    tableCategoriaTaxaFederal: TFloatField;
    tableProduto: TFDQuery;
    tableProdutoDESCRICAO: TWideMemoField;
    tableProdutoCATEGORIA: TWideMemoField;
    tableProdutoVALOR: TCurrencyField;
    tableProdutoID: TFDAutoIncField;
    conAuditoria: TFDConnection;
    scrAuditoriaDb: TFDScript;
    srcAuxTributacaoDb: TFDScript;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure CriarBD();
    procedure CriarAuditoriaDB();
    procedure VerificaBdTributacaoAux;
    procedure ConverterBancosDados;
    procedure ConnectarControleDb();
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmCons: TdmCons;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

uses uSistema, Form.Principal, FMX.Forms, FMX.Dialogs,
  uConverteParaDadosDb, System.Types;

{$R *.dfm}


/// <summary>
///   M�todo usado para connectar no banco da vers�o anterior banco controle.db
/// </summary>
procedure TdmCons.ConnectarControleDb;
var
  arquivo: string;
begin
  arquivo:= ExtractFilePath(ParamStr(0))+'controle.db';
  if FileExists(arquivo) then
  begin
    try
      conControle.Params.Database:= arquivo;
      conControle.Connected:= true;
    except on e: exception do
    end;
  end;

end;

/// <summary>
///   M�todo para acionar a classe que implementa a convers�o dos bancos de dados
/// </summary>
procedure TdmCons.ConverterBancosDados;
var
  arquivo: string;
  converte: TConverteParaDadosDb;
begin
  arquivo:= ExtractFilePath(ParamStr(0))+'controle.db';

  if FileExists(arquivo) then
  begin
    converte:= TConverteParaDadosDb.Create;
    try
    finally
      converte.free;
      FreeAndNil(conControle);
      DeleteFile(arquivo);
    end;
  end
  else
    FreeAndNil(conControle);

end;

/// <summary>
///   M�todo para criar o banco de dados onde tem as auditorias
/// </summary>
procedure TdmCons.CriarAuditoriaDB;
var
  arquivo: string;
begin
  try
    arquivo:= ExtractFilePath(ParamStr(0))+'aud.db';
      if not FileExists(arquivo) then
      begin
        try
          conAuditoria.Params.Values['Database']:= arquivo;
          conAuditoria.Params.Values['OpenMode']:= 'CreateUTF8';

          conAuditoria.DriverName:= 'SQLite';
          scrAuditoriaDb.Connection:= conAuditoria;

          scrAuditoriaDb.ValidateAll;
          scrAuditoriaDb.ExecuteAll;
        except
          raise Exception.Create('Erro ao criar a conex�o');
        end;
      end
      else
      begin
        conAuditoria.Params.Values['Database']:= arquivo;
      end;
  except on e: exception do

  end;

end;

/// <summary>
///   M�todo usado para criar o banco de dados
/// </summary>
procedure TdmCons.CriarBd;
var
  arquivo: string;
begin
  try
    arquivo:= ExtractFilePath(ParamStr(0))+'dados.db';
      if not FileExists(arquivo) then
      begin
        try
          conDados.Params.Values['Database']:= arquivo;
          conDados.Params.Values['OpenMode']:= 'CreateUTF8';
          conDados.DriverName:= 'SQLite';
          scrCriacaoDb.Connection:= conDados;

          scrCriacaoDb.ValidateAll;
          scrCriacaoDb.ExecuteAll;
        except
          raise Exception.Create('Erro ao criar a conex�o');
        end;
      end
      else
      begin
        conDados.Params.Values['Database']:= arquivo;
      end;

  except on e: exception do

  end;

end;

/// <summary>
///   M�todo usado para criar o banco de dados de tributa��es auxiliar
/// </summary>
procedure TdmCons.VerificaBdTributacaoAux;
var
  arquivo: string;
  resource: TResourceStream;
  arquivoTxt: TStringList;
begin
  resource:= TResourceStream.Create(HInstance, 'auxTributacao', RT_RCDATA);
  arquivoTxt:= TStringList.Create;

  try
    arquivoTxt.LoadFromStream(resource);
    arquivo:= ExtractFilePath(ParamStr(0))+'AuxTributacao.db';
      if not FileExists(arquivo) then
      begin
        try
          ConAuxTributacao.Params.Values['Database']:= arquivo;
          ConAuxTributacao.Params.Values['OpenMode']:= 'CreateUTF8';

          ConAuxTributacao.DriverName:= 'SQLite';
          srcAuxTributacaoDb.Connection:= ConAuxTributacao;

          srcAuxTributacaoDb.ExecuteScript(arquivoTxt);
        except
          raise Exception.Create('Erro ao criar a conex�o');
        end;
      end
      else
      begin
        ConAuxTributacao.Params.Values['Database']:= arquivo;
      end;
  finally
    arquivoTxt.Free;
    resource.Free;
  end;
end;

/// <summary>
///   M�todo construtor do data module que usa os m�todo aanteriores 
/// </summary>
procedure TdmCons.DataModuleCreate(Sender: TObject);
var
  arquivo: string;
begin
  arquivo:= ExtractFilePath(ParamStr(0))+'controle.db';
  try
    VerificaBdTributacaoAux;
    CriarBD;
    ConnectarControleDb;
    converterBancosDados;
    CriarAuditoriaDB;
    tableUsuario.Connection:= dmCons.conDados;
    tableCategoria.Connection:= dmCons.conDados;
    tableProduto.Connection:= dmCons.conDados;
  finally
    if FileExists(arquivo) then
      DeleteFile(arquivo);
  end;
end;

end.
