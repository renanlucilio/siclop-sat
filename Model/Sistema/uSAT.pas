/// <summary>
///   Unit que Tem a classe TSAT
/// </summary>
unit uSAT;

interface

uses
  PersistenciaConfiguracao.Interfaces;

type
  TSAT = class(TInterfacedObject, iConfiguravel)
  private
    FDLL: string;
    FCodigoAtivacao: string;
    FVersaoXML: Double;
    FCodigoPagina: Integer;
    FEhUTF: Boolean;
    FEhSTDCall: Boolean;
    FEhProducao: Boolean;
    FEnvioAutomatico: Boolean;
    procedure SetDLL(const Value: string);
  public
    constructor Create();

    procedure Salvar;
    procedure Carregar;
    function GetConfigurado: boolean;

    property DLL:             string  read FDLL             write SetDLL;
    property CodigoAtivacao:  string  read FCodigoAtivacao  write FCodigoAtivacao;
    property VersaoXML:       Double  read FVersaoXML       write FVersaoXML;
    property CodigoPagina:    Integer read FCodigoPagina    write FCodigoPagina;
    property EhUTF:           Boolean read FEhUTF           write FEhUTF;
    property EhSTDCall:       Boolean read FEhSTDCall       write FEhSTDCall;
    property EhProducao:      Boolean read FEhProducao      write FEhProducao;
    property EnvioAutomatico: Boolean read FEnvioAutomatico write FEnvioAutomatico;

    function ListarXML(const Pasta: string;const dataDe, dataAte: TDate): TArray<string>;
var
  end;
implementation

uses
  System.Generics.Collections,
  System.SysUtils,
  System.IOUtils,
  System.DateUtils,
  Factory.PersistenciaConfigucao;

{ TSAT }

/// <summary>
///   M�todo que carrega as propriedades de um arquivo de configura��o
/// </summary>
procedure TSAT.Carregar;
var
  ini: iSalvaCarregaConfiguracao;
begin
  ini := TFactoryPersistenciaConfiguracao.getPersistenciaConfiguracao(tpcIni);
  ini.carrega(Self);

  DLL := Ini.GetResultado.Items['DLL'];

  CodigoAtivacao := Ini.GetResultado.Items['CodigoAtivacao'];

  VersaoXML := StrToFloatDef(Ini.GetResultado.Items['VersaoXML'], 0.07);

  if VersaoXML = 0 then VersaoXML:=0.07;

//  {$IFNDEF DEBUG}
//      EhProducao:= false;
//  {$ELSE}
     EhProducao:= true;
//  {$ENDIF}

  CodigoPagina := StrToIntDef(Ini.GetResultado.Items['CodigoPagina'], 0);

  if Ini.GetResultado.Items['EhUTF'] = 'True' then EhUTF := true else EhUTF := false;

  if Ini.GetResultado.Items['EhSTDCall'].IsEmpty then
    EhSTDCall := true
  else
    if Ini.GetResultado.Items['EhSTDCall'] = 'True' then EhSTDCall := true else EhSTDCall := false;

  if ini.getResultado.Items['EnvioAutomatico'].Equals('True') then EnvioAutomatico:= true else EnvioAutomatico:= false;


  if EhUTF then CodigoPagina := 65001;

end;

/// <summary>
///   M�todo que cria uma instancia e carrega as propriedades
/// </summary>
constructor TSAT.Create;
begin
  Carregar();
end;

/// <summary>
///   M�todo que verificaca se as propriedades est�o configuradas
/// <returns>
///   retorna true se estiver configurado
/// </returns>
/// </summary>
function TSAT.GetConfigurado: boolean;
begin
  Result:= not(DLL.IsEmpty) and
           FileExists(DLL) and
           not(codigoAtivacao.IsEmpty) and
           (Length(CodigoAtivacao) >= 8);
end;

/// <summary>
///   M�todo que lista os arquivos XML em uma lista
/// <param name="pasta">
///   pasta onde ser� verificados os arquivos
/// </param>
/// <param name="dataDe">
///   Data de inicio que envia os dados
/// </param>
/// <param name="dataAte">
///   Data de fim que envia os dados
/// </param>
/// <returns>
///   retorna uma lista de arquivos XML
/// </returns>
/// </summary>
function TSAT.ListarXML(const Pasta: string;const dataDe, dataAte: TDate): TArray<string>;
var
  item: string;
  listaDeArquivos: TList<string>;
  dataDoArquivo: TDateTime;
begin

  listaDeArquivos := TList<string>.Create;
  try
    if DirectoryExists(Pasta) then
    begin
      for item in TDirectory.GetFiles(Pasta, '*.xml') do
      begin

        FileAge(item, dataDoArquivo);
        if DateInRange(DateOf(dataDoArquivo), dataDe, dataAte) then
        begin
          listaDeArquivos.Add(item);
        end;

      end;
    end;

    result := listaDeArquivos.ToArray();
  finally
    FreeAndNil(listaDeArquivos);

  end;
end;

/// <summary>
///   M�todo que salva as propriedades em um arquivo de configura��o
/// </summary>
procedure TSAT.Salvar;
var
  ini: iSalvaCarregaConfiguracao;
begin
  ini:= TFactoryPersistenciaConfiguracao.getPersistenciaConfiguracao(tpcIni);

  ini.salva(self);
end;

/// <summary>
///   M�todo que define um uma dll

/// </summary>
procedure TSAT.SetDLL(const Value: string);
begin
  if FileExists(Value) then
    FDLL := Value
  else
    FDLL := '';
end;

end.
