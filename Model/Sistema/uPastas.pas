/// <summary>
///   Unit que tem a class TPasta
/// </summary>
unit uPastas;

interface

uses
  Factory.PersistenciaConfigucao,
  PersistenciaConfiguracao.Interfaces;

type
  TPasta = class(TInterfacedObject, iConfiguravel)
  private
    FPastaCupom: string;
  public
    constructor Create();

    function Ref: iConfiguravel;
    procedure Salvar;
    procedure Carregar;
    function GetConfigurado: boolean;

    property PastaCupom: string read FPastaCupom write FPastaCupom;

    function PastaExtrato:  string;
    function PastaErro:     string;
    function PastaEnviado:  string;
    function PastaVenda:    string;
    function PastaCancelos: string;
    function PastaXML:      string;
    function PastaPDF:      string;
    function PastaLog:      string;
  end;

implementation

uses
  System.IOUtils,
  System.SysUtils;

{ TPasta }

/// <summary>
///   M�todo que carrega as propriedades da classe
/// </summary>
procedure TPasta.Carregar;
var
  ini: iSalvaCarregaConfiguracao;
begin
  ini:= TFactoryPersistenciaConfiguracao.getPersistenciaConfiguracao(tpcIni);

  ini.carrega(self);

  PastaCupom:= ini.getResultado.Items['PastaCupom'];
end;

/// <summary>
///   M�todo que cria uma instancia e carrega as propridades
/// </summary>
constructor TPasta.Create;
begin
  Carregar();
end;

/// <summary>
///   M�todo que Verifica se as propriedades est�o devidamente configuradas
/// <returns>
///   retorna true se estiver configurado
/// </returns>
/// </summary>
function TPasta.GetConfigurado: boolean;
begin
  result:= not(PastaCupom.IsEmpty) and
            DirectoryExists(PastaCupom);
end;

/// <summary>
///   M�todo que Retorna a pasta de cancelados configurada
/// <returns>
///   O caminho da pasta de cancelados
/// </returns>
/// </summary>
function TPasta.PastaCancelos: string;
begin
  result:= PastaExtrato+'Cancelados\';
  ForceDirectories(result);
end;

/// <summary>
///   M�todo que Retorna a pasta de enviados configurada
/// <returns>
///    O caminho da pasta de enviados
/// </returns>
/// </summary>
function TPasta.PastaEnviado: string;
begin
  result:= PastaExtrato+'Enviados\';
  ForceDirectories(result);
end;

/// <summary>
///   M�todo que Retorna a pasta de Erros configurada
/// <returns>
///    O caminho da pasta de Erros
/// </returns>
/// </summary>
function TPasta.PastaErro: string;
begin
  result:= PastaExtrato+'Erros\';
  ForceDirectories(result);
end;

/// <summary>
///   M�todo que Retorna a pasta de Extrato configurada
/// <returns>
///    O caminho da pasta de Extrato
/// </returns>
/// </summary>
function TPasta.PastaExtrato: string;
begin
  result := 'C:\SICLOP\SAT\Extratos\';
  ForceDirectories(result);
end;

/// <summary>
///   M�todo que Retorna a pasta de log configurada
/// <returns>
///    O caminho da pasta de log
/// </returns>
/// </summary>
function TPasta.PastaLog: string;
begin
  result:= TPath.GetDocumentsPath+'\SICLOP\SAT\';
  ForceDirectories(result);
end;

/// <summary>
///   M�todo que Retorna a pasta de PDF configurada
/// <returns>
///    O caminho da pasta de PDF
/// </returns>
/// </summary>
function TPasta.PastaPDF: string;
begin
  result:= PastaExtrato+'PDF\';
  ForceDirectories(result);
end;

/// <summary>
///   M�todo que Retorna a pasta de venda configurada
/// <returns>
///    O caminho da pasta de venda
/// </returns>
/// </summary>
function TPasta.PastaVenda: string;
begin
  result:= PastaExtrato+'Venda\';
  ForceDirectories(result);
end;

/// <summary>
///   M�todo que Retorna a pasta de XML configurada
/// <returns>
///    O caminho da pasta de XML
/// </returns>
/// </summary>
function TPasta.PastaXML: string;
begin
  result:= PastaExtrato+'XML\';
  ForceDirectories(result);
end;

/// <summary>
///   M�todo que Retorna uma interface que retorna a pr�pria instancia transformando assim em uma interface
/// <returns>
///    IConfiguravel
/// </returns>
/// </summary>
function TPasta.Ref: iConfiguravel;
begin
  result:= self;
end;

/// <summary>
///   M�todo para salva as propriedades em um arquivo de configura��o
/// </summary>
procedure TPasta.Salvar;
var
  ini: iSalvaCarregaConfiguracao;
begin
  ini:= TFactoryPersistenciaConfiguracao.getPersistenciaConfiguracao(tpcIni);

  ini.salva(self);
end;


end.
