﻿/// <summary>
///   Método que tem a classe TUsuario
/// </summary>
unit uusuario;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('Usuario', '')]
  [PrimaryKey('Id', NotInc, NoSort, False, 'Chave prim�ria')]
  TUsuario = class
  private
    { Private declarations } 
    FId: Nullable<Integer>;
    FNome: String;
    FSenha: String;
    FNivel: Nullable<Integer>;
  public 
    { Public declarations } 
    [Column('Id', ftInteger)]
    [Dictionary('Id', 'Mensagem de valida��o', '', '', '', taCenter)]
    property Id: Nullable<Integer> read FId write FId;

    [Restrictions([NotNull])]
    [Column('Nome', ftString)]
    [Dictionary('Nome', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Nome: String read FNome write FNome;

    [Restrictions([NotNull])]
    [Column('Senha', ftString)]
    [Dictionary('Senha', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Senha: String read FSenha write FSenha;

    [Column('Nivel', ftInteger)]
    [Dictionary('Nivel', 'Mensagem de valida��o', '', '', '', taCenter)]
    property Nivel: Nullable<Integer> read FNivel write FNivel;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TUsuario)

end.
