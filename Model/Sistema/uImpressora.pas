/// <summary>
///   Unit que tem a classe TImpressora
/// </summary>
unit uImpressora;

interface

uses
  Factory.PersistenciaConfigucao,
  PersistenciaConfiguracao.Interfaces;

type
  { TODO : COLOCAR O LOGO DA SICLOP }
  TImpressora = class(TInterfacedObject, iConfiguravel)
  private
    FPreview:                  Boolean;
    FItemPorLinha:             Boolean;
    FImprimeDescontoAcrescimo: Boolean;
    FLarguraBobina:            integer;
    FMargeTopo:                Integer;
    FMargeInferior:            integer;
    FMargeDireita:             Integer;
    FMargeEsquerda:            Integer;
    FNomeImpressora:           string;
    FLogo:                     string;
  public
    constructor Create();

    procedure Carregar;
    function GetConfigurado: boolean;
    procedure Salvar;

    property Preview:                  Boolean read FPreview                  write FPreview;
    property ItemPorLinha:             Boolean read FItemPorLinha             write FItemPorLinha;
    property ImprimeDescontoAcrescimo: Boolean read FImprimeDescontoAcrescimo write FImprimeDescontoAcrescimo;
    property LarguraBobina:            integer read FLarguraBobina            write FLarguraBobina;
    property MargeTopo:                Integer read FMargeTopo                write FMargeTopo;
    property MargeInferior:            integer read FMargeInferior            write FMargeInferior;
    property MargeDireita:             Integer read FMargeDireita             write FMargeDireita;
    property MargeEsquerda:            Integer read FMargeEsquerda            write FMargeEsquerda;
    property NomeImpressora:           string  read FNomeImpressora           write FNomeImpressora;
    property Logo:                     string  read FLogo                     write FLogo;
  end;

implementation

uses
  System.SysUtils;

/// <summary>
///   M�todo que carrega as propriedades de um arquivo de configura��o
/// </summary>
procedure TImpressora.Carregar;
var
  ini: iSalvaCarregaConfiguracao;
begin
  ini:= TFactoryPersistenciaConfiguracao.getPersistenciaConfiguracao(tpcIni);

  ini.carrega(self);

  if ini.getResultado.Items['Preview'].Equals('True') then Preview:= true else Preview:= false;

  if ini.getResultado.Items['ItemPorLinha'].Equals('True') then ItemPorLinha:= true else ItemPorLinha:= false;

  if ini.getResultado.Items['ImprimeDescontoAcrescimo'].Equals('True') then ImprimeDescontoAcrescimo:= true else ImprimeDescontoAcrescimo:= false;

  LarguraBobina:= StrToIntDef(ini.getResultado.Items['LarguraBobina'], 270);

  MargeTopo :=  StrToIntDef(ini.getResultado.Items['MargeTopo'], 0);

  MargeInferior :=  StrToIntDef(ini.getResultado.Items['MargeInferior'], 0);

  MargeDireita :=  StrToIntDef(ini.getResultado.Items['MargeDireita'], 0);

  MargeEsquerda :=  StrToIntDef(ini.getResultado.Items['MargeEsquerda'], 0);

  NomeImpressora := ini.getResultado.Items['NomeImpressora'];

  Logo := ini.getResultado.Items['Logo'];
end;

/// <summary>
///   M�todo que salva as propriedades em um arquivo de configura��o
/// </summary>
procedure TImpressora.Salvar;
var
  ini: iSalvaCarregaConfiguracao;
begin
  ini:= TFactoryPersistenciaConfiguracao.getPersistenciaConfiguracao(tpcIni);

  ini.salva(self);
end;

/// <summary>
///   M�todo de instancia e carrega as propriedades
/// </summary>
constructor TImpressora.Create;
begin
  Carregar();
end;

/// <summary>
///   M�todo que Verifica se propriedades est�o configuradas
/// <returns>
///   retorna true se estiver configurado
/// </returns>
/// </summary>
function TImpressora.GetConfigurado: boolean;
begin
  result:= not(nomeImpressora.IsEmpty);
end;

end.
