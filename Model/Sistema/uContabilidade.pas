﻿/// <summary>
///   Unit que tem a classe TContabilidade
/// </summary>
unit uContabilidade;

interface

uses
  System.Classes,
  Factory.PersistenciaConfigucao,
  PersistenciaConfiguracao.Interfaces,
  uEstabelecimento, System.IOUtils;

type
  TEventStr = procedure(const value: string) of object;

  TContabilidade = class(TInterfacedObject, iConfiguravel)
  private
    FNomeFantasia:       string;
    FNomeResponsavel:    string;
    FEmail:              TStringList;
    FEnvioAutomatico:    Boolean;
    FDiaEnvioAutomatico: integer;
    FEstabelecimento: TEstabelecimento;
    FOnStatusEmail: TEventStr;
    procedure SetDiaEnvioAutomatico(const Value: integer);

    procedure EnviarRelatorio(anexos, emails: TArray<string>; assunto,
      mensagem: string);
    procedure MoverParaDocumentos(const Arquivo: string);
    procedure EnviarRelatorioAutomatico();
  public
    constructor Create; overload;


    procedure Salvar;
    procedure Carregar;
    function GetConfigurado: boolean;

    destructor Destroy; override;
    property NomeFantasia:       string        read FNomeFantasia       write FNomeFantasia;
    property NomeResponsavel:    string        read FNomeResponsavel    write FNomeResponsavel;
    property Email:              TStringList   read FEmail;
    property EnvioAutomatico:    Boolean       read FEnvioAutomatico    write FEnvioAutomatico;
    property DiaEnvioAutomatico: integer       read FDiaEnvioAutomatico write SetDiaEnvioAutomatico;
    property OnStatusEmail:      TEventStr     read FOnStatusEmail      write FOnStatusEmail;

    procedure AdicionarEmail(const email: string);
    procedure limparEmail();

    procedure SalvarRelatoriosDocumentos(const dataDe, dataAte: TDate);
    procedure EnviarRelatorioManual(const dataDe, dataAte: TDate);
  end;

implementation

uses
  uSAT,
  uPastas,
  Model.Email,
  Model.LibUtil,
  System.SysUtils,
  System.Generics.Collections,
  System.DateUtils,
  Model.PowerCMD,
  Model.ViewUtil,
  System.Threading, uAuRepository, Form.Principal;

CONST
//  EMAILREMETENTE = 'desenvolvimento@centralsiclop.com.br';
//  SERV = 'mail.centralsiclop.com.br';
//  SENHA = 'php787878';
  EMAILREMETENTE = 'siclopsat@gmail.com';
  SERV = 'smtp.gmail.com';
  SENHA = 'sat787878';

{ TContabilidade }

/// <summary>
///   Método que adiciona um email a lista de emails cadastrados
/// <param name="email">
///   Email a ser cadastrado
/// </param>
/// </summary>
procedure TContabilidade.AdicionarEmail(const email: string);
begin
  if FEmail.IndexOf(email) < 0 then
    FEmail.Add(email);
end;

/// <summary>
///   Método usado para carregar as propriedades da classe
/// <returns>
///   retorna true se estiver configurado
/// </returns>
/// </summary>
procedure TContabilidade.Carregar;
var
  ini: iSalvaCarregaConfiguracao;
  it: string;
  split: TArray<string>;
begin
  ini:= TFactoryPersistenciaConfiguracao.getPersistenciaConfiguracao(tpcIni);

  ini.carrega(self);

  NomeFantasia:= ini.getResultado.Items['NomeFantasia'];

  NomeResponsavel:= ini.getResultado.Items['NomeResponsavel'];

  split:= ini.getResultado.Items['Email'].Split(['|']);

  for it in split do
  begin
    if not string.IsNullOrWhiteSpace(it) then
      Email.Add(it);
  end;

  if ini.getResultado.Items['EnvioAutomatico'].Equals('True') then EnvioAutomatico:= true else EnvioAutomatico:= false;

  DiaEnvioAutomatico:= StrToIntDef(ini.getResultado.Items['DiaEnvioAutomatico'], 1);
end;

/// <summary>
///   Método construtor usado criar a instancia da classe e iniciar a lista de emails e carregar os dados e enviar o relatório automático
/// </summary>
constructor TContabilidade.Create;
begin
  FEmail:= TStringList.Create;
  Carregar();
  FEstabelecimento:= TEstabelecimento.Create;
  EnviarRelatorioAutomatico();
end;

/// <summary>
///   Método destrutor que elimina a instancia
/// </summary>
destructor TContabilidade.Destroy;
begin
  FEmail.Free;
  FEstabelecimento.Free;
  inherited;
end;

/// <summary>
///   Método que envia o relatório para o contador dos arquivos enviados
/// <param name="anexos">
///   arquivos que serão enviados no email
/// </param>
/// <param name="emails">
///   endereços de emails que receberam o relatório
/// </param>
/// <param name="assunto">
///   Assunto do email
/// </param>
/// <param name="mensagem">
///   mensagem que será enviada no email
/// </param>
/// </summary>
procedure TContabilidade.EnviarRelatorio(anexos, emails: TArray<string>; assunto,
      mensagem: string);
var
  emailTemp: TModelEmail;
begin
  emailTemp:= TModelEmail.Create(EMAILREMETENTE, 'SICLOP SAT');

    emailTemp
      .setServer(SERV, SENHA)
      .setEnvio(emails, assunto)
      .setMensagem(mensagem)
      .setAnexos(anexos)
      .enviar;

    limpaArquivosZip;
end;

/// <summary>
///   Método que envia o relatório automaticamente
/// </summary>
procedure TContabilidade.EnviarRelatorioAutomatico;
var
 comecoDoMesPassado, dataUltimoRelatorio: TDate;
 mesAnterior: word;
 finalDoMesAnterior: TDateTime;
 esta: TEstabelecimento;
begin

  dataUltimoRelatorio:= StrToDate(FEstabelecimento.UltimoRelatorio);
  if EnvioAutomatico then
  begin
    if temInternet and EnvioAutomatico then
    begin
      if MonthOf(dataUltimoRelatorio) = MonthOf(now) then
        exit;

       mesAnterior:= MonthOf(now) - 1;

       if mesAnterior <= 0 then
       begin
         mesAnterior:= 12;
         comecoDoMesPassado:= EncodeDate(YearOf(Now), mesAnterior, 1);
         finalDoMesAnterior:= EndOfAMonth(YearOf(now)-1, mesAnterior);
       end
       else
       begin
        comecoDoMesPassado:= EncodeDate(YearOf(Now), mesAnterior, 1);
        finalDoMesAnterior:= EndOfAMonth(YearOf(now), mesAnterior);
       end;

       EnviarRelatorioManual(comecoDoMesPassado, DateOf(finalDoMesAnterior));
       FEstabelecimento.UltimoRelatorio:= DateToStr(DateOf(now));
       FEstabelecimento.Salvar();
       if Assigned(OnStatusEmail) then
          OnStatusEmail('Relatório enviado');
    end;

  end;
end;

/// <summary>
///   Método que envia o relatório de maneira manual
/// <param name="dataDe">
///   Data de inicio que envia os dados
/// </param>
/// <param name="dataAte">
///   Data de fim que envia os dados
/// </param>
/// </summary>
procedure TContabilidade.EnviarRelatorioManual(const dataDe, dataAte: TDate);
var
  it, emailStr: string;
  msg,titulo,nomeDoArquivo: TStringBuilder;
  emails: TList<string>;
  anexos: TList<string>;
  arquivoZipVenda, arquivoZipCancelos: string;
  sat: TSAT;
  _contabilidade: TContabilidade;
  processamento: ITask;
  pasta: TPasta;
begin
  _contabilidade:= Self;
  msg := TStringBuilder.Create();
  titulo := TStringBuilder.Create();
  nomeDoArquivo := TStringBuilder.Create();
  emails:= TList<string>.create();
  anexos:= TList<string>.Create();
  sat:= TSAT.Create;
  pasta:= TPasta.Create;
  try
    emails.AddRange(FEmail.ToStringArray);

    if not FEstabelecimento.Email.IsEmpty then
      emails.Add(FEstabelecimento.Email);

    nomeDoArquivo.Append('XMLs Vendas de ')
      .Append(FormatDateTime('dd.mm.yy', dataDe)).Append(' até ')
      .Append(FormatDateTime('dd.mm.yy', dataAte));


      arquivoZipVenda := compactarArquivo
      (sat.ListarXML(pasta.PastaVenda + FEstabelecimento.cnpj, dataDe, dataAte), nomeDoArquivo.ToString);

    if arquivoZipVenda.IsEmpty then
    begin
      if Assigned(FOnStatusEmail) then
        OnStatusEmail('Não Há arquivos para serem enviados');
      exit;
    end;

    if FileExists(arquivoZipVenda) then
      anexos.Add(arquivoZipVenda);

    nomeDoArquivo.Clear();

    nomeDoArquivo.Append('XMLs Canceladas de ')
      .Append(FormatDateTime('dd.mm.yy', dataDe)).Append(' até ')
      .Append(FormatDateTime('dd.mm.yy', dataAte));

    arquivoZipCancelos := compactarArquivo
      (sat.ListarXML(pasta.PastaCancelos +
      FEstabelecimento.cnpj, dataDe, dataAte), nomeDoArquivo.ToString);

    if FileExists(arquivoZipCancelos) then
      anexos.Add(arquivoZipCancelos);

    titulo.Append('XMLs do S@T ').Append(FEstabelecimento.nomeFantasia);

    msg.Append('Segue em anexo os XML do S@T do cliente: ').AppendLine()
      .Append(FEstabelecimento.nomeFantasia).AppendLine().Append('CNPJ: ')
      .AppendLine().Append(FEstabelecimento.cnpj).AppendLine().AppendLine()
      .Append('Esse é um E-Mail automático').AppendLine()
      .Append('Qualquer duvida entre em contato conosco: ').AppendLine()
      .Append('Email:').AppendLine().Append(EMAILREMETENTE).AppendLine()
      .Append('Tel: ').AppendLine().Append('(11) 2841-6556').AppendLine()
      .AppendLine();

    processamento:= TTask.Create(procedure
    var
      it2: string;
    begin
      try
        try
          EnviarRelatorio(anexos.ToArray(), emails.ToArray(), titulo.ToString() , msg.ToString());
          if Assigned(_contabilidade.OnStatusEmail) then
            OnStatusEmail('Relatório enviado');

          for it2 in emails.ToArray() do
          begin
            emailStr:= emailStr + ', ' + it2;
          end;

          TAuditoriaRepository.GravarLog(
              'Enviou o Relatório',
               FormHome.txtUsuario.Text + #13 +
              ' Enviou o relatório para os emails: ' + emailStr + #13 +
              ' Horario:' + TimeToStr(Now),
              'R');
        except
          if Assigned(_contabilidade.OnStatusEmail) then
            OnStatusEmail('Erro ao enviar o Relatório automático');

          TAuditoriaRepository.GravarLog(
              'Erro ao enviar o Relatório',
               FormHome.txtUsuario.Text + #13 +
              ' Tentou enviar o relatório para os emails: ' + emailStr + #13 +
              ' Horario:' + TimeToStr(Now),
              'R');
        end;
      finally
        anexos.free;
        emails.free;
        FreeAndNil(msg);
        FreeAndNil(titulo);
      end;
    end);

    processamento.Start();
  finally
    pasta.Free;
    sat.free;
    FreeAndNil(nomeDoArquivo);
  end;
end;

/// <summary>
///   Método que verifica se todas as propriedades estão preenchidas
/// </summary>
function TContabilidade.GetConfigurado: boolean;
begin
  result:= not(NomeFantasia.IsEmpty) and
           not(NomeResponsavel.IsEmpty) and
            (Email.Count > 0);
end;

/// <summary>
///   Método que limpa os emails
/// </summary>
procedure TContabilidade.limparEmail;
begin
  FEmail.Clear();
end;

/// <summary>
///   Método que move um arquivo para os documentos
/// <param name="Arquivos">
///   arquivo que será enviado para documentos
/// </param>
/// </summary>
procedure TContabilidade.MoverParaDocumentos(const Arquivo: string);
var
  arquivoDestino: string;
begin

  arquivoDestino := TPath.GetDocumentsPath() + '\' + TPath.GetFileName(Arquivo);

  if FileExists(arquivoDestino) then
    tfile.Delete(arquivoDestino);

  tfile.Move(Arquivo, arquivoDestino);
end;

/// <summary>
///   Método que salva as propriedades em um arquivo de configuração
/// </summary>
procedure TContabilidade.Salvar;
var
  ini: iSalvaCarregaConfiguracao;
  it: string;
  indice: integer;
begin
  ini:= TFactoryPersistenciaConfiguracao.getPersistenciaConfiguracao(tpcIni);
  for it in Email do
  begin
    if string.IsNullOrWhiteSpace(it) then
    begin
     indice:= Email.IndexOf(it);
     email.Delete(indice);
    end;
  end;
  ini.salva(self);
end;

/// <summary>
///   Método que salva o relatórios em documentos
/// <param name="dataDe">
///   Data de inicio que envia os dados
/// </param>
/// <param name="dataAte">
///   Data de fim que envia os dados
/// </param>
/// </summary>
procedure TContabilidade.SalvarRelatoriosDocumentos(const dataDe, dataAte: TDate);
var
  nomeDoArquivo: TStringBuilder;
  arquivoZipVenda: string;
  arquivoZipCancelos: string;
  sat: TSAT;
  pasta: TPasta;
begin
  nomeDoArquivo := TStringBuilder.Create();
  sat:= TSAT.Create();
  pasta:= TPasta.Create;
  try

    nomeDoArquivo.Append('XMLs Vendas de ')
      .Append(FormatDateTime('dd.mm.yy', dataDe)).Append(' até ')
      .Append(FormatDateTime('dd.mm.yy', dataAte));

    arquivoZipVenda := compactarArquivo(
      sat.ListarXML(pasta.PastaVenda + FEstabelecimento.CNPJ, dataDe, dataAte),
       nomeDoArquivo.ToString());

    if arquivoZipVenda.IsEmpty then
    begin
      OnStatusEmail('Não Há arquivos para serem enviados');
      exit;
    end;

    if FileExists(arquivoZipVenda) then
      MoverParaDocumentos(arquivoZipVenda);

    nomeDoArquivo.Clear;

    nomeDoArquivo.Append('XMLs Cancelados de ')
      .Append(FormatDateTime('dd.mm.yy', dataDe)).Append(' até ')
      .Append(FormatDateTime('dd.mm.yy', dataAte));

    arquivoZipCancelos := compactarArquivo
      (sat.ListarXML(pasta.PastaCancelos +
      FEstabelecimento.cnpj, dataDe, dataAte), nomeDoArquivo.ToString);

    if FileExists(arquivoZipCancelos) then
      MoverParaDocumentos(arquivoZipCancelos);

    OnStatusEmail('Relatório salvo!');
    TModelPowerCMD.New(HInstance).ExecDir(TPath.GetDocumentsPath());

    TAuditoriaRepository.GravarLog(
              'Salvou o Relatório',
               FormHome.txtUsuario.Text + #13 +
              ' Salvou o relatório nos "Documentos": ' + #13 +
              ' Horario:' + TimeToStr(Now),
              'R');

  finally
    pasta.Free;
    sat.free;
    if Assigned(nomeDoArquivo) then
      FreeAndNil(nomeDoArquivo);
  end;
end;

/// <summary>
///   Método que define um dia para envio automático dos relatórios
/// </summary>
procedure TContabilidade.SetDiaEnvioAutomatico(const Value: integer);
begin
  if (Value <= 0) or (Value >= 31) then
    FDiaEnvioAutomatico:= 1
  else
    FDiaEnvioAutomatico := Value;
end;

end.
