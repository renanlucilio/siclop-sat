/// <summary>
///   Unit que tem a classe TSoftwareHouse
/// </summary>
unit uSoftwareHouse;

interface

uses
  PersistenciaConfiguracao.Interfaces;

type
  TSoftwareHouse = class(TInterfacedObject, iConfiguravel)
  private
    FNomeFantasia: string;
    FCNPJ:         string;
    FChaveAC:      string;
    FNumeroCaixa: integer;
  public
    constructor Create();

    procedure Salvar;
    procedure Carregar;
    function GetConfigurado: boolean;

    property NomeFantasia: string  read FNomeFantasia write FNomeFantasia;
    property CNPJ:         string  read FCNPJ         write FCNPJ;
    property ChaveAC:      string  read FChaveAC      write FChaveAC;
    property NumeroCaixa:  integer read FNumeroCaixa  write FNumeroCaixa;
  end;

implementation

uses
  System.SysUtils,
  uValidacoes,
  Factory.PersistenciaConfigucao;

{ TSoftwareHouse }

/// <summary>
///   M�todo que carrega as informa��es de propriedade de um arquivo de configura��o
/// </summary>
procedure TSoftwareHouse.Carregar;
var
  ini: iSalvaCarregaConfiguracao;
begin
  ini:= TFactoryPersistenciaConfiguracao.getPersistenciaConfiguracao(tpcIni);
  ini.carrega(Self);

  NomeFantasia := Ini.getResultado().Items['NomeFantasia'];

  if NomeFantasia.IsEmpty() then nomeFantasia := 'SICLOP SERVICOS E SISTEMAS EIRELI - ME';

  CNPJ := Ini.getResultado().Items['CNPJ'];

   if cnpj.IsEmpty then cnpj := '19832566000108';

  NumeroCaixa:= StrToIntDef(Ini.getResultado().Items['NumeroCaixa'], 1);

  ChaveAC := Ini.getResultado().Items['ChaveAC'];
end;

/// <summary>
///   M�todo construtor que cria uma instancia e carrega as informa��es
/// </summary>
constructor TSoftwareHouse.Create;
begin
  Carregar();
end;

/// <summary>
///   M�todo que verifica se as propriedades est�o configuradas
/// <returns>
///   Retorna true se as configura��es est�o configuradas
/// </returns>
/// </summary>
function TSoftwareHouse.GetConfigurado: boolean;
begin
  Result:= not(ChaveAC.IsEmpty) and
            (Length(ChaveAC) >= 8);
end;

/// <summary>
///   M�todo que salva as propriedades em um arquivo de configura��o
/// </summary>
procedure TSoftwareHouse.Salvar;
var
  ini: iSalvaCarregaConfiguracao;
begin
  ini:= TFactoryPersistenciaConfiguracao.getPersistenciaConfiguracao(tpcIni);
  ini.salva(Self);
end;

end.
