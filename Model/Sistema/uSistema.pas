/// <summary>
///   Unit que tem a classe TSistema
/// </summary>
unit uSistema;

interface

uses
  Factory.PersistenciaConfigucao,
  PersistenciaConfiguracao.Interfaces,
  System.SysUtils;

type
  TSistema = class(TInterfacedObject, iConfiguravel)
  private
    FAutonamo: Boolean;
    FCofigurado: Boolean;
    function GetConfigurado: boolean;
  public
    constructor Create;
    destructor Destroy; override;

    property Autonamo: Boolean read FAutonamo write FAutonamo;

    procedure Salvar;
    procedure Carregar;


  end;

implementation

{ TSistema }

/// <summary>
///   M�todo que carrega as propriedades de um arquivo de configura��o
/// </summary>
procedure TSistema.Carregar;
var
  ini: iSalvaCarregaConfiguracao;
begin
  ini:= TFactoryPersistenciaConfiguracao.getPersistenciaConfiguracao(tpcIni);
  ini.carrega(self);
  if ini.getResultado['Autonamo'].IsEmpty then
    Autonamo:= false
  else
    Autonamo:= ini.getResultado['Autonamo'].Equals('True');
end;

/// <summary>
///   M�todo construtor que cria um instancia e carrega as informa��es
/// </summary>
constructor TSistema.Create;
begin
  Carregar;
end;

/// <summary>
///   M�todo destrutor que elimina uma instancia
/// </summary>
destructor TSistema.Destroy;
begin

  inherited;
end;

/// <summary>
///   M�todo que vefirifica se as propriedades est�o configuradas
/// <returns>
///   retorna true se estiver configurado
/// </returns>
/// </summary>
function TSistema.GetConfigurado: boolean;
begin
  result:= true;
end;

procedure TSistema.Salvar;
var
  ini: iSalvaCarregaConfiguracao;
begin
  ini:= TFactoryPersistenciaConfiguracao.getPersistenciaConfiguracao(tpcIni);
  ini.salva(self);

end;

end.
