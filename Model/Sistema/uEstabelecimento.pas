/// <summary>
///   Unit que tem a classe TEstabelecimento
/// </summary>
unit uEstabelecimento;

interface

uses
  pcnConversao,
  Factory.PersistenciaConfigucao,
  PersistenciaConfiguracao.Interfaces;

type
  TEstabelecimento = class(TInterfacedObject, iConfiguravel)
  private
    FNomeFantasia:      string;
    FCNPJ:              string;
    FIE:                string;
    FIM:                string;
    FCodigoUF:          integer;
    FehSimplesNacional: Boolean;
    FFazerRateio:       Boolean;
    FISSQN:             TpcnRegTribISSQN;
    FEmail:             string;
    FUltimoRelatorio:   string;
  public
    constructor Create();

    procedure Carregar;
    function GetConfigurado: boolean;
    procedure Salvar;

    procedure SetISSQN(indice: integer);
    property NomeFantasia:      string           read FNomeFantasia      write FNomeFantasia;
    property CNPJ:              string           read FCNPJ              write FCNPJ;
    property IE:                string           read FIE                write FIE;
    property IM:                string           read FIM                write FIM;
    property CodigoUF:          integer          read FCodigoUF          write FCodigoUF;
    property ehSimplesNacional: Boolean          read FehSimplesNacional write FehSimplesNacional;
    property FazerRateio:       Boolean          read FFazerRateio       write FFazerRateio;
    property ISSQN:             TpcnRegTribISSQN read FISSQN             write FISSQN;
    property Email:             string           read FEmail             write FEmail;
    property UltimoRelatorio:   string            read FUltimoRelatorio   write FUltimoRelatorio;
  end;

implementation

uses
  uValidacoes,
  System.SysUtils,
  System.DateUtils;
{ TEstabelecimento }


/// <summary>
///   M�todo que define o ISSQN com base em um numero
/// <param name="indice ">
///   Indice que ser� convertido em ISSQN
/// </param>
/// </summary>
procedure TEstabelecimento.SetISSQN(indice: integer);
begin
  ISSQN := TpcnRegTribISSQN(indice);
end;

/// <summary>
///   M�todo que salva as propriedades em um arquivo de configura��o
/// </summary>
procedure TEstabelecimento.Salvar;
var
  ini: iSalvaCarregaConfiguracao;
begin
  ini:= TFactoryPersistenciaConfiguracao.getPersistenciaConfiguracao(tpcIni);

  ini.salva(self);

end;

/// <summary>
///   M�todo que carrega as propriedades do arquivo de configura��o
/// <returns>
///   retorna true se estiver configurado
/// </returns>
/// </summary>
procedure TEstabelecimento.Carregar;
var
  ini: iSalvaCarregaConfiguracao;
begin
  ini:= TFactoryPersistenciaConfiguracao.getPersistenciaConfiguracao(tpcIni);

  ini.carrega(self);

   NomeFantasia:= ini.getResultado.Items['NomeFantasia'];

  CNPJ:= ini.getResultado.Items['CNPJ'];

  IE:= ini.getResultado.Items['IE'];

  IM:= ini.getResultado.Items['IM'];

  Email:= ini.getResultado.Items['Email'];

  CodigoUF := StrToIntDef(ini.getResultado.Items['CodigoUF'], 35);

  if ini.getResultado.Items['ehSimplesNacional'].Equals('False') then ehSimplesNacional:= False else ehSimplesNacional:= True;

  if ini.getResultado.Items['FazerRateio'].Equals('True') then FazerRateio:= True else FazerRateio:= false;

  ISSQN := TpcnRegTribISSQN(StrToIntDef(ini.getResultado.Items['ISSQN'], 6));

  if ini.getResultado.Items['UltimoRelatorio'].IsEmpty then
    UltimoRelatorio := DateToStr(DateOf(0))
  else
    UltimoRelatorio := ini.getResultado.Items['UltimoRelatorio'];
end;

/// <summary>
///   M�todo construtor que cria uma instancia e carregar as propriedades
/// </summary>
constructor TEstabelecimento.Create;
begin
  Carregar();
end;

/// <summary>
///   M�tdo que verifica se as propriedades foram preenchidas
/// </summary>
function TEstabelecimento.GetConfigurado: boolean;
begin
  result:= not(NomeFantasia.IsEmpty) and
           not(CNPJ.IsEmpty) and
           not(IE.IsEmpty);
end;

end.
