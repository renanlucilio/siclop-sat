unit uCon;

interface

uses
  uConexao.interfaces,
  uConexaoORM,
  uConexao.SQLITE;

type
  TConexao = class(TInterfacedObject, IDefCon)
  private
    FConexao: IConexao;
    FORM:     TConexaoORM;

    function GetConexao: IConexao;
    procedure SetConexao(const Value: IConexao);
  public
    constructor Create;
    destructor Destroy; override;

    property Conexao: IConexao    read GetConexao write SetConexao;
    property ORM:     TConexaoORM read FORM       write FORM;
    function Ref:     IDefCon;
  end;

implementation

uses
  System.IOUtils,
  ormbr.factory.interfaces,
  System.Classes,
  System.Types,
  System.SysUtils,

  Model.LibUtil;

{ TConexao }

constructor TConexao.Create;
const
  verificarSeExiste =  'SELECT * FROM sqlite_master WHERE type=''VENDA_IMPOSTO''';
var
  parametros: TParamsSQLite;
  SQLFile, listaTabelas: TStringList;
  arquivo: string;
  index: integer;
  recurso: TStream;
begin
   arquivo:= ExtractFilePath(ParamStr(0))+'controle.db';
  parametros.Database := arquivo;
  parametros.User_Name:= '';
  parametros.Password := '';
  parametros.Exclusive:= true;
  parametros.Encrypt  := true;

  FConexao := TConexaoSQLite.New(parametros);
  FORM:= TConexaoORM.Create(FConexao.Connection, dnSQLite);
  listaTabelas:= TStringList.Create;
  FConexao.Connection.GetTableNames('', '', '', listaTabelas);
  try
    if not listaTabelas.Find('VENDA_IMPOSTO', index) then
    begin
      SQLFile:= TStringList.Create();
      recurso:= TResourceStream.Create(HInstance, 'ControleSQL', RT_RCDATA);
      SQLFile.LoadFromStream(recurso);
      try
        FConexao.Connection.ExecSQL(SQLFile.Text);
      finally
        recurso.Free;
        SQLFile.Free;
      end;
    end;
  finally
    listaTabelas.Free;
  end;
end;

destructor TConexao.Destroy;
begin
  FORM.Free;
  inherited;
end;

function TConexao.GetConexao: IConexao;
begin
  result:= FConexao;
end;

function TConexao.Ref: IDefCon;
begin
  result:= self;
end;

procedure TConexao.SetConexao(const Value: IConexao);
begin
   FConexao:= value;
end;

end.



