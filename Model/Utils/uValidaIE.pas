/// <summary>
///   Unit que tem os m�todos de verifica��o de IE
/// </summary>
unit uValidaIE;

interface

Uses SysUtils;


/// <summary>
///   M�todo de verifica��o de Inscri��o estadual
///  /// <param name="ie">
     ///   valor a ser verificado
     /// </param>
     ///  /// <param name="uf">
          ///   Estado a ser verificado
          /// </param>
          ///  /// <returns>
               ///   Retorna true se estiver tudo ok
               /// </returns>
/// </summary>
Function ChkInscEstadual(const ie, uf: string): Boolean;
/// <summary>
///   M�todo que verifica de mascara de uma Inscri��o estadual
///  /// <param name="inscricao">
     ///   mascara a ser validada
     /// </param>
     ///  /// <param name="estado">
          ///   estado a ser validado
          /// </param>
          ///  /// <returns>
               ///   retorna a ie com a mascara
               /// </returns>
/// </summary>
Function Mascara_Inscricao(Inscricao, Estado: String): String;

implementation

const
  OrdZero = Ord('0');

Function AllTrim(const S: string): string;

{ -Return a string with leading and trailing white space removed } var
  I, L: Integer;
begin
  L := Length(S);
  I := 1;
  while (I <= L) and (S[I] <= ' ') do
    Inc(I);
  if I > L then
    Result := ''
  else
  begin
    while S[L] <= ' ' do
      Dec(L);
    Result := Copy(S, I, L - I + 1);
  end;

end; { AllTrim }

Function Empty(const S: String): Boolean;
var
  aux: string;
begin
  aux := AllTrim(S);
  if Length(aux) = 0 then
    Result := true
  else
    Result := false;
end;

Function IsNumero(const S: string): Boolean;
var
  I: byte;
begin
  Result := false;
  for I := 1 to Length(S) do
    if not(S[I] in ['0' .. '9']) then
      exit;
  Result := true;
end; { IsNumero }

{ chInt - Converte um caracter num�rico para o valor inteiro correspondente. }
function CharToInt(ch: Char): ShortInt;
begin
  Result := Ord(ch) - OrdZero;
end;

{ intCh = Converte um valor inteiro (de 0 a 9) para o caracter num�rico
  correspondente. }

function IntToChar(int: ShortInt): Char;
begin
  Result := Chr(int + OrdZero);
end;

Function CHKIEMG(const iemg: string): Boolean;
var
  npos, I: byte;
  ptotal, psoma: Integer;
  dig1, dig2: string[1];
  ie, insc: string;
  nresto: SmallInt;
begin
  //
  Result := true;
  ie := AllTrim(iemg);
  if (Empty(ie)) then
    exit;
  if Copy(ie, 1, 2) = 'PR' then
    exit;
  if Copy(ie, 1, 5) = 'ISENT' then
    exit;

  Result := false;
  If (Trim(iemg) = '.') Then
    exit;
  if (Length(ie) <> 13) then
    exit;
  if not IsNumero(ie) then
    exit;

  dig1 := Copy(ie, 12, 1);
  dig2 := Copy(ie, 13, 1);
  insc := Copy(ie, 1, 3) + '0' + Copy(ie, 4, 8);
  // CALCULA DIGITO 1
  npos := 12;
  I := 1;
  ptotal := 0;
  while npos > 0 do
  begin
    Inc(I);
    psoma := CharToInt(insc[npos]) * I;
    IF psoma >= 10 then
      psoma := psoma - 9;
    Inc(ptotal, psoma);
    IF I = 2 then
      I := 0;
    Dec(npos);
  end;
  nresto := ptotal mod 10;
  if nresto = 0 then
    nresto := 10;
  nresto := 10 - nresto;
  if nresto <> CharToInt(Char(dig1[1])) then
    exit;

  // CALCULA DIGITO 2
  npos := 12;
  I := 1;
  ptotal := 0;
  while npos > 0 do
  begin
    Inc(I);
    if I = 12 then
      I := 2;
    Inc(ptotal, CharToInt(ie[npos]) * I);
    Dec(npos);
  end;
  nresto := ptotal mod 11;
  if (nresto = 0) or (nresto = 1) then
    nresto := 11;
  nresto := 11 - nresto;
  if nresto <> CharToInt(Char(dig2[1])) then
    exit;
  Result := true;
end; // ChkMG


// ----------------------------------- Inscri��es Estaduais

Function ChkIEAC(const ie: string): Boolean; // 99.999.999/999-99
var
  b, I, soma: Integer;
  dig: SmallInt;
begin
  Result := false;
  if (Length(ie) <> 13) then
    exit;
  if not IsNumero(ie) then
    exit;
  b := 4;
  soma := 0;
  for I := 1 to 11 do
  begin
    Inc(soma, CharToInt(ie[I]) * b);
    Dec(b);
    if b = 1 then
      b := 9;
  end;
  dig := 11 - (soma mod 11);
  if (dig >= 10) then
    dig := 0;
  Result := (IntToChar(dig) = ie[12]);
  if not Result then
    exit;

  b := 5;
  soma := 0;
  for I := 1 to 12 do
  begin
    Inc(soma, CharToInt(ie[I]) * b);
    Dec(b);
    if b = 1 then
      b := 9;
  end;
  dig := 11 - (soma mod 11);
  if (dig >= 10) then
    dig := 0;
  Result := (IntToChar(dig) = ie[13]);
end; // ChkIEAC

Function ChkIEAL(const ie: string): Boolean; // 24XNNNNND
var
  b, I, soma: Integer;
  dig: SmallInt;
begin
  Result := false;
  if (Length(ie) <> 9) then
    exit;
  if not IsNumero(ie) then
    exit;
  if Copy(ie, 1, 2) <> '24' then
    exit;
  b := 9;
  soma := 0;
  for I := 1 to 08 do
  begin
    Inc(soma, CharToInt(ie[I]) * b);
    Dec(b);
  end;
  soma := soma * 10;
  dig := soma - trunc(soma / 11) * 11;
  if dig = 10 then
    dig := 0;
  Result := (IntToChar(dig) = ie[09]);
end; // ChkIEAL

Function ChkIEAM(const ie: string): Boolean; // 99.999.999-9
var
  b, I, soma: Integer;
  dig: SmallInt;
begin
  Result := false;
  if (Length(ie) <> 9) then
    exit;
  if not IsNumero(ie) then
    exit;
  b := 9;
  soma := 0;
  for I := 1 to 08 do
  begin
    Inc(soma, CharToInt(ie[I]) * b);
    Dec(b);
  end;
  if soma < 11 then
    dig := 11 - soma
  else
  begin
    I := (soma mod 11);
    if I <= 1 then
      dig := 0
    else
      dig := 11 - I;
  end;
  Result := (IntToChar(dig) = ie[09]);
end; // ChkIEAM

Function ChkIEAP(const ie: string): Boolean; // 999999999
var
  p, d, b, I, soma: Integer;
  dig: SmallInt;
begin
  Result := false;
  if (Length(ie) <> 9) then
    exit;
  if not IsNumero(ie) then
    exit;
  p := 0;
  d := 0;
  I := StrToInt(Copy(ie, 1, 8));
  if (I >= 03000001) and (I <= 03017000) then
  begin
    p := 5;
    d := 0;
  end
  else if (I >= 03017001) and (I <= 03019022) then
  begin
    p := 9;
    d := 1;
  end;
  b := 9;
  soma := p;
  for I := 1 to 08 do
  begin
    Inc(soma, CharToInt(ie[I]) * b);
    Dec(b);
  end;
  dig := 11 - (soma mod 11);
  if dig = 10 then
    dig := 0
  else if dig = 11 then
    dig := d;
  Result := (IntToChar(dig) = ie[09]);
end; // ChkIEAP

Function ChkIEBA(const ie: string): Boolean; // 999999-99
var
  b, I, soma: Integer;
  nro: array [1 .. 8] of byte;
  NumMod: word;
  dig: SmallInt;
  die: string;
begin
  Result := false;
  if (Length(ie) <> 8) then
    exit;
  die := Copy(ie, 1, 8);
  if not IsNumero(die) then
    exit;
  for I := 1 to 8 do
    nro[I] := CharToInt(die[I]);
  if nro[1] in [0, 1, 2, 3, 4, 5, 8] then
    NumMod := 10
  else
    NumMod := 11;
  // calculo segundo
  b := 7;
  soma := 0;
  for I := 1 to 06 do
  begin
    Inc(soma, (nro[I] * b));
    Dec(b);
  end;
  I := soma mod NumMod;
  if NumMod = 10 then
  begin
    if I = 0 then
      dig := 0
    else
      dig := NumMod - I;
  end
  else
  begin
    if I <= 1 then
      dig := 0
    else
      dig := NumMod - I;
  end;
  Result := (dig = nro[8]);
  if not Result then
    exit;
  // calculo segundo
  b := 8;
  soma := 0;
  for I := 1 to 06 do
  begin
    Inc(soma, (nro[I] * b));
    Dec(b);
  end;
  Inc(soma, (nro[8] * 2));
  I := soma mod NumMod;
  if NumMod = 10 then
  begin
    if I = 0 then
      dig := 0
    else
      dig := NumMod - I;
  end
  else
  begin
    if I <= 1 then
      dig := 0
    else
      dig := NumMod - I;
  end;
  Result := (dig = nro[7]);
end; // ChkIEBA

Function ChkIECE(const ie: string): Boolean; // 999999999
var
  b, I, soma: Integer;
  nro: array [1 .. 9] of byte;
  dig: SmallInt;
  die: string;
begin
  Result := false;
  if (Length(ie) > 9) then
    exit;
  if not IsNumero(ie) then
    exit;
  die := ie;
  if Length(ie) < 9 then
  begin
    repeat
      die := '0' + die;
    until Length(die) = 9;
  end;
  for I := 1 to 9 do
    nro[I] := CharToInt(die[I]);
  b := 9;
  soma := 0;
  for I := 1 to 08 do
  begin
    Inc(soma, (nro[I] * b));
    Dec(b);
  end;
  dig := 11 - (soma mod 11);
  if dig >= 10 then
    dig := 0;
  Result := (dig = nro[9]);
end; // ChkIECE

Function ChkIEDF(const ie: string): Boolean; // 999.99999.999.99
var
  b, I, soma: Integer;
  nro: array [1 .. 13] of byte;
  dig: SmallInt;
begin
  Result := false;
  if (Length(ie) <> 13) then
    exit;
  if not IsNumero(ie) then
    exit;
  for I := 1 to 13 do
    nro[I] := CharToInt(ie[I]);
  b := 4;
  soma := 0;
  for I := 1 to 11 do
  begin
    Inc(soma, nro[I] * b);
    Dec(b);
    if b = 1 then
      b := 9;
  end;
  dig := 11 - (soma mod 11);
  if dig >= 10 then
    dig := 0;
  Result := (dig = nro[12]);
  if not Result then
    exit;

  b := 5;
  soma := 0;
  for I := 1 to 12 do
  begin
    Inc(soma, nro[I] * b);
    Dec(b);
    if b = 1 then
      b := 9;
  end;
  dig := 11 - (soma mod 11);
  if dig >= 10 then
    dig := 0;
  Result := (dig = nro[13]);
end; // ChkIEDF

Function ChkIEES(const ie: string): Boolean; // 99999999-9
var
  b, I, soma: Integer;
  nro: array [1 .. 9] of byte;
  dig: SmallInt;
begin
  Result := false;
  if (Length(ie) <> 9) then
    exit;
  if not IsNumero(ie) then
    exit;
  for I := 1 to 9 do
    nro[I] := CharToInt(ie[I]);
  b := 9;
  soma := 0;
  for I := 1 to 08 do
  begin
    Inc(soma, nro[I] * b);
    Dec(b);
  end;
  I := soma mod 11;
  if I < 2 then
    dig := 0
  else
    dig := 11 - I;
  Result := (dig = nro[9]);
end; // ChkIEES

Function ChkIEGO(const ie: string): Boolean; // 99.999.999.9
var
  n, b, I, soma: Integer;
  nro: array [1 .. 9] of byte;
  dig: SmallInt;
  S: string;
begin
  Result := false;
  if (Length(ie) <> 9) then
    exit;
  if not IsNumero(ie) then
    exit;
  S := Copy(ie, 1, 2);
  if (S = '10') or (S = '11') or (S = '15') then
  begin
    for I := 1 to 9 do
      nro[I] := CharToInt(ie[I]);
    n := trunc(StrToFloat(ie) / 10);
    if n = 11094402 then
    begin
      if (nro[9] = 0) or (nro[9] = 1) then
      begin
        Result := true;
        exit;
      end;
    end;

    b := 9;
    soma := 0;
    for I := 1 to 08 do
    begin
      Inc(soma, nro[I] * b);
      Dec(b);
    end;
    I := (soma mod 11);
    if I = 0 then
      dig := 0
    else if I = 1 then
    begin
      if (n >= 10103105) and (n <= 10119997) then
        dig := 1
      else
        dig := 0;
    end
    else
    begin
      dig := 11 - I;
    end;
    Result := (dig = nro[9]);
  end;
end; // ChkIEGO

Function ChkIEMA(const ie: string): Boolean; // 999999999
var
  b, I, soma: Integer;
  nro: array [1 .. 9] of byte;
  dig: SmallInt;
begin
  Result := false;
  if (Length(ie) <> 9) then
    exit;
  if not IsNumero(ie) then
    exit;
  for I := 1 to 9 do
    nro[I] := CharToInt(ie[I]);
  b := 9;
  soma := 0;
  for I := 1 to 08 do
  begin
    Inc(soma, nro[I] * b);
    Dec(b);
  end;
  I := (soma mod 11);
  if (I <= 1) then
    dig := 0
  else
    dig := 11 - I;
  Result := (dig = nro[9]);
end; // ChkIEMA

Function ChkIEMT(const ie: string): Boolean; // 9999999999-9
var
  b, I, soma: Integer;
  nro: array [1 .. 11] of byte;
  dig: SmallInt;
  die: string;
begin
  Result := false;
  if (Length(ie) < 9) then
    exit;
  die := ie;
  if Length(die) < 11 then
  begin
    repeat
      die := '0' + die;
    until Length(die) = 11;
  end;
  if not IsNumero(die) then
    exit;
  for I := 1 to 11 do
    nro[I] := CharToInt(die[I]);
  b := 3;
  soma := 0;
  for I := 1 to 10 do
  begin
    Inc(soma, nro[I] * b);
    Dec(b);
    if b = 1 then
      b := 9;
  end;
  I := (soma mod 11);
  if (I <= 1) then
    dig := 0
  else
    dig := 11 - I;
  Result := (dig = nro[11]);
end; // ChkIEMT

Function ChkIEMS(const ie: string): Boolean; // 999999999
var
  b, I, soma: Integer;
  nro: array [1 .. 09] of byte;
  dig: SmallInt;
begin
  Result := false;
  if (Length(ie) <> 09) then
    exit;
  if not IsNumero(ie) then
    exit;
  if Copy(ie, 1, 2) <> '28' then
    exit;
  for I := 1 to 09 do
    nro[I] := CharToInt(ie[I]);
  b := 9;
  soma := 0;
  for I := 1 to 08 do
  begin
    Inc(soma, nro[I] * b);
    Dec(b);
  end;
  I := (soma mod 11);
  if (I <= 1) then
    dig := 0
  else
    dig := 11 - I;
  Result := (dig = nro[09]);
end; // ChkIEMS

Function ChkIEPA(const ie: string): Boolean; // 99.999999-9
var
  b, I, soma: Integer;
  nro: array [1 .. 09] of byte;
  dig: SmallInt;
begin
  Result := false;
  if (Length(ie) <> 09) then
    exit;
  if not IsNumero(ie) then
    exit;
  if Copy(ie, 1, 2) <> '15' then
    exit;
  for I := 1 to 09 do
    nro[I] := CharToInt(ie[I]);
  b := 9;
  soma := 0;
  for I := 1 to 08 do
  begin
    Inc(soma, nro[I] * b);
    Dec(b);
  end;
  I := (soma mod 11);
  if (I <= 1) then
    dig := 0
  else
    dig := 11 - I;
  Result := (dig = nro[09]);
end; // ChkIEPA

Function ChkIEPB(const ie: string): Boolean; // 99999999-9
var
  b, I, soma: Integer;
  nro: array [1 .. 09] of byte;
  dig: SmallInt;
begin
  Result := false;
  if (Length(ie) <> 09) then
    exit;
  if not IsNumero(ie) then
    exit;
  for I := 1 to 09 do
    nro[I] := CharToInt(ie[I]);
  b := 9;
  soma := 0;
  for I := 1 to 08 do
  begin
    Inc(soma, nro[I] * b);
    Dec(b);
  end;
  I := (soma mod 11);
  if (I <= 1) then
    dig := 0
  else
    dig := 11 - I;
  Result := (dig = nro[09]);
end; // ChkIEPB

Function ChkIEPR(const ie: string): Boolean; // 99999999-99
var
  b, I, soma: Integer;
  nro: array [1 .. 10] of byte;
  dig: SmallInt;
begin
  Result := false;
  if (Length(ie) <> 10) then
    exit;
  if not IsNumero(ie) then
    exit;
  for I := 1 to 10 do
    nro[I] := CharToInt(ie[I]);
  b := 3;
  soma := 0;
  for I := 1 to 08 do
  begin
    Inc(soma, nro[I] * b);
    Dec(b);
    if b = 1 then
      b := 7;
  end;
  I := (soma mod 11);
  if (I <= 1) then
    dig := 0
  else
    dig := 11 - I;
  Result := (dig = nro[09]);
  if not Result then
    exit;

  b := 4;
  soma := 0;
  for I := 1 to 09 do
  begin
    Inc(soma, nro[I] * b);
    Dec(b);
    if b = 1 then
      b := 7;
  end;
  I := (soma mod 11);
  if (I <= 1) then
    dig := 0
  else
    dig := 11 - I;
  Result := (dig = nro[10]);
end; // ChkIEPR

Function ChkIEPE(const ie: string): Boolean; // 99.9.999.9999999-9
var
  b, I, soma: Integer;
  nro: array [1 .. 14] of byte;
  dig: SmallInt;
begin
  Result := false;
  if (Length(ie) <> 14) then
    exit;
  if not IsNumero(ie) then
    exit;
  for I := 1 to 14 do
    nro[I] := CharToInt(ie[I]);
  b := 5;
  soma := 0;
  for I := 1 to 13 do
  begin
    Inc(soma, nro[I] * b);
    Dec(b);
    if b = 0 then
      b := 9;
  end;
  dig := 11 - (soma mod 11);
  if dig > 9 then
    dig := dig - 10;
  Result := (dig = nro[14]);
end; // ChkIEPE

Function ChkIEPI(const ie: string): Boolean; // 999999999
var
  b, I, soma: Integer;
  nro: array [1 .. 09] of byte;
  dig: SmallInt;
begin
  Result := false;
  if (Length(ie) <> 09) then
    exit;
  if not IsNumero(ie) then
    exit;
  for I := 1 to 09 do
    nro[I] := CharToInt(ie[I]);
  b := 9;
  soma := 0;
  for I := 1 to 08 do
  begin
    Inc(soma, nro[I] * b);
    Dec(b);
  end;
  I := (soma mod 11);
  if (I <= 1) then
    dig := 0
  else
    dig := 11 - I;
  Result := (dig = nro[09]);
end; // ChkIEPI

Function ChkIERJ(const ie: string): Boolean; // 99.999.99-9
var
  b, I, soma: Integer;
  nro: array [1 .. 08] of byte;
  dig: SmallInt;
begin
  Result := false;
  if (Length(ie) <> 08) then
    exit;
  if not IsNumero(ie) then
    exit;
  for I := 1 to 08 do
    nro[I] := CharToInt(ie[I]);
  b := 2;
  soma := 0;
  for I := 1 to 07 do
  begin
    Inc(soma, nro[I] * b);
    Dec(b);
    if b = 1 then
      b := 7;
  end;
  I := (soma mod 11);
  if (I <= 1) then
    dig := 0
  else
    dig := 11 - I;
  Result := (dig = nro[08]);
end; // ChkIERJ

Function ChkIERN(const ie: string): Boolean; // 99.999.999-9
var
  b, I, soma: Integer;
  nro: array [1 .. 09] of byte;
  dig: SmallInt;
begin
  Result := false;
  if (Length(ie) <> 09) then
    exit;
  if not IsNumero(ie) then
    exit;
  for I := 1 to 09 do
    nro[I] := CharToInt(ie[I]);
  b := 9;
  soma := 0;
  for I := 1 to 08 do
  begin
    Inc(soma, nro[I] * b);
    Dec(b);
  end;
  soma := soma * 10;
  dig := (soma mod 11);
  if (dig = 10) then
    dig := 0;
  Result := (dig = nro[09]);
end; // ChkIERN

Function ChkIERS(const ie: string): Boolean; // 999.999999-9
var
  b, I, soma: Integer;
  nro: array [1 .. 10] of byte;
  dig: SmallInt;
begin
  Result := false;
  if (Length(ie) <> 10) then
    exit;
  if not IsNumero(ie) then
    exit;
  I := StrToInt(Copy(ie, 1, 3));
  if (I >= 1) and (I <= 467) then
  begin
    for I := 1 to 10 do
      nro[I] := CharToInt(ie[I]);
    b := 2;
    soma := 0;
    for I := 1 to 09 do
    begin
      Inc(soma, nro[I] * b);
      Dec(b);
      if b = 1 then
        b := 9;
    end;
    dig := 11 - (soma mod 11);
    if (dig >= 10) then
      dig := 0;
    Result := (dig = nro[10]);
  end;
end; // ChkIERS

// Rond�nia - vers�o antiga
Function ChkIERO(const ie: string): Boolean; // 999.99999-9
var
  b, I, soma: Integer;
  nro: array [1 .. 09] of byte;
  dig: SmallInt;
begin
  Result := false;
  if (Length(ie) <> 09) then
    exit;
  if not IsNumero(ie) then
    exit;
  for I := 1 to 09 do
    nro[I] := CharToInt(ie[I]);
  b := 6;
  soma := 0;
  for I := 4 to 08 do
  begin
    Inc(soma, nro[I] * b);
    Dec(b)
  end;
  dig := 11 - (soma mod 11);
  if (dig >= 10) then
    dig := dig - 10;
  Result := (dig = nro[09]);
end; // ChkIERO

// Rond�nia - vers�o nova
Function ValidaInscRO(SIE: string): Boolean;
var
  I, x, y, z, j: Integer;
  S: string;
begin
  I := 1;
  y := 6;
  x := 0;
  z := 0;
  j := 0;
  for j := 1 to Length(Trim(SIE)) do
    if SIE[j] in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0'] then
      S := S + SIE[j];
  if not Length(S) <> 14 then
  begin
    for I := 1 to (14 - Length(Trim(S))) do
      S := '0' + Trim(S)
  end;
  for I := 1 to (Length(S) - 1) do
  begin
    x := StrToInt(S[I]) * y;
    z := z + x;
    if y > 2 then
      Dec(y)
    else
      y := 9;
  end;
  x := z mod 11;
  y := 11 - x;
  if inttostr(y) = S[14] then
    Result := true
  else
    Result := false;
end;

Function ChkIERR(const ie: string): Boolean; // 99.999999-9
var
  I, soma: Integer;
  nro: array [1 .. 09] of byte;
  dig: SmallInt;
begin
  Result := false;
  if (Length(ie) <> 09) then
    exit;
  if not IsNumero(ie) then
    exit;
  if Copy(ie, 1, 2) <> '24' then
    exit;
  for I := 1 to 09 do
    nro[I] := CharToInt(ie[I]);
  soma := 0;
  for I := 1 to 08 do
  begin
    Inc(soma, nro[I] * I);
  end;
  dig := (soma mod 09);
  Result := (dig = nro[09]);
end; // ChkIERR

Function ChkIESC(const ie: string): Boolean; // 999.999.999
var
  b, I, soma: Integer;
  nro: array [1 .. 09] of byte;
  dig: SmallInt;
begin
  Result := false;
  if (Length(ie) <> 09) then
    exit;
  if not IsNumero(ie) then
    exit;
  for I := 1 to 09 do
    nro[I] := CharToInt(ie[I]);
  b := 9;
  soma := 0;
  for I := 1 to 08 do
  begin
    Inc(soma, nro[I] * b);
    Dec(b);
  end;
  I := (soma mod 11);
  if (I <= 1) then
    dig := 0
  else
    dig := 11 - I;
  Result := (dig = nro[09]);
end; // ChkIESC

Function ChkIESP(const ie: string): Boolean;
var
  I, soma: Integer;
  nro: array [1 .. 12] of byte;
  dig: SmallInt;
  S: string;
begin
  Result := false;
  if UpperCase(Copy(ie, 1, 1)) = 'P' then
  begin
    S := Copy(ie, 2, 9);
    if not IsNumero(S) then
      exit;
    for I := 1 to 8 do
      nro[I] := CharToInt(S[I]);
    soma := (nro[1] * 1) + (nro[2] * 3) + (nro[3] * 4) + (nro[4] * 5) +
      (nro[5] * 6) + (nro[6] * 7) + (nro[7] * 8) + (nro[8] * 10);
    dig := (soma mod 11);
    if (dig >= 10) then
      dig := 0;
    Result := (dig = nro[09]);
    if not Result then
      exit;
  end
  else
  begin
    if (Length(ie) < 12) then
      exit;
    if not IsNumero(ie) then
      exit;
    for I := 1 to 12 do
      nro[I] := CharToInt(ie[I]);
    soma := (nro[1] * 1) + (nro[2] * 3) + (nro[3] * 4) + (nro[4] * 5) +
      (nro[5] * 6) + (nro[6] * 7) + (nro[7] * 8) + (nro[8] * 10);
    dig := (soma mod 11);
    if (dig >= 10) then
      dig := 0;
    Result := (dig = nro[09]);
    if not Result then
      exit;
    soma := (nro[1] * 3) + (nro[2] * 2) + (nro[3] * 10) + (nro[4] * 9) +
      (nro[5] * 8) + (nro[6] * 7) + (nro[7] * 6) + (nro[8] * 5) + (nro[9] * 4) +
      (nro[10] * 3) + (nro[11] * 2);

    dig := (soma mod 11);
    if (dig >= 10) then
      dig := 0;
    Result := (dig = nro[12]);
  end;
end; // ChkIESP

Function ChkIESE(const ie: string): Boolean; // 99999999-9
var
  b, I, soma: Integer;
  nro: array [1 .. 09] of byte;
  dig: SmallInt;
begin
  Result := false;
  if (Length(ie) <> 09) then
    exit;
  if not IsNumero(ie) then
    exit;
  for I := 1 to 09 do
    nro[I] := CharToInt(ie[I]);
  b := 9;
  soma := 0;
  for I := 1 to 08 do
  begin
    Inc(soma, nro[I] * b);
    Dec(b);
  end;
  dig := 11 - (soma mod 11);
  if (dig >= 10) then
    dig := 0;
  Result := (dig = nro[09]);
end; // ChkIESE

Function ChkIETO(const ie: string): Boolean; // 99.99.999999-9
var
  b, I, soma: Integer;
  nro: array [1 .. 11] of byte;
  dig: SmallInt;
  S: string;
begin
  Result := false;
  if (Length(ie) <> 11) then
    exit;
  if not IsNumero(ie) then
    exit;
  S := Copy(ie, 3, 2);
  if (S = '01') or (S = '02') or (S = '03') or (S = '99') then
  begin
    for I := 1 to 11 do
      nro[I] := CharToInt(ie[I]);
    b := 9;
    soma := 0;
    for I := 1 to 10 do
    begin
      if (I <> 3) and (I <> 4) then
      begin
        Inc(soma, nro[I] * b);
        Dec(b);
      end;
    end;
    I := (soma mod 11);
    if (I <= 1) then
      dig := 0
    else
      dig := 11 - I;
    Result := (dig = nro[11]);
  end;
end; // ChkIETO

// --------------------------------------------------------------

Function ChkInscEstadual(const ie, uf: string): Boolean;
var
  duf, die: string;
begin
  //
  Result := false;
  If (Trim(ie) = '.') Then
    exit;
  //
  duf := UpperCase(uf);
  die := UpperCase(AllTrim(ie));
  if (Copy(die, 1, 5) = 'ISENT') or (die = '') then
  begin
    Result := true;
    exit;
  end;
  if duf = 'AC' then
    Result := ChkIEAC(die)
  else if duf = 'AL' then
    Result := ChkIEAL(die)
  else if duf = 'AP' then
    Result := ChkIEAP(die)
  else if duf = 'AM' then
    Result := ChkIEAM(die)
  else if duf = 'BA' then
    Result := ChkIEBA(die)
  else if duf = 'CE' then
    Result := ChkIECE(die)
  else if duf = 'DF' then
    Result := ChkIEDF(die)
  else if duf = 'ES' then
    Result := ChkIEES(die)
  else if duf = 'GO' then
    Result := ChkIEGO(die)
  else if duf = 'MA' then
    Result := ChkIEMA(die)
  else if duf = 'MG' then
    Result := CHKIEMG(die)
  else if duf = 'MT' then
    Result := ChkIEMT(die)
  else if duf = 'MS' then
    Result := ChkIEMS(die)
  else if duf = 'PA' then
    Result := ChkIEPA(die)
  else if duf = 'PB' then
    Result := ChkIEPB(die)
  else if duf = 'PR' then
    Result := ChkIEPR(die)
  else if duf = 'PE' then
    Result := ChkIEPE(die)
  else if duf = 'PI' then
    Result := ChkIEPI(die)
  else if duf = 'RJ' then
    Result := ChkIERJ(die)
  else if duf = 'RN' then
    Result := ChkIERN(die)
  else if duf = 'RS' then
    Result := ChkIERS(die)
  else if duf = 'RO' then
    Result := (ChkIERO(die) Or ValidaInscRO(die))
  else if duf = 'RR' then
    Result := ChkIERR(die)
  else if duf = 'SC' then
    Result := ChkIESC(die)
  else if duf = 'SP' then
    Result := ChkIESP(die)
  else if duf = 'SE' then
    Result := ChkIESE(die)
  else if duf = 'TO' then
    Result := ChkIETO(die)
  else
    Result := false;
end; // ChkInscEstadual

Function Mascara_Inscricao(Inscricao, Estado: String): String;
Var
  Mascara: String;
  Contador_1: Integer;
  Contador_2: Integer;
Begin
  IF Estado = 'AC' Then
    Mascara := '**.***.***/***-**';
  IF Estado = 'AL' Then
    Mascara := '*********';
  IF Estado = 'AP' Then
    Mascara := '*********';
  IF Estado = 'AM' Then
    Mascara := '**.***.***-*';
  IF Estado = 'BA' Then
    Mascara := '******-**';
  IF Estado = 'CE' Then
    Mascara := '********-*';
  IF Estado = 'DF' Then
    Mascara := '***********-**';
  IF Estado = 'ES' Then
    Mascara := '*********';
  IF Estado = 'GO' Then
    Mascara := '**.***.***-*';
  IF Estado = 'MA' Then
    Mascara := '*********';
  IF Estado = 'MT' Then
    Mascara := '**********-*';
  IF Estado = 'MS' Then
    Mascara := '*********';
  IF Estado = 'MG' Then
    Mascara := '***.***.***/****';
  IF Estado = 'PA' Then
    Mascara := '**-******-*';
  IF Estado = 'PB' Then
    Mascara := '********-*';
  IF Estado = 'PR' Then
    Mascara := '********-**';
  IF Estado = 'PE' Then
    Mascara := '**.*.***.*******-*';
  IF Estado = 'PI' Then
    Mascara := '*********';
  IF Estado = 'RJ' Then
    Mascara := '**.***.**-*';
  IF Estado = 'RN' Then
    Mascara := '**.***.***-*';
  IF Estado = 'RS' Then
    Mascara := '***/*******';
  IF Estado = 'RO' Then
    Mascara := '***.*****-*';
  IF Estado = 'RR' Then
    Mascara := '********-*';
  IF Estado = 'SC' Then
    Mascara := '***.***.***';
  IF Estado = 'SP' Then
    Mascara := '***.***.***.***';
  IF Estado = 'SE' Then
    Mascara := '*********-*';
  IF Estado = 'TO' Then
    Mascara := '***********';
  Contador_2 := 1;
  Result := '';
  Mascara := Mascara + '****';
  For Contador_1 := 1 To Length(Mascara) Do
  Begin
    IF Copy(Mascara, Contador_1, 1) = '*' Then
      Result := Result + Copy(Inscricao, Contador_2, 1);
    IF Copy(Mascara, Contador_1, 1) <> '*' Then
      Result := Result + Copy(Mascara, Contador_1, 1);
    IF Copy(Mascara, Contador_1, 1) = '*' Then
      Contador_2 := Contador_2 + 1;
  End;
  Result := Trim(Result);
End;

end.
