/// <summary>
///   Unit que tem a classe TValidacoes
/// </summary>
unit uValidacoes;

interface

type
  TValidacaoes = class
  private
  public
    class function ValidarMascaraCPF(const value: string):   Boolean;
    class function ValidarMascaraCNPJ(const value: string):  Boolean;
    class function ValidarMascaraIE(const value: string):    Boolean;

    class function ValidarEmail(const value: string): Boolean;
    class function ValidarCPF(value: string): Boolean;
    class function ValidarCNPJ(value: string): Boolean;
    class function ValidarIE(value: string): Boolean;

    class function VerificaCest(value: string): Boolean;
    class function VerificaNcm(value: string): Boolean;
    class function VerificaCfop(value: string): Boolean;
    class function VerificaIcmsCst(value: Integer): Boolean;
    class function VerificaIcmsOrigem(value: Integer): Boolean;
    class function VerificaCofinsCst(value: Integer): Boolean;
    class function VerificaPisCst(value: Integer): Boolean;
    class function VerificaIssqnNatureza(value: Integer): Boolean;
  end;

implementation

uses
  System.RegularExpressions,
  System.Math,
  uValidaIE,
  System.SysUtils, uListaCESTRepository, uListaNCMRepository;

{ TValidacaoes }

/// <summary>
///   M�todo que valida uma inscri��o estadual
///  /// <param name="value">
     ///  valor a ser verificado
     /// </param>
     ///  /// <returns>
          ///   estiver tudo ok returna true
          /// </returns>
/// </summary>
class function TValidacaoes.ValidarIE(value: string): Boolean;
begin
  value:= StringReplace(value, '.', '', [rfReplaceAll, rfIgnoreCase]);
  value:= StringReplace(value, '-', '', [rfReplaceAll, rfIgnoreCase]);
  value:= StringReplace(value, ',', '', [rfReplaceAll, rfIgnoreCase]);
  value:= StringReplace(value, '/', '', [rfReplaceAll, rfIgnoreCase]);
  value:= StringReplace(value, '\', '', [rfReplaceAll, rfIgnoreCase]);

  if value = '' then
    exit(false);

  if value = '111111111111' then
    exit(true);

  result:= ChkInscEstadual(value, 'SP');
end;

/// <summary>
///   M�todo que valida uma mascara de CNPJ
///  /// <param name="value">
     ///  valor a ser verificado
     /// </param>
     ///  /// <returns>
          ///   estiver tudo ok returna true
          /// </returns>
/// </summary>
class function TValidacaoes.ValidarMascaraCNPJ(const value: string): Boolean;
var
  regex: string;
begin
  if value = '11111111111111' then
    exit(true);

  regex:= '^\d{2}\.?\d{3}\.?\d{3}\/\d{4}\-?\d{2}$';

  result:= TRegEx.IsMatch(value, regex);
end;

/// <summary>
///   M�todo que valida uma mascara de CPF
///  /// <param name="value">
     ///  valor a ser verificado
     /// </param>
     ///  /// <returns>
          ///   estiver tudo ok returna true
          /// </returns>
/// </summary>
class function TValidacaoes.ValidarMascaraCPF(const value: string): Boolean;
var
  regex: string;
begin
  regex:= '^\d{3}\.?\d{3}\.?\d{3}\-?\d{2}$';

  result:= TRegEx.IsMatch(value, regex);
end;

/// <summary>
///   M�todo que valida um CNPJ
///  /// <param name="value">
     ///  valor a ser verificado
     /// </param>
     ///  /// <returns>
          ///   estiver tudo ok returna true
          /// </returns>
/// </summary>
class function TValidacaoes.ValidarCNPJ(value: string): Boolean;
var
  v: array[1..2] of Word;
  cnpj: array[1..14] of Byte;
  I: Byte;
begin
  value:= StringReplace(value, '.', '', [rfReplaceAll, rfIgnoreCase]);
  value:= StringReplace(value, '-', '', [rfReplaceAll, rfIgnoreCase]);
  value:= StringReplace(value, ',', '', [rfReplaceAll, rfIgnoreCase]);
  value:= StringReplace(value, '/', '', [rfReplaceAll, rfIgnoreCase]);
  value:= StringReplace(value, '\', '', [rfReplaceAll, rfIgnoreCase]);

  if value = '11111111111111' then
    exit(true);

  if value = '' then
    exit(false);
  try
    for I := 1 to 14 do
      cnpj[i] := StrToInt(value[i]);
    //Nota: Calcula o primeiro d�gito de verifica��o.
    v[1] := 5*cnpj[1] + 4*cnpj[2]  + 3*cnpj[3]  + 2*cnpj[4];
    v[1] := v[1] + 9*cnpj[5] + 8*cnpj[6]  + 7*cnpj[7]  + 6*cnpj[8];
    v[1] := v[1] + 5*cnpj[9] + 4*cnpj[10] + 3*cnpj[11] + 2*cnpj[12];
    v[1] := 11 - v[1] mod 11;
    v[1] := IfThen(v[1] >= 10, 0, v[1]);
    //Nota: Calcula o segundo d�gito de verifica��o.
    v[2] := 6*cnpj[1] + 5*cnpj[2]  + 4*cnpj[3]  + 3*cnpj[4];
    v[2] := v[2] + 2*cnpj[5] + 9*cnpj[6]  + 8*cnpj[7]  + 7*cnpj[8];
    v[2] := v[2] + 6*cnpj[9] + 5*cnpj[10] + 4*cnpj[11] + 3*cnpj[12];
    v[2] := v[2] + 2*v[1];
    v[2] := 11 - v[2] mod 11;
    v[2] := IfThen(v[2] >= 10, 0, v[2]);
    //Nota: Verdadeiro se os d�gitos de verifica��o s�o os esperados.
    Result := ((v[1] = cnpj[13]) and (v[2] = cnpj[14]));
  except on E: Exception do
    Result := False;
  end;
end;

/// <summary>
///   M�todo que valida um CPF
///  /// <param name="value">
     ///  valor a ser verificado
     /// </param>
     ///  /// <returns>
          ///   estiver tudo ok returna true
          /// </returns>
/// </summary>
class function TValidacaoes.ValidarCPF(value: string): Boolean;
var
  v: array[0..1] of Word;
  cpf: array[0..10] of Byte;
  I: Byte;
begin
  value:= StringReplace(value, '.', '', [rfReplaceAll, rfIgnoreCase]);
  value:= StringReplace(value, '-', '', [rfReplaceAll, rfIgnoreCase]);
  value:= StringReplace(value, ',', '', [rfReplaceAll, rfIgnoreCase]);
  value:= StringReplace(value, '/', '', [rfReplaceAll, rfIgnoreCase]);
  value:= StringReplace(value, '\', '', [rfReplaceAll, rfIgnoreCase]);
  if value = '' then
    exit(false);
  try
    for I := 1 to 11 do
      cpf[i-1] := StrToIntDef(value[i], 0);
    //Nota: Calcula o primeiro d�gito de verifica��o.
    v[0] := 10*cpf[0] + 9*cpf[1] + 8*cpf[2];
    v[0] := v[0] + 7*cpf[3] + 6*cpf[4] + 5*cpf[5];
    v[0] := v[0] + 4*cpf[6] + 3*cpf[7] + 2*cpf[8];
    v[0] := 11 - v[0] mod 11;
    v[0] := IfThen(v[0] >= 10, 0, v[0]);
    //Nota: Calcula o segundo d�gito de verifica��o.
    v[1] := 11*cpf[0] + 10*cpf[1] + 9*cpf[2];
    v[1] := v[1] + 8*cpf[3] +  7*cpf[4] + 6*cpf[5];
    v[1] := v[1] + 5*cpf[6] +  4*cpf[7] + 3*cpf[8];
    v[1] := v[1] + 2*v[0];
    v[1] := 11 - v[1] mod 11;
    v[1] := IfThen(v[1] >= 10, 0, v[1]);
    //Nota: Verdadeiro se os d�gitos de verifica��o s�o os esperados.
    Result :=  ((v[0] = cpf[9]) and (v[1] = cpf[10]));
  except on E: Exception do
    Result := False;
  end;
end;

/// <summary>
///   M�todo que valida um email
///  /// <param name="value">
     ///  valor a ser verificado
     /// </param>
     ///  /// <returns>
          ///   estiver tudo ok returna true
          /// </returns>
/// </summary>
class function TValidacaoes.ValidarEmail(const value: string): Boolean;
var
  regex: string;
begin
  regex:= '[a-zA-Z0-9.!#$%&*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)';

  result:= TRegEx.IsMatch(value, regex);
end;

/// <summary>
///   M�todo que valida uma mascara de IE
///  /// <param name="value">
     ///  valor a ser verificado
     /// </param>
     ///  /// <returns>
          ///   estiver tudo ok returna true
          /// </returns>
/// </summary>
class function TValidacaoes.ValidarMascaraIE(const value: string): Boolean;
begin
  result:= Length(value) = 12;
end;

/// <summary>
///   M�todo que valida um CEST
///  /// <param name="value">
     ///  valor a ser verificado
     /// </param>
     ///  /// <returns>
          ///   estiver tudo ok returna true
          /// </returns>
/// </summary>
class function TValidacaoes.VerificaCest(value: string): Boolean;
var
  repo: TListaCESTRepository;
begin
  repo := TListaCESTRepository.Create;
  try
    if repo.IsValidCEST(Value) then
      result:= true
    else
      result:= false;

  finally
    repo.Free;
  end;

end;

/// <summary>
///   M�todo que valida um CFOP
///  /// <param name="value">
     ///  valor a ser verificado
     /// </param>
     ///  /// <returns>
          ///   estiver tudo ok returna true
          /// </returns>
/// </summary>
class function TValidacaoes.VerificaCfop(value: string): Boolean;
var
  valueTemp: string;
begin

  valueTemp:= StringReplace(Value, '.', '', [rfReplaceAll]);
  valueTemp:= StringReplace(valueTemp, ',', '', [rfReplaceAll]);

  if (valueTemp.Equals('5101')) or
    (valueTemp.Equals('5102')) or
    (valueTemp.Equals('5103')) or
    (valueTemp.Equals('5405')) then
    result:= true
  else
    result:= false;


end;

/// <summary>
///   M�todo que valida um CST do Cofins
///  /// <param name="value">
     ///  valor a ser verificado
     /// </param>
     ///  /// <returns>
          ///   estiver tudo ok returna true
          /// </returns>
/// </summary>
class function TValidacaoes.VerificaCofinsCst(value: Integer): Boolean;
begin
  if Value in [1..9, 49..56, 60..67, 70..75, 98, 99] then
    Result:= true
  else
    Result:= false;
end;

/// <summary>
///   M�todo que valida um CST do Icms
///  /// <param name="value">
     ///  valor a ser verificado
     /// </param>
     ///  /// <returns>
          ///   estiver tudo ok returna true
          /// </returns>
/// </summary>
class function TValidacaoes.VerificaIcmsCst(value: Integer): Boolean;
begin
  if (Value in [0, 10, 20, 30, 40, 41, 50, 51, 60, 70, 80, 90,
    101, 102, 103, 201, 202, 203]) or
    (Value = 300) or
    (Value = 400) or
    (Value = 500) or
    (Value = 900) then
    Result:= true
  else
    Result:= false;

end;

/// <summary>
///   M�todo que valida um origem do Icms
///  /// <param name="value">
     ///  valor a ser verificado
     /// </param>
     ///  /// <returns>
          ///   estiver tudo ok returna true
          /// </returns>
/// </summary>
class function TValidacaoes.VerificaIcmsOrigem(value: Integer): Boolean;
begin
  if value in [0..8] then
    Result:= true
  else
    Result:= false;
end;

/// <summary>
///   M�todo que valida um Natureza do Issqn
///  /// <param name="value">
     ///  valor a ser verificado
     /// </param>
     ///  /// <returns>
          ///   estiver tudo ok returna true
          /// </returns>
/// </summary>
class function TValidacaoes.VerificaIssqnNatureza(value: Integer): Boolean;
begin
  if value in [0..8] then
    Result:= true
  else
    Result:= false;
end;

/// <summary>
///   M�todo que valida um ncm
///  /// <param name="value">
     ///  valor a ser verificado
     /// </param>
     ///  /// <returns>
          ///   estiver tudo ok returna true
          /// </returns>
/// </summary>
class function TValidacaoes.VerificaNcm(value: string): Boolean;
var
  repo: TListaNCMRepository;
begin
  repo := TListaNCMRepository.Create;
  try
    if repo.IsValidNCM(Value) then
      Result:= true
    else
      Result:= false;

  finally
    repo.Free;
  end;

end;

/// <summary>
///   M�todo que valida um CST do PIS
///  /// <param name="value">
     ///  valor a ser verificado
     /// </param>
     ///  /// <returns>
          ///   estiver tudo ok returna true
          /// </returns>
/// </summary>
class function TValidacaoes.VerificaPisCst(value: Integer): Boolean;
begin
  if Value in [1..9, 49..56, 60..67, 70..75, 98, 99] then
    Result:= true
  else
    Result:= false;
end;

end.
