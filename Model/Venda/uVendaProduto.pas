﻿/// <summary>
///   Unit que tem a classe a TVendaProduto
/// </summary>
unit uvendaproduto;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  uproduto,
  uvenda,
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('VendaProduto', '')]
  [PrimaryKey('Id', NotInc, NoSort, False, 'Chave prim�ria')]
  TVendaProduto = class
  private
    { Private declarations } 
    FId: Nullable<Integer>;
    FVendaId: Integer;
    FProdutoId: Nullable<Integer>;
    FQuantidade: Currency;
    FDescricaoProdutoAtual: String;
    FValorProdutoAtual: Currency;
    FPis_CST: Integer;
    FCofins_CST: Integer;
    FCFOP: String;
    FNCM: String;

    FProduto_0:  TProduto  ;
    FVenda_1:  TVenda  ;
  public 
    { Public declarations } 
    constructor Create;
    destructor Destroy; override;
    [Column('Id', ftInteger)]
    [Dictionary('Id', 'Mensagem de valida��o', '', '', '', taCenter)]
    property Id: Nullable<Integer> read FId write FId;

    [Restrictions([NotNull])]
    [Column('VendaId', ftInteger)]
    [ForeignKey('FK_0', 'VendaId', 'Venda', 'Id', SetNull, SetNull)]
    [Dictionary('VendaId', 'Mensagem de valida��o', '', '', '', taCenter)]
    property VendaId: Integer read FVendaId write FVendaId;

    [Column('ProdutoId', ftInteger)]
    [ForeignKey('FK_1', 'ProdutoId', 'Produto', 'Id', SetNull, SetNull)]
    [Dictionary('ProdutoId', 'Mensagem de valida��o', '', '', '', taCenter)]
    property ProdutoId: Nullable<Integer> read FProdutoId write FProdutoId;

    [Restrictions([NotNull])]
    [Column('Quantidade', ftCurrency)]
    [Dictionary('Quantidade', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property Quantidade: Currency read FQuantidade write FQuantidade;

    [Restrictions([NotNull])]
    [Column('DescricaoProdutoAtual', ftString)]
    [Dictionary('DescricaoProdutoAtual', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property DescricaoProdutoAtual: String read FDescricaoProdutoAtual write FDescricaoProdutoAtual;

    [Restrictions([NotNull])]
    [Column('ValorProdutoAtual', ftCurrency)]
    [Dictionary('ValorProdutoAtual', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property ValorProdutoAtual: Currency read FValorProdutoAtual write FValorProdutoAtual;

    [Restrictions([NotNull])]
    [Column('Pis_CST', ftInteger)]
    [Dictionary('Pis_CST', 'Mensagem de valida��o', '', '', '', taCenter)]
    property Pis_CST: Integer read FPis_CST write FPis_CST;

    [Restrictions([NotNull])]
    [Column('Cofins_CST', ftInteger)]
    [Dictionary('Cofins_CST', 'Mensagem de valida��o', '', '', '', taCenter)]
    property Cofins_CST: Integer read FCofins_CST write FCofins_CST;

    [Restrictions([NotNull])]
    [Column('CFOP', ftString)]
    [Dictionary('CFOP', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property CFOP: String read FCFOP write FCFOP;

    [Restrictions([NotNull])]
    [Column('NCM', ftString)]
    [Dictionary('NCM', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property NCM: String read FNCM write FNCM;

    [Association(OneToOne,'ProdutoId','Produto','Id')]
    property Produto: TProduto read FProduto_0 write FProduto_0;

    [Association(OneToOne,'VendaId','Venda','Id')]
    property Venda: TVenda read FVenda_1 write FVenda_1;

  end;

implementation

/// <summary>
///   Cria uma instancia
/// </summary>
constructor TVendaProduto.Create;
begin
  FProduto_0 := TProduto.Create;
  FVenda_1 := TVenda.Create;
end;

/// <summary>
///   Elimina uma instancia
/// </summary>
destructor TVendaProduto.Destroy;
begin
  if Assigned(FProduto_0) then
    FProduto_0.Free;

  if Assigned(FVenda_1) then
    FVenda_1.Free;

  inherited;
end;

initialization

  TRegisterClass.RegisterEntity(TVendaProduto)

end.
