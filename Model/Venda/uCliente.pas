﻿/// <summary>
///   Unit que tem a classe TCliente
/// </summary>
unit ucliente;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('Cliente', '')]
  [PrimaryKey('Id', NotInc, NoSort, False, 'Chave prim�ria')]
  TCliente = class
  private
    { Private declarations } 
    FId: Nullable<Integer>;
    FNome: String;
    FDocumento: String;
  public 
    { Public declarations } 
    [Column('Id', ftInteger)]
    [Dictionary('Id', 'Mensagem de valida��o', '', '', '', taCenter)]
    property Id: Nullable<Integer> read FId write FId;

    [Column('Nome', ftString)]
    [Dictionary('Nome', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Nome: String read FNome write FNome;

    [Column('Documento', ftString)]
    [Dictionary('Documento', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Documento: String read FDocumento write FDocumento;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TCliente)

end.
