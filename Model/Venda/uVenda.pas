﻿/// <summary>
///   Unit que tem a classe TVenda
/// </summary>
unit uvenda;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ucliente,
  ucupom,
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('Venda', '')]
  [PrimaryKey('Id', NotInc, NoSort, False, 'Chave prim�ria')]
  TVenda = class
  private
    { Private declarations } 
    FId: Nullable<Integer>;
    FDataHora: String;
    FValor: Currency;
    FValorTributacao: Nullable<Currency>;
    FAcrescimo: Nullable<Currency>;
    FDesconto: Nullable<Currency>;
    FCupomId: Nullable<Integer>;
    FFormaPagamento: String;
    FOperadoraCartao: String;
    FGerada: Nullable<Integer>;
    FClienteId: Nullable<Integer>;

    FCliente_0:  TCliente  ;
    FCupom_1:  TCupom  ;
  public 
    { Public declarations } 
    constructor Create;
    destructor Destroy; override;
    [Column('Id', ftInteger)]
    [Dictionary('Id', 'Mensagem de valida��o', '', '', '', taCenter)]
    property Id: Nullable<Integer> read FId write FId;

    [Restrictions([NotNull])]
    [Column('DataHora', ftString)]
    [Dictionary('DataHora', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property DataHora: String read FDataHora write FDataHora;

    [Restrictions([NotNull])]
    [Column('Valor', ftCurrency)]
    [Dictionary('Valor', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property Valor: Currency read FValor write FValor;

    [Column('ValorTributacao', ftCurrency)]
    [Dictionary('ValorTributacao', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property ValorTributacao: Nullable<Currency> read FValorTributacao write FValorTributacao;

    [Column('Acrescimo', ftCurrency)]
    [Dictionary('Acrescimo', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property Acrescimo: Nullable<Currency> read FAcrescimo write FAcrescimo;

    [Column('Desconto', ftCurrency)]
    [Dictionary('Desconto', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property Desconto: Nullable<Currency> read FDesconto write FDesconto;

    [Column('CupomId', ftInteger)]
    [ForeignKey('FK_0', 'CupomId', 'Cupom', 'Id', SetNull, SetNull)]
    [Dictionary('CupomId', 'Mensagem de valida��o', '', '', '', taCenter)]
    property CupomId: Nullable<Integer> read FCupomId write FCupomId;

    [Column('FormaPagamento', ftString)]
    [Dictionary('FormaPagamento', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property FormaPagamento: String read FFormaPagamento write FFormaPagamento;

    [Column('OperadoraCartao', ftString)]
    [Dictionary('OperadoraCartao', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property OperadoraCartao: String read FOperadoraCartao write FOperadoraCartao;

    [Column('Gerada', ftInteger)]
    [Dictionary('Gerada', 'Mensagem de valida��o', '', '', '', taCenter)]
    property Gerada: Nullable<Integer> read FGerada write FGerada;

    [Column('ClienteId', ftInteger)]
    [ForeignKey('FK_1', 'ClienteId', 'Cliente', 'Id', SetNull, SetNull)]
    [Dictionary('ClienteId', 'Mensagem de valida��o', '', '', '', taCenter)]
    property ClienteId: Nullable<Integer> read FClienteId write FClienteId;

    [Association(OneToOne,'ClienteId','Cliente','Id')]
    property Cliente: TCliente read FCliente_0 write FCliente_0;

    [Association(OneToOne,'CupomId','Cupom','Id')]
    property Cupom: TCupom read FCupom_1 write FCupom_1;

  end;

implementation

/// <summary>
///   Método que cria uma instancia
/// </summary>
constructor TVenda.Create;
begin
  FCliente_0 := TCliente.Create;
  FCupom_1 := TCupom.Create;
end;

/// <summary>
///   Método que elimina uma instancia
/// </summary>
destructor TVenda.Destroy;
begin
  if Assigned(FCliente_0) then
    FCliente_0.Free;

  if Assigned(FCupom_1) then
    FCupom_1.Free;

  inherited;
end;

initialization

  TRegisterClass.RegisterEntity(TVenda)

end.
