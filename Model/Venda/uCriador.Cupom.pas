/// <summary>
///   Unit que tem a classe TCriadorCupom
/// </summary>
unit uCriador.Cupom;

interface

uses uCliente, uVenda, uVendaProduto, System.Generics.Collections, System.SysUtils;

type
  TCriadorCupom = class
  private

    Fcliente: TCliente;
    Fvenda: TVenda;
    FprodutosVendidos: TObjectList<TVendaProduto>;
    procedure SetTaxas();

  public
    constructor Create;
    destructor Destroy; override;

    property Cliente: TCliente read FCliente;
    property Venda: TVenda read FVenda;
    property ProdutosVendidos: TObjectList<TVendaProduto> read FprodutosVendidos;

    procedure SetarCliente(aCliente: TCliente);
    procedure SetarProdutos(aProdutos: TObjectList<TVendaProduto>);
    procedure SetarPagamento(aVenda: TVenda);

    procedure ConfirmarCupom(iraGerar: Boolean);
  end;
implementation

{ TCriadorCupom }

uses uProdutoRepository, uProduto, uVendaRepository, uCFe.CriadorVenda;

/// <summary>
///   M�todo construtor que cria uma instancia
/// </summary>
constructor TCriadorCupom.Create;
begin
  Fcliente:= nil;
  FprodutosVendidos:= nil;
  Fvenda:= nil;
end;

/// <summary>
///   M�todo destrutor que elimina uma instancia
/// </summary>
destructor TCriadorCupom.Destroy;
begin
  if Assigned(Fcliente) then
    FreeAndNil(Fcliente);

  if Assigned(FprodutosVendidos) then
    FreeAndNil(FprodutosVendidos);

  if Assigned(Fvenda) then
    FreeAndNil(Fvenda);


  inherited;
end;

/// <summary>
///   M�todo que confirma a cria��o de cupom
/// <param name="iraGerar">
///   confirma��o se ira gerar ou n�o o cupom
/// </param>
/// </summary>
procedure TCriadorCupom.ConfirmarCupom(iraGerar: Boolean);
var
  repo: TVendaRepository;
  criadorTxt: TCFeCriador;
begin
  repo:= TVendaRepository.Create();
  try
    SetTaxas();
    repo.AddCliente(Cliente, Venda.Id);
    if iraGerar then
    begin
      venda.Gerada:= 1;
      repo.IraGerar(venda.Id);
      repo.AddProdutosDaVenda(ProdutosVendidos, Venda.Id);
      try
        criadorTxt:= TCFeCriador.Create(self);
      finally
          criadorTxt.Free;
      end;
    end
    else
    begin
      repo.AddProdutosDaVenda(ProdutosVendidos, Venda.Id);
    end;
  finally
    repo.Free;
  end;
end;

/// <summary>
///   M�todo que define um cliente ao cupom
/// <param name="aCliente">
///   Cliente a ser colocado no cupom
/// </param>
/// </summary>
procedure TCriadorCupom.SetarCliente(aCliente: TCliente);
begin
  if Assigned(Fcliente) then
    Fcliente.Free;

  Fcliente:= aCliente;
end;

/// <summary>
///   m�todo para definir as informa��es de venda no cupom
/// <param name="aVenda">
///   Informa��es da venda a ser colocada no cupom
/// </param>
/// </summary>
procedure TCriadorCupom.SetarPagamento(aVenda: TVenda);
var
  prod: TVendaProduto;
  repo: TVendaRepository;
begin
  if Assigned(Fvenda) then
  begin
    repo.Deletar(Venda.Id.GetValueOrDefault);

    Fvenda.Free;
  end;

  Fvenda:= aVenda;
  repo:= TVendaRepository.Create;
  try
    venda.Id:= repo.CriarVenda(venda);

    for prod in produtosVendidos do
    begin
      prod.VendaId:= venda.Id;
      prod.Venda.Id:= venda.Id;
      prod.Venda.Valor:= Venda.Valor;
      prod.Venda.Desconto:= Venda.Desconto;
      prod.Venda.Acrescimo:= Venda.Acrescimo;
    end;

  finally
    repo.Free;
  end;
end;

/// <summary>
///   M�todo para definir os produtos do cupom
/// <param name="aProdutos">
///   Produtos a serem adicionados no cupom
/// </param>
/// </summary>
procedure TCriadorCupom.SetarProdutos(
  aProdutos: TObjectList<TVendaProduto>);
begin
  FprodutosVendidos:= aProdutos;
end;

/// <summary>
///   M�todo para define as taxas fiscais do cupom
/// </summary>
procedure TCriadorCupom.SetTaxas;
var
  repo: TProdutoRepository;
  imposto:  Currency;
  federal:  Currency;
  estadual: Currency;
  prodVenda: TVendaProduto;
  produto: TProduto;
begin
  imposto:= 0;
  repo:= TProdutoRepository.Create();
  try
    for prodVenda in produtosVendidos do
    begin
      produto:= repo.Get(prodVenda.Produto.Id.Value);
      try
        if Assigned(produto) then
        begin
          federal := 0;
          estadual:= 0;
          if produto.NCM <> '' then
          begin
            if produto.TaxaFederal > 0 then
            federal := ((produto.Preco * prodVenda.Quantidade) * produto.TaxaFederal) / 100;

            if produto.TaxaEstadual > 0 then
            estadual:= ((produto.Preco * prodVenda.Quantidade) * produto.TaxaEstadual)/ 100;

            imposto:= federal + estadual;
          end;
        end;
      finally
        if Assigned(produto) then
          FreeAndNil(produto);
      end;

    end;

  finally
    venda.ValorTributacao:= imposto;

    repo.Free;
  end;

end;

end.
