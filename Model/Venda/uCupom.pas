﻿/// <summary>
///   Unit que tem a classe TCupom
/// </summary>
unit ucupom;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('Cupom', '')]
  [PrimaryKey('Id', NotInc, NoSort, False, 'Chave prim�ria')]
  TCupom = class
  private
    { Private declarations } 
    FId: Nullable<Integer>;
    FNome: String;
    FArquivoXml: String;
    FArquivoPdf: String;
    FCancelado: Nullable<Integer>;
    FGerado: Nullable<Integer>;
  public 
    { Public declarations } 
    [Column('Id', ftInteger)]
    [Dictionary('Id', 'Mensagem de valida��o', '', '', '', taCenter)]
    property Id: Nullable<Integer> read FId write FId;

    [Restrictions([NotNull])]
    [Column('Nome', ftString)]
    [Dictionary('Nome', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Nome: String read FNome write FNome;

    [Restrictions([NotNull])]
    [Column('ArquivoXml', ftString)]
    [Dictionary('ArquivoXml', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property ArquivoXml: String read FArquivoXml write FArquivoXml;

    [Restrictions([NotNull])]
    [Column('ArquivoPdf', ftString)]
    [Dictionary('ArquivoPdf', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property ArquivoPdf: String read FArquivoPdf write FArquivoPdf;

    [Column('Cancelado', ftInteger)]
    [Dictionary('Cancelado', 'Mensagem de valida��o', '', '', '', taCenter)]
    property Cancelado: Nullable<Integer> read FCancelado write FCancelado;

    [Column('Gerado', ftInteger)]
    [Dictionary('Gerado', 'Mensagem de valida��o', '', '', '', taCenter)]
    property Gerado: Nullable<Integer> read FGerado write FGerado;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TCupom)

end.
