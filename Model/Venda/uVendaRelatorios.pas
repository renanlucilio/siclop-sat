/// <summary>
///   Unit que tem a classe TVendaRelatorios
/// </summary>
unit uVendaRelatorios;

interface

uses
  System.Generics.Collections;


type
  TVendaRelatorios = class
  private
  public
    function ValorVendido(const dataDe, dataAte: TDate): Currency;
    function QuantidadeVendido(const dataDe, dataAte: TDate): Integer;
    function ImpostosVendido(const dataDe, dataAte: TDate): Currency;
    function UltimasVendasParaCancelar30Minutos(): TDictionary<Integer,string>;
    function GetArquivoXML(const id: Integer): string;
    function QuantidadeCancelados(const dataDe, dataAte: TDate): integer;
    function GetArquivoPDF(const id: integer): string;


  end;

implementation

uses

  uConexao.Query,
  System.SysUtils,
  System.DateUtils, uDadosDbContext, uDmCons, uVenda, uCupom;

{ TVendas }

/// <summary>
///   M�todo que retorna arquivo PDF com base noo id
/// <param name="id">
///   id do cupom
/// </param>
/// <returns>
///   Caminho do arquivo
/// </returns>
/// </summary>
function TVendaRelatorios.GetArquivoPDF(const id: integer): string;
var
  contexto: TDadosDbContext;
  cupom: TCupom;
begin
  contexto := TDadosDbContext.Create(dmCons.conDados);
  try
    cupom:= contexto.ORM.DAO<TCupom>().Find(id);

    if Assigned(cupom) then
      Result:= cupom.ArquivoPdf
    else
      Result:= '';
  finally
    if Assigned(cupom) then
       cupom.Free;

    contexto.Free;
  end;

end;

/// <summary>
///   M�todo que retorna arquivo XML com base no id
/// <param name="id">
///   id do cupom
/// </param>
/// <returns>
///   Caminho do arquivo
/// </returns>
/// </summary>
function TVendaRelatorios.GetArquivoXML(const id: Integer): string;
var
  contexto: TDadosDbContext;
  cupom: TCupom;
begin
  contexto := TDadosDbContext.Create(dmCons.conDados);
  try
    cupom:= contexto.ORM.DAO<TCupom>().Find(id);

    if Assigned(cupom) then
      Result:= cupom.ArquivoXml
    else
      Result:= '';
  finally
    if Assigned(cupom) then
       cupom.Free;

    contexto.Free;
  end;

end;

/// <summary>
///   M�todo que retorna o valor total vendido em um periodo
/// <param name="dataDe">
///   Data de inicio para verifica��o dos dados
/// </param>
/// <param name="dataAte">
///   Data de fim para verifica��o dos dados
/// </param>
/// <returns>
///   Retorna o valor total de imposto
/// </returns>
/// </summary>
function TVendaRelatorios.ImpostosVendido(const dataDe, dataAte: TDate): Currency;
var
  contexto: TDadosDbContext;
  vendas: TObjectList<TVenda>;
  totalValorImposto: double;
  it: TVenda;
  dataVenda: TDateTime;
begin
  contexto:= TDadosDbContext.Create(dmCons.conDados);
  try
    totalValorImposto:= 0;
    vendas:= contexto.ORM.DAO<TVenda>().Find();

    for it in vendas do
    begin
      dataVenda:= ISO8601ToDate(it.DataHora);
      if DateInRange(DateOf(dataVenda), dataDe, dataAte, true) then
        if it.Cupom.Cancelado = 0 then
          totalValorImposto:= totalValorImposto + it.ValorTributacao.Value;
    end;

    Result:= totalValorImposto;
  finally
    if Assigned(vendas) then
      vendas.Free;
    contexto.Free;
  end;
end;

/// <summary>
///   M�todo que retorna a quantidade de vendas canceladas de um periodo
/// <param name="dataDe">
///   Data de inicio para verifica��o dos dados
/// </param>
/// <param name="dataAte">
///   Data de fim para verifica��o dos dados
/// </param>
/// <returns>
///   Retorna a quantidade de vendas canceladas
/// </returns>
/// </summary>
function TVendaRelatorios.QuantidadeCancelados(const dataDe,
  dataAte: TDate): integer;
var
  contexto: TDadosDbContext;
  vendas: TObjectList<TVenda>;
  qtdTotalCancelado: integer;
  it: TVenda;
  dataVenda: TDateTime;
begin
  contexto:= TDadosDbContext.Create(dmCons.conDados);
  try
    qtdTotalCancelado:= 0;
    vendas:= contexto.ORM.DAO<TVenda>().Find();

    for it in vendas do
    begin
      dataVenda:= ISO8601ToDate(it.DataHora);
      if DateInRange(DateOf(dataVenda), dataDe, dataAte, true) then
        if it.Cupom.Cancelado = 1 then
          qtdTotalCancelado:= qtdTotalCancelado + 1;
    end;

    Result:= qtdTotalCancelado;
  finally
    if Assigned(vendas) then
      vendas.Free;
    contexto.Free;
  end;
end;

/// <summary>
///   M�todo que retorna a quantidade de vendas de um periodo
/// <param name="dataDe">
///   Data de inicio para verifica��o dos dados
/// </param>
/// <param name="dataAte">
///   Data de fim para verifica��o dos dados
/// </param>
/// <returns>
///   Retorna a quantidade de vendas
/// </returns>
/// </summary>
function TVendaRelatorios.QuantidadeVendido(const dataDe, dataAte: TDate): Integer;
var
  contexto: TDadosDbContext;
  vendas: TObjectList<TVenda>;
  qtdVendida: integer;
  it: TVenda;
  dataVenda: TDateTime;
begin
  contexto:= TDadosDbContext.Create(dmCons.conDados);
  try
    qtdVendida:= 0;
    vendas:= contexto.ORM.DAO<TVenda>().Find();

    for it in vendas do
    begin
      dataVenda:= ISO8601ToDate(it.DataHora);
      if DateInRange(DateOf(dataVenda), dataDe, dataAte, true) then
        if it.Cupom.Cancelado = 0 then
          qtdVendida:= qtdVendida + 1;
    end;

    Result:= qtdVendida;
  finally
  if Assigned(vendas) then
      vendas.Free;
    contexto.Free;
  end;
end;

//m�todo que retorna as ultimas vendas no intevalo de 310 minutos
function TVendaRelatorios.UltimasVendasParaCancelar30Minutos: TDictionary<Integer,string>;
var
  contexto: TDadosDbContext;
  vendas: TObjectList<TVenda>;
  it: TVenda;
  horaAtual: TTime;
  horaVenda: TDateTime;
begin
  Result:= TDictionary<integer,string>.Create();

  contexto := TDadosDbContext.Create(dmCons.conDados);
  try
    vendas:= contexto.ORM.DAO<TVenda>().Find();

    if Assigned(vendas) then
    begin
      for it in vendas do
      begin
        if DateOf(Now()) = DateOf(ISO8601ToDate(it.DataHora)) then
        begin
          horaAtual:= TimeOf(Now());
          horaVenda:= ISO8601ToDate(it.DataHora);
          if TimeInRange(horaAtual , TimeOf(horaVenda), TimeOf(IncMinute(horaVenda, 30))) then
            if it.Cupom.Cancelado = 0 then
              Result.Add(it.CupomId, 'Emitido:' + TimeToStr(horaVenda) +' => CFe: '+Copy(it.Cupom.Nome, 0, 5)+'...');
        end;
      end;
    end;

  finally
    if Assigned(vendas) then
      vendas.Free;

    contexto.Free;
  end;

end;

//m�todo que retorna o valor vendido em um intervalo
function TVendaRelatorios.ValorVendido(const dataDe, dataAte: TDate): Currency;
var
  contexto: TDadosDbContext;
  vendas: TObjectList<TVenda>;
  totalValorVendido: double;
  it: TVenda;
  dataVenda: TDateTime;
begin
  contexto:= TDadosDbContext.Create(dmCons.conDados);
  try
    totalValorVendido:=0;
    vendas:= contexto.ORM.DAO<TVenda>().Find();

    for it in vendas do
    begin
      dataVenda:= ISO8601ToDate(it.DataHora);
      if DateInRange(DateOf(dataVenda), dataDe, dataAte, true) then
        if it.Cupom.Cancelado = 0 then
          totalValorVendido:= totalValorVendido + ((it.Valor + it.Acrescimo.Value) - it.Desconto.Value);
    end;

    Result:= totalValorVendido;
  finally
    if Assigned(vendas) then
      vendas.Free;
    contexto.Free;
  end;
end;


end.
