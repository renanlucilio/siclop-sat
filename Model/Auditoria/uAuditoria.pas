/// <summary>
///   M�todo que tem a classe para a classe de auditoria
/// </summary>
unit uauditoria;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ormbr.types.blob, 
  ormbr.types.lazy,
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('Auditoria', '')]
  [PrimaryKey('Id', NotInc, NoSort, False, 'Chave prim�ria')]
  TAuditoria = class
  private
    { Private declarations } 
    FId: Nullable<Integer>;
    FResumo: string;
    FDescricao: String;
    FCategoria: String;
    FDataHora: string;
  public 
    { Public declarations } 
    [Column('Id', ftInteger)]
    [Dictionary('Id', 'Mensagem de valida��o', '', '', '', taCenter)]
    property Id: Nullable<Integer> read FId write FId;

    [Restrictions([NotNull])]
    [Column('Resumo', ftString)]
    [Dictionary('Resumo', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Resumo: string read FResumo write FResumo;

    [Restrictions([NotNull])]
    [Column('Descricao', ftString, 32767)]
    [Dictionary('Descricao', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Descricao: String read FDescricao write FDescricao;

    [Restrictions([NotNull])]
    [Column('Categoria', ftString, 32767)]
    [Dictionary('Categoria', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Categoria: String read FCategoria write FCategoria;

    [Restrictions([NotNull])]
    [Column('DataHora', ftString)]
    [Dictionary('DataHora', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property DataHora: string read FDataHora write FDataHora;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TAuditoria)

end.
