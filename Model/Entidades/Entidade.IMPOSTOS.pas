unit Entidade.impostos;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections,
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('IMPOSTOS', '')]
  [PrimaryKey('IMP_ID', NotInc, NoSort, False, 'Chave prim�ria')]
  TIMPOSTOS = class
  private
    { Private declarations } 
    FIMP_ID: Nullable<Integer>;
    FIMP_VALOR: Currency;
  public 
    { Public declarations } 
    [Column('IMP_ID', ftInteger)]
    [Dictionary('IMP_ID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property IMP_ID: Nullable<Integer> read FIMP_ID write FIMP_ID;

    [Restrictions([NotNull])]
    [Column('IMP_VALOR', ftCurrency)]
    [Dictionary('IMP_VALOR', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property IMP_VALOR: Currency read FIMP_VALOR write FIMP_VALOR;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TIMPOSTOS)

end.
