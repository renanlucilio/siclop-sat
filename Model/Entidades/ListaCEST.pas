/// <summary>
///   Unit que tem a classe TListaCEST
/// </summary>
unit listacest;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('ListaCEST', '')]
  [PrimaryKey('ID', NotInc, NoSort, False, 'Chave prim�ria')]
  TListaCEST = class
  private
    { Private declarations } 
    FID: Nullable<Integer>;
    FCEST: String;
  public 
    { Public declarations } 
    [Column('ID', ftInteger)]
    [Dictionary('ID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property ID: Nullable<Integer> read FID write FID;

    [Restrictions([NotNull])]
    [Column('CEST', ftString, 25)]
    [Dictionary('CEST', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property CEST: String read FCEST write FCEST;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TListaCEST)

end.
