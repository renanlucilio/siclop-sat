unit Entidade.venda;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections,
  Entidade.xml,
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('VENDA', '')]
  [PrimaryKey('VND_ID', NotInc, NoSort, False, 'Chave prim�ria')]
  TVENDA = class
  private
    { Private declarations } 
    FVND_ID: Nullable<Integer>;
    FVND_HORA: String;
    FVND_DATA: String;
    FVND_PDF: String;
    FVND_VALOR: Currency;
    FVND_QUANTIDADE: Integer;
    FXML_ID: Nullable<Integer>;

    FXML_0:  TXML  ;
  public 
    { Public declarations } 
    constructor Create;
    destructor Destroy; override;
    [Column('VND_ID', ftInteger)]
    [Dictionary('VND_ID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property VND_ID: Nullable<Integer> read FVND_ID write FVND_ID;

    [Restrictions([NotNull])]
    [Column('VND_HORA', ftString, 100)]
    [Dictionary('VND_HORA', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property VND_HORA: String read FVND_HORA write FVND_HORA;

    [Restrictions([NotNull])]
    [Column('VND_DATA', ftString, 100)]
    [Dictionary('VND_DATA', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property VND_DATA: String read FVND_DATA write FVND_DATA;

    [Restrictions([NotNull])]
    [Column('VND_PDF', ftString, 100)]
    [Dictionary('VND_PDF', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property VND_PDF: String read FVND_PDF write FVND_PDF;

    [Restrictions([NotNull])]
    [Column('VND_VALOR', ftCurrency)]
    [Dictionary('VND_VALOR', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property VND_VALOR: Currency read FVND_VALOR write FVND_VALOR;

    [Restrictions([NotNull])]
    [Column('VND_QUANTIDADE', ftInteger)]
    [Dictionary('VND_QUANTIDADE', 'Mensagem de valida��o', '', '', '', taCenter)]
    property VND_QUANTIDADE: Integer read FVND_QUANTIDADE write FVND_QUANTIDADE;

    [Column('XML_ID', ftInteger)]
    [ForeignKey('FK_0', 'XML_ID', 'XML', 'XML_ID', SetNull, SetNull)]
    [Dictionary('XML_ID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property XML_ID: Nullable<Integer> read FXML_ID write FXML_ID;

    [Association(OneToOne,'XML_ID','XML','XML_ID')]
    property XML: TXML read FXML_0 write FXML_0;

  end;

implementation

constructor TVENDA.Create;
begin
  FXML_0 := TXML.Create;
end;

destructor TVENDA.Destroy;
begin
  if Assigned(FXML_0) then
    FXML_0.Free;

  inherited;
end;

initialization

  TRegisterClass.RegisterEntity(TVENDA)

end.
