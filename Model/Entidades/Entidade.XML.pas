unit Entidade.xml;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections,
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('XML', '')]
  [PrimaryKey('XML_ID', NotInc, NoSort, False, 'Chave prim�ria')]
  TXML = class
  private
    { Private declarations } 
    FXML_ID: Nullable<Integer>;
    FXML_NOME: String;
    FXML_CANCELADO: String;
    FXML_CAMINHO: String;
  public 
    { Public declarations } 
    [Column('XML_ID', ftInteger)]
    [Dictionary('XML_ID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property XML_ID: Nullable<Integer> read FXML_ID write FXML_ID;

    [Restrictions([NotNull])]
    [Column('XML_NOME', ftString, 255)]
    [Dictionary('XML_NOME', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property XML_NOME: String read FXML_NOME write FXML_NOME;

    [Restrictions([NotNull])]
    [Column('XML_CANCELADO', ftString, 32767)]
    [Dictionary('XML_CANCELADO', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property XML_CANCELADO: String read FXML_CANCELADO write FXML_CANCELADO;

    [Restrictions([NotNull])]
    [Column('XML_CAMINHO', ftString, 500)]
    [Dictionary('XML_CAMINHO', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property XML_CAMINHO: String read FXML_CAMINHO write FXML_CAMINHO;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TXML)

end.
