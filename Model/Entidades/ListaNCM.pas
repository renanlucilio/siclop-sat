/// <summary>
///   Unit que tem a classe TListaNCM
/// </summary>
unit listancm;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('ListaNCM', '')]
  [PrimaryKey('ID', NotInc, NoSort, False, 'Chave prim�ria')]
  TListaNCM = class
  private
    { Private declarations } 
    FID: Nullable<Integer>;
    FNCM: String;
  public 
    { Public declarations } 
    [Column('ID', ftInteger)]
    [Dictionary('ID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property ID: Nullable<Integer> read FID write FID;

    [Restrictions([NotNull])]
    [Column('NCM', ftString, 25)]
    [Dictionary('NCM', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property NCM: String read FNCM write FNCM;
  end;

implementation

initialization

  TRegisterClass.RegisterEntity(TListaNCM)

end.
