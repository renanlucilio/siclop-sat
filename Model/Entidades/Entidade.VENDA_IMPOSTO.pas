unit Entidade.venda_imposto;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections,
  Entidade.impostos,
  Entidade.venda,
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('VENDA_IMPOSTO', '')]
  [PrimaryKey('VDIP_ID', NotInc, NoSort, False, 'Chave prim�ria')]
  TVENDA_IMPOSTO = class
  private
    { Private declarations } 
    FVDIP_ID: Nullable<Integer>;
    FVND_ID: Integer;
    FIMP_ID: Integer;

    FIMPOSTOS_0:  TIMPOSTOS  ;
    FVENDA_1:  TVENDA  ;
  public 
    { Public declarations } 
    constructor Create;
    destructor Destroy; override;
    [Column('VDIP_ID', ftInteger)]
    [Dictionary('VDIP_ID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property VDIP_ID: Nullable<Integer> read FVDIP_ID write FVDIP_ID;

    [Restrictions([NotNull])]
    [Column('VND_ID', ftInteger)]
    [ForeignKey('FK_1', 'VND_ID', 'VENDA', 'VND_ID', SetNull, SetNull)]
    [Dictionary('VND_ID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property VND_ID: Integer read FVND_ID write FVND_ID;

    [Restrictions([NotNull])]
    [Column('IMP_ID', ftInteger)]
    [ForeignKey('FK_0', 'IMP_ID', 'IMPOSTOS', 'IMP_ID', SetNull, SetNull)]
    [Dictionary('IMP_ID', 'Mensagem de valida��o', '', '', '', taCenter)]
    property IMP_ID: Integer read FIMP_ID write FIMP_ID;

    [Association(OneToOne,'IMP_ID','IMPOSTOS','IMP_ID')]
    property IMPOSTOS: TIMPOSTOS read FIMPOSTOS_0 write FIMPOSTOS_0;

    [Association(OneToOne,'VND_ID','VENDA','VND_ID')]
    property VENDA: TVENDA read FVENDA_1 write FVENDA_1;

  end;

implementation

constructor TVENDA_IMPOSTO.Create;
begin
  FIMPOSTOS_0 := TIMPOSTOS.Create;
  FVENDA_1 := TVENDA.Create;
end;

destructor TVENDA_IMPOSTO.Destroy;
begin
  if Assigned(FIMPOSTOS_0) then
    FIMPOSTOS_0.Free;

  if Assigned(FVENDA_1) then
    FVENDA_1.Free;

  inherited;
end;

initialization

  TRegisterClass.RegisterEntity(TVENDA_IMPOSTO)

end.
