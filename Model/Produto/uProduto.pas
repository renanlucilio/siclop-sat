﻿/// <summary>
///   Unit que tem a classe TProduto
/// </summary>
unit uproduto;

interface

uses
  DB, 
  Classes, 
  SysUtils, 
  Generics.Collections, 

  /// orm 
  ucategoria,
  ormbr.types.blob, 
  ormbr.types.lazy, 
  ormbr.types.mapping, 
  ormbr.types.nullable, 
  ormbr.mapping.classes, 
  ormbr.mapping.register, 
  ormbr.mapping.attributes; 

type
  [Entity]
  [Table('Produto', '')]
  [PrimaryKey('Id', NotInc, NoSort, False, 'Chave prim�ria')]
  TProduto = class
  private
    { Private declarations } 
    FId: Nullable<Integer>;
    FDescricao: String;
    FPreco: Currency;
    FUnidadeMedida: String;
    FCategoriaId: Integer;
    FNcm: String;
    FCest: String;
    FCfop: String;
    FCofins_CST: Integer;
    FCofins_BaseCalculo: Currency;
    FCofins_Porcento: Currency;
    FCofins_Valor: Currency;
    FCofins_BaseCalculoPorProduto: Currency;
    FCofins_Aliquota: Currency;
    FCofinsSt_BaseCalculo: Currency;
    FCofinsSt_Porcento: Currency;
    FCofinsSt_Valor: Currency;
    FCofinsSt_BaseCalculoPorProduto: Currency;
    FCofinsSt_Aliquota: Currency;
    FPis_CST: Integer;
    FPis_BaseCalculo: Currency;
    FPis_Porcento: Currency;
    FPis_Valor: Currency;
    FPis_BaseCalculoPorProduto: Currency;
    FPis_Aliquota: Currency;
    FPisSt_BaseCalculo: Currency;
    FPisSt_Porcento: Currency;
    FPisSt_Valor: Currency;
    FPisSt_BaseCalculoPorProduto: Currency;
    FPisSt_Aliquota: Currency;
    FIcms_Origem: Integer;
    FIcms_Cst: Integer;
    FIcms_Porcento: Currency;
    FIcms_Valor: Currency;
    FIssqn_BaseCalculo: Currency;
    FIssqn_Deducao: Currency;
    FIssqn_CodigoIbge: String;
    FIssqn_BaseCalculoProduto: Currency;
    FIssqn_ItemDaListaServico: String;
    FIssqn_CodigoTributarioIssqn: String;
    FIssqn_Natureza: Integer;
    FTaxaEstadual: Currency;
    FTaxaFederal: Currency;

    FCategoria_0:  TCategoria  ;
  public 
    { Public declarations } 
    constructor Create;
    destructor Destroy; override;
    [Column('Id', ftInteger)]
    [Dictionary('Id', 'Mensagem de valida��o', '', '', '', taCenter)]
    property Id: Nullable<Integer> read FId write FId;

    [Restrictions([NotNull])]
    [Column('Descricao', ftString)]
    [Dictionary('Descricao', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Descricao: String read FDescricao write FDescricao;

    [Restrictions([NotNull])]
    [Column('Preco', ftCurrency)]
    [Dictionary('Preco', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property Preco: Currency read FPreco write FPreco;

    [Restrictions([NotNull])]
    [Column('UnidadeMedida', ftString)]
    [Dictionary('UnidadeMedida', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property UnidadeMedida: String read FUnidadeMedida write FUnidadeMedida;

    [Restrictions([NotNull])]
    [Column('CategoriaId', ftInteger)]
    [ForeignKey('FK_0', 'CategoriaId', 'Categoria', 'Id', SetNull, SetNull)]
    [Dictionary('CategoriaId', 'Mensagem de valida��o', '', '', '', taCenter)]
    property CategoriaId: Integer read FCategoriaId write FCategoriaId;

    [Restrictions([NotNull])]
    [Column('Ncm', ftString)]
    [Dictionary('Ncm', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Ncm: String read FNcm write FNcm;

    [Restrictions([NotNull])]
    [Column('Cest', ftString)]
    [Dictionary('Cest', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Cest: String read FCest write FCest;

    [Restrictions([NotNull])]
    [Column('Cfop', ftString)]
    [Dictionary('Cfop', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Cfop: String read FCfop write FCfop;

    [Restrictions([NotNull])]
    [Column('Cofins_CST', ftInteger)]
    [Dictionary('Cofins_CST', 'Mensagem de valida��o', '', '', '', taCenter)]
    property Cofins_CST: Integer read FCofins_CST write FCofins_CST;

    [Restrictions([NotNull])]
    [Column('Cofins_BaseCalculo', ftCurrency)]
    [Dictionary('Cofins_BaseCalculo', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property Cofins_BaseCalculo: Currency read FCofins_BaseCalculo write FCofins_BaseCalculo;

    [Restrictions([NotNull])]
    [Column('Cofins_Porcento', ftCurrency)]
    [Dictionary('Cofins_Porcento', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property Cofins_Porcento: Currency read FCofins_Porcento write FCofins_Porcento;

    [Restrictions([NotNull])]
    [Column('Cofins_Valor', ftCurrency)]
    [Dictionary('Cofins_Valor', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property Cofins_Valor: Currency read FCofins_Valor write FCofins_Valor;

    [Restrictions([NotNull])]
    [Column('Cofins_BaseCalculoPorProduto', ftCurrency)]
    [Dictionary('Cofins_BaseCalculoPorProduto', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property Cofins_BaseCalculoPorProduto: Currency read FCofins_BaseCalculoPorProduto write FCofins_BaseCalculoPorProduto;

    [Restrictions([NotNull])]
    [Column('Cofins_Aliquota', ftCurrency)]
    [Dictionary('Cofins_Aliquota', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property Cofins_Aliquota: Currency read FCofins_Aliquota write FCofins_Aliquota;

    [Restrictions([NotNull])]
    [Column('CofinsSt_BaseCalculo', ftCurrency)]
    [Dictionary('CofinsSt_BaseCalculo', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property CofinsSt_BaseCalculo: Currency read FCofinsSt_BaseCalculo write FCofinsSt_BaseCalculo;

    [Restrictions([NotNull])]
    [Column('CofinsSt_Porcento', ftCurrency)]
    [Dictionary('CofinsSt_Porcento', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property CofinsSt_Porcento: Currency read FCofinsSt_Porcento write FCofinsSt_Porcento;

    [Restrictions([NotNull])]
    [Column('CofinsSt_Valor', ftCurrency)]
    [Dictionary('CofinsSt_Valor', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property CofinsSt_Valor: Currency read FCofinsSt_Valor write FCofinsSt_Valor;

    [Restrictions([NotNull])]
    [Column('CofinsSt_BaseCalculoPorProduto', ftCurrency)]
    [Dictionary('CofinsSt_BaseCalculoPorProduto', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property CofinsSt_BaseCalculoPorProduto: Currency read FCofinsSt_BaseCalculoPorProduto write FCofinsSt_BaseCalculoPorProduto;

    [Restrictions([NotNull])]
    [Column('CofinsSt_Aliquota', ftCurrency)]
    [Dictionary('CofinsSt_Aliquota', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property CofinsSt_Aliquota: Currency read FCofinsSt_Aliquota write FCofinsSt_Aliquota;

    [Restrictions([NotNull])]
    [Column('Pis_CST', ftInteger)]
    [Dictionary('Pis_CST', 'Mensagem de valida��o', '', '', '', taCenter)]
    property Pis_CST: Integer read FPis_CST write FPis_CST;

    [Restrictions([NotNull])]
    [Column('Pis_BaseCalculo', ftCurrency)]
    [Dictionary('Pis_BaseCalculo', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property Pis_BaseCalculo: Currency read FPis_BaseCalculo write FPis_BaseCalculo;

    [Restrictions([NotNull])]
    [Column('Pis_Porcento', ftCurrency)]
    [Dictionary('Pis_Porcento', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property Pis_Porcento: Currency read FPis_Porcento write FPis_Porcento;

    [Restrictions([NotNull])]
    [Column('Pis_Valor', ftCurrency)]
    [Dictionary('Pis_Valor', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property Pis_Valor: Currency read FPis_Valor write FPis_Valor;

    [Restrictions([NotNull])]
    [Column('Pis_BaseCalculoPorProduto', ftCurrency)]
    [Dictionary('Pis_BaseCalculoPorProduto', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property Pis_BaseCalculoPorProduto: Currency read FPis_BaseCalculoPorProduto write FPis_BaseCalculoPorProduto;

    [Restrictions([NotNull])]
    [Column('Pis_Aliquota', ftCurrency)]
    [Dictionary('Pis_Aliquota', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property Pis_Aliquota: Currency read FPis_Aliquota write FPis_Aliquota;

    [Restrictions([NotNull])]
    [Column('PisSt_BaseCalculo', ftCurrency)]
    [Dictionary('PisSt_BaseCalculo', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property PisSt_BaseCalculo: Currency read FPisSt_BaseCalculo write FPisSt_BaseCalculo;

    [Restrictions([NotNull])]
    [Column('PisSt_Porcento', ftCurrency)]
    [Dictionary('PisSt_Porcento', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property PisSt_Porcento: Currency read FPisSt_Porcento write FPisSt_Porcento;

    [Restrictions([NotNull])]
    [Column('PisSt_Valor', ftCurrency)]
    [Dictionary('PisSt_Valor', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property PisSt_Valor: Currency read FPisSt_Valor write FPisSt_Valor;

    [Restrictions([NotNull])]
    [Column('PisSt_BaseCalculoPorProduto', ftCurrency)]
    [Dictionary('PisSt_BaseCalculoPorProduto', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property PisSt_BaseCalculoPorProduto: Currency read FPisSt_BaseCalculoPorProduto write FPisSt_BaseCalculoPorProduto;

    [Restrictions([NotNull])]
    [Column('PisSt_Aliquota', ftCurrency)]
    [Dictionary('PisSt_Aliquota', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property PisSt_Aliquota: Currency read FPisSt_Aliquota write FPisSt_Aliquota;

    [Restrictions([NotNull])]
    [Column('Icms_Origem', ftInteger)]
    [Dictionary('Icms_Origem', 'Mensagem de valida��o', '', '', '', taCenter)]
    property Icms_Origem: Integer read FIcms_Origem write FIcms_Origem;

    [Restrictions([NotNull])]
    [Column('Icms_Cst', ftInteger)]
    [Dictionary('Icms_Cst', 'Mensagem de valida��o', '', '', '', taCenter)]
    property Icms_Cst: Integer read FIcms_Cst write FIcms_Cst;

    [Restrictions([NotNull])]
    [Column('Icms_Porcento', ftCurrency)]
    [Dictionary('Icms_Porcento', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property Icms_Porcento: Currency read FIcms_Porcento write FIcms_Porcento;

    [Restrictions([NotNull])]
    [Column('Icms_Valor', ftCurrency)]
    [Dictionary('Icms_Valor', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property Icms_Valor: Currency read FIcms_Valor write FIcms_Valor;

    [Restrictions([NotNull])]
    [Column('Issqn_BaseCalculo', ftCurrency)]
    [Dictionary('Issqn_BaseCalculo', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property Issqn_BaseCalculo: Currency read FIssqn_BaseCalculo write FIssqn_BaseCalculo;

    [Restrictions([NotNull])]
    [Column('Issqn_Deducao', ftCurrency)]
    [Dictionary('Issqn_Deducao', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property Issqn_Deducao: Currency read FIssqn_Deducao write FIssqn_Deducao;

    [Restrictions([NotNull])]
    [Column('Issqn_CodigoIbge', ftString)]
    [Dictionary('Issqn_CodigoIbge', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Issqn_CodigoIbge: String read FIssqn_CodigoIbge write FIssqn_CodigoIbge;

    [Restrictions([NotNull])]
    [Column('Issqn_BaseCalculoProduto', ftCurrency)]
    [Dictionary('Issqn_BaseCalculoProduto', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property Issqn_BaseCalculoProduto: Currency read FIssqn_BaseCalculoProduto write FIssqn_BaseCalculoProduto;

    [Restrictions([NotNull])]
    [Column('Issqn_ItemDaListaServico', ftString)]
    [Dictionary('Issqn_ItemDaListaServico', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Issqn_ItemDaListaServico: String read FIssqn_ItemDaListaServico write FIssqn_ItemDaListaServico;

    [Restrictions([NotNull])]
    [Column('Issqn_CodigoTributarioIssqn', ftString)]
    [Dictionary('Issqn_CodigoTributarioIssqn', 'Mensagem de valida��o', '', '', '', taLeftJustify)]
    property Issqn_CodigoTributarioIssqn: String read FIssqn_CodigoTributarioIssqn write FIssqn_CodigoTributarioIssqn;

    [Restrictions([NotNull])]
    [Column('Issqn_Natureza', ftInteger)]
    [Dictionary('Issqn_Natureza', 'Mensagem de valida��o', '', '', '', taCenter)]
    property Issqn_Natureza: Integer read FIssqn_Natureza write FIssqn_Natureza;

    [Restrictions([NotNull])]
    [Column('TaxaEstadual', ftCurrency)]
    [Dictionary('TaxaEstadual', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property TaxaEstadual: Currency read FTaxaEstadual write FTaxaEstadual;

    [Restrictions([NotNull])]
    [Column('TaxaFederal', ftCurrency)]
    [Dictionary('TaxaFederal', 'Mensagem de valida��o', '0', '', '', taRightJustify)]
    property TaxaFederal: Currency read FTaxaFederal write FTaxaFederal;

    [Association(OneToOne,'CategoriaId','Categoria','Id')]
    property Categoria: TCategoria read FCategoria_0 write FCategoria_0;

  end;

implementation

constructor TProduto.Create;
begin
  FCategoria_0 := TCategoria.Create;
end;

destructor TProduto.Destroy;
begin
  if Assigned(FCategoria_0) then
    FCategoria_0.Free;

  inherited;
end;

initialization

  TRegisterClass.RegisterEntity(TProduto)

end.
