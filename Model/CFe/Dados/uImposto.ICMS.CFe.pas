 /// <summary>
 ///   Unit que tem a classe ICMS
 /// </summary>
unit uImposto.ICMS.CFe;

interface

uses
  pcnCFe,
  pcnConversao;
type
  TImposto_ICMS = class
  private
    FCST_CSOSN: Integer;
    FOrigem:    integer;
    FPorcento:  Double;
    FValor:     Currency;
    procedure SetCSTCSOSN(ICMS: TICMS);
    procedure SetOrigemIcms(ICMS: TICMS);


  public
    property Origem:       integer  read FOrigem write FOrigem;
    property CST_CSOSN:    Integer  read FCST_CSOSN write FCST_CSOSN;
    property Porcento:     Double read FPorcento  write FPorcento;
    property Valor:        Currency read FValor     write FValor;
    procedure SetICMS(ICMS: TICMS);
  end;

implementation

uses
  System.SysUtils;

{ TImposto_ICMS }

/// <summary>
///   M�todo para definir qual � o CST do Icms com base numero
/// <param name="ICMS">
///   instacia da classe TICMS
/// </param>
/// </summary>
procedure TImposto_ICMS.setCSTCSOSN(ICMS: TICMS);
begin
  case CST_CSOSN of
    0:
      ICMS.CST := cst00;
    10:
      ICMS.CST := cst10;
    20:
      ICMS.CST := cst20;
    30:
      ICMS.CST := cst30;
    40:
      ICMS.CST := cst40;
    41:
      ICMS.CST := cst41;
    45:
      ICMS.CST := cst45;
    50:
      ICMS.CST := cst50;
    51:
      ICMS.CST := cst51;
    60:
      ICMS.CST := cst60;
    70:
      ICMS.CST := cst70;
    80:
      ICMS.CST := cst80;
    90:
      ICMS.CST := cst90;
    101:
      ICMS.CSOSN := csosn101;
    102:
      ICMS.CSOSN := csosn102;
    103:
      ICMS.CSOSN := csosn103;
    201:
      ICMS.CSOSN := csosn201;
    202:
      ICMS.CSOSN := csosn202;
    203:
      ICMS.CSOSN := csosn203;
    300:
      ICMS.CSOSN := csosn300;
    400:
      ICMS.CSOSN := csosn400;
    500:
      ICMS.CSOSN := csosn500;
    900:
      ICMS.CSOSN := csosn900
    else
    begin
      ICMS.CST:= cstVazio;
      ICMS.CSOSN:= csosnVazio;
    end;
  end;
end;

/// <summary>
///   M�todo que tem a defini��o o ICMS no cupom
/// <param name="ICMS">
///   instanciaa da TICMS
/// </param>
/// </summary>
procedure TImposto_ICMS.SetICMS(ICMS: TICMS);
begin
  ICMS.pICMS:= Porcento;
  ICMS.vICMS:= Valor;
  setCSTCSOSN(ICMS);
  SetOrigemIcms(ICMS);
end;

/// <summary>
///   M�todo que defini a origem do ICMS com base no numero
/// <param name="ICMS">
///   Instacia TICMS
/// </param>
/// </summary>
procedure TImposto_ICMS.SetOrigemIcms(ICMS: TICMS);
begin
  case Origem of
    0: ICMS.orig:= oeNacional;
    1: ICMS.orig:= oeEstrangeiraImportacaoDireta;
    2: ICMS.orig:= oeEstrangeiraAdquiridaBrasil;
    3: ICMS.orig:= oeNacionalConteudoImportacaoSuperior40;
    4: ICMS.orig:= oeNacionalProcessosBasicos;
    5: ICMS.orig:= oeNacionalConteudoImportacaoInferiorIgual40;
    6: ICMS.orig:= oeEstrangeiraImportacaoDiretaSemSimilar;
    7: ICMS.orig:= oeEstrangeiraAdquiridaBrasilSemSimilar;
    8: ICMS.orig:= oeNacionalConteudoImportacaoSuperior70;
    else
      ICMS.orig:= oeNacional;
  end;

end;

end.
