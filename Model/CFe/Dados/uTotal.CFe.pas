/// <summary>
///   Unit q tem a classe de Total
/// </summary>
unit uTotal.CFe;

interface

uses
  pcnCFe,
  uItem.CFe;

 type
  TTotalCFe = class
  private
    FDesconto:           Currency;
    FAcrescimo:          Currency;
    FValor:              Currency;
    FValorImposto:       Currency;
    FTributacaoFederal:  Currency;
    FTributacaoEstadual: Currency;
    FItens:              TArray<TItem>;
    procedure SetImposto(Itens: TArray<TItem>);
    procedure SetTotal(Itens: TArray<TItem>);
    procedure SetDesconto(Itens: TArray<TItem>);
    procedure SetAcrescimo(Itens: TArray<TItem>);
  public
    property Desconto:           Currency read FDesconto;
    property Acrescimo:          Currency read FAcrescimo;
    property Valor:              Currency read FValor;
    property ValorImposto:       Currency read FValorImposto;

    property TributacaoFederal:  Currency read FTributacaoFederal  write FTributacaoFederal;
    property TributacaoEstadual: Currency read FTributacaoEstadual write FTributacaoEstadual;

    procedure SetTotalCFe(Total: TTotal; Itens: TArray<TItem>);
  end;

implementation

uses
  System.SysUtils;

{ TTotalCFe }

/// <summary>
///   M�todo que define o acr�scimo do cupom
/// <param name="itens">
///   Uma lista de Titens
/// </param>
/// </summary>
procedure TTotalCFe.SetAcrescimo(Itens: TArray<TItem>);
begin
 FAcrescimo:= Itens[Low(Itens)].Produto.ValorAcrescimo;
end;

/// <summary>
///   M�todo que define o desconto do cupom
/// <param name="itens">
///   Uma lista de Titens
/// </param>
/// </summary>
procedure TTotalCFe.SetDesconto(Itens: TArray<TItem>);
begin
  FDesconto:= Itens[Low(Itens)].Produto.ValorDesconto;
end;

/// <summary>
///   M�todo que define o valor do imposto do cuporm
/// <param name="itens">
///   Uma lista de Titens
/// </param>
/// </summary>
procedure TTotalCFe.SetImposto(Itens: TArray<TItem>);
var
  imposto:  Currency;
  federal:  Currency;
  estadual: Currency;
  Item: TItem;
begin
  imposto:= 0;


  for Item in itens do
  begin
    federal := 0;
    estadual:= 0;
    if item.Produto.NCM <> '' then
    begin
      if TributacaoFederal > 0 then
      federal := ((Item.Produto.ValorUnitario * Item.Produto.Quantidade) * TributacaoFederal) / 100;

      if TributacaoEstadual > 0 then
      estadual:= ((Item.Produto.ValorUnitario * Item.Produto.Quantidade) * TributacaoEstadual)/ 100;

      imposto:= federal + estadual;
    end;

  end;

  FValorImposto:= imposto;

end;

/// <summary>
///   M�todo que define o valores do cupom
/// <param name="Total">
///   Intancia de TTotal
/// </param>
/// <param name="itens">
///   Uma lista de Titens
/// </param>
/// </summary>
procedure TTotalCFe.SetTotalCFe(Total: TTotal; Itens: TArray<TItem>);
begin
  SetImposto(Itens);
  SetTotal(Itens);
  SetDesconto(Itens);
  SetAcrescimo(Itens);

  if Desconto > Valor then
    raise EArgumentException.Create('Desconto n�o pode ser maior que o valor do total');

  Total.DescAcrEntr.vDescSubtot := Desconto;
//  Total.DescAcrEntr.vAcresSubtot:= Acrescimo;
  Total.vCFe                    := Valor;
  Total.vCFeLei12741            := ValorImposto;
end;

/// <summary>
///   M�todo que define o valor total do cupom
/// <param name="itens">
///   Uma lista de Titens
/// </param>
/// </summary>
procedure TTotalCFe.SetTotal(Itens: TArray<TItem>);
var
  total: Currency;
  Item:  TItem;
begin
  total:= 0;
  for Item in itens do
  begin
    total:= total + (Item.Produto.ValorUnitario*item.Produto.Quantidade);
  end;

  FValor:= total;
end;

end.
