/// <summary>
///   Unit com a classe de cliente do cupom
/// </summary>
unit uCliente.CFe;

interface

uses
  pcnCFe;

type
  TCliente = class
  private
    FDocumento: string;
    FNome:      string;

  public
    property Documento: string read FDocumento write FDocumento;
    property Nome:      string read FNome      write FNome;


    procedure SetDest(Dest: TDest);
  end;

implementation

{ TCliente }

uses uValidacoes, System.SysUtils;


/// <summary>
///   M�todo usado para definir os dados do cliente nas informa��es do cupom
/// <param name="Dest">
///   Parametro que � a instancia de TDest
/// </param>
/// </summary>
procedure TCliente.SetDest(Dest: TDest);
begin
  Dest.CNPJCPF:= Documento ;
  Dest.xNome  := Nome ;
end;

end.
