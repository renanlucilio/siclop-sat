/// <summary>
///   Unit com a classe de COFINS
/// </summary>
unit uImposto.COFINS.CFe;

interface

uses
  pcnCFe,
  pcnConversao;
type
  TImposto_COFINS = class
  private
    FCST:                  integer;
    FBaseDeCalculo:        Currency;
    FPorcento:             Double;
    FValor:                Currency;
    FAliquota:             Currency;
    FBaseDeCalculoProduto: Currency;
    procedure SetCofinsCST(COFINS: TCOFINS);
  public
    property CST:                  integer   read FCST write FCST;
    property BaseDeCalculo:        Currency read FBaseDeCalculo        write FBaseDeCalculo;
    property Porcento:             Double read FPorcento             write FPorcento;
    property Valor:                Currency read FValor                write FValor;
    property Aliquota:             Currency read FAliquota             write FAliquota;
    property BaseDeCalculoProduto: Currency read FBaseDeCalculoProduto write FBaseDeCalculoProduto;


    procedure SetCOFINS(COFINS: TCOFINS);
  end;

implementation

uses
  System.SysUTils;

{ TImposto_COFINS }

/// <summary>
///   M�todo usado para passar as informa��es COFINS para o cupom
/// <param name="COFINS">
///   Instancia da TCOFINS
/// </param>
/// </summary>
procedure TImposto_COFINS.SetCOFINS(COFINS: TCOFINS);
var
  ok: Boolean;
begin
  COFINS.vBC      := BaseDeCalculo;
  COFINS.pCOFINS  := Porcento;
  COFINS.vCOFINS  := Valor;
  COFINS.vAliqProd:= Aliquota;
  COFINS.qBCProd  := BaseDeCalculoProduto;
  SetCofinsCST(COFINS);
end;

/// <summary>
///   M�todo usado para definir um CST Do Cofins com base no numero
/// <param name="COFINS">
///   Instancia de TCOFINS
/// </param>
/// </summary>
procedure TImposto_COFINS.SetCofinsCST(COFINS: TCOFINS);
begin
case CST of
    1:
      COFINS.CST := cof01;
    2:
      COFINS.CST := cof02;
    3:
      COFINS.CST := cof03;
    4:
      COFINS.CST := cof04;
    5:
      COFINS.CST := cof05;
    6:
      COFINS.CST := cof06;
    7:
      COFINS.CST := cof07;
    8:
      COFINS.CST := cof08;
    9:
      COFINS.CST := cof09;
    49:
      COFINS.CST := cof49;
    50:
      COFINS.CST := cof50;
    51:
      COFINS.CST := cof51;
    52:
      COFINS.CST := cof52;
    53:
      COFINS.CST := cof53;
    54:
      COFINS.CST := cof54;
    55:
      COFINS.CST := cof55;
    56:
      COFINS.CST := cof56;
    60:
      COFINS.CST := cof60;
    61:
      COFINS.CST := cof61;
    62:
      COFINS.CST := cof62;
    63:
      COFINS.CST := cof63;
    64:
      COFINS.CST := cof64;
    65:
      COFINS.CST := cof65;
    66:
      COFINS.CST := cof66;
    67:
      COFINS.CST := cof67;
    70:
      COFINS.CST := cof70;
    71:
      COFINS.CST := cof71;
    72:
      COFINS.CST := cof72;
    73:
      COFINS.CST := cof73;
    74:
      COFINS.CST := cof74;
    75:
      COFINS.CST := cof75;
    98:
      COFINS.CST := cof98;
    99:
      COFINS.CST := cof99;
    else
    begin
      COFINS.CST := cof01;
    end;
  end;
end;

end.
