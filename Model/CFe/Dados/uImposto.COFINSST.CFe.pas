/// <summary>
///   Unit que tem a classe do COFINSST
/// </summary>
unit uImposto.COFINSST.CFe;

interface

uses
  pcnCFe;
type
  TImposto_COFINSST = class
  private
    FBaseDeCalculo:        Currency;
    FPorcento:             Double;
    FValor:                Currency;
    FAliquota:             Currency;
    FBaseDeCalculoProduto: Currency;
  public
    property BaseDeCalculo:        Currency read FBaseDeCalculo        write FBaseDeCalculo;
    property Porcento:             Double read FPorcento             write FPorcento;
    property Valor:                Currency read FValor                write FValor;
    property Aliquota:             Currency read FAliquota             write FAliquota;
    property BaseDeCalculoProduto: Currency read FBaseDeCalculoProduto write FBaseDeCalculoProduto;
    procedure SetCONFINSST(COFINSST: TCOFINSST);
  end;

implementation

{ TImposto_COFINSST }

/// <summary>
///   M�todo que passa as informa��es do Cofinsst para o cupom
/// <param name="COFINSST">
///   Instancia da classe TCOFINSST
/// </param>
/// </summary>
procedure TImposto_COFINSST.SetCONFINSST(COFINSST: TCOFINSST);
begin
  COFINSST.vBC      := BaseDeCalculo;
  COFINSST.pCOFINS  := Porcento;
  COFINSST.vCOFINS  := Valor;
  COFINSST.vAliqProd:= Aliquota;
  COFINSST.qBCProd  := BaseDeCalculoProduto;
end;

end.
