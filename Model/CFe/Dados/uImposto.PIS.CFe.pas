/// <summary>
///   Unit que tem a classe PIS
/// </summary>
unit uImposto.PIS.CFe;

interface

uses
  pcnCFe,
  pcnConversao;

type
  TImposto_PIS = class
  private
    FCST:                  integer;
    FBaseDeCalculo:        Currency;
    FPorcento:             Double;
    FValor:                Currency;
    FBaseDeCalculoProduto: Currency;
    FAliquota:             Currency;
    procedure SetPisCST(PIS: TPIS);

  public
    property CST:                  integer read FCST write FCST;
    property BaseDeCalculo:        Currency read FBaseDeCalculo        write FBaseDeCalculo;
    property Porcento:             Double read FPorcento             write FPorcento;
    property Valor:                Currency read FValor                write FValor;
    property BaseDeCalculoProduto: Currency read FBaseDeCalculoProduto write FBaseDeCalculoProduto;
    property Aliquota:             Currency read FAliquota             write FAliquota;
    procedure SetPIS(PIS: TPIS);

  end;

implementation

uses
  System.SysUtils;

{ TImposto_PIS }

/// <summary>
///   M�todo que define o CST do PIS com base no numero
/// <param name="PIS">
///   Instancia de TPIS
/// </param>
/// </summary>
procedure TImposto_PIS.SetPisCST(PIS: TPIS);
begin
  case CST of
    1:
      PIS.CST := pis01;
    2:
      PIS.CST := pis02;
    3:
      PIS.CST := pis03;
    4:
      PIS.CST := pis04;
    5:
      PIS.CST := pis05;
    6:
      PIS.CST := pis06;
    7:
      PIS.CST := pis07;
    8:
      PIS.CST := pis08;
    9:
      PIS.CST := pis09;
    49:
      PIS.CST := pis49;
    50:
      PIS.CST := pis50;
    51:
      PIS.CST := pis51;
    52:
      PIS.CST := pis52;
    53:
      PIS.CST := pis53;
    54:
      PIS.CST := pis54;
    55:
      PIS.CST := pis55;
    56:
      PIS.CST := pis56;
    60:
      PIS.CST := pis60;
    61:
      PIS.CST := pis61;
    62:
      PIS.CST := pis62;
    63:
      PIS.CST := pis63;
    64:
      PIS.CST := pis64;
    65:
      PIS.CST := pis65;
    66:
      PIS.CST := pis66;
    67:
      PIS.CST := pis67;
    70:
      PIS.CST := pis70;
    71:
      PIS.CST := pis71;
    72:
      PIS.CST := pis72;
    73:
      PIS.CST := pis73;
    74:
      PIS.CST := pis74;
    75:
      PIS.CST := pis75;
    98:
      PIS.CST := pis98;
    99:
      PIS.CST := pis99;
    else
    begin
      PIS.CST:= pis01;
    end;
  end;
end;

/// <summary>
///   M�todo que define o PIS no cupom
/// <param name="PIS">
///   Instacia de TPIS
/// </param>
/// </summary>
procedure TImposto_PIS.SetPIS(PIS: TPIS);
var
  ok: Boolean;
begin
  PIS.vBC      := BaseDeCalculo;
  PIS.pPIS     := Porcento;
  PIS.vPIS     := Valor;
  PIS.vAliqProd:= Aliquota;
  PIS.qBCProd  := BaseDeCalculoProduto;
  SetPisCST(PIS);
end;

end.
