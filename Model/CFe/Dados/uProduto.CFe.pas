/// <summary>
///   Unit que tem a Classe TProduto
/// </summary>
unit uProduto.CFe;

interface

uses
  pcnCFe,
  pcnConversao;

type
  TProduto = class
  private
    FCodigoProduto:  string;
    FDescricao:      string;
    FEAN:            string;
    FSiglaDaUnidade: string;
    FQuantidade:     Double;
    FValorUnitario:  Currency;
    FValorDesconto:  Currency;
    FValorAcrescimo: Currency;
    FNCM:            string;
    FCEST:           string;
    FCFOP:           string;


  public
    property CodigoProduto:  string        read FCodigoProduto  write FCodigoProduto;
    property Descricao:      string        read FDescricao      write FDescricao;
    property EAN:            string        read FEAN            write FEAN;
    property SiglaDaUnidade: string        read FSiglaDaUnidade write FSiglaDaUnidade;
    property Quantidade:     Double        read FQuantidade     write FQuantidade;
    property ValorUnitario:  Currency      read FValorUnitario  write FValorUnitario;
    property ValorDesconto:  Currency      read FValorDesconto  write FValorDesconto;
    property ValorAcrescimo: Currency      read FValorAcrescimo write FValorAcrescimo;
    property NCM:            string        read FNCM write FNCM;
    property CEST:           string        read FCEST write FCEST;
    property CFOP:           string        read FCFOP write FCFOP;

    procedure SetProd(Prod: TProd);
    function GetValorProduto(): Currency;
  end;

implementation

uses
  System.SysUtils, uListaCESTRepository, uListaNCMRepository;


{ TItem }

/// <summary>
///   M�todo que define um produto para o cupom
/// <param name="Prod">
///   instancia de TProd
/// </param>
/// </summary>
procedure TProduto.SetProd(Prod: TProd);
begin
  Prod.cProd        := CodigoProduto;
  Prod.cEAN         := EAN;
  Prod.xProd        := Descricao;
  Prod.NCM          := NCM;
  Prod.CEST         := CEST;
  Prod.CFOP         := CFOP;
  Prod.uCom         := SiglaDaUnidade;
  Prod.qCom         := Quantidade;
  Prod.vUnCom       := ValorUnitario;
  Prod.vProd        := GetValorProduto;
  Prod.indRegra     := irTruncamento;
//  Item.Prod.EhCombustivel:= ;
//  Prod.vDesc        := ;
//  Prod.vOutro       := ;
//  Prod.vItem        := ;
//  Prod.vRatDesc     := ;
//  Prod.vRatAcr      := ;
//  Prod.obsFiscoDet  := ;
end;

/// <summary>
///   M�todo que retorna o valor total de um produto
/// <returns>
///   Currency que tem o valor total do produto
/// </returns>
/// </summary>
function TProduto.GetValorProduto(): Currency;
begin
  Result:= (ValorUnitario * Quantidade);
end;

end.
