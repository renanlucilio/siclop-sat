/// <summary>
///   Unit que tem a classe PISST
/// </summary>
unit uImposto.PISST.CFe;

interface

uses
  pcnCFe;

type
  TImposto_PISST = class
  private
    FBaseDeCalculo:        Currency;
    FPorcento:             Double;
    FBaseDeCalculoProduto: Currency;
    FAliquota:             Currency;
    FValor:                Currency;
  public
    property BaseDeCalculo:        Currency read FBaseDeCalculo        write FBaseDeCalculo;
    property Porcento:             Double read FPorcento             write FPorcento;
    property BaseDeCalculoProduto: Currency read FBaseDeCalculoProduto write FBaseDeCalculoProduto;
    property Aliquota:             Currency read FAliquota             write FAliquota;
    property Valor:                Currency read FValor                write FValor;
    procedure SetPISST(PISST: TPISST);
  end;

implementation


{ TImposto_PISST }

/// <summary>
///   M�todo que define o PISST no cupom
/// <param name="PISST">
///   Instancia de TPISST
/// </param>
/// </summary>
procedure TImposto_PISST.SetPISST(PISST: TPISST);
begin
  PISST.vBC      := BaseDeCalculo;
  PISST.pPIS     := Porcento;
  PISST.vPIS     := Valor;
  PISST.vAliqProd:= Aliquota;
  PISST.qBCProd  := BaseDeCalculoProduto;
end;

end.
