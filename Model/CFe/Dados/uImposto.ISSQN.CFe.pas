/// <summary>
///   Unit com a classe ISSQN
/// </summary>
unit uImposto.ISSQN.CFe;

interface

uses
  pcnCFe,
  pcnConversao;

type

  TImposto_ISSQN = class
  private
    FBaseDeCalculo:    Currency;
    FAliquota:         Currency;
    FValor:            Currency;
    FDeducao:          Currency;
    FCodigoMunicipio:  Integer;
    FItemListaServico: string;
    FCodigoTributacao: string;
    FNaturezaOperacao: Integer;
    FIncentivoFiscal:  TpcnindIncentivo;

  public
    property BaseDeCalculo:    Currency         read FBaseDeCalculo    write FBaseDeCalculo;
    property Aliquota:         Currency         read FAliquota         write FAliquota;
    property Valor:            Currency         read FValor            write FValor;
    property Deducao:          Currency         read FDeducao          write FDeducao;
    property CodigoMunicipio:  Integer          read FCodigoMunicipio  write FCodigoMunicipio;
    property ItemListaServico: string           read FItemListaServico write FItemListaServico;
    property CodigoTributacao: string           read FCodigoTributacao write FCodigoTributacao;
    property NaturezaOperacao: Integer          read FNaturezaOperacao write FNaturezaOperacao;
    property IncentivoFiscal:  TpcnindIncentivo read FIncentivoFiscal  write FIncentivoFiscal;
    procedure SetISSQN(ISSQN: TISSQN);
  end;

implementation

uses
  System.SysUtils;

{ TImposto_ISSQN }

/// <summary>
///   M�todo que define o ISSQN para o cupom
/// <param name="ISSQN">
///   Instancia de TISSQN
/// </param>
/// </summary>
procedure TImposto_ISSQN.SetISSQN(ISSQN: TISSQN);
begin
  ISSQN.vDeducISSQN := Deducao;
  ISSQN.vBC         := BaseDeCalculo;
  ISSQN.vAliq       := Aliquota;
  ISSQN.vISSQN      := Valor;
  ISSQN.cMunFG      := CodigoMunicipio;
  ISSQN.cListServ   := ItemListaServico;
  ISSQN.cNatOp      := NaturezaOperacao;
  ISSQN.cServTribMun:= CodigoTributacao;
  ISSQN.indIncFisc  := IncentivoFiscal;
end;

end.
