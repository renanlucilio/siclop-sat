/// <summary>
///   Unit que tem a classe de informa��es pagamento
/// </summary>
unit uPagamento.CFe;

interface

uses
  pcnCFe;

type
  TPagamento = class
  private
    FTipoPagamento: Integer;
    FCodigoOperadora: Integer;
    FValor: Currency;
    procedure SetcMP(Pagamento: TMPCollectionItem);
    procedure SetcAdmC();
    function GetCodigoOperadora: Integer;
    procedure SetCodigoOperadora(const Value: Integer);
  public
    property TipoPagamento:      Integer  read FTipoPagamento      write FTipoPagamento;
    property CodigoOperadora:    Integer  read FCodigoOperadora    write FCodigoOperadora;
    property Valor:              Currency read FValor              write FValor;       

    procedure SetPagamento(Pagamento: TMPCollectionItem);
  end;


implementation

uses
  pcnConversao, System.SysUtils;

{ TPagamento }

/// <summary>
///   M�todo que recupera o c�digo da operadora
/// <returns>
///   Integer com o c�digo da operadora
/// </returns>
/// </summary>
function TPagamento.GetCodigoOperadora: Integer;
begin
  Result:= FCodigoOperadora;
end;

/// <summary>
///   M�todo que define uma operacora de cart�o
/// </summary>
procedure TPagamento.SetcAdmC();
begin
  Randomize;
   CodigoOperadora := 999;
   case Random(9) of
      0:
         CodigoOperadora := 999;
      1:
         CodigoOperadora := 012;
      2:
         CodigoOperadora := 019;
      3:
         CodigoOperadora := 025;
      4:
         CodigoOperadora := 029;
      5:
         CodigoOperadora := 034;
      6:
         CodigoOperadora := 012;
      7:
         CodigoOperadora := 025;
      8:
         CodigoOperadora := 030;
      9:
         CodigoOperadora := 019;
   end;
end;

/// <summary>
///   M�todo que define uma forma de pagamento com base no numero
/// <param name="Pagamento">
///   Instancia de TMPCollectionItem
/// </param>
/// </summary>
procedure TPagamento.SetcMP(Pagamento: TMPCollectionItem);
begin
  case TipoPagamento of
    01:
      Pagamento.cMP := mpDinheiro;
    02:
      Pagamento.cMP := mpCheque;
    03:
      Pagamento.cMP := mpValeAlimentacao;
    04:
      Pagamento.cMP := mpCreditoLoja;
    05:
      Pagamento.cMP := mpValeRefeicao;
    06:
      begin
        SetcAdmC();
        Pagamento.cMP := mpCartaodeDebito;
        Pagamento.cAdmC := CodigoOperadora;
      end;
    07:
      begin
        SetcAdmC();
        Pagamento.cMP := mpCartaodeCredito;
        Pagamento.cAdmC := CodigoOperadora;
      end;
    09:
      Pagamento.cMP := mpOutros;
    76:
      Pagamento.cMP := mpValeCombustivel;
    86:
      Pagamento.cMP := mpValePresente
    else
      raise EArgumentException.Create('Tipo de pagamento inv�lido');
  end;
end;

/// <summary>
///   M�todo que define um c�digo de operadora
/// <param name="value">
///   valor do c�digo da operadora com base no numero
/// </param>
/// </summary>
procedure TPagamento.SetCodigoOperadora(const Value: Integer);
begin
  case Value of
    001: FCodigoOperadora:= Value;
    002: FCodigoOperadora:= Value;
    003: FCodigoOperadora:= Value;
    004: FCodigoOperadora:= Value;
    005: FCodigoOperadora:= Value;
    006: FCodigoOperadora:= Value;
    007: FCodigoOperadora:= Value;
    008: FCodigoOperadora:= Value;
    009: FCodigoOperadora:= Value;
    010: FCodigoOperadora:= Value;
    011: FCodigoOperadora:= Value;
    012: FCodigoOperadora:= Value;
    013: FCodigoOperadora:= Value;
    014: FCodigoOperadora:= Value;
    015: FCodigoOperadora:= Value;
    016: FCodigoOperadora:= Value;
    017: FCodigoOperadora:= Value;
    018: FCodigoOperadora:= Value;
    019: FCodigoOperadora:= Value;
    020: FCodigoOperadora:= Value;
    021: FCodigoOperadora:= Value;
    022: FCodigoOperadora:= Value;
    023: FCodigoOperadora:= Value;
    024: FCodigoOperadora:= Value;
    025: FCodigoOperadora:= Value;
    026: FCodigoOperadora:= Value;
    027: FCodigoOperadora:= Value;
    028: FCodigoOperadora:= Value;
    029: FCodigoOperadora:= Value;
    030: FCodigoOperadora:= Value;
    031: FCodigoOperadora:= Value;
    032: FCodigoOperadora:= Value;
    033: FCodigoOperadora:= Value;
    034: FCodigoOperadora:= Value;
    999: FCodigoOperadora:= Value;
    else
      raise EArgumentException.Create('C�digo da operadora inv�lido');
  end;

end;

/// <summary>
///   M�todo que define o pagamento no cupom
/// <param name="Pagamento">
///   Instancia de TPagamento
/// </param>
/// </summary>
procedure TPagamento.SetPagamento(Pagamento: TMPCollectionItem);
begin
  SetcMP(pagamento);
  Pagamento.vMP:= Valor;
end;

end.
