/// <summary>
///   Unit que tem a classe com os dados do cupom fiscal
/// </summary>
unit uIdentificacao.CFe;

interface

uses
  pcnCFe,
  pcnConversao;

type
  TIdentificacao = class
  private
    FNumero:            Cardinal;
  public
    property Numero: Cardinal read FNumero write FNumero;

    procedure SetIde(ide: Tide);
  end;

implementation

uses
  System.SysUtils;

{ TIdentificacao }

/// <summary>
///   M�todo que passa os dados do cupom para
/// <param name="ide">
///   Instancia de TIde
/// </param>
/// </summary>
procedure TIdentificacao.SetIde(ide: Tide);
begin
//  ide.cUF:= ;
    ide.cNF:= Numero ;
//  ide.modelo:= ;
//  ide.nserieSAT:= ;
//  ide.nCFe:= ;
//  ide.dEmi:= ;
//  ide.hEmi:= ;
//  ide.cDV:= ;
//  ide.tpAmb:= ;
//  ide.CNPJ:= ;
//  ide.signAC:=;
//  ide.assinaturaQRCODE:= ;
//  ide.numeroCaixa:= ;
end;

end.
