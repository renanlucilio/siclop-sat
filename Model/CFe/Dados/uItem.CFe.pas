/// <summary>
///   Unit com de um ITEM
/// </summary>
unit uItem.CFe;

interface

uses
  uImposto.CFe,
  uProduto.CFe,
  pcnCFe;

type
  TItem = class
  private
    FImposto: TImpostoCFe;
    FProduto: TProduto;
    FID: Integer;
  public
    constructor Create();
    destructor Destroy(); override;

    property Imposto: TImpostoCFe read FImposto write FImposto;
    property Produto: TProduto    read FProduto write FProduto;

    procedure SetItem(Item: TDetCollectionItem);
  end;


implementation

{ TItem }

/// <summary>
///   M�todo construtor que cria um produto e os impostos
/// </summary>
constructor TItem.Create();
begin
  FImposto:= TImpostoCFe.Create;
  FProduto:= TProduto.Create;
end;

/// <summary>
///   M�todo que elimina a instancia
/// </summary>
destructor TItem.Destroy;
begin
  FImposto.Free;
  FProduto.Free;
  inherited;
end;

/// <summary>
///   M�todo que define um ITEM para o cupom
/// <param name="Item">
///   Instacia de TDetCollectionItem
/// </param>
/// </summary>
procedure TItem.SetItem(Item: TDetCollectionItem);
begin
  Produto.SetProd(item.Prod);
  Imposto.SetImposto(item.Imposto);
end;

end.
