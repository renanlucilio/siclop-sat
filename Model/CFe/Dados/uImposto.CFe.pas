/// <summary>
///   Unit que tem a classe com as informa��es de imposto
/// </summary>
unit uImposto.CFe;

interface

uses
  pcnCFe,
  uImposto.ICMS.CFe,
  uImposto.PIS.CFe,
  uImposto.PISST.CFe,
  uImposto.COFINS.CFe,
  uImposto.COFINSST.CFe,
  uImposto.ISSQN.CFe,
  uProduto.CFe;

type
  TImpostoCFe = class
  private
    FValor:    Currency;
    FICMS:     TImposto_ICMS;
    FPIS:      TImposto_PIS;
    FPISST:    TImposto_PISST;
    FCOFINS:   TImposto_COFINS;
    FCOFINSST: TImposto_COFINSST;
    FISSQN:    TImposto_ISSQN;
  public
    constructor Create;
    destructor Destroy; override;
    property Valor:    Currency          read FValor;
    property ICMS:     TImposto_ICMS     read FICMS     write FICMS;
    property PIS:      TImposto_PIS      read FPIS      write FPIS;
    property PISST:    TImposto_PISST    read FPISST    write FPISST;
    property COFINS:   TImposto_COFINS   read FCOFINS   write FCOFINS;
    property COFINSST: TImposto_COFINSST read FCOFINSST write FCOFINSST;
    property ISSQN:    TImposto_ISSQN    read FISSQN    write FISSQN;

    procedure SetValor(const federal, estadual: Currency; const produto: TProduto);
    procedure SetImposto(Imposto: TImposto);
  end;

implementation



{ TImposto }

/// <summary>
///   M�todo construtor para criar os tipos diferentes de classes de imposto
/// </summary>
constructor TImpostoCFe.Create;
begin
  FICMS:=     TImposto_ICMS.Create;
  FPIS:=      TImposto_PIS.Create;
  FPISST:=    TImposto_PISST.Create;
  FCOFINS:=   TImposto_COFINS.Create;
  FCOFINSST:= TImposto_COFINSST.Create;
  FISSQN:=    TImposto_ISSQN.Create;
end;

/// <summary>
///   M�todo destrutor para eliminar a instacia do obj
/// </summary>
destructor TImpostoCFe.Destroy;
begin
  FICMS.Free;
  FPIS.Free;
  FPISST.Free;
  FCOFINS.Free;
  FCOFINSST.Free;
  FISSQN.Free;
  inherited;
end;

/// <summary>
///   M�todo para adicionar as informa��es de imposto no cupom
/// <param name="Imposto">
///   Da Classe TImposto
/// </param>
/// </summary>
procedure TImpostoCFe.SetImposto(Imposto: TImposto);
begin
  Imposto.vItem12741:= Valor;
  COFINS.SetCOFINS(Imposto.COFINS);
  COFINSST.SetCONFINSST(Imposto.COFINSST);
  PIS.SetPIS(Imposto.PIS);
  PISST.SetPISST(Imposto.PISST);
  ICMS.SetICMS(Imposto.ICMS);
  ISSQN.SetISSQN(Imposto.ISSQN);
end;

/// <summary>
///   M�todo para setar o valor dos imposto
/// <param name="federal">
///   Valor da tributa��o federal
/// </param>
/// <param name="estadual">
///   Valor da tributa��o estadual
/// </param>
/// <param name="produto">
///   Instancia da classe de protudo
/// </param>
/// </summary>
procedure TImpostoCFe.SetValor(const federal, estadual: Currency; const produto: TProduto);
var
  tributacaoFederal:  Currency;
  tributacaoEstadual: Currency;
begin
  tributacaoFederal:=  0;
  tributacaoEstadual:= 0;
  if produto.NCM <> '' then
  begin
    if federal > 0 then
      tributacaoFederal := ((produto.ValorUnitario*produto.Quantidade) * federal) / 100;

    if estadual > 0 then
      tributacaoEstadual:= ((produto.ValorUnitario*produto.Quantidade) * estadual) / 100;
  end;

  if ICMS.Origem = 0 then
    FValor:= tributacaoEstadual
  else
    FValor:= tributacaoFederal + estadual;

end;

end.
