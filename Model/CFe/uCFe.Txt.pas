/// <summary>
///   Unit que tem aa classe TCFeTxt
/// </summary>
unit uCFe.Txt;

interface

uses
  uCFe,
  System.IOUtils;

type
  TCFeTxt = class
  private
    FDadosCFe: TDadosCFe;
    procedure SetCliente(const linha: string);
    procedure SetNota(const linha: string);
    procedure SetItem(const linha: string);
    procedure SetPagamento(const linha: string);
    procedure SetEmissao(const linha: string);
  public
    constructor Create();
    destructor Destroy(); override;
    procedure MoverParaErro(const arquivo: string);
    function TxtToCFe(const arquivo: string): TDadosCFe;
    function VerificarTxt(const arquivo: string): string;
    function ListarArquivos(const pastaCupom: string): TArray<string>;
    class function TotalErros(): Integer;
    class function TotalXML(): Integer;
  end;

implementation

uses
  System.DateUtils,
  System.Classes,
  System.SysUtils,
  uItem.CFe,
  uPastas,
  uPagamento.CFe,
  System.Generics.Collections, uValidacoes;

{ TCFeTxt }

/// <summary>
///   M�todo construtor da classe
/// </summary>
constructor TCFeTxt.Create;
begin

end;

/// <summary>
///   M�todo Destrutor que elimina a instancia
/// </summary>
destructor TCFeTxt.Destroy;
begin
  FDadosCFe.Free;
  inherited;
end;

/// <summary>
///   M�todo para Listar os arquivos do cupom na pasta
/// <param name="pastaCupom">
///   Pasta onde ser� verificado se tem cupons
/// </param>
/// <returns>
///   Retorna uma lista de arquivos de cupom
/// </returns>
/// </summary>
function TCFeTxt.ListarArquivos(const pastaCupom: string): TArray<string>;
var
  listaArquivos: TList<string>;
  it: string;
begin
  listaArquivos:= TList<string>.Create();
  try
    for it in TDirectory.GetFiles(pastaCupom, '*.SCP') do
    begin
      listaArquivos.Add(it);
    end;

    Result:= listaArquivos.ToArray();
  finally
    listaArquivos.Free;
  end;
end;

/// <summary>
///   M�todo que move um cupom para a pasta de erro na pasta configurada
/// <param name="arquivo">
///   arquivo que ser� movido
/// </param>
/// </summary>
procedure TCFeTxt.MoverParaErro(const arquivo: string);
var
  pasta: TPasta;
begin

  pasta := TPasta.Create;
  try
    if FileExists(TPath.Combine(pasta.PastaErro, ExtractFileName(arquivo))) then
      TFile.Delete(TPath.Combine(pasta.PastaErro, ExtractFileName(arquivo)));

    TFile.Move(arquivo, TPath.Combine(pasta.PastaErro, ExtractFileName(arquivo)));
  finally
    pasta.Free;
  end;

end;

/// <summary>
///   M�todo que pega as informa��es do cliente no TXT
/// <param name="linha">
///   linha do txt
/// </param>
/// </summary>
procedure TCFeTxt.SetCliente(const linha: string);
var
  dados: TArray<string>;
begin
    dados:= linha.Split(['|']);
    FDadosCFe.Cliente.Documento:= dados[1];
    FDadosCFe.Cliente.Nome     := dados[2];
end;

/// <summary>
///   M�todo que paga as informa��es da emiss�o no txt
/// <param name="linha">
///   linha do txt
/// </param>
/// </summary>
procedure TCFeTxt.SetEmissao(const linha: string);
var
  dados: TArray<string>;
begin
    dados:= linha.Split(['|']);
    FDadosCFe.Criado:= dados[1].Equals('S');
end;

/// <summary>
///   M�todo que pega as informa��e do item no TXT
/// <param name="linha">
///   linha do txt
/// </param>
/// </summary>
procedure TCFeTxt.SetItem(const linha: string);
var
  item: TItem;
  dados: TArray<string>;
begin
    dados:= linha.Split(['|']);
    item:= TItem.Create;

    item.Produto.CodigoProduto  := dados[1];
    item.Produto.Descricao      := dados[2];
    item.Produto.EAN            := dados[3];
    item.Produto.NCM  := dados[4];
    item.Produto.CEST:= dados[5];
    item.Produto.SiglaDaUnidade := dados[6];
    item.Produto.CFOP:= dados[7];
    item.Produto.Quantidade     := StrToFloatDeF(dados[8], 0);
    item.Produto.ValorUnitario  := StrToCurrDef(dados[9],  0);
    item.Produto.ValorDesconto  := StrToCurrDef(dados[11], 0);
    item.Produto.ValorAcrescimo := StrToCurrDef(dados[12], 0);

    item.Imposto.ICMS.Origem:= StrToIntDef(dados[13], 0);
    item.Imposto.ICMS.CST_CSOSN:= StrToIntDef(dados[16], 0);
    item.Imposto.ICMS.Porcento  := StrToFloatDef(dados[17], 0);
    item.Imposto.ICMS.Valor     := StrToFloatDef(dados[18], 0);

    item.Imposto.PIS.CST:= StrToIntDef(dados[19], 0);
    item.Imposto.PIS.BaseDeCalculo        := StrToFloatDef(dados[20], 0);
    item.Imposto.PIS.Porcento             := StrToFloatDef(dados[21], 0);
    item.Imposto.PIS.Valor                := StrToFloatDef(dados[22], 0);
    item.Imposto.PIS.BaseDeCalculoProduto := StrToFloatDef(dados[23], 0);
    item.Imposto.PIS.Aliquota             := StrToFloatDef(dados[24], 0);

    item.Imposto.PISST.BaseDeCalculo        := StrToFloatDef(dados[31], 0);
    item.Imposto.PISST.Porcento             := StrToFloatDef(dados[32], 0);
    item.Imposto.PISST.BaseDeCalculoProduto := StrToFloatDef(dados[33], 0);
    item.Imposto.PISST.Aliquota             := StrToFloatDef(dados[34], 0);
    item.Imposto.PISST.Valor                := StrToFloatDef(dados[35], 0);

    item.Imposto.COFINS.CST := StrToIntDef(dados[25], 0);
    item.Imposto.COFINS.BaseDeCalculo        := StrToFloatDef(dados[26], 0);
    item.Imposto.COFINS.Porcento             := StrToFloatDef(dados[27], 0);
    item.Imposto.COFINS.Valor                := StrToFloatDef(dados[28], 0);
    item.Imposto.COFINS.Aliquota             := StrToFloatDef(dados[29], 0);
    item.Imposto.COFINS.BaseDeCalculoProduto := StrToFloatDef(dados[30], 0);

    item.Imposto.COFINSST.BaseDeCalculo       := StrToFloatDef(dados[36], 0);
    item.Imposto.COFINSST.Porcento            := StrToFloatDef(dados[37], 0);
    item.Imposto.COFINSST.Valor               := StrToFloatDef(dados[40], 0);
    item.Imposto.COFINSST.Aliquota            := StrToFloatDef(dados[39], 0);
    item.Imposto.COFINSST.BaseDeCalculoProduto:= StrToFloatDef(dados[38], 0);

    item.Imposto.ISSQN.BaseDeCalculo    := StrToFloatDef(dados[42], 0);
    item.Imposto.ISSQN.Aliquota         := StrToFloatDef(dados[43], 0);
    item.Imposto.ISSQN.Valor            := StrToFloatDef(dados[44], 0);
    item.Imposto.ISSQN.Deducao          := StrToFloatDef(dados[41], 0);
  //  item.Imposto.ISSQN.CodigoMunicipio  := ;
  //  item.Imposto.ISSQN.ItemListaServico := ;
  //  item.Imposto.ISSQN.CodigoTributacao := ;
  //  item.Imposto.ISSQN.NaturezaOperacao := ;
  //  item.Imposto.ISSQN.IncentivoFiscal  := ;

    FDadosCFe.Total.TributacaoEstadual := StrToFloatDef(dados[14], 0);
    FDadosCFe.Total.TributacaoFederal  := StrToFloatDef(dados[15], 0);

    item.Imposto.SetValor(
      FDadosCFe.Total.TributacaoFederal,
      FDadosCFe.Total.TributacaoEstadual,
      item.Produto);

    FDadosCFe.Itens.Add(item);
end;

/// <summary>
///   M�todo que pega as informa��es da nota do TXT
/// <param name="linha">
///   Linha do TXT
/// </param>
/// </summary>
procedure TCFeTxt.SetNota(const linha: string);
var
  dados: TArray<string>;
begin
    dados:= linha.Split(['|']);
    FDadosCFe.Identificacao.Numero:= StrToIntDef(dados[1], 0);
end;

/// <summary>
///   M�todo que pega as informa��es de pagamento do TXT
/// <param name="linha">
///   Linha do TXT
/// </param>
/// </summary>
procedure TCFeTxt.SetPagamento(const linha: string);
var
  dados: TArray<string>;
  pagamento: TPagamento;
begin
    dados:= linha.Split(['|']);
    pagamento:= TPagamento.Create();
    pagamento.TipoPagamento  := StrToIntDef(dados[1], 01);
    pagamento.Valor          := StrToFloatDef(dados[2], 0);
    pagamento.CodigoOperadora:= StrToIntDef(dados[3], 0);
    FDadosCFe.Pagamentos.Add(pagamento);
end;

/// <summary>
///   M�todo que pega o total de arquivos de cupom
/// <returns>
///   Integer que retorna a quantidade de erros
/// </returns>
/// </summary>
class function TCFeTxt.TotalErros(): Integer;
var
  it: string;
  dataArquivo: TDateTime;
  pasta: TPasta;
begin
  Result:= 0;
  pasta:= TPasta.Create();
  try
    for it in TDirectory.GetFiles(pasta.PastaErro(), '*.SCP') do
    begin
     FileAge(it, dataArquivo);

     if DateOf(Now()) = DateOf(dataArquivo) then
      Inc(Result);
    end;
  finally
    pasta.Free;
  end;
end;

/// <summary>
///   M�todo que pega a quantidade de arquivos cupom em uma pasta
///  /// <returns>
     ///   Integer retorna a quantidade de Arquivos
     /// </returns>
/// </summary>
class function TCFeTxt.TotalXML: Integer;
var
  it: string;
  dataArquivo: TDateTime;
  pasta: TPasta;
begin
  Result:= 0;
  pasta:= TPasta.Create;
  try
    for it in TDirectory.GetFiles(pasta.PastaXML(), '*.*') do
    begin
     FileAge(it, dataArquivo);

     if DateOf(Now()) = DateOf(dataArquivo) then
      Inc(Result);
    end;
  finally
    pasta.Free;
  end;
end;

/// <summary>
///   M�todo que converte um TXT em um Cupom
/// <param name="arquivo">
///   Arquivo txt que ser� convertido
/// </param>
/// <returns>
///   Retorna o cupom que � uma instancia de TDadosCFe
/// </returns>
/// </summary>
function TCFeTxt.TxtToCFe(const arquivo: string): TDadosCFe;
var
  txt: TStringList;
  item: string;
begin
  txt:= TStringList.Create;
  FDadosCFe:= TDadosCFe.Create;
  try
    try
      if not FileExists(arquivo) then
        Exit(nil);

      txt.LoadFromFile(arquivo);

      for item in txt do
      begin
        if item.Contains('EM|') then
          SetEmissao(item);

        if item.Contains('CL|') then
          SetCliente(item);

        if item.Contains('NF|') then
          SetNota(item);

        if item.Contains('IT|') then
          SetItem(item);

        if item.Contains('PG|') then
          SetPagamento(item);
      end;
      Result:= FDadosCFe;
    except on e: exception do
      begin
        raise Exception.Create(e.Message);
      end;
    end;
  finally
    txt.Free;
  end;
end;

/// <summary>
///   M�todo que verifica se o txt do cupom est� valido
/// <param name="arquivo">
///   Arquivo txt que ser� verificado 
/// </param>
/// <returns>
///   Retorna os erros do arquivo caso tenha algum
/// </returns>
/// </summary>
function TCFeTxt.VerificarTxt(const arquivo: string): string;
var
  txt: TStringList;
  item: string;
  dados: TArray<string>;
  retorno: TStringBuilder;
begin
  txt:= TStringList.Create;
  retorno:= TStringBuilder.Create();
  try
      if not FileExists(arquivo) then
        Exit('Arquivo n�o existe');

      txt.LoadFromFile(arquivo);

      if txt.Count <= 0 then
        Exit('Arquivo sem dados');

      if not txt[0].Contains('EM') then
        Exit('Arquivo inv�lido');


      for item in txt do
      begin

        if item.Contains('CL|') then
        begin
          dados:= item.Split(['|']);
          if not ((dados[1].Trim = '') or
           TValidacaoes.ValidarCPF(dados[1]) or
           TValidacaoes.ValidarCNPJ(dados[1])) then
            retorno.Append('Documento inv�lido' + dados[1].QuotedString()).AppendLine;
        end;

        if item.Contains('IT|') then
        begin
          dados:= item.Split(['|']);

          if not(TValidacaoes.VerificaCest(dados[5])) then
            retorno.Append('CEST inv�lido do produto ').Append(dados[2].QuotedString).AppendLine;

          if not(TValidacaoes.VerificaNcm(dados[4])) then
            retorno.Append('NCM inv�lido do produto ').Append(dados[2].QuotedString).AppendLine;

          if not(TValidacaoes.VerificaCfop(dados[7])) then
            retorno.Append('CFOP inv�lido do produto ').Append(dados[2].QuotedString).AppendLine;

          if not(TValidacaoes.VerificaIcmsCst(StrToIntDef(dados[16], -1))) then
            retorno.Append('CST DO ICMS inv�lido do produto ').Append(dados[2].QuotedString).AppendLine;

          if not(TValidacaoes.VerificaIcmsOrigem(StrToIntDef(dados[13], -1))) then
            retorno.Append('ORIGEM inv�lido do produto ').Append(dados[2].QuotedString).AppendLine;

          if not(TValidacaoes.VerificaCofinsCst(StrToIntDef(dados[25], -1))) then
            retorno.Append('CST DO COFINS inv�lido do produto ').Append(dados[2].QuotedString).AppendLine;

          if not(TValidacaoes.VerificaPisCst(StrToIntDef(dados[19], -1))) then
            retorno.Append('CST DO PIS inv�lido do produto ').Append(dados[2].QuotedString).AppendLine;
        end;
      end;

      result:= retorno.ToString();
  finally
    retorno.Free;
    txt.Free;
  end;
end;

end.
