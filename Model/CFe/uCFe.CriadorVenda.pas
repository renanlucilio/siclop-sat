
/// <summary>
///   Unit que tem a classe TCFeCriador
/// </summary>
unit uCFe.CriadorVenda;

interface

uses uCriador.Cupom, System.SysUtils, System.Classes, uVendaProduto;

type
  TCFeCriador = class
  private
    txt: TStringList;
    procedure CriarLinhaEmissao(cupom: TCriadorCupom);
    procedure CriarLinhaCliente(cupom: TCriadorCupom);
    procedure CriarLinhaNotaFiscal(cupom: TCriadorCupom);
    procedure CriarLinhasItens(cupom: TCriadorCupom);
    procedure CriarLinhasPagemento(cupom: TCriadorCupom);

    procedure SalvarTxt();
  public
    constructor Create(cupom: TCriadorCupom);
    destructor Destroy; override;
  end;

implementation

{ TCFeCriador }

uses uPastas, uProdutoRepository, uProduto;

/// <summary>
///   M�todo construtor que cria as partes de um cupom
/// <param name="cupom">
///   Instacnai de TCriadorCupom
/// </param>
/// </summary>
constructor TCFeCriador.Create(cupom: TCriadorCupom);
begin
  if Assigned(cupom) then
  begin
    txt:= TStringList.Create;
    CriarLinhaEmissao(cupom);
    CriarLinhaCliente(cupom);
    CriarLinhaNotaFiscal(cupom);
    CriarLinhasItens(cupom);
    CriarLinhasPagemento(cupom);

    SalvarTxt();
  end;
end;

/// <summary>
///   M�todo que criar a linha do cupom sobre o cliente
/// <param name="cupom">
///   Instancia de TCriadorCupom
/// </param>
/// </summary>
procedure TCFeCriador.CriarLinhaCliente(cupom: TCriadorCupom);
var
  blocoTxt: TStringBuilder;
begin
  blocoTxt := TStringBuilder.Create;
  try
    blocoTxt
      .Append('CL').Append('|')
      .Append(cupom.Cliente.Documento).Append('|')
      .Append(cupom.Cliente.Nome).Append('|');

    txt.Add(blocoTxt.ToString());
  finally
    blocoTxt.Free;
  end;
end;


/// <summary>
///   M�todo para a cria��o da linha da emiss�o do cupom
/// <param name="cupom">
///   instancia de TCriadorCupom
/// </param>
/// </summary>
procedure TCFeCriador.CriarLinhaEmissao(cupom: TCriadorCupom);
var
  blocoTxt: TStringBuilder;
begin
  blocoTxt := TStringBuilder.Create;
  try
    blocoTxt
      .Append('EM').Append('|')
      .Append('S').Append('|');

    txt.Add(blocoTxt.ToString());
  finally
    blocoTxt.Free;
  end;
end;

/// <summary>
///   M�todo que Cria a linha da nota fiscal do cupom
/// <param name="cupom">
///   instancia de TCriadorCupom
/// </param>
/// </summary>
procedure TCFeCriador.CriarLinhaNotaFiscal(cupom: TCriadorCupom);
var
  blocoTxt: TStringBuilder;
begin
  blocoTxt := TStringBuilder.Create;
  try
    blocoTxt
      .Append('NF').Append('|')
      .Append(cupom.Venda.Id).Append('|');

    txt.Add(blocoTxt.ToString());
  finally
    blocoTxt.Free;
  end;
end;

/// <summary>
///   M�todo para cria��o as linhas do cupom sobre item
/// <param name="cupom">
///   Instancia de TCriadorCupom
/// </param>
/// </summary>
procedure TCFeCriador.CriarLinhasItens(cupom: TCriadorCupom);
var
  blocoTxt: TStringBuilder;
  prodVenda: TVendaProduto;
  produto: TProduto;
  repo: TProdutoRepository;
begin
  blocoTxt := TStringBuilder.Create;
  repo:= TProdutoRepository.Create;
  try
    for prodVenda in cupom.ProdutosVendidos do
    begin
      produto:= repo.Get(prodVenda.ProdutoId);
      blocoTxt.Clear();


      blocoTxt.Append('IT').Append('|');
      blocoTxt.Append(produto.Id).Append('|');
      blocoTxt.Append(prodVenda.Produto.Descricao).Append('|');
      blocoTxt.Append('').Append('|');
      blocoTxt.Append(produto.Ncm).Append('|');
      blocoTxt.Append(produto.Cest).Append('|');
      blocoTxt.Append(produto.UnidadeMedida).Append('|');
      blocoTxt.Append(produto.Cfop).Append('|');
      blocoTxt.Append(prodVenda.Quantidade).Append('|');
      blocoTxt.Append(prodVenda.Produto.Preco).Append('|');
      blocoTxt.Append(Double(produto.Preco * prodVenda.Quantidade)).Append('|');
      blocoTxt.Append(prodVenda.Venda.Desconto.GetValueOrDefault).Append('|');
      blocoTxt.Append(prodVenda.Venda.Acrescimo.GetValueOrDefault).Append('|');
      blocoTxt.Append(produto.Icms_Origem).Append('|');
      blocoTxt.Append(produto.TaxaEstadual).Append('|');
      blocoTxt.Append(produto.TaxaFederal).Append('|') ;
      blocoTxt.Append(produto.Icms_Cst).Append('|')   ;
      blocoTxt.Append(produto.Icms_Porcento).Append('|');
      blocoTxt.Append(produto.Icms_Valor).Append('|');
      blocoTxt.Append(produto.Pis_CST).Append('|');
      blocoTxt.Append(produto.Pis_BaseCalculo).Append('|');
      blocoTxt.Append(produto.Pis_Porcento).Append('|');
      blocoTxt.Append(produto.Pis_Valor).Append('|');
      blocoTxt.Append(produto.Pis_BaseCalculoPorProduto).Append('|');
      blocoTxt.Append(produto.Pis_Aliquota).Append('|');
      blocoTxt.Append(produto.Cofins_CST).Append('|');
      blocoTxt.Append(produto.Cofins_BaseCalculo).Append('|');
      blocoTxt.Append(produto.Cofins_Porcento).Append('|');
      blocoTxt.Append(produto.Cofins_Valor).Append('|');
      blocoTxt.Append(produto.Cofins_Aliquota).Append('|');
      blocoTxt.Append(produto.Cofins_BaseCalculoPorProduto).Append('|');
      blocoTxt.Append(produto.PisSt_BaseCalculo).Append('|');
      blocoTxt.Append(produto.PisSt_Porcento).Append('|');
      blocoTxt.Append(produto.PisSt_BaseCalculoPorProduto).Append('|');
      blocoTxt.Append(produto.PisSt_Aliquota).Append('|');
      blocoTxt.Append(produto.PisSt_Valor).Append('|');
      blocoTxt.Append(produto.CofinsSt_BaseCalculo).Append('|');
      blocoTxt.Append(produto.CofinsSt_Porcento).Append('|');
      blocoTxt.Append(produto.CofinsSt_BaseCalculoPorProduto).Append('|');
      blocoTxt.Append(produto.CofinsSt_Aliquota).Append('|');
      blocoTxt.Append(produto.CofinsSt_Valor).Append('|');
      blocoTxt.Append(produto.Issqn_Deducao).Append('|');
      blocoTxt.Append(produto.Issqn_BaseCalculo).Append('|');
      blocoTxt.Append(0).Append('|');
      blocoTxt.Append(0).Append('|');

      txt.Add(blocoTxt.ToString());
      produto.Free;
    end;
  finally
    repo.Free;
    blocoTxt.Free;
  end;
end;

/// <summary>
///   M�todo que cria as linhas de pagamento do cupom
/// <param name="cupom">
///   instancia de TCriadorCupom
/// </param>
/// </summary>
procedure TCFeCriador.CriarLinhasPagemento(cupom: TCriadorCupom);
var
  blocoTxt: TStringBuilder;
  dados: TArray<string>;
begin
  blocoTxt := TStringBuilder.Create;
  try
    blocoTxt.Append('PG').Append('|');

    if cupom.Venda.FormaPagamento = 'Dinheiro' then
      blocoTxt.Append('01').Append('|')
    else if cupom.Venda.FormaPagamento = 'Cheque' then
      blocoTxt.Append('02').Append('|')
    else if cupom.Venda.FormaPagamento = 'Cart�o de cr�dito' then
      blocoTxt.Append('07').Append('|')
    else if cupom.Venda.FormaPagamento = 'Cart�o de d�bito' then
      blocoTxt.Append('06').Append('|')
    else if cupom.Venda.FormaPagamento = 'Cr�dito em loja' then
      blocoTxt.Append('04').Append('|')
    else if cupom.Venda.FormaPagamento = 'Vale alimenta��o' then
      blocoTxt.Append('03').Append('|')
    else if cupom.Venda.FormaPagamento = 'Vale refei��o' then
      blocoTxt.Append('05').Append('|')
    else if cupom.Venda.FormaPagamento = 'Vale presente' then
      blocoTxt.Append('86').Append('|')
    else if cupom.Venda.FormaPagamento = 'Vale compust�vel' then
      blocoTxt.Append('76').Append('|')
    else if cupom.Venda.FormaPagamento = 'Outros' then
      blocoTxt.Append('09').Append('|');

    blocoTxt.Append(Double((cupom.Venda.Valor +cupom.Venda.Acrescimo.GetValueOrDefault)-
    cupom.Venda.Desconto.GetValueOrDefault)).Append('|');
    dados :=cupom.Venda.OperadoraCartao.Split(['|']);

    if Length(dados) > 0 then
      blocoTxt.Append(dados[0]).Append('|')
    else
      blocoTxt.Append('').Append('|');

    txt.Add(blocoTxt.ToString());
  finally
    blocoTxt.Free;
  end;
end;

/// <summary>
///   M�todo destrutor para eliminar a instancia da TCFeCriador
/// </summary>
destructor TCFeCriador.Destroy;
begin
  txt.Free;
  inherited;
end;

/// <summary>
///   M�todo que salva o txt do cupom na pasta configurada
/// </summary>
procedure TCFeCriador.SalvarTxt;
var
  nomeArquivo: string;
  pasta: TPasta;
begin
  nomeArquivo:= 'VD' + FormatDateTime('ddmmyyyyhhnnsszzz',Now()) + '.SCP';

  pasta:= TPasta.Create;
  try
    if pasta.PastaCupom.EndsWith('\') then
      txt.SaveToFile(pasta.PastaCupom+nomeArquivo)
    else
      txt.SaveToFile(pasta.PastaCupom+ '\' + nomeArquivo);
  finally
    pasta.Free;
  end;

end;

end.
