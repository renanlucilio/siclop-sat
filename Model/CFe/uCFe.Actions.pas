unit uCFe.Actions;

interface

type
  TCfeActions = class
  private
  public
    procedure ListarXMLParaCancelar;
    procedure ListarArquivosDisponiveis(const pastaCUpom: string);
    procedure ListarNotas();
    procedure MostrarCFeTxt();
    procedure MostrarPDF();
  end;

implementation

{ TCfeActions }

procedure TCfeActions.ListarArquivosDisponiveis(const pastaCUpom: string);
begin

end;

procedure TCfeActions.ListarNotas;
begin

end;

procedure TCfeActions.ListarXMLParaCancelar;
begin

end;

procedure TCfeActions.MostrarCFeTxt;
begin

end;

procedure TCfeActions.MostrarPDF;
begin

end;

end.
