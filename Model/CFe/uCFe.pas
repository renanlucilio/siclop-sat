/// <summary>
///   Unit que tem a classe TDadosCFe
/// </summary>
unit uCFe;

interface

uses
  pcnCFe,
  uPagamento.CFe,
  uTotal.CFe,
  uItem.CFe,
  uProduto.CFe,
  uIdentificacao.CFe,
  uImposto.CFe,
  uCliente.CFe,
  System.Generics.Collections;

type
  TDadosCFe = class
  private
    FCliente:       TCliente;
    FIdentificacao: TIdentificacao;
    FItens:         TObjectList<TItem>;
    FPagamentos:    TObjectList<TPagamento>;
    FTotal:         TTotalCFe;
    FCriado: Boolean;
  public
    constructor Create();
    destructor Destroy(); override;

    property Criado:        Boolean read FCriado write FCriado;
    property Cliente:       TCliente                read FCliente       write FCliente;
    property Identificacao: TIdentificacao          read FIdentificacao write FIdentificacao;
    property Itens:         TObjectList<TItem>      read FItens         write FItens;
    property Pagamentos:    TObjectList<TPagamento> read FPagamentos    write FPagamentos;
    property Total:         TTotalCFe               read FTotal         write FTotal;

    procedure SetCFe(CFe: TCFe);
    function GetCFe(): TCFe;
  end;

implementation

uses
  System.SysUtils;

{ TCFe }

/// <summary>
///   M�todo construtor que cria as partes dos cupom
/// </summary>
constructor TDadosCFe.Create;
begin
  FCliente       := TCliente.Create;
  FIdentificacao := TIdentificacao.Create;
  FItens         := TObjectList<TItem>.Create();
  FPagamentos    := TObjectList<TPagamento>.Create();
  FTotal         := TTotalCFe.Create();
end;

/// <summary>
///   m�todo destrutor que elmina as partes do cupom
/// </summary>
destructor TDadosCFe.Destroy;
begin
  FreeAndNil(FCliente);
  FreeAndNil(FIdentificacao);
  FreeAndNil(FItens);
  FreeAndNil(FPagamentos);
  FreeAndNil(FTotal);
  inherited;
end;

/// <summary>
///   m�todo que retorna uma instancia de TCFe
/// <returns>
///   Instancia de TCFe
/// </returns>
/// </summary>
function TDadosCFe.GetCFe: TCFe;
begin
  result:= TCFe.Create;
  SetCFe(Result);
end;

/// <summary>
///   M�todo que define os dados TCFe
/// </summary>
procedure TDadosCFe.SetCFe(CFe: TCFe);
var
  item:  TItem;
  _item: TDetCollectionItem;
  id:    integer;
  item2: TPagamento;
  _pagamento: TMPCollectionItem;
begin

  Identificacao.SetIde(CFe.ide);
  Cliente.SetDest(cfe.Dest);

  for item in Itens do
  begin
    id:= Itens.IndexOf(item);
    _item:= CFe.Det.Add();
    _item.nItem := id+1;
    item.SetItem(CFe.Det.Items[id]);
    item.Imposto.SetImposto(_item.Imposto);

    if Itens.Last = item then
    begin
      if item.Produto.ValorAcrescimo < 0 then
        raise EArgumentException.Create('O Valor de acr�scimo n�o pode ser menor que 0');

      CFe.Det.Items[id].Prod.vOutro:= item.Produto.ValorAcrescimo;
    end;
  end;

  for item2 in Pagamentos do
  begin
    _pagamento:= CFe.Pagto.Add;
    item2.SetPagamento(_pagamento);
  end;

  Total.SetTotalCFe(CFe.Total, Itens.ToArray);
end;

end.
