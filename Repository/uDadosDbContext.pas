unit uDadosDbContext;

interface

uses
  uConexao.interfaces,
  uConexaoORM,
  ormbr.dml.generator.sqlite,
  FireDAC.Stan.Intf,FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
   FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
   FireDAC.Stan.Async, FireDAC.Phys, FireDAC.FMXUI.Wait, Data.DB,
   FireDAC.Comp.Client, FireDAC.Comp.UI, System.Classes;

type
//Classe de contexto para o dados DB
  TDadosDbContext = class
  private
    FORM:     TConexaoORM;
    connection: TFDConnection;

  public
    constructor Create(con: TFDConnection);
    destructor Destroy; override;

    property ORM:     TConexaoORM read FORM       write FORM;
  end;

implementation

uses
  ormbr.factory.interfaces,
  Model.LibUtil;


{ TDadosDbContext }

constructor TDadosDbContext.Create(con: TFDConnection);
begin
  connection :=  con;
  connection.Connected:= true;
  FORM:= TConexaoORM.Create(connection, dnSQLite);
end;

destructor TDadosDbContext.Destroy;
begin
  connection.Connected:= false;
  FORM.Free;
  inherited;
end;

end.
