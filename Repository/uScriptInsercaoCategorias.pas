unit uScriptInsercaoCategorias;

interface

uses uCategoria;

type
//classe que tem scripts de inser��o de algumas categorias
  TScriptInsercaoCategorias = class
  private
    pizzas: TCategoria;
    esfihas: TCategoria;
    refrigerantes: TCategoria;
    cervejas: TCategoria;
    fogazzas: TCategoria;
    sucos: TCategoria;
    aguas: TCategoria;
    vinhos: TCategoria;
    lanches: TCategoria;
    refeicoes: TCategoria;
    sobremesas: TCategoria;

    //m�todo que adiciona a categoria de pizza
    procedure DefinirPizzas();
        //m�todo que adiciona a categoria de esfihas
    procedure DefinirEsfihas();
        //m�todo que adiciona a categoria de refrigerantes
    procedure DefinirRefrigerantes();
        //m�todo que adiciona a categoria de cervejas
    procedure DefinirCervejas();
        //m�todo que adiciona a categoria de fogazzas
    procedure DefinirFogazzas();
        //m�todo que adiciona a categoria de sucos
    procedure DefinirSucos();
        //m�todo que adiciona a categoria de aguas
    procedure DefinirAguas();
        //m�todo que adiciona a categoria de vinhos
    procedure DefinirVinhos();
        //m�todo que adiciona a categoria de lanches
    procedure DefinirLanches();
        //m�todo que adiciona a categoria de refei��es
    procedure DefinirRefeicoes();
        //m�todo que adiciona a categoria de sobremesas
    procedure DefinirSobremesas();

  public
    constructor Create;
    destructor Destroy; override;

    procedure AdicionarCategorias();
  end;

implementation



{ TScriptInsercaoCategorias }

uses uCategoriaRepository, Form.Principal;

procedure TScriptInsercaoCategorias.AdicionarCategorias;
var
  repo: TCategoriaRepository;
begin
  repo := TCategoriaRepository.Create;
  try

    if FormHome.chkPizzas.IsChecked then
    begin
      DefinirPizzas;

      if not repo.Exist(pizzas.Descricao) then
        repo.Insert(pizzas);
    end;

    if FormHome.chkEsfihas.IsChecked then
    begin
      DefinirEsfihas;

      if not repo.Exist(esfihas.Descricao) then
        repo.Insert(esfihas);
    end;

    if FormHome.chkFogazzas.IsChecked then
    begin
      DefinirFogazzas;

      if not repo.Exist(fogazzas.Descricao) then
        repo.Insert(fogazzas);
    end;

    if FormHome.chkLanches.IsChecked then
    begin
      DefinirLanches;

      if not repo.Exist(lanches.Descricao) then
        repo.Insert(lanches);
    end;

    if FormHome.chkRefeicoes.IsChecked then
    begin
      DefinirRefeicoes;

      if not repo.Exist(refeicoes.Descricao) then
        repo.Insert(refeicoes);
    end;

    if FormHome.chkRefrigerantes.IsChecked then
    begin
      DefinirRefrigerantes;

      if not repo.Exist(refrigerantes.Descricao) then
        repo.Insert(refrigerantes);
    end;

    if FormHome.chkVinhos.IsChecked then
    begin
      DefinirVinhos;

      if not repo.Exist(vinhos.Descricao) then
        repo.Insert(vinhos);
    end;

    if FormHome.chkSucos.IsChecked then
    begin
      DefinirSucos;

      if not repo.Exist(sucos.Descricao) then
        repo.Insert(sucos);
    end;

    if FormHome.chkSobremesas.IsChecked then
    begin
      DefinirSobremesas;

      if not repo.Exist(sobremesas.Descricao) then
        repo.Insert(sobremesas);
    end;

    if FormHome.chkAgua.IsChecked then
    begin
      DefinirAguas;

      if not repo.Exist(aguas.Descricao) then
        repo.Insert(aguas);
    end;

    if FormHome.chkCervejas.IsChecked then
    begin
      DefinirCervejas;

      if not repo.Exist(cervejas.Descricao) then
        repo.Insert(cervejas);
    end;


  finally
    FormHome.MostraMensage('Categorias atualizadas!');
    FormHome.chkPizzas.IsChecked:= false;
    FormHome.chkEsfihas.IsChecked:= false;
    FormHome.chkFogazzas.IsChecked:= false;
    FormHome.chkLanches.IsChecked:= false;
    FormHome.chkRefeicoes.IsChecked:= false;
    FormHome.chkRefrigerantes.IsChecked:= false;
    FormHome.chkVinhos.IsChecked:= false;
    FormHome.chkSucos.IsChecked:= false;
    repo.Free;
  end;
end;

constructor TScriptInsercaoCategorias.Create;
begin
  pizzas:= TCategoria.Create;
  esfihas:= TCategoria.Create;
  refrigerantes:= TCategoria.Create;
  cervejas:= TCategoria.Create;
  fogazzas:= TCategoria.Create;
  sucos:= TCategoria.Create;
  aguas:= TCategoria.Create;
  vinhos:= TCategoria.Create;
  lanches:= TCategoria.Create;
  refeicoes:= TCategoria.Create;
  sobremesas:= TCategoria.Create;
end;

procedure TScriptInsercaoCategorias.DefinirAguas;
begin
  aguas.Descricao:= '�GUA';
  aguas.Ncm:= '22011000';
  aguas.Cest:= '0';
  aguas.Cfop:= '5405';

  aguas.Pis_BaseCalculo:= 0;
  aguas.Pis_CST:= 49;
  aguas.Pis_Porcento:= 0;
  aguas.Pis_Valor:= 0;
  aguas.Pis_BaseCalculoPorProduto:= 0;
  aguas.Pis_Aliquota:= 0;

  aguas.PisSt_BaseCalculo:= 0;
  aguas.PisSt_Porcento:= 0;
  aguas.PisSt_Valor:= 0;
  aguas.PisSt_BaseCalculoPorProduto:= 0;
  aguas.PisSt_Aliquota:= 0;

  aguas.Cofins_CST:= 49;
  aguas.Cofins_BaseCalculo:= 0;
  aguas.Cofins_Porcento:= 0;
  aguas.Cofins_Valor:= 0;
  aguas.Cofins_BaseCalculoPorProduto:= 0;
  aguas.Cofins_Aliquota:= 0;

  aguas.CofinsSt_BaseCalculo:= 0;
  aguas.CofinsSt_Porcento:= 0;
  aguas.CofinsSt_Valor:= 0;
  aguas.CofinsSt_BaseCalculoPorProduto:= 0;
  aguas.CofinsSt_Aliquota:= 0;

  aguas.Icms_Origem:= 0;
  aguas.Icms_Cst:= 102;
  aguas.Icms_Porcento:= 0;
  aguas.Icms_Valor:= 0;

  aguas.Issqn_Natureza:= 0;
  aguas.Issqn_Deducao:= 0;
  aguas.Issqn_CodigoIbge:= '';
  aguas.Issqn_BaseCalculoProduto:= 0;
  aguas.Issqn_ItemDaListaServico:= '';
  aguas.Issqn_CodigoTributarioIssqn:= '';
  aguas.Issqn_BaseCalculo:= 0;

  aguas.TaxaEstadual:= 0;
  aguas.TaxaFederal:= 0;
end;

procedure TScriptInsercaoCategorias.DefinirCervejas;
begin
  cervejas.Descricao:= 'CERVEJAS';
  cervejas.Ncm:= '22030000';
  cervejas.Cest:= '';
  cervejas.Cfop:= '5405';

  cervejas.Pis_BaseCalculo:= 0;
  cervejas.Pis_CST:= 49;
  cervejas.Pis_Porcento:= 0;
  cervejas.Pis_Valor:= 0;
  cervejas.Pis_BaseCalculoPorProduto:= 0;
  cervejas.Pis_Aliquota:= 0;

  cervejas.PisSt_BaseCalculo:= 0;
  cervejas.PisSt_Porcento:= 0;
  cervejas.PisSt_Valor:= 0;
  cervejas.PisSt_BaseCalculoPorProduto:= 0;
  cervejas.PisSt_Aliquota:= 0;

  cervejas.Cofins_CST:= 49;
  cervejas.Cofins_BaseCalculo:= 0;
  cervejas.Cofins_Porcento:= 0;
  cervejas.Cofins_Valor:= 0;
  cervejas.Cofins_BaseCalculoPorProduto:= 0;
  cervejas.Cofins_Aliquota:= 0;

  cervejas.CofinsSt_BaseCalculo:= 0;
  cervejas.CofinsSt_Porcento:= 0;
  cervejas.CofinsSt_Valor:= 0;
  cervejas.CofinsSt_BaseCalculoPorProduto:= 0;
  cervejas.CofinsSt_Aliquota:= 0;

  cervejas.Icms_Origem:= 0;
  cervejas.Icms_Cst:= 102;
  cervejas.Icms_Porcento:= 0;
  cervejas.Icms_Valor:= 0;

  cervejas.Issqn_Natureza:= 0;
  cervejas.Issqn_Deducao:= 0;
  cervejas.Issqn_CodigoIbge:= '';
  cervejas.Issqn_BaseCalculoProduto:= 0;
  cervejas.Issqn_ItemDaListaServico:= '';
  cervejas.Issqn_CodigoTributarioIssqn:= '';
  cervejas.Issqn_BaseCalculo:= 0;

  cervejas.TaxaEstadual:= 0;
  cervejas.TaxaFederal:= 0;
end;

procedure TScriptInsercaoCategorias.DefinirEsfihas;
begin
  esfihas.Descricao:= 'ESFIHAS';
  esfihas.Ncm:= '19022000';
  esfihas.Cest:= '';
  esfihas.Cfop:= '5102';

  esfihas.Pis_BaseCalculo:= 0;
  esfihas.Pis_CST:= 49;
  esfihas.Pis_Porcento:= 0;
  esfihas.Pis_Valor:= 0;
  esfihas.Pis_BaseCalculoPorProduto:= 0;
  esfihas.Pis_Aliquota:= 0;

  esfihas.PisSt_BaseCalculo:= 0;
  esfihas.PisSt_Porcento:= 0;
  esfihas.PisSt_Valor:= 0;
  esfihas.PisSt_BaseCalculoPorProduto:= 0;
  esfihas.PisSt_Aliquota:= 0;

  esfihas.Cofins_CST:= 49;
  esfihas.Cofins_BaseCalculo:= 0;
  esfihas.Cofins_Porcento:= 0;
  esfihas.Cofins_Valor:= 0;
  esfihas.Cofins_BaseCalculoPorProduto:= 0;
  esfihas.Cofins_Aliquota:= 0;

  esfihas.CofinsSt_BaseCalculo:= 0;
  esfihas.CofinsSt_Porcento:= 0;
  esfihas.CofinsSt_Valor:= 0;
  esfihas.CofinsSt_BaseCalculoPorProduto:= 0;
  esfihas.CofinsSt_Aliquota:= 0;

  esfihas.Icms_Origem:= 0;
  esfihas.Icms_Cst:= 102;
  esfihas.Icms_Porcento:= 0;
  esfihas.Icms_Valor:= 0;

  esfihas.Issqn_Natureza:= 0;
  esfihas.Issqn_Deducao:= 0;
  esfihas.Issqn_CodigoIbge:= '';
  esfihas.Issqn_BaseCalculoProduto:= 0;
  esfihas.Issqn_ItemDaListaServico:= '';
  esfihas.Issqn_CodigoTributarioIssqn:= '';
  esfihas.Issqn_BaseCalculo:= 0;

  esfihas.TaxaEstadual:= 0;
  esfihas.TaxaFederal:= 0;
end;

procedure TScriptInsercaoCategorias.DefinirFogazzas;
begin
  fogazzas.Descricao:= 'FOGAZZAS';
  fogazzas.Ncm:= '19022000';
  fogazzas.Cest:= '0';
  fogazzas.Cfop:= '5102';

  fogazzas.Pis_BaseCalculo:= 0;
  fogazzas.Pis_CST:= 49;
  fogazzas.Pis_Porcento:= 0;
  fogazzas.Pis_Valor:= 0;
  fogazzas.Pis_BaseCalculoPorProduto:= 0;
  fogazzas.Pis_Aliquota:= 0;

  fogazzas.PisSt_BaseCalculo:= 0;
  fogazzas.PisSt_Porcento:= 0;
  fogazzas.PisSt_Valor:= 0;
  fogazzas.PisSt_BaseCalculoPorProduto:= 0;
  fogazzas.PisSt_Aliquota:= 0;

  fogazzas.Cofins_CST:= 49;
  fogazzas.Cofins_BaseCalculo:= 0;
  fogazzas.Cofins_Porcento:= 0;
  fogazzas.Cofins_Valor:= 0;
  fogazzas.Cofins_BaseCalculoPorProduto:= 0;
  fogazzas.Cofins_Aliquota:= 0;

  fogazzas.CofinsSt_BaseCalculo:= 0;
  fogazzas.CofinsSt_Porcento:= 0;
  fogazzas.CofinsSt_Valor:= 0;
  fogazzas.CofinsSt_BaseCalculoPorProduto:= 0;
  fogazzas.CofinsSt_Aliquota:= 0;

  fogazzas.Icms_Origem:= 0;
  fogazzas.Icms_Cst:= 102;
  fogazzas.Icms_Porcento:= 0;
  fogazzas.Icms_Valor:= 0;

  fogazzas.Issqn_Natureza:= 0;
  fogazzas.Issqn_Deducao:= 0;
  fogazzas.Issqn_CodigoIbge:= '';
  fogazzas.Issqn_BaseCalculoProduto:= 0;
  fogazzas.Issqn_ItemDaListaServico:= '';
  fogazzas.Issqn_CodigoTributarioIssqn:= '';
  fogazzas.Issqn_BaseCalculo:= 0;

  fogazzas.TaxaEstadual:= 0;
  fogazzas.TaxaFederal:= 0;
end;

procedure TScriptInsercaoCategorias.DefinirLanches;
begin
  lanches.Descricao:= 'LANCHES';
  lanches.Ncm:= '21069090';
  lanches.Cest:= '0';
  lanches.Cfop:= '5102';

  lanches.Pis_BaseCalculo:= 0;
  lanches.Pis_CST:= 49;
  lanches.Pis_Porcento:= 0;
  lanches.Pis_Valor:= 0;
  lanches.Pis_BaseCalculoPorProduto:= 0;
  lanches.Pis_Aliquota:= 0;

  lanches.PisSt_BaseCalculo:= 0;
  lanches.PisSt_Porcento:= 0;
  lanches.PisSt_Valor:= 0;
  lanches.PisSt_BaseCalculoPorProduto:= 0;
  lanches.PisSt_Aliquota:= 0;

  lanches.Cofins_CST:= 49;
  lanches.Cofins_BaseCalculo:= 0;
  lanches.Cofins_Porcento:= 0;
  lanches.Cofins_Valor:= 0;
  lanches.Cofins_BaseCalculoPorProduto:= 0;
  lanches.Cofins_Aliquota:= 0;

  lanches.CofinsSt_BaseCalculo:= 0;
  lanches.CofinsSt_Porcento:= 0;
  lanches.CofinsSt_Valor:= 0;
  lanches.CofinsSt_BaseCalculoPorProduto:= 0;
  lanches.CofinsSt_Aliquota:= 0;

  lanches.Icms_Origem:= 0;
  lanches.Icms_Cst:= 102;
  lanches.Icms_Porcento:= 0;
  lanches.Icms_Valor:= 0;

  lanches.Issqn_Natureza:= 0;
  lanches.Issqn_Deducao:= 0;
  lanches.Issqn_CodigoIbge:= '';
  lanches.Issqn_BaseCalculoProduto:= 0;
  lanches.Issqn_ItemDaListaServico:= '';
  lanches.Issqn_CodigoTributarioIssqn:= '';
  lanches.Issqn_BaseCalculo:= 0;

  lanches.TaxaEstadual:= 0;
  lanches.TaxaFederal:= 0;
end;

procedure TScriptInsercaoCategorias.DefinirPizzas;
begin
  pizzas.Descricao:= 'PIZZAS';
  pizzas.Ncm:= '19022000';
  pizzas.Cest:= '0';
  pizzas.Cfop:= '5102';

  pizzas.Pis_BaseCalculo:= 0;
  pizzas.Pis_CST:= 49;
  pizzas.Pis_Porcento:= 0;
  pizzas.Pis_Valor:= 0;
  pizzas.Pis_BaseCalculoPorProduto:= 0;
  pizzas.Pis_Aliquota:= 0;

  pizzas.PisSt_BaseCalculo:= 0;
  pizzas.PisSt_Porcento:= 0;
  pizzas.PisSt_Valor:= 0;
  pizzas.PisSt_BaseCalculoPorProduto:= 0;
  pizzas.PisSt_Aliquota:= 0;

  pizzas.Cofins_CST:= 49;
  pizzas.Cofins_BaseCalculo:= 0;
  pizzas.Cofins_Porcento:= 0;
  pizzas.Cofins_Valor:= 0;
  pizzas.Cofins_BaseCalculoPorProduto:= 0;
  pizzas.Cofins_Aliquota:= 0;

  pizzas.CofinsSt_BaseCalculo:= 0;
  pizzas.CofinsSt_Porcento:= 0;
  pizzas.CofinsSt_Valor:= 0;
  pizzas.CofinsSt_BaseCalculoPorProduto:= 0;
  pizzas.CofinsSt_Aliquota:= 0;

  pizzas.Icms_Origem:= 0;
  pizzas.Icms_Cst:= 102;
  pizzas.Icms_Porcento:= 0;
  pizzas.Icms_Valor:= 0;

  pizzas.Issqn_Natureza:= 0;
  pizzas.Issqn_Deducao:= 0;
  pizzas.Issqn_CodigoIbge:= '';
  pizzas.Issqn_BaseCalculoProduto:= 0;
  pizzas.Issqn_ItemDaListaServico:= '';
  pizzas.Issqn_CodigoTributarioIssqn:= '';
  pizzas.Issqn_BaseCalculo:= 0;

  pizzas.TaxaEstadual:= 0;
  pizzas.TaxaFederal:= 0;

end;

procedure TScriptInsercaoCategorias.DefinirRefeicoes;
begin
  refeicoes.Descricao:= 'REFEI��ES';
  refeicoes.Ncm:= '21069090';
  refeicoes.Cest:= '0';
  refeicoes.Cfop:= '5102';

  refeicoes.Pis_BaseCalculo:= 0;
  refeicoes.Pis_CST:= 49;
  refeicoes.Pis_Porcento:= 0;
  refeicoes.Pis_Valor:= 0;
  refeicoes.Pis_BaseCalculoPorProduto:= 0;
  refeicoes.Pis_Aliquota:= 0;

  refeicoes.PisSt_BaseCalculo:= 0;
  refeicoes.PisSt_Porcento:= 0;
  refeicoes.PisSt_Valor:= 0;
  refeicoes.PisSt_BaseCalculoPorProduto:= 0;
  refeicoes.PisSt_Aliquota:= 0;

  refeicoes.Cofins_CST:= 49;
  refeicoes.Cofins_BaseCalculo:= 0;
  refeicoes.Cofins_Porcento:= 0;
  refeicoes.Cofins_Valor:= 0;
  refeicoes.Cofins_BaseCalculoPorProduto:= 0;
  refeicoes.Cofins_Aliquota:= 0;

  refeicoes.CofinsSt_BaseCalculo:= 0;
  refeicoes.CofinsSt_Porcento:= 0;
  refeicoes.CofinsSt_Valor:= 0;
  refeicoes.CofinsSt_BaseCalculoPorProduto:= 0;
  refeicoes.CofinsSt_Aliquota:= 0;

  refeicoes.Icms_Origem:= 0;
  refeicoes.Icms_Cst:= 102;
  refeicoes.Icms_Porcento:= 0;
  refeicoes.Icms_Valor:= 0;

  refeicoes.Issqn_Natureza:= 0;
  refeicoes.Issqn_Deducao:= 0;
  refeicoes.Issqn_CodigoIbge:= '';
  refeicoes.Issqn_BaseCalculoProduto:= 0;
  refeicoes.Issqn_ItemDaListaServico:= '';
  refeicoes.Issqn_CodigoTributarioIssqn:= '';
  refeicoes.Issqn_BaseCalculo:= 0;

  refeicoes.TaxaEstadual:= 0;
  refeicoes.TaxaFederal:= 0;
end;

procedure TScriptInsercaoCategorias.DefinirRefrigerantes;
begin
  refrigerantes.Descricao:= 'REFRIGERANTES';
  refrigerantes.Ncm:= '22021000';
  refrigerantes.Cest:= '';
  refrigerantes.Cfop:= '5405';

  refrigerantes.Pis_BaseCalculo:= 0;
  refrigerantes.Pis_CST:= 49;
  refrigerantes.Pis_Porcento:= 0;
  refrigerantes.Pis_Valor:= 0;
  refrigerantes.Pis_BaseCalculoPorProduto:= 0;
  refrigerantes.Pis_Aliquota:= 0;

  refrigerantes.PisSt_BaseCalculo:= 0;
  refrigerantes.PisSt_Porcento:= 0;
  refrigerantes.PisSt_Valor:= 0;
  refrigerantes.PisSt_BaseCalculoPorProduto:= 0;
  refrigerantes.PisSt_Aliquota:= 0;

  refrigerantes.Cofins_CST:= 49;
  refrigerantes.Cofins_BaseCalculo:= 0;
  refrigerantes.Cofins_Porcento:= 0;
  refrigerantes.Cofins_Valor:= 0;
  refrigerantes.Cofins_BaseCalculoPorProduto:= 0;
  refrigerantes.Cofins_Aliquota:= 0;

  refrigerantes.CofinsSt_BaseCalculo:= 0;
  refrigerantes.CofinsSt_Porcento:= 0;
  refrigerantes.CofinsSt_Valor:= 0;
  refrigerantes.CofinsSt_BaseCalculoPorProduto:= 0;
  refrigerantes.CofinsSt_Aliquota:= 0;

  refrigerantes.Icms_Origem:= 0;
  refrigerantes.Icms_Cst:= 102;
  refrigerantes.Icms_Porcento:= 0;
  refrigerantes.Icms_Valor:= 0;

  refrigerantes.Issqn_Natureza:= 0;
  refrigerantes.Issqn_Deducao:= 0;
  refrigerantes.Issqn_CodigoIbge:= '';
  refrigerantes.Issqn_BaseCalculoProduto:= 0;
  refrigerantes.Issqn_ItemDaListaServico:= '';
  refrigerantes.Issqn_CodigoTributarioIssqn:= '';
  refrigerantes.Issqn_BaseCalculo:= 0;

  refrigerantes.TaxaEstadual:= 0;
  refrigerantes.TaxaFederal:= 0;
end;

procedure TScriptInsercaoCategorias.DefinirSobremesas;
begin
  sobremesas.Descricao:= 'SOBREMESAS';
  sobremesas.Ncm:= '21069029';
  sobremesas.Cest:= '0';
  sobremesas.Cfop:= '5102';

  sobremesas.Pis_BaseCalculo:= 0;
  sobremesas.Pis_CST:= 49;
  sobremesas.Pis_Porcento:= 0;
  sobremesas.Pis_Valor:= 0;
  sobremesas.Pis_BaseCalculoPorProduto:= 0;
  sobremesas.Pis_Aliquota:= 0;

  sobremesas.PisSt_BaseCalculo:= 0;
  sobremesas.PisSt_Porcento:= 0;
  sobremesas.PisSt_Valor:= 0;
  sobremesas.PisSt_BaseCalculoPorProduto:= 0;
  sobremesas.PisSt_Aliquota:= 0;

  sobremesas.Cofins_CST:= 49;
  sobremesas.Cofins_BaseCalculo:= 0;
  sobremesas.Cofins_Porcento:= 0;
  sobremesas.Cofins_Valor:= 0;
  sobremesas.Cofins_BaseCalculoPorProduto:= 0;
  sobremesas.Cofins_Aliquota:= 0;

  sobremesas.CofinsSt_BaseCalculo:= 0;
  sobremesas.CofinsSt_Porcento:= 0;
  sobremesas.CofinsSt_Valor:= 0;
  sobremesas.CofinsSt_BaseCalculoPorProduto:= 0;
  sobremesas.CofinsSt_Aliquota:= 0;

  sobremesas.Icms_Origem:= 0;
  sobremesas.Icms_Cst:= 102;
  sobremesas.Icms_Porcento:= 0;
  sobremesas.Icms_Valor:= 0;

  sobremesas.Issqn_Natureza:= 0;
  sobremesas.Issqn_Deducao:= 0;
  sobremesas.Issqn_CodigoIbge:= '';
  sobremesas.Issqn_BaseCalculoProduto:= 0;
  sobremesas.Issqn_ItemDaListaServico:= '';
  sobremesas.Issqn_CodigoTributarioIssqn:= '';
  sobremesas.Issqn_BaseCalculo:= 0;

  sobremesas.TaxaEstadual:= 0;
  sobremesas.TaxaFederal:= 0;
end;

procedure TScriptInsercaoCategorias.DefinirSucos;
begin
  sucos.Descricao:= 'SUCOS';
  sucos.Ncm:= '22029900';
  sucos.Cest:= '0';
  sucos.Cfop:= '5405';

  sucos.Pis_BaseCalculo:= 0;
  sucos.Pis_CST:= 49;
  sucos.Pis_Porcento:= 0;
  sucos.Pis_Valor:= 0;
  sucos.Pis_BaseCalculoPorProduto:= 0;
  sucos.Pis_Aliquota:= 0;

  sucos.PisSt_BaseCalculo:= 0;
  sucos.PisSt_Porcento:= 0;
  sucos.PisSt_Valor:= 0;
  sucos.PisSt_BaseCalculoPorProduto:= 0;
  sucos.PisSt_Aliquota:= 0;

  sucos.Cofins_CST:= 49;
  sucos.Cofins_BaseCalculo:= 0;
  sucos.Cofins_Porcento:= 0;
  sucos.Cofins_Valor:= 0;
  sucos.Cofins_BaseCalculoPorProduto:= 0;
  sucos.Cofins_Aliquota:= 0;

  sucos.CofinsSt_BaseCalculo:= 0;
  sucos.CofinsSt_Porcento:= 0;
  sucos.CofinsSt_Valor:= 0;
  sucos.CofinsSt_BaseCalculoPorProduto:= 0;
  sucos.CofinsSt_Aliquota:= 0;

  sucos.Icms_Origem:= 0;
  sucos.Icms_Cst:= 102;
  sucos.Icms_Porcento:= 0;
  sucos.Icms_Valor:= 0;

  sucos.Issqn_Natureza:= 0;
  sucos.Issqn_Deducao:= 0;
  sucos.Issqn_CodigoIbge:= '';
  sucos.Issqn_BaseCalculoProduto:= 0;
  sucos.Issqn_ItemDaListaServico:= '';
  sucos.Issqn_CodigoTributarioIssqn:= '';
  sucos.Issqn_BaseCalculo:= 0;

  sucos.TaxaEstadual:= 0;
  sucos.TaxaFederal:= 0;
end;

procedure TScriptInsercaoCategorias.DefinirVinhos;
begin
  vinhos.Descricao:= 'VINHOS';
  vinhos.Ncm:= '22042910';
  vinhos.Cest:= '0';
  vinhos.Cfop:= '5405';

  vinhos.Pis_BaseCalculo:= 0;
  vinhos.Pis_CST:= 49;
  vinhos.Pis_Porcento:= 0;
  vinhos.Pis_Valor:= 0;
  vinhos.Pis_BaseCalculoPorProduto:= 0;
  vinhos.Pis_Aliquota:= 0;

  vinhos.PisSt_BaseCalculo:= 0;
  vinhos.PisSt_Porcento:= 0;
  vinhos.PisSt_Valor:= 0;
  vinhos.PisSt_BaseCalculoPorProduto:= 0;
  vinhos.PisSt_Aliquota:= 0;

  vinhos.Cofins_CST:= 49;
  vinhos.Cofins_BaseCalculo:= 0;
  vinhos.Cofins_Porcento:= 0;
  vinhos.Cofins_Valor:= 0;
  vinhos.Cofins_BaseCalculoPorProduto:= 0;
  vinhos.Cofins_Aliquota:= 0;

  vinhos.CofinsSt_BaseCalculo:= 0;
  vinhos.CofinsSt_Porcento:= 0;
  vinhos.CofinsSt_Valor:= 0;
  vinhos.CofinsSt_BaseCalculoPorProduto:= 0;
  vinhos.CofinsSt_Aliquota:= 0;

  vinhos.Icms_Origem:= 0;
  vinhos.Icms_Cst:= 102;
  vinhos.Icms_Porcento:= 0;
  vinhos.Icms_Valor:= 0;

  vinhos.Issqn_Natureza:= 0;
  vinhos.Issqn_Deducao:= 0;
  vinhos.Issqn_CodigoIbge:= '';
  vinhos.Issqn_BaseCalculoProduto:= 0;
  vinhos.Issqn_ItemDaListaServico:= '';
  vinhos.Issqn_CodigoTributarioIssqn:= '';
  vinhos.Issqn_BaseCalculo:= 0;

  vinhos.TaxaEstadual:= 0;
  vinhos.TaxaFederal:= 0;
end;

destructor TScriptInsercaoCategorias.Destroy;
begin
  pizzas.Free;
  esfihas.Free;
  refrigerantes.Free;
  cervejas.Free;
  fogazzas.Free;
  sucos.Free;
  aguas.Free;
  vinhos.Free;
  lanches.Free;
  refeicoes.Free;
  sobremesas.Free;
  inherited;
end;

end.
