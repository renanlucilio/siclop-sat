unit uAuRepository;

interface

uses uAuditoria, System.Generics.Collections, System.DateUtils, System.SysUtils,
  uAuditoriaContexto;

type
//Classe usada de repository para o contexto de auditoria
  TAuditoriaRepository = class
  private
    contexto: TAuditoriaContext;
    //M�todo gen�rico para gravar uma ocorrencia no bd
    procedure Gravar(resumo, descricao: string; categoria: char);
    //m�todo que retorna todas as ocorrencias
    function GetAll(): TObjectList<TAuditoria>;
    //m�todo usado para filtrar uma lista por categoria
    procedure FiltrarPorCategoria(categoria: Char;auds: TObjectList<TAuditoria>);
    //m�todo usado para filtrar por uma data
    procedure FiltrarPorData(data: TDate; auds: TObjectList<TAuditoria>);
  public
    constructor Create;
    destructor Destroy; override;

    //m�todo est�tico para gravar um log
    class procedure GravarLog(resumo, descricao: string; categoria: char);

    //m�todo usado para listar um tipo de ocorrencias
    function ListarOcorrencias(data: TDate; categoria: Char): TObjectList<TAuditoria>;
    //m�todo usado para carregar um ocorrencia especifica
    function Get(id: integer): TAuditoria;
  end;

implementation

{ TAuditoriaRepository }

uses uDmCons;

constructor TAuditoriaRepository.Create;
begin
  contexto:= TAuditoriaContext.Create(dmCons.conAuditoria);
end;

destructor TAuditoriaRepository.Destroy;
begin
  contexto.Free;
  inherited;
end;


procedure TAuditoriaRepository.FiltrarPorCategoria(categoria: Char;auds: TObjectList<TAuditoria>);
var
  listaParaDeletar: TObjectList<TAuditoria>;
  it: TAuditoria;
  paraDeletar: TAuditoria;
begin
  listaParaDeletar:= TObjectList<TAuditoria>.Create;

  for it in auds do
  begin
    if it.Categoria <> categoria then
      listaParaDeletar.Add(it);
  end;

  for it in listaParaDeletar do
  begin
    paraDeletar:= auds.Extract(it);
    paraDeletar.Free;
  end;
end;

procedure TAuditoriaRepository.FiltrarPorData(data: TDate; auds: TObjectList<TAuditoria>);
var
  listaParaDeletar: TObjectList<TAuditoria>;
  it: TAuditoria;
  paraDeletar: TAuditoria;
begin
  listaParaDeletar:= TObjectList<TAuditoria>.Create;

  for it in auds do
  begin
    if DateOf(StrToDate(it.DataHora)) <> Date then
      listaParaDeletar.Add(it);
  end;

  for it in listaParaDeletar do
  begin
    paraDeletar:= auds.Extract(it);
    paraDeletar.Free;
  end;
end;

function TAuditoriaRepository.Get(id: integer): TAuditoria;
begin
  result:= nil;

  result:= contexto.ORM.DAO<TAuditoria>().Find(id);
end;

function TAuditoriaRepository.GetAll: TObjectList<TAuditoria>;
begin
  result:= contexto.ORM.DAO<TAuditoria>().Find();
end;

procedure TAuditoriaRepository.Gravar(resumo, descricao: string;
  categoria: char);
var
  aud: TAuditoria;
begin
  aud:= TAuditoria.Create;
  try
    aud.Resumo:= resumo;
    aud.Descricao:= descricao;
    aud.Categoria:= categoria;
    aud.DataHora:= DateToStr(DateOf(Now));

    contexto.ORM.DAO<TAuditoria>().Insert(aud);
  finally
    aud.Free;
  end;
end;

class procedure TAuditoriaRepository.GravarLog(resumo, descricao: string;
  categoria: char);
var
  repo: TAuditoriaRepository;
begin
  repo:= TAuditoriaRepository.Create();
  try
    try
      repo.Gravar(resumo, descricao, categoria);
    except
    end;
  finally
    repo.Free;
  end;
end;

function TAuditoriaRepository.ListarOcorrencias(data: TDate;
  categoria: Char): TObjectList<TAuditoria>;
var
  lista: TObjectList<TAuditoria>;
begin
  lista:= GetAll();
  FiltrarPorCategoria(categoria, lista);
  FiltrarPorData(data, lista);

  Result:= lista;
end;

end.
