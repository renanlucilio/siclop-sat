unit uTributacaoRepository;

interface
uses
  System.Generics.Collections, uDadosDbContext, uDmCons, uTributacao;

type
  TTributacaoRepository = class
  private
    contexto: TDadosDbContext;
    procedure EditarCofins(model: TTributacao);
    procedure EditarCofinsSt(model: TTributacao);
    procedure EditarPis(model: TTributacao);
    procedure EditarPisSt(model: TTributacao);
    procedure EditarIcms(model: TTributacao);
    procedure EditarIssqn(model: TTributacao);
    procedure EditarTaxas(model: TTributacao);

    function InsertCofins(model: TTributacao): Integer;
    function InsertCofinsSt(model: TTributacao): Integer;
    function InsertPis(model: TTributacao): Integer;
    function InsertPisSt(model: TTributacao): Integer;
    function InsertIcms(model: TTributacao): Integer;
    function InsertIssqn(model: TTributacao): Integer;
    function InsertTaxas(model: TTributacao): Integer;

  public
    constructor Create;
    destructor Destroy; override;

    function InitEdit(id: integer): TTributacao;
    procedure Edit(model: TTributacao);

    procedure Insert(model: TTributacao);
    function Delete(id: integer): TTributacao;
    procedure SelectAll();
    function GetAll(): TObjectList<TTributacao>;
   end;

implementation

uses
  uConexao.Query,uLibUtil, System.SysUtils, uCofins, uPis, uPisSt, uIcms,
  uIssqn, uTaxasFiscais, uCofinsSt;


{ TTributacaoRepository }

constructor TTributacaoRepository.Create;
begin
  contexto:= TDadosDbContext.Create(dmCons.conDados);
end;

function TTributacaoRepository.Delete(id: integer): TTributacao;
var
  model: TTributacao;
begin
  model:= contexto
    .ORM
    .DAO<TTributacao>()
    .Find(id);

  if Assigned(model) then
    contexto
      .ORM
      .DAO<TTributacao>()
      .Delete(model);

end;

destructor TTributacaoRepository.Destroy;
begin
  contexto.Free;
  inherited;
end;

procedure TTributacaoRepository.Edit(model: TTributacao);
var
  qry: TQuery;
begin
  if Assigned(model) then
  begin
    try
      try
        qry:= TQuery.Create(dmCons.conDados);
        qry.ref;

        qry.SQL.Update('Tributacao')
          .&Set('Descricao', model.Descricao.GetValueOrDefault.ToUpper().QuotedString())
          .&Set('Ncm', model.Ncm.GetValueOrDefault.ToUpper().QuotedString())
          .&Set('Cest', model.Cest.GetValueOrDefault.ToUpper().QuotedString())
          .&Set('Cfop', model.Cfop.GetValueOrDefault.ToUpper().QuotedString())
        .Where('ID =' + model.Id.Value.ToString());

        EditarCofins(model);
        EditarCofinsSt(model);
        EditarPis(model);
        EditarPisSt(model);
        EditarIcms(model);
        EditarIssqn(model);
        EditarTaxas(model);

        qry.Exec();

        SelectAll();
      except
        raise Exception.Create('Erro ao editar produto');
      end;

    finally
      model.Free;
    end;
  end;
end;

procedure TTributacaoRepository.EditarCofins(model: TTributacao);
var
  qry: TQuery;
begin
  if Assigned(model) then
  begin
      try
        qry:= TQuery.Create(dmCons.conDados);
        qry.ref;

        qry.SQL.Update('Cofins')
          .&Set('BaseCalculo', CurrToStr(model.Cofins.BaseCalculo))
          .&Set('Porcento', CurrToStr(model.Cofins.Porcento))
          .&Set('Valor', CurrToStr(model.Cofins.Valor))
          .&Set('BaseCalculoPorProduto', CurrToStr(model.Cofins.BaseCalculoPorProduto))
          .&Set('Aliquota', CurrToStr(model.Cofins.Aliquota))
          .&Set('CST', model.Cofins.CST.ToString())
        .Where('ID =' + model.CofinsId.ToString());


        qry.Exec();
      except
        raise Exception.Create('Erro ao editar cofins');
      end;
  end;
end;

procedure TTributacaoRepository.EditarCofinsSt(model: TTributacao);
var
  qry: TQuery;
begin
  if Assigned(model) then
  begin
      try
        qry:= TQuery.Create(dmCons.conDados);
        qry.ref;

        qry.SQL.Update('CofinsSt')
          .&Set('BaseCalculo', CurrToStr(model.CofinsSt.BaseCalculo))
          .&Set('Porcento', CurrToStr(model.CofinsSt.Porcento))
          .&Set('Valor', CurrToStr(model.CofinsSt.Valor))
          .&Set('BaseCalculoPorProduto', CurrToStr(model.CofinsSt.BaseCalculoPorProduto))
          .&Set('Aliquota', CurrToStr(model.CofinsSt.Aliquota))
        .Where('ID =' + model.CofinsStId.Value.ToString());


        qry.Exec();
      except
        raise Exception.Create('Erro ao editar cofinsst');
      end;
  end;
end;

procedure TTributacaoRepository.EditarIcms(model: TTributacao);
var
  qry: TQuery;
begin
  if Assigned(model) then
  begin
      try
        qry:= TQuery.Create(dmCons.conDados);
        qry.ref;

        qry.SQL.Update('Icms')
          .&Set('Origem', model.Icms.Origem.ToString)
          .&Set('Cst', model.Icms.Cst.ToString)
          .&Set('Porcento', CurrToStr(model.Icms.Porcento))
          .&Set('Valor', CurrToStr(model.Icms.Valor))
        .Where('ID =' + model.IcmsId.ToString());


        qry.Exec();
      except
        raise Exception.Create('Erro ao editar Icms');
      end;
  end;
end;


procedure TTributacaoRepository.EditarIssqn(model: TTributacao);
var
  qry: TQuery;
begin
  if Assigned(model) then
  begin
      try
        qry:= TQuery.Create(dmCons.conDados);
        qry.ref;

        qry.SQL.Update('Issqn')
          .&Set('BaseCalculo', CurrToStr(model.Issqn.BaseCalculo))
          .&Set('Deducao', CurrToStr(model.Issqn.Deducao))
          .&Set('CodigoIbge', model.Issqn.CodigoIbge.Value.QuotedString)
          .&Set('BaseCalculoProduto', CurrToStr(model.Issqn.BaseCalculoProduto))
          .&Set('ItemDaListaServico', model.Issqn.ItemDaListaServico.Value.QuotedString)
          .&Set('CodigoTributarioIssqn', model.Issqn.CodigoTributarioIssqn.Value.QuotedString)
          .&Set('Natureza', model.Issqn.Natureza.ToString)
        .Where('ID =' + model.IssqnId.Value.ToString());


        qry.Exec();
      except
        raise Exception.Create('Erro ao editar issqn');
      end;
  end;
end;


procedure TTributacaoRepository.EditarPis(model: TTributacao);
var
  qry: TQuery;
begin
  if Assigned(model) then
  begin
      try
        qry:= TQuery.Create(dmCons.conDados);
        qry.ref;

        qry.SQL.Update('Pis')
          .&Set('BaseCalculo', CurrToStr(model.Pis.BaseCalculo))
          .&Set('Porcento', CurrToStr(model.Pis.Porcento))
          .&Set('Valor', CurrToStr(model.Pis.Valor))
          .&Set('BaseCalculoPorProduto', CurrToStr(model.Pis.BaseCalculoPorProduto))
          .&Set('Aliquota', CurrToStr(model.Pis.Aliquota))
          .&Set('CST', model.Pis.CST.ToString())
        .Where('ID =' + model.PisId.ToString());


        qry.Exec();
      except
        raise Exception.Create('Erro ao editar pis');
      end;
  end;
end;

procedure TTributacaoRepository.EditarPisSt(model: TTributacao);
var
  qry: TQuery;
begin
  if Assigned(model) then
  begin
      try
        qry:= TQuery.Create(dmCons.conDados);
        qry.ref;

        qry.SQL.Update('PisSt')
          .&Set('BaseCalculo', CurrToStr(model.PisSt.BaseCalculo))
          .&Set('Porcento', CurrToStr(model.PisSt.Porcento))
          .&Set('Valor', CurrToStr(model.PisSt.Valor))
          .&Set('BaseCalculoPorProduto', CurrToStr(model.PisSt.BaseCalculoPorProduto))
          .&Set('Aliquota', CurrToStr(model.PisSt.Aliquota))
        .Where('ID =' + model.PisStId.Value.ToString());


        qry.Exec();
      except
        raise Exception.Create('Erro ao editar pisSt');
      end;
  end;
end;

procedure TTributacaoRepository.EditarTaxas(model: TTributacao);
var
  qry: TQuery;
begin
  if Assigned(model) then
  begin
      try
        qry:= TQuery.Create(dmCons.conDados);
        qry.ref;

        qry.SQL.Update('TaxasFiscais')
          .&Set('TaxaEstadual', CurrToStr(model.TaxasFiscais.TaxaEstadual))
          .&Set('TaxaFederal', CurrToStr(model.TaxasFiscais.TaxaFederal))
        .Where('ID =' + model.TaxasId.Value.ToString());


        qry.Exec();
      except
        raise Exception.Create('Erro ao editar taxas');
      end;
  end;
end;

function TTributacaoRepository.GetAll: TObjectList<TTributacao>;
begin
  result:= contexto.ORM.DAO<TTributacao>().Find();
end;

function TTributacaoRepository.InitEdit(id: integer): TTributacao;
begin
  Result:= contexto
    .ORM
    .DAO<TTributacao>()
    .Find(id);
end;

procedure TTributacaoRepository.Insert(model: TTributacao);
begin
  if Assigned(model) then
  begin

    model.PisId:= InsertPis(model);
    model.PisStId:= InsertPisSt(model);
    model.CofinsId:= InsertCofins(model);
    model.CofinsStId:= InsertCofinsSt(model);
    model.IcmsId:= InsertIcms(model);
    model.IssqnId:= InsertIssqn(model);
    model.TaxasId:= InsertTaxas(model);

    contexto
        .ORM
        .DAO<TTributacao>()
        .Insert(model);
  end;
end;

function TTributacaoRepository.InsertCofins(model: TTributacao): Integer;
var
  lista: TObjectList<TCofins>;
begin
  if Assigned(model) then
  begin
    contexto
        .ORM
        .DAO<TCofins>()
        .Insert(model.Cofins);

    lista:= contexto.ORM.DAO<TCofins>().Find();
    try
      result:= lista.Last.Id;
    finally
      lista.Free;
    end;
  end;
end;

function TTributacaoRepository.InsertCofinsSt(model: TTributacao): Integer;
var
  lista: TObjectList<TCofinsSt>;
begin
  if Assigned(model) then
  begin
    contexto
        .ORM
        .DAO<TCofinsSt>()
        .Insert(model.CofinsSt);

    lista:= contexto.ORM.DAO<TCofinsSt>().Find();
    try
      result:= lista.Last.Id;
    finally
      lista.Free;
    end;
  end;
end;

function TTributacaoRepository.InsertIcms(model: TTributacao): Integer;
var
  lista: TObjectList<TIcms>;
begin
  if Assigned(model) then
  begin
    contexto
        .ORM
        .DAO<TIcms>()
        .Insert(model.Icms);

    lista:= contexto.ORM.DAO<TIcms>().Find();
    try
      result:= lista.Last.Id;
    finally
      lista.Free;
    end;
  end;
end;

function TTributacaoRepository.InsertIssqn(model: TTributacao): Integer;
var
  lista: TObjectList<TIssqn>;
begin
  if Assigned(model) then
  begin
    contexto
        .ORM
        .DAO<TIssqn>()
        .Insert(model.Issqn);

    lista:= contexto.ORM.DAO<TIssqn>().Find();
    try
      result:= lista.Last.Id;
    finally
      lista.Free;
    end;
  end;
end;

function TTributacaoRepository.InsertPis(model: TTributacao): Integer;
var
  lista: TObjectList<TPis>;
begin
  if Assigned(model) then
  begin
    contexto
        .ORM
        .DAO<TPis>()
        .Insert(model.Pis);

    lista:= contexto.ORM.DAO<TPis>().Find();
    try
      result:= lista.Last.Id;
    finally
      lista.Free;
    end;
  end;
end;

function TTributacaoRepository.InsertPisSt(model: TTributacao): Integer;
var
  lista: TObjectList<TPisSt>;
begin
  if Assigned(model) then
  begin
    contexto
        .ORM
        .DAO<TPisSt>()
        .Insert(model.PisSt);

    lista:= contexto.ORM.DAO<TPisSt>().Find();
    try
      result:= lista.Last.Id;
    finally
      lista.Free;
    end;
  end;
end;

function TTributacaoRepository.InsertTaxas(model: TTributacao): Integer;
var
  lista: TObjectList<TTaxasFiscais>;
begin
  if Assigned(model) then
  begin
    contexto
        .ORM
        .DAO<TTaxasFiscais>()
        .Insert(model.TaxasFiscais);

    lista:= contexto.ORM.DAO<TTaxasFiscais>().Find();
    try
      result:= lista.Last.Id;
    finally
      lista.Free;
    end;
  end;
end;

procedure TTributacaoRepository.SelectAll;
begin
  dmCons.conDados.Close();
  dmCons.tableTributacao.Active:= false;
  dmCons.conDados.Open();
  dmCons.tableTributacao.Active:= true;
end;

end.
