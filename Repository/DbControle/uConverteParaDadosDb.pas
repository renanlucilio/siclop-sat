unit uConverteParaDadosDb;

interface

uses
  uConexao.interfaces,
  uConexaoORM,
  ormbr.factory.interfaces,
  ormbr.dml.generator.sqlite,
  System.Generics.Collections,
  FireDAC.Stan.Intf,FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
   FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
   FireDAC.Stan.Async, FireDAC.Phys, FireDAC.FMXUI.Wait, Data.DB,
   FireDAC.Comp.Client, FireDAC.Comp.UI, System.Classes, System.SysUtils;

type
//Classe que converte os bancos de dados do antigo para o novo
  TConverteParaDadosDb = class
  private
    //m�todo para converter a tabela de XML para tabela cupom
    function TableXmlToCupom: TDictionary<integer,integer>;
    //m�todo para converte para tabela de venda para venda
    function TableVendaToVenda(vendaIdECupomId: TDictionary<integer,integer>): TDictionary<integer,integer>;
    //m�todo para converte os dados do xml na tabela de cliente
    procedure XmlToCliente(vendaIdECupomId: TDictionary<integer,integer>);
    //m�todo para converte os dados do xml para produtos
    procedure XmlToVendaProduto(vendaIdECupomId: TDictionary<integer,integer>);
  public
    constructor Create;
    destructor Destroy; override;
  end;

implementation

{ TConverteParaDadosDb }

uses uDmCons, uCupom, uDadosDbContext, uConexao.Query, uVenda, ACBrUtil,
  uCliente, uVendaProduto;

constructor TConverteParaDadosDb.Create;
var
  tableCupom:TDictionary<integer,integer>;
  tableConvertida: TDictionary<integer,integer>;
begin
  tableCupom:= TableXmlToCupom;
  try
    tableConvertida:= TableVendaToVenda(tableCupom);
    XmlToCliente(tableConvertida);
    XmlToVendaProduto(tableConvertida);
  finally
    tableConvertida.Free;
    tableCupom.Free;
  end;
end;

destructor TConverteParaDadosDb.Destroy;
begin

  inherited;
end;

function TConverteParaDadosDb.TableVendaToVenda(vendaIdECupomId: TDictionary<integer,integer>): TDictionary<integer,integer>;
var
  contexto: TDadosDbContext;
  venda: TVenda;
  lista: TObjectList<TVenda>;
  qry: TQuery;
  it: integer;
begin
  result:= TDictionary<integer,integer>.Create();

  contexto:= TDadosDbContext.Create(dmCons.conDados);
  try
    for it in vendaIdECupomId.Keys do
    begin
      qry:= TQuery.Create(dmCons.conControle);
      qry.Ref;

      qry.SQL
        .Select('V.VND_HORA').&As('HORA')
          .Column('V.VND_DATA').&As('DATA')
          .Column('V.VND_VALOR').&As('VALOR_VENDA')
          .Column('I.IMP_VALOR').&As('VALOR')
        .From('VENDA').&As('V')
        .InnerJoin('VENDA_IMPOSTO').&As('VP')
          .&On('V.VND_ID = VP.VND_ID')
        .InnerJoin('IMPOSTOS').&As('I')
          .&On('VP.IMP_ID = I.IMP_ID').
        Where('V.VND_ID = ' + it.ToString());

        qry.Open;

        qry.DataSet.First;
        while not(qry.DataSet.Eof) do
        begin
          venda:= TVenda.Create;
          try
            venda.DataHora:=
              qry.DataSet.FieldByName('DATA').AsString + 'T' +
              qry.DataSet.FieldByName('HORA').AsString + '.000Z';
            venda.Valor:= qry.DataSet.FieldByName('VALOR_VENDA').AsFloat;
            venda.ValorTributacao:= qry.DataSet.FieldByName('VALOR').AsFloat;
            venda.CupomId:= vendaIdECupomId[it];
            venda.Desconto:= 0;
            venda.Acrescimo:= 0;
            venda.FormaPagamento:= 'Dinheiro';
            venda.OperadoraCartao:= '';
            venda.Gerada:= 1;

            contexto.ORM.DAO<TVenda>().Insert(venda);
            lista:= contexto.ORM.DAO<TVenda>().Find;
            try
              Result.Add(lista.Last.Id, vendaIdECupomId[it]);
            finally
              FreeAndNil(lista);
            end;


          finally
            qry.DataSet.Next;
            FreeAndNil(venda);
          end;


        end;
      end;

  finally
    contexto.Free;
  end;
end;

function TConverteParaDadosDb.TableXmlToCupom: TDictionary<integer,integer>;
var
  contexto: TDadosDbContext;
  qry: TQuery;
  cupom: TCupom;
  lista: TObjectList<TCupom>;
begin
  result:= TDictionary<integer, integer>.Create();
  contexto:= TDadosDbContext.Create(dmCons.conDados);
  qry:= TQuery.Create(dmCons.conControle);
  qry.Ref;
  try
    qry.SQL
      .Select('V.VND_PDF').&As('PDF')
        .Column('V.VND_ID').&As('VENDA')
        .Column('X.XML_NOME').&As('NOME')
        .Column('X.XML_CAMINHO').&As('CAMINHO')
        .Column('X.XML_CANCELADO').&As('CANCELADO')
      .From('VENDA').&As('V')
      .InnerJoin('XML X')
        .&On('X.XML_ID = V.XML_ID');

    qry.Open();

    qry.DataSet.First;
    while not(qry.DataSet.Eof) do
    begin
      cupom:= TCupom.Create;
      try
        cupom.Nome:= qry.DataSet.FieldByName('NOME').AsString;
        cupom.ArquivoXml:= qry.DataSet.FieldByName('CAMINHO').AsString;
        cupom.ArquivoPdf:= qry.DataSet.FieldByName('PDF').AsString;
        if qry.DataSet.FieldByName('CANCELADO').AsString = 'S' then
          cupom.Cancelado:= 1
        else
          cupom.Cancelado:= 0;

        cupom.Gerado:= 1;
        contexto.ORM.DAO<TCupom>().Insert(cupom);
        lista:= contexto.ORM.DAO<TCupom>().Find;
        try
          Result.Add(qry.DataSet.FieldByName('VENDA').AsInteger, lista.Last.Id);
        finally
          FreeAndNil(lista);
        end;

      finally
        qry.DataSet.Next;
        FreeAndNil(cupom);
      end;


    end;
  finally
    contexto.Free;
  end;

end;

procedure TConverteParaDadosDb.XmlToCliente(vendaIdECupomId: TDictionary<integer,integer>);
var
  it: integer;
  contexto: TDadosDbContext;
  venda: TVenda;
  cupom: TCupom;
  content: string;
  xml: TStringList;
  cliente: TCliente;
  lista: TObjectList<TCliente>;
  qry: TQuery;
begin
  contexto:= TDadosDbContext.Create(dmCons.conDados);
  xml:= TStringList.Create;
  try
    for it in vendaIdECupomId.Keys do
    begin
      venda:= contexto.ORM.DAO<TVenda>().Find(it);
      cupom:= contexto.ORM.DAO<TCupom>().Find(vendaIdECupomId[it]);
      try
        cliente := TCliente.Create;
        try
          if FileExists(cupom.ArquivoXml) then
          begin
              xml.LoadFromFile(cupom.ArquivoXml);
              content:= LerTagXML(xml.Text, 'Dest');

              if LerTagXML(content, 'xNome').Trim.IsEmpty then
                cliente.Nome:= ''
              else
                cliente.Nome:= LerTagXML(content, 'xNome');

              if content.Contains('CNPJ') then
                cliente.Documento:= LerTagXML(content, 'CNPJ')
              else if content.Contains('CPF') then
                cliente.Documento:= LerTagXML(content, 'CPF')
              else
                cliente.Documento:= '';
          end
          else
          begin
            cliente.Documento:= '';
            cliente.Nome:= '';
          end;

          contexto.ORM.DAO<TCliente>().Insert(cliente);

          lista:= contexto.ORM.DAO<TCliente>().Find;
          try
            qry:= TQuery.Create(dmCons.conDados);

            qry.SQL
              .Update('Venda')
                .&Set('ClienteId', lista.Last.Id.Value.ToString())
              .Where('Id = ' + venda.Id.Value.ToString());

            qry.Exec();
          finally
            lista.Free;
          end;
        finally
          cliente.Free;
        end;

      finally
        FreeAndNil(venda);
        FreeAndNil(cupom);
      end;
    end;
  finally
    xml.Free;
    contexto.Free;
  end;
end;

procedure TConverteParaDadosDb.XmlToVendaProduto(vendaIdECupomId: TDictionary<integer,integer>);
var
  it: integer;
  contexto: TDadosDbContext;
  venda: TVenda;
  cupom: TCupom;
  produtos: TArray<string>;
  xml: TStringList;
  vendaProduto: TVendaProduto;
  produto: string;
begin
  contexto:= TDadosDbContext.Create(dmCons.conDados);
  xml:= TStringList.Create;
  FormatSettings.DecimalSeparator:= '.';
  try
    for it in vendaIdECupomId.Keys do
    begin
      venda:= contexto.ORM.DAO<TVenda>().Find(it);
      cupom:= contexto.ORM.DAO<TCupom>().Find(vendaIdECupomId[it]);
      try

          if FileExists(cupom.ArquivoXml) then
          begin
              xml.LoadFromFile(cupom.ArquivoXml);
              produtos:= xml.Text.Split(['<det nItem']);

                for produto in produtos do
                begin
                  vendaProduto := TVendaProduto.Create;
                  try
                    vendaProduto.VendaId:= venda.Id;
                    vendaProduto.Venda.Id:= venda.Id;
                    vendaProduto.Quantidade:= StrToFloatDef(LerTagXML(produto, 'qCom'), 0);
                    vendaProduto.DescricaoProdutoAtual:= LerTagXML(produto, 'xProd');
                    vendaProduto.ValorProdutoAtual:= StrToFloatDef(LerTagXML(produto, 'vUnCom'), 0);

                    vendaProduto.Pis_CST:= StrToIntDef(LerTagXML(LerTagXML(produto, 'PIS'), 'CST'), 49);
                    vendaProduto.Cofins_CST:= StrToIntDef(LerTagXML(LerTagXML(produto, 'COFINS'), 'CST'), 49);
                    vendaProduto.NCM:= LerTagXML(produto, 'CFOP');
                    vendaProduto.CFOP:= LerTagXML(produto, 'NCM');

                    if not String.IsNullOrEmpty(vendaProduto.DescricaoProdutoAtual) then
                      contexto.ORM.DAO<TVendaProduto>().Insert(vendaProduto);
                  finally
                    vendaProduto.Free;
                  end;
                end;

          end;


      finally
        FreeAndNil(venda);
        FreeAndNil(cupom);
      end;
    end;
  finally
    FormatSettings.DecimalSeparator:= ',';
    xml.Free;
    contexto.Free;
  end;
end;

end.
