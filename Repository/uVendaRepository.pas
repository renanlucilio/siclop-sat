unit uVendaRepository;

interface

uses
  uDadosDbContext, uVenda, uVendaProduto,uCupom, System.Generics.Collections,  uConexao.Query, System.SysUtils,
  uCliente;

type
//classe de repositorio para venda
  TVendaRepository = class
  private
    contexto: TDadosDbContext;
  public
    constructor Create;
    destructor Destroy; override;

    //m�todo para deletar uma venda
    procedure Deletar(idVenda: Integer);
    //m�todo para criar uma venda
    function CriarVenda(venda: TVenda): integer;
    //m�todo para confirmar que vai gerar uma venda
    procedure IraGerar(idVenda: integer);
    //m�todo para adicionar um cliente na venda
    procedure AddCliente(cliente: TCliente; idVenda: integer);
    //m�todo para adicionar produtos a venda
    procedure AddProdutosDaVenda(produtos: TObjectList<TVendaProduto>; idVenda: integer; comProduto: Boolean = true);

    //m�todo para cancelar uma venda
    procedure CancelarVenda(idVenda: integer);
    //m�todo para
    procedure AddCupom(cupom: TCupom; idVenda: integer);
  end;

implementation

{ TVendaRepository }

uses uDmCons, uProduto, uProdutoRepository;

procedure TVendaRepository.AddCliente(cliente: TCliente; idVenda: integer);
var
  lista: TObjectList<TCliente>;
  id: integer;
  qry:TQuery;
begin

  if Assigned(cliente) then
  begin
    contexto.ORM.DAO<TCliente>().Insert(cliente);

    lista:= contexto.ORM.DAO<TCliente>().Find();
    try
      id:= lista.Last.Id;

      if id>0 then
      begin

        qry:= TQuery.Create(dmCons.conDados);
        qry.Ref;
        qry.SQL
          .Update('Venda')
            .&Set('ClienteId', id.ToString())
          .Where('Id =' + idVenda.ToString);

        qry.Exec();

      end;

    finally
      lista.Free;
    end;
  end;

end;

procedure TVendaRepository.AddCupom(cupom: TCupom; idVenda: integer);
var
  lista: TObjectList<TCupom>;
  id: integer;
  qry:TQuery;
begin

  if Assigned(cupom) then
  begin
    contexto.ORM.DAO<TCupom>().Insert(cupom);

    lista:= contexto.ORM.DAO<TCupom>().Find();
    try
      id:= lista.Last.Id;

      if id>0 then
      begin

        qry:= TQuery.Create(dmCons.conDados);
        qry.Ref;
        qry.SQL
          .Update('Venda')
            .&Set('CupomId', id.ToString())
          .Where('Id =' + idVenda.ToString);

        qry.Exec();

      end;

    finally
      lista.Free;
    end;
  end;

end;

procedure TVendaRepository.AddProdutosDaVenda(produtos: TObjectList<TVendaProduto>; idVenda: integer; comProduto: Boolean = true);
var
  prodVenda: TVendaProduto;
  produtoVenda: TVendaProduto;
  repo: TProdutoRepository;
  produto: TProduto;
begin
  repo:= TProdutoRepository.Create;
  try
    for prodVenda in produtos do
      begin
        if comProduto then
        begin
          produto:= repo.Get(prodVenda.ProdutoId);
          produtoVenda:= TVendaProduto.Create;
          try
            if Assigned(produto) then
            begin
              produtoVenda.VendaId:= idVenda;
              produtoVenda.Venda.Id:= idVenda;

              produtoVenda.ProdutoId:= prodVenda.ProdutoId;
              produtoVenda.Produto.Id:= prodVenda.ProdutoId;

              produtoVenda.Quantidade:= prodVenda.Quantidade;
              produtoVenda.DescricaoProdutoAtual:= prodVenda.Produto.Descricao;
              produtoVenda.ValorProdutoAtual:= prodVenda.Produto.Preco;
              produtoVenda.Pis_CST:= produto.Pis_CST;
              produtoVenda.Cofins_CST:= produto.Cofins_CST;
              produtoVenda.CFOP:= produto.Cfop;
              produtoVenda.NCM:= produto.Ncm;

              contexto.ORM.DAO<TVendaProduto>().Insert(produtoVenda);
            end
            else
            begin
              produtoVenda.VendaId:= idVenda;
              produtoVenda.Venda.Id:= idVenda;

              produtoVenda.ProdutoId:= prodVenda.ProdutoId;
              produtoVenda.Produto.Id:= prodVenda.ProdutoId;

              produtoVenda.Quantidade:= prodVenda.Quantidade;
              produtoVenda.DescricaoProdutoAtual:= prodVenda.Produto.Descricao;
              produtoVenda.ValorProdutoAtual:= prodVenda.Produto.Preco;
              produtoVenda.Pis_CST:= prodVenda.Pis_CST;
              produtoVenda.Cofins_CST:= produto.Cofins_CST;
              produtoVenda.CFOP:= produto.Cfop;
              produtoVenda.NCM:= produto.Ncm;

              contexto.ORM.DAO<TVendaProduto>().Insert(produtoVenda);
            end;
          finally
            if Assigned(produto) then
              FreeAndNil(produto);

            produtoVenda.Free;
          end;
        end
        else
        begin
          contexto.ORM.DAO<TVendaProduto>().Insert(prodVenda);


        end;
    end;
  finally
    repo.Free;
  end;
end;

procedure TVendaRepository.CancelarVenda(idVenda: integer);
var
  qry: TQuery;
begin
  qry:= TQuery.Create(dmCons.conDados);
  qry.Ref;

  qry.SQL
    .Update('Cupom')
      .&Set('Cancelado', 1.ToString())
    .Where('Id = ' + idVenda.ToString());

  qry.Exec();

end;

constructor TVendaRepository.Create;
begin
  contexto:= TDadosDbContext.Create(dmCons.conDados);
end;

function TVendaRepository.CriarVenda(venda: TVenda): integer;
var
  lista: TObjectList<TVenda>;
begin

  if Assigned(venda) then
  begin
    contexto.ORM.DAO<TVenda>().Insert(venda);

    lista:= contexto.ORM.DAO<TVenda>().Find();
    try
      result:= lista.Last.Id;
    finally
      lista.Free;
    end;
  end;

end;

procedure TVendaRepository.Deletar(idVenda: Integer);
var
  qry: TQuery;
begin
  qry:= TQuery.Create(dmCons.conDados);
  qry.Ref;

  qry.SQL
    .Delete.From('Venda')
    .Where('Id = ' + idVenda.ToString());

  qry.Exec();

end;
destructor TVendaRepository.Destroy;
begin
  contexto.Free;
  inherited;
end;

procedure TVendaRepository.IraGerar(idVenda: integer);
var
  qry: TQuery;
begin
  qry:= TQuery.Create(dmCons.conDados);
  qry.Ref;
  qry.SQL
    .Update('Venda')
      .&Set('Gerada', 1.ToString())
    .Where('Id =' + idVenda.ToString());

  qry.Exec();
end;

end.
