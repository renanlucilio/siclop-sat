unit uProdutoRepository;

interface

uses
  uProduto, System.Generics.Collections, uDadosDbContext, uDmCons;

type
//classe de repositorio de produtos
  TProdutoRepository = class
  private
    contexto: TDadosDbContext;
   public
    constructor Create;
    destructor Destroy; override;

    //m�todo para iniciar a edi��o
    function InitEdit(id: integer): TProduto;
    //m�todo para editar um produto
    procedure Edit(produto: TProduto);

    //m�todop para inserir um produto
    procedure Insert(produto: TProduto);
    //m�todo para deletar um produto
    function Delete(id: integer): TProduto;
    //m�todo para listar todas os produtos
    procedure SelectAll();

    //m�todo para carregar uma lista de produtos
    function GetAll(): TObjectList<TProduto>;
    //m�todo para carregar um produto pelo id
    function Get(id: integer): TProduto;
   end;

implementation

uses
  uConexao.Query,uLibUtil, System.SysUtils, uAuRepository, Form.Principal,
  FMX.ListBox, FMX.Objects, FMX.Layouts, FMX.Types, Vcl.Graphics;

{ TProdutoRepository }

constructor TProdutoRepository.Create;
begin
  contexto:= TDadosDbContext.Create(dmCons.conDados);
end;

function TProdutoRepository.Delete(id: integer): TProduto;
var
  produto: TProduto;
  qry: TQuery;
begin
  produto:= contexto
    .ORM
    .DAO<TProduto>()
    .Find(id);

  if Assigned(produto) then
  begin
    try
        qry:= TQuery.Create(dmCons.conDados);
        qry.ref;
        FormatSettings.DecimalSeparator:= '.';
        qry.SQL.Update('Produto')
          .&Set('Descricao', 'DELEXC'.QuotedString())
          .Where('ID =' + Id.ToString());

        qry.Exec();

        SelectAll();

    finally
      FormatSettings.DecimalSeparator:= ',';
      produto.Free;
    end;

    TAuditoriaRepository.GravarLog(
        'Cadastro exclu�do',
         FormHome.txtUsuario.Text + #13 +
        ' Excluiu o produto "' + produto.Descricao + '"'+ #13 +
        ' Horario:' + TimeToStr(Now),
        'C');
  end;

end;

destructor TProdutoRepository.Destroy;
begin
  contexto.Free;
  inherited;
end;

procedure TProdutoRepository.Edit(produto: TProduto);
var
  qry: TQuery;
begin
  if Assigned(produto) then
  begin
      try
        qry:= TQuery.Create(dmCons.conDados);
        qry.ref;

        FormatSettings.DecimalSeparator:= '.';
        qry.SQL.Update('Produto')
          .&Set('Descricao', produto.Descricao.ToUpper().QuotedString())
          .&Set('Preco', CurrToStr(produto.Preco))
          .&Set('UnidadeMedida', produto.UnidadeMedida.ToUpper().QuotedString())
          .&Set('CategoriaId', produto.CategoriaId.ToString())
          .&Set('Ncm', produto.Ncm.ToUpper().Trim().QuotedString())
          .&Set('Cest', produto.Cest.ToUpper().Trim().QuotedString())
          .&Set('Cfop', produto.Cfop.ToUpper().Trim().QuotedString())
          .&Set('Cofins_CST', IntToStr(produto.Cofins_CST))
          .&Set('Cofins_BaseCalculo', FloatToStr(produto.Cofins_BaseCalculo))
          .&Set('Cofins_Porcento', FloatToStr(produto.Cofins_Porcento))
          .&Set('Cofins_Valor', FloatToStr(produto.Cofins_Valor))
          .&Set('Cofins_BaseCalculoPorProduto', FloatToStr(produto.Cofins_BaseCalculoPorProduto))
          .&Set('Cofins_Aliquota', FloatToStr(produto.Cofins_Aliquota))
          .&Set('CofinsSt_BaseCalculo', FloatToStr(produto.CofinsSt_BaseCalculo))
          .&Set('CofinsSt_Porcento', FloatToStr(produto.CofinsSt_Porcento))
          .&Set('CofinsSt_Valor', FloatToStr(produto.CofinsSt_Valor))
          .&Set('CofinsSt_BaseCalculoPorProduto', FloatToStr(produto.CofinsSt_BaseCalculoPorProduto))
          .&Set('CofinsSt_Aliquota', FloatToStr(produto.CofinsSt_Aliquota))
          .&Set('Pis_CST', IntToStr(produto.Pis_CST))
          .&Set('Pis_BaseCalculo', FloatToStr(produto.Pis_BaseCalculo))
          .&Set('Pis_Porcento', FloatToStr(produto.Pis_Porcento))
          .&Set('Pis_Valor', FloatToStr(produto.Pis_Valor))
          .&Set('Pis_BaseCalculoPorProduto', FloatToStr(produto.Pis_BaseCalculoPorProduto))
          .&Set('Pis_Aliquota', FloatToStr(produto.Pis_Aliquota))
          .&Set('PisSt_BaseCalculo', FloatToStr(produto.PisSt_BaseCalculo))
          .&Set('PisSt_Porcento', FloatToStr(produto.PisSt_Porcento))
          .&Set('PisSt_Valor', FloatToStr(produto.PisSt_Valor))
          .&Set('PisSt_BaseCalculoPorProduto', FloatToStr(produto.PisSt_BaseCalculoPorProduto))
          .&Set('PisSt_Aliquota', FloatToStr(produto.PisSt_Aliquota))
          .&Set('TaxaEstadual', FloatToStr(produto.TaxaEstadual))
          .&Set('TaxaFederal', FloatToStr(produto.TaxaFederal))
          .&Set('Issqn_BaseCalculo', FloatToStr(produto.Issqn_BaseCalculo))
          .&Set('Issqn_Deducao', FloatToStr(produto.Issqn_Deducao))
          .&Set('Issqn_CodigoIbge', produto.Issqn_CodigoIbge.ToUpper().Trim().QuotedString())
          .&Set('Issqn_BaseCalculoProduto', FloatToStr(produto.Issqn_BaseCalculoProduto))
          .&Set('Issqn_ItemDaListaServico', produto.Issqn_ItemDaListaServico.ToUpper().Trim().QuotedString())
          .&Set('Issqn_CodigoTributarioIssqn', produto.Issqn_CodigoTributarioIssqn.ToUpper().Trim().QuotedString())
          .&Set('Issqn_Natureza', IntToStr(produto.Issqn_Natureza))
          .&Set('Icms_Origem', IntToStr(produto.Icms_Origem))
          .&Set('Icms_Cst', IntToStr(produto.Icms_Cst))
          .&Set('Icms_Porcento', FloatToStr(produto.Icms_Porcento))
          .&Set('Icms_Valor', FloatToStr(produto.Icms_Valor))
          .Where('ID =' + produto.Id.Value.ToString());

        qry.Exec();

        SelectAll();

    finally
      FormatSettings.DecimalSeparator:= ',';
      produto.Free;
    end;
  end;
end;


function TProdutoRepository.Get(id: integer): TProduto;
var
  produto: TProduto;
begin
  try
    produto:= contexto.ORM.DAO<TProduto>().Find(id);

    if Assigned(produto) then
      result:= produto
    else
      Result:= nil;
  except
  end;
end;

function TProdutoRepository.GetAll: TObjectList<TProduto>;
var
  produtos: TObjectList<TProduto>;
  remover: TList<TProduto>;
  produto: TProduto;
begin
  produtos:= contexto
    .ORM
    .DAO<TProduto>()
    .Find();

  remover:= TList<TProduto>.Create();
  try
    for produto in produtos do
    begin
      if produto.Descricao.Equals('DELEXC') then
        remover.Add(produto);
    end;

    for produto in remover do
    begin
      produtos.Extract(produto).Free;
    end;

    Result:= produtos;
  finally
    remover.Free;
  end;
end;

function TProdutoRepository.InitEdit(id: integer): TProduto;
begin
  Result:= contexto
    .ORM
    .DAO<TProduto>()
    .Find(id);
end;

procedure TProdutoRepository.Insert(produto: TProduto);
begin

  if Assigned(produto) then
    begin
      contexto
        .ORM
        .DAO<TProduto>()
        .Insert(produto);
    end;
end;

procedure TProdutoRepository.SelectAll;
var
  produto: TProduto;
  listItem: TListBoxItem;
  txtCodigo: TText;
  containerDescricaoCategoria: TLayout;
  txtDescricao: TText;
  txtCategoria: TText;
  txtValor: TText;
  list: TObjectList<TProduto>;
begin
  list:= GetAll();
  FormHome.lstProdutosGrid.BeginUpdate;
  FormHome.lstProdutosGrid.Clear;
  try
    for produto in list do
    begin
      listItem := TListBoxItem.Create(FormHome.lstProdutosGrid);
      listItem.Height := 40;

      txtCodigo := TText.Create(listItem);
      txtCodigo.Width := 40;
      txtCodigo.Font.Family := 'Consolas';
      txtCodigo.TextSettings.HorzAlign := TTextAlign.Leading;
      txtCodigo.Font.Size := 12;
      txtCodigo.Parent := listItem;
      txtCodigo.Align := TAlignLayout.Left;
      txtCodigo.Text := produto.Id.Value.ToString;
      txtCodigo.HitTest := false;

      containerDescricaoCategoria := TLayout.Create(listItem);
      containerDescricaoCategoria.Parent := listItem;
      containerDescricaoCategoria.Align := TAlignLayout.Left;
      containerDescricaoCategoria.Width := 240;
      containerDescricaoCategoria.HitTest := false;
      containerDescricaoCategoria.Height := 40;

      txtDescricao := TText.Create(listItem);
      txtDescricao.Font.Family := 'Consolas';
      txtDescricao.TextSettings.HorzAlign := TTextAlign.Leading;
      txtDescricao.Font.Size := 16;
      txtDescricao.Height := 20;
      txtDescricao.Font.Style := [TFontStyle.fsBold];
      txtDescricao.Parent := containerDescricaoCategoria;
      txtDescricao.Align := TAlignLayout.Top;
      txtDescricao.Text := produto.Descricao;
      txtDescricao.HitTest := false;

      txtCategoria := TText.Create(listItem);
      txtCategoria.Font.Family := 'Consolas';
      txtCategoria.Font.Size := 10;
      txtCategoria.Font.Style := [TFontStyle.fsBold];
      txtCategoria.Height := 20;
      txtCategoria.TextSettings.HorzAlign := TTextAlign.Leading;
      txtCategoria.Parent := containerDescricaoCategoria;
      txtCategoria.Align := TAlignLayout.Top;
      txtCategoria.Text := produto.Categoria.Descricao;
      txtCategoria.HitTest := false;

      containerDescricaoCategoria.AddObject(txtDescricao);
      containerDescricaoCategoria.AddObject(txtCategoria);

      txtValor := TText.Create(listItem);
      txtValor.Width := 100;
      txtValor.Font.Family := 'Consolas';
      txtValor.TextSettings.HorzAlign := TTextAlign.Trailing;
      txtValor.Font.Size := 16;
      txtValor.Parent := listItem;
      txtValor.Align := TAlignLayout.Right;
      txtValor.Text := FormatFloat('R$ ###,###,##0.00', produto.Preco);
      txtValor.HitTest := false;

      listItem.AddObject(txtCodigo);
      listItem.AddObject(containerDescricaoCategoria);
      listItem.AddObject(txtValor);

      listItem.Tag := produto.Id;
      listItem.StyledSettings:= [];
      listItem.TextSettings.FontColor:= TColor($ffffff);
      listItem.TextSettings.Font.Size := 1;
      listItem.Text := produto.Id.Value.ToString + ' '
       + produto.Categoria.Descricao + ' '
        + produto.Descricao;

      FormHome.lstProdutosGrid.AddObject(listItem);
    end;
    FormHome.lstProdutosGrid.EndUpdate;
  finally
    list.Free;
  end;
end;
end.

