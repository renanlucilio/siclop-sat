unit uUsuarioReporitory;

interface
uses
  System.Generics.Collections, uDadosDbContext, uDmCons, uUsuario;

type
//Classe de repositorio para usuario
  TUsuarioRepository = class
  private
    contexto: TDadosDbContext;
   public
    constructor Create;
    destructor Destroy; override;

    //m�todo usado para iniciar a edi��o
    function InitEdit(id: integer): TUsuario;
    //m�todo para editar um usuario
    procedure Edit(model: TUsuario);

    //m�todo para inserir um usuario
    procedure Insert(model: TUsuario);
    //m�todo para  deletar um usuario
    function Delete(id: integer): TUsuario;
    //m�todo para listar os produtos
    procedure SelectAll();

    //m�todo para verificar se tem usuarios cadastrados
    function TemUsuariosCadastrados: Boolean;
    //m�todo para verificar se um usu�rio � valido
    function Logar(usuario, senha: string): TUsuario;
   end;

implementation

uses
  uConexao.Query,uLibUtil, System.SysUtils, uAuRepository, Form.Principal,
  FMX.Objects, FMX.Layouts, FMX.ListBox, System.UITypes, FMX.Types;

{ TUsuarioRepository }

constructor TUsuarioRepository.Create;
begin
  contexto:= TDadosDbContext.Create(dmCons.conDados);
end;

function TUsuarioRepository.Delete(id: integer): TUsuario;
var
  model: TUsuario;
begin
  model:= contexto
    .ORM
    .DAO<TUsuario>()
    .Find(id);

  if Assigned(model) then
  begin
    TAuditoriaRepository.GravarLog(
        'Cadastro exclu�do',
         FormHome.txtUsuario.Text + #13 +
        ' Excluiu o usu�rio "' + model.Nome + '"'+ #13 +
        ' Horario:' + TimeToStr(Now),
        'C');

    contexto
      .ORM
      .DAO<TUsuario>()
      .Delete(model);
  end;

end;

destructor TUsuarioRepository.Destroy;
begin
  contexto.Free;
  inherited;
end;

procedure TUsuarioRepository.Edit(model: TUsuario);
var
  qry: TQuery;
begin
  if Assigned(model) then
  begin
    try
      try
        qry:= TQuery.Create(dmCons.conDados);
        qry.ref;

        qry.SQL.Update('Usuario')
          .&Set('Nome', model.Nome.ToUpper().QuotedString())
          .&Set('Senha', model.Senha.ToUpper().QuotedString())
          .&Set('Nivel', model.Nivel.Value.ToString())
          .Where('ID =' + model.Id.Value.ToString());

        qry.Exec();

        SelectAll();
      except
        raise Exception.Create('Erro ao editar produto');
      end;

    finally
      model.Free;
    end;
  end;
end;

function TUsuarioRepository.InitEdit(id: integer): TUsuario;
begin
  Result:= contexto
    .ORM
    .DAO<TUsuario>()
    .Find(id);
end;

procedure TUsuarioRepository.Insert(model: TUsuario);
begin
    if Assigned(model) then
    begin
      contexto
        .ORM
        .DAO<TUsuario>()
        .Insert(model);
    end;
end;

function TUsuarioRepository.Logar(usuario, senha: string): TUsuario;
var
  lista: TObjectList<TUsuario>;
  it: TUsuario;
  user: TUsuario;
begin
  result:= nil;
  user:= nil;
  lista:= contexto.ORM.DAO<TUsuario>().Find();
  try
    for it in lista do
    begin
      if (usuario.Trim = it.Nome.Trim) and (senha.Trim = it.Senha.Trim) then
      begin
        user:= it;
        break;
      end;
    end;

    if Assigned(user) then
      lista.Extract(user);

    Result:= user;
  finally
    lista.Free;
  end;

end;

procedure TUsuarioRepository.SelectAll;
var
  lista: TObjectList<TUsuario>;
  it: TUsuario;
  listItem: TListBoxItem;
  txtDescricao: TText;
begin
  lista:= contexto.ORM.DAO<TUsuario>().Find;
  try
    FormHome.lstUsuariosGrid.BeginUpdate;
    FormHome.lstUsuariosGrid.Clear;
    for it in lista do
    begin
      listItem := TListBoxItem.Create(FormHome.lstUsuariosGrid);
      listItem.Height := 40;

      txtDescricao := TText.Create(listItem);
      txtDescricao.Font.Family := 'Consolas';
      txtDescricao.TextSettings.HorzAlign := TTextAlign.Leading;
      txtDescricao.Font.Size := 16;
      txtDescricao.Height := 20;
      txtDescricao.Font.Style := [TFontStyle.fsBold];
      txtDescricao.Parent := FormHome.lstUsuariosGrid;
      txtDescricao.Align := TAlignLayout.Client;
      txtDescricao.Text := it.Nome;
      txtDescricao.HitTest := false;

      listItem.AddObject(txtDescricao);
      listItem.Tag := it.Id;
      listItem.StyledSettings:= [];
      listItem.TextSettings.FontColor:= TColor($ffffff);
      listItem.TextSettings.Font.Size := 1;
      listItem.Text := it.Nome;

      FormHome.lstUsuariosGrid.AddObject(listItem);
    end;
    FormHome.lstUsuariosGrid.EndUpdate;
  finally
    lista.Free;
  end;

end;

function TUsuarioRepository.TemUsuariosCadastrados: Boolean;
var
  lista: TObjectList<TUsuario>;
begin
  lista:= contexto.ORM.DAO<TUsuario>().Find;
  try
    result:= lista.Count > 0;
  finally
    lista.Free;
  end;

end;

end.
