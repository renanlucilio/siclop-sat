unit uDBTributacaoAuxiliarContexto;

interface

uses
  uConexao.interfaces,
  uConexaoORM,
  ormbr.factory.interfaces,
  ormbr.dml.generator.sqlite,
  FireDAC.Stan.Intf,FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
   FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
   FireDAC.Stan.Async, FireDAC.Phys, FireDAC.FMXUI.Wait, Data.DB,
   FireDAC.Comp.Client, FireDAC.Comp.UI, System.Classes;

type
//Classe de contexto para o bancao de tributações
  TTributacaoAuxiliarContexto = class
  private
    FORM:     TConexaoORM;
    connection: TFDConnection;
  public
    constructor Create();
    destructor Destroy; override;

    property Con: TFDConnection read connection write connection;
    property ORM: TConexaoORM read FORM       write FORM;
  end;

implementation

{ TTributacaoAuxiliarContexto }

uses uDmCons;

constructor TTributacaoAuxiliarContexto.Create();
begin
  connection :=  dmCons.ConAuxTributacao;
  FORM:= TConexaoORM.Create(connection, dnSQLite);
  Con.Connected:= true;
end;

destructor TTributacaoAuxiliarContexto.Destroy;
begin
  Con.Connected:= false;
  FORM.Free;
  inherited;
end;

end.
