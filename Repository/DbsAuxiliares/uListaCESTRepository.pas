unit uListaCESTRepository;

interface

uses uDBTributacaoAuxiliarContexto;

type
//Classe de repositorio para tabela CEST
  TListaCESTRepository = class
  private
    contexto: TTributacaoAuxiliarContexto;
  public
    constructor Create;
    destructor Destroy; override;

    //m�todo que verifica se cest � valido
    function IsValidCEST(const value: string): Boolean;
  end;

implementation


{ TListaICMRepository }

uses uDmCons, System.SysUtils, System.Generics.Collections, ListaCEST;

constructor TListaCESTRepository.Create;
begin
    contexto:= TTributacaoAuxiliarContexto.Create()
end;

destructor TListaCESTRepository.Destroy;
begin
  contexto.Free;
  inherited;
end;

function TListaCESTRepository.IsValidCEST(const value: string): Boolean;
var
  cest: TObjectList<TListaCEST>;
begin
  if StrToFloatDef(StringReplace(value, '.', '', [rfReplaceAll]), -1) = 0 then
    exit(true);

  if value.IsEmpty then
    exit(true);

  cest:= contexto.ORM.DAO<TListaCEST>().FindWhere('CEST = '+ value.QuotedString);
  try
    Result:= cest.Count > 0;
  finally
    cest.Free;
  end;
end;


end.
