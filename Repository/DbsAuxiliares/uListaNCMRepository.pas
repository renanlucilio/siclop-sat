unit uListaNCMRepository;

interface

uses uDBTributacaoAuxiliarContexto;

type
//Classe de repositorio para NCM
  TListaNCMRepository = class
  private
    contexto: TTributacaoAuxiliarContexto;
  public
    constructor Create;
    destructor Destroy; override;

  //m�todo que verifica se NCM � valido
    function IsValidNCM(const value: string): Boolean;
  end;

implementation


{ TListaICMRepository }

uses uDmCons, System.SysUtils, ListaNCM, System.Generics.Collections;

constructor TListaNCMRepository.Create;
begin
  contexto:= TTributacaoAuxiliarContexto.Create()
end;

destructor TListaNCMRepository.Destroy;
begin
  contexto.Free;
  inherited;
end;

function TListaNCMRepository.IsValidNCM(const value: string): Boolean;
var
  ncm: TObjectList<TListaNCM>;
begin
  ncm:= contexto.ORM.DAO<TListaNCM>().FindWhere('NCM = '+ value.QuotedString);
  try
    Result:= ncm.Count > 0;
  finally
    ncm.Free;
  end;
end;

end.
