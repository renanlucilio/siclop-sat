unit uCategoriaRepository;

interface

uses uCategoria, System.Generics.Collections, uDadosDbContext, uDmCons;

type
//classe repositorio para categoria
   TCategoriaRepository = class
   private
    contexto: TDadosDbContext;
   public
    constructor Create;
    destructor Destroy; override;

    //m�todo para iniciar a edi��o da categoria
    function InitEdit(id: integer): TCategoria;
    //m�todo para editar uma categoria
    procedure Edit(categoria: TCategoria);

    //m�todo para inserir uma categoria
    procedure Insert(categoria: TCategoria);
    //m�todo para deletar uma categoria
    function Delete(id: integer): TCategoria;
    //m�todo para carregar todas as categorias
    procedure SelectAll();
    //m�todo para retornar uma lista com as categorias
    function GetAll(): TObjectList<TCategoria>;
    //m�todo para retorna uma categoria com base em um id
    function Get(id: Integer): TCategoria;
    //m�todo para verificar se a categoria j� existe
    function Exist(descricao: string): Boolean;
   end;

implementation

uses
  uConexao.Query,uLibUtil, System.SysUtils, Form.Principal, uAuRepository,
  FMX.ListBox, FMX.Objects, FMX.Types, System.UITypes;

{ TCategoriaRepository }

constructor TCategoriaRepository.Create;
begin
  contexto:= TDadosDbContext.Create(dmCons.conDados);
end;

function TCategoriaRepository.Delete(id: integer): TCategoria;
var
  categoria: TCategoria;
  qry: TQuery;
begin
  categoria:= contexto
    .ORM
    .DAO<TCategoria>()
    .Find(id);

  if Assigned(categoria) then
  begin
    TAuditoriaRepository.GravarLog(
        'Cadastro exclu�do',
         FormHome.txtUsuario.Text + #13 +
        ' Excluiu a categoria "' + categoria.Descricao + '"'+ #13 +
        ' Horario:' + TimeToStr(Now),
        'C');

    contexto
      .ORM
      .DAO<TCategoria>()
      .Delete(categoria);

    try
        qry:= TQuery.Create(dmCons.conDados);
        qry.ref;

        qry.SQL.Update('Produto')
          .&Set('Descricao', 'DELEXC'.QuotedString())
          .Where('CategoriaId =' + Id.ToString());

        qry.Exec();

        SelectAll();

    finally
      categoria.Free;
    end;
  end;
end;

destructor TCategoriaRepository.Destroy;
begin
  contexto.Free;
  inherited;
end;

procedure TCategoriaRepository.Edit(categoria: TCategoria);
var
  qry: TQuery;
begin
  if Assigned(categoria) then
  begin       
    try
        qry:= TQuery.Create(dmCons.conDados);
        qry.ref;  
        FormatSettings.DecimalSeparator:= '.';
        qry.SQL.Update('Categoria')
          .&Set('Descricao', categoria.Descricao.ToUpper().QuotedString())
          .&Set('Ncm', categoria.Ncm.ToUpper().Trim().QuotedString())
          .&Set('Cest', categoria.Cest.ToUpper().Trim().QuotedString())
          .&Set('Cfop', categoria.Cfop.ToUpper().Trim().QuotedString())
          .&Set('Cofins_CST', IntToStr(categoria.Cofins_CST))
          .&Set('Cofins_BaseCalculo', FloatToStr(categoria.Cofins_BaseCalculo))
          .&Set('Cofins_Porcento', FloatToStr(categoria.Cofins_Porcento))
          .&Set('Cofins_Valor', FloatToStr(categoria.Cofins_Valor))
          .&Set('Cofins_BaseCalculoPorProduto', FloatToStr(categoria.Cofins_BaseCalculoPorProduto))
          .&Set('Cofins_Aliquota', FloatToStr(categoria.Cofins_Aliquota))
          .&Set('CofinsSt_BaseCalculo', FloatToStr(categoria.CofinsSt_BaseCalculo))
          .&Set('CofinsSt_Porcento', FloatToStr(categoria.CofinsSt_Porcento))
          .&Set('CofinsSt_Valor', FloatToStr(categoria.CofinsSt_Valor))
          .&Set('CofinsSt_BaseCalculoPorProduto', FloatToStr(categoria.CofinsSt_BaseCalculoPorProduto))
          .&Set('CofinsSt_Aliquota', FloatToStr(categoria.CofinsSt_Aliquota))
          .&Set('Pis_CST', IntToStr(categoria.Pis_CST))
          .&Set('Pis_BaseCalculo', FloatToStr(categoria.Pis_BaseCalculo))
          .&Set('Pis_Porcento', FloatToStr(categoria.Pis_Porcento))
          .&Set('Pis_Valor', FloatToStr(categoria.Pis_Valor))
          .&Set('Pis_BaseCalculoPorProduto', FloatToStr(categoria.Pis_BaseCalculoPorProduto))
          .&Set('Pis_Aliquota', FloatToStr(categoria.Pis_Aliquota))
          .&Set('PisSt_BaseCalculo', FloatToStr(categoria.PisSt_BaseCalculo))
          .&Set('PisSt_Porcento', FloatToStr(categoria.PisSt_Porcento))
          .&Set('PisSt_Valor', FloatToStr(categoria.PisSt_Valor))
          .&Set('PisSt_BaseCalculoPorProduto', FloatToStr(categoria.PisSt_BaseCalculoPorProduto))
          .&Set('PisSt_Aliquota', FloatToStr(categoria.PisSt_Aliquota))
          .&Set('TaxaEstadual', FloatToStr(categoria.TaxaEstadual))
          .&Set('TaxaFederal', FloatToStr(categoria.TaxaFederal))
          .&Set('Issqn_BaseCalculo', FloatToStr(categoria.Issqn_BaseCalculo))
          .&Set('Issqn_Deducao', FloatToStr(categoria.Issqn_Deducao))
          .&Set('Issqn_CodigoIbge', categoria.Issqn_CodigoIbge.ToUpper().Trim().QuotedString())
          .&Set('Issqn_BaseCalculoProduto', FloatToStr(categoria.Issqn_BaseCalculoProduto))
          .&Set('Issqn_ItemDaListaServico', categoria.Issqn_ItemDaListaServico.ToUpper().Trim().QuotedString())
          .&Set('Issqn_CodigoTributarioIssqn', categoria.Issqn_CodigoTributarioIssqn.ToUpper().Trim().QuotedString())
          .&Set('Issqn_Natureza', IntToStr(categoria.Issqn_Natureza))
          .&Set('Icms_Origem', IntToStr(categoria.Icms_Origem))
          .&Set('Icms_Cst', IntToStr(categoria.Icms_Cst))
          .&Set('Icms_Porcento', FloatToStr(categoria.Icms_Porcento))
          .&Set('Icms_Valor', FloatToStr(categoria.Icms_Valor))
          .Where('ID =' + categoria.Id.Value.ToString());

        qry.Exec();

        SelectAll();
    finally
      FormatSettings.DecimalSeparator:= ',';
      categoria.Free;
    end;
  end;          
end;



function TCategoriaRepository.Exist(descricao: string): Boolean;
var
  lista: TObjectList<TCategoria>;
  it: TCategoria;
begin
  lista:= contexto.ORM.DAO<TCategoria>().Find();
  try
    for it in lista do
    begin
      if it.Descricao.ToUpper.Trim = descricao.ToUpper.Trim then
        exit(true);
    end;

    Result:= False;
  finally
    lista.Free;
  end;
end;

function TCategoriaRepository.Get(id: Integer): TCategoria;
begin
  result:= contexto.ORM.DAO<TCategoria>().Find(id);
end;

function TCategoriaRepository.GetAll: TObjectList<TCategoria>;
begin
  result:= contexto.ORM.DAO<TCategoria>().Find();
end;

function TCategoriaRepository.InitEdit(id: integer): TCategoria;
begin
  Result:= contexto
    .ORM
    .DAO<TCategoria>()
    .Find(id);
end;

procedure TCategoriaRepository.Insert(categoria: TCategoria);
begin

    if Assigned(categoria) then
    begin
      contexto
        .ORM
        .DAO<TCategoria>()
        .Insert(categoria);
    end;
end;


procedure TCategoriaRepository.SelectAll;
var
  lista: TObjectList<TCategoria>;
  it: TCategoria;
  listItem: TListBoxItem;
  txtDescricao: TText;
begin
  lista:= contexto.ORM.DAO<TCategoria>().Find;
  try
    FormHome.lstCategoriasGrid.BeginUpdate;
    FormHome.lstCategoriasGrid.Clear;
    for it in lista do
    begin
      listItem := TListBoxItem.Create(FormHome.lstCategoriasGrid);
      listItem.Height := 40;

      txtDescricao := TText.Create(listItem);
      txtDescricao.Font.Family := 'Consolas';
      txtDescricao.TextSettings.HorzAlign := TTextAlign.Leading;
      txtDescricao.Font.Size := 16;
      txtDescricao.Height := 20;
      txtDescricao.Font.Style := [TFontStyle.fsBold];
      txtDescricao.Parent := FormHome.lstCategoriasGrid;
      txtDescricao.Align := TAlignLayout.Client;
      txtDescricao.Text := it.Descricao;
      txtDescricao.HitTest := false;

      listItem.AddObject(txtDescricao);
      listItem.Tag := it.Id;
      listItem.StyledSettings:= [];
      listItem.TextSettings.FontColor:= TColor($ffffff);
      listItem.TextSettings.Font.Size := 1;
      listItem.Text := it.Descricao;

      FormHome.lstCategoriasGrid.AddObject(listItem);
    end;
    FormHome.lstCategoriasGrid.EndUpdate;
  finally
    lista.Free;
  end;

end;


end.
