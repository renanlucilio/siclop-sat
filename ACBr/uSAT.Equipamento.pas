/// <summary>
///   Nessa unit temos uma classe que implementa os m�todos com base no componente
///   ACBrSAT
/// </summary>
unit uSAT.Equipamento;

interface

uses
  ACBrBase,
  ACBrSAT,
  ACBrSATClass,
  uSAT,
  uSoftwareHouse,
  uEstabelecimento,
  uSAT.Impressora,
  pcnConversao,
  uCFe,
  FMX.Types,
  System.IOUtils,
  uVendaRepository,
  uCupom, uCliente,
  uVenda,
  uPastas, uVendaProduto;

type
  TEvent = procedure of object;
  TEventStr = procedure(const value: string) of object;

  TSAT_Equipamento = class
  private
    FPastaCupom: string;
    _Iniciado:         Boolean;
    FEquipamento:      TACBrSAT;
    FSat:              TSAT;
    FSoftHouse:        TSoftwareHouse;
    FEstabelecimento:  TEstabelecimento;
    FImpressora:       TSAT_Impressora;
    FTmrEnviarSAT:     TTimer;
    FPasta:            TPasta;
    FOnIniciado:       TEvent;
    FOnErroIniciar:    TEvent;
    FOnNumeroSerieSat: TEventStr;
    FOnMensagemSat:    TEventStr;
    procedure GetCodigoDeAtivacao(var chave: AnsiString);
    procedure GetChaveAC(var chave: AnsiString);
    procedure GetTotalCFe(var total: Currency);
    procedure SetTotalEQuantidadeCFe(const CFe: TDadosCFe; var total: Currency; var quantidade: Integer);
    procedure VerificarSat();
    procedure EnviarSATAutomatico();
    procedure AdicionarCupomAVenda(const idVenda: integer; arquivoSaida: string; repo: TVendaRepository);
    procedure AdicionarVenda(const CFe: TDadosCFe; valorImposto: Currency; total: Currency; repo: TVendaRepository; var idVenda: Integer);
    procedure AdicionarClienteAVenda(const CFe: TDadosCFe; repo: TVendaRepository; idVenda: Integer);
    procedure AdicionarProdutosAVenda(const CFe: TDadosCFe; repo: TVendaRepository; idVenda: Integer);
  public
    constructor Create( SAT: TSAT; SoftHouse: TSoftwareHouse;
                        estabelecimento: TEstabelecimento; impressora: TSAT_Impressora;
                        pasta: TPasta);
    destructor Destroy; override;

    property Equipamento: TACBrSAT read FEquipamento write FEquipamento;
    property OnIniciado:       TEvent    read FOnIniciado       write FOnIniciado;
    property OnErroIniciar:    TEvent    read FOnErroIniciar    write FOnErroIniciar;
    property OnNumeroSerieSat: TEventStr read FOnNumeroSerieSat write FOnNumeroSerieSat;
    property OnMensagemSat:    TEventStr read FOnMensagemSat    write FOnMensagemSat;
    property Inicializado:     Boolean read _Iniciado;

    procedure IniciarSAT();
    procedure EnviarSAT(const CFe: TDadosCFe);
    procedure CancelarSAT(const arquivo: string; const id: integer);
    function GetNumSerie(): string;
    function GetStatus(): string;
    function GetLog(): string;
    function GetConsulta(): string;
  end;

implementation

uses
  ACBrDFeSSL,
  System.Classes,
  System.SysUtils,
  pcnCFe,
  uItem.CFe,
  ACBrSATExtratoClass,
  uTimerAnonymous, uCFe.Txt, System.DateUtils, System.Generics.Collections;

{ TSAT_Equipamento }

/// <summary>
///   M�todo para gerar a Chave AC com o componente ACBrSat
/// <param name="chave">
///   Parametro usado como saida de informa�ao para a chave AC
/// </param>
/// </summary>
procedure TSAT_Equipamento.GetChaveAC(var chave: AnsiString);
begin
  chave:= AnsiString(FSoftHouse.ChaveAC);
end;

/// <summary>
///   M�todo usado para cancelar algum cupom SAT
/// <param name="arquivo">
///   Arquivo XML usado para cancelar as informa��es
/// </param>
/// <param name="id">
///   Parametro que pede o ID da venda para cancelar o cupom
/// </param>
/// </summary>
procedure TSAT_Equipamento.CancelarSAT(const arquivo: string; const id: integer);
var
  venda: TVendaRepository;
  msgErro: string;
begin

  if _Iniciado then
  begin
    Equipamento.CFe.LoadFromFile(arquivo);
    Equipamento.CFe2CFeCanc();
    Equipamento.CancelarUltimaVenda
      (AnsiString(Equipamento.CFeCanc.infCFe.chCanc),
      AnsiString(Equipamento.CFeCanc.GerarXML(true)));

    if Equipamento.Resposta.codigoDeRetorno = 7000 then
    begin
      Equipamento.Extrato.Filtro := fiNenhum;
      Equipamento.ImprimirExtratoCancelamento;

      venda:= TVendaRepository.Create();
      try
        venda.CancelarVenda(id);
      finally
        venda.Free;
      end;

      OnMensagemSat('SAT Cancelado com sucesso');
    end
    else
    begin
      if Equipamento.Resposta.mensagemRetorno <> 'null' then
      begin
        msgErro:= 'Mensagem: ' + Equipamento.Resposta.mensagemRetorno + #13 +
                  'Sefaz: ' + Equipamento.Resposta.mensagemSEFAZ;
      end;

      OnMensagemSat(msgErro);
      raise Exception.Create(msgErro);
    end;
  end;
end;

/// <summary>
///   M�todo construtor que inicia o componente ACBrSat com base em outras classes
/// <param name="SAT">
///   Instancia da classe TSAT
/// </param>
/// <param name="SoftHouse">
///   Instancia da classe TSoftwareHouse
/// </param>
/// <param name="estabelecimento">
///   Instancia da classe TEstabelecimento
/// </param>
/// <param name="impressora">
///   Instancia do classe TSAT_Impressora
/// </param>
/// <param name="pasta">
///   Instancia da classe TPasta
/// </param>
/// </summary>
constructor TSAT_Equipamento.Create( SAT: TSAT; SoftHouse: TSoftwareHouse;
                        estabelecimento: TEstabelecimento; impressora: TSAT_Impressora;
                        pasta: TPasta);
begin
  FSat             := SAT;
  FSoftHouse       := SoftHouse;
  FEstabelecimento := estabelecimento;
  FImpressora      := impressora;
  FPasta           := pasta;
  FPastaCupom:= pasta.PastaCupom;
  FEquipamento:= TACBrSAT.Create(nil);

  Equipamento.ArqLOG           := FPasta.PastaLog+ 'SATLog.log';
  Equipamento.Config.XmlSignLib:= xsNone;

  Equipamento.ConfigArquivos.PastaCFeVenda        := FPasta.PastaVenda;
  Equipamento.ConfigArquivos.PastaCFeCancelamento := FPasta.PastaCancelos;
  Equipamento.ConfigArquivos.PastaEnvio           := FPasta.PastaEnviado;
  Equipamento.ConfigArquivos.SalvarCFe            := True;
  Equipamento.ConfigArquivos.SalvarCFeCanc        := True;
  Equipamento.ConfigArquivos.SalvarEnvio          := True;
  Equipamento.ConfigArquivos.SepararPorCNPJ       := True;
  Equipamento.ConfigArquivos.SepararPorModelo     := False;
  Equipamento.ConfigArquivos.SepararPorAno        := False;
  Equipamento.ConfigArquivos.SepararPorMes        := False;
  Equipamento.ConfigArquivos.SepararPorDia        := False;

  Equipamento.ValidarNumeroSessaoResposta         := False;
  Equipamento.CFe.IdentarXML                      := True;
  Equipamento.CFe.TamanhoIdentacao                := 3;
  Equipamento.CFeCanc.IdentarXML                  := True;
  Equipamento.CFeCanc.TamanhoIdentacao            := 3;

  Equipamento.NomeDLL                      := SAT.DLL;
  Equipamento.Config.EhUTF8                := SAT.EhUTF;
  Equipamento.Config.infCFe_versaoDadosEnt := SAT.VersaoXML;
  Equipamento.Config.PaginaDeCodigo        := SAT.CodigoPagina;
  Equipamento.OnGetcodigoDeAtivacao        := GetCodigoDeAtivacao;
  Equipamento.OnGetsignAC                  := GetChaveAC;

  Equipamento.Config.ide_CNPJ        := SoftHouse.CNPJ;
  Equipamento.Config.ide_numeroCaixa := SoftHouse.NumeroCaixa;

  Equipamento.Config.emit_CNPJ          := estabelecimento.CNPJ;
  Equipamento.Config.emit_IE            := estabelecimento.IE;
  Equipamento.Config.emit_IM            := estabelecimento.IM;
  Equipamento.Config.emit_cRegTribISSQN := estabelecimento.ISSQN;

  Equipamento.CFe.RetirarAcentos := False;
  Equipamento.CFeCanc.RetirarAcentos := False;

  Equipamento.Extrato := impressora.Impressora;

  if estabelecimento.FazerRateio then
    Equipamento.Config.emit_indRatISSQN := irSim
  else
    Equipamento.Config.emit_indRatISSQN := irNao;

  if estabelecimento.ehSimplesNacional then
    Equipamento.Config.emit_cRegTrib := RTSimplesNacional
  else
    Equipamento.Config.emit_cRegTrib := RTRegimeNormal;

  {$IFDEF DEBUG}
    Equipamento.Config.ide_tpAmb := taHomologacao;
  {$ELSE}
    Equipamento.Config.ide_tpAmb := taProducao;
  {$ENDIF}

  if SAT.EhSTDCall then
    Equipamento.Modelo := satDinamico_stdcall
  else
    Equipamento.Modelo := satDinamico_cdecl;

  Equipamento.Extrato.PathPDF := FPasta.PastaPDF;
  VerificarSat();
end;

/// <summary>
///   M�todo destrutor que remove a instacia da TSAT_Equipamento
/// </summary>
destructor TSAT_Equipamento.Destroy;
begin
  FEquipamento.Free;
  OnIniciado:= nil;
  OnErroIniciar:= nil;
  OnNumeroSerieSat:= nil;
  OnMensagemSat:= nil;
  inherited;
end;

/// <summary>
///   M�todo usado para criar um cupom SAT e eviar para o equipamento e tamb�m
///   persiste as informa��es no banco de dados
/// <param name="CFe">
///   Parametro que tem os dados do cupom, que � uma instancia TDadosCFe
/// </param>
/// </summary>
procedure TSAT_Equipamento.EnviarSAT(const CFe: TDadosCFe);
var
  valorImposto: Currency;
  total: Currency;
  quantidade: Integer;
  arquivoSaida: string;
  repo: TVendaRepository;
  idVenda: integer;
  msgErro: string;
begin
  if not Assigned(CFe) then
    Exit;

  if _Iniciado then
  begin
    Equipamento.CFe.Clear;
    Equipamento.InicializaCFe();

    CFe.SetCFe(Equipamento.CFe);

    valorImposto:= Equipamento.CFe.Total.vCFeLei12741;

    SetTotalEQuantidadeCFe(CFe, total, quantidade);

    total:= total + Equipamento.CFe.Det.Items[Equipamento.CFe.Det.Count - 1].Prod.vOutro;
    total:= total - Equipamento.CFe.Total.DescAcrEntr.vDescSubtot;
    if FEstabelecimento.ehSimplesNacional then
      Equipamento.CFe.InfAdic.infCpl:= 'Optante pelo simples nacional lei 123/2006';

    Equipamento.EnviarDadosVenda();

    Randomize;
    arquivoSaida := FormatDateTime('ddmmyyyyhhnnsszzz', now) + IntToStr(Random(100000));

    Equipamento.CFe.NomeArquivo:= arquivoSaida;

    if (Equipamento.Resposta.codigoDeRetorno = 6000) then
    begin

      Equipamento.CFe.SaveToFile(FPasta.PastaXML + arquivoSaida + '.xml');
      try
        Equipamento.Extrato.Filtro := fiNenhum;
        Equipamento.ImprimirExtrato;
        Equipamento.Extrato.NomeDocumento:= Equipamento.Extrato.PathPDF + arquivoSaida + '.pdf';
        Equipamento.Extrato.Filtro := fiPDF;
        Equipamento.ImprimirExtrato;
      except
        raise Exception.Create('Erro na impressora');
      end;


      repo:= TVendaRepository.Create;
      try

        if CFe.Criado then
        begin
          AdicionarCupomAVenda(CFe.Identificacao.Numero, arquivoSaida, repo);
        end
        else
        begin
          AdicionarVenda(CFe, valorImposto, total, repo, idVenda);
          AdicionarCupomAVenda(idVenda, arquivoSaida, repo);
          AdicionarClienteAVenda(CFe, repo, idVenda);
          AdicionarProdutosAVenda(CFe, repo, idVenda);

        end;
      finally
        repo.Free;
      end;

      if not FSat.EnvioAutomatico then
        OnMensagemSat('SAT Enviado com sucesso');
    end
    else
    begin
      if Equipamento.Resposta.mensagemRetorno <> 'null' then
      begin
        msgErro:= 'Mensagem: ' + Equipamento.Resposta.mensagemRetorno + #13 +
                  'Sefaz: ' + Equipamento.Resposta.mensagemSEFAZ;
      end;

      if Equipamento.Resposta.codigoDeRetorno = 6010 then
      begin
        msgErro := msgErro + #13 + 'Alguns dos dados est� inv�lido, verifique o TXT';
      end;

      OnMensagemSat(msgErro);
      raise Exception.Create(msgErro);
    end;
  end
  else
    raise Exception.Create('N�o � poss�vel enviar o CUPOM com o SAT OFF');

end;

/// <summary>
///   M�todo que � disparado automaticamente pelo o timer para enviar os cupoms sat
/// </summary>
procedure TSAT_Equipamento.EnviarSATAutomatico;
var
  CFe: TDadosCFe;
  arquivoTxt: TCFeTxt;
  it: string;
  msg: string;
begin
  if _Iniciado then
  begin

    for it in arquivoTxt.ListarArquivos(FPastaCupom) do
    begin
      arquivoTxt:= TCFeTxt.Create();
      try
        try

          msg:= arquivoTxt.VerificarTxt(it).Trim;
          if msg <> '' then
          begin
            arquivoTxt.MoverParaErro(it);
            raise Exception.Create(msg);
          end;

          CFe := arquivoTxt.TxtToCFe(it);
          EnviarSAT(CFe);

        except
          on e:Exception do
          begin
            msg:= e.Message;
            msg:= msg.ToLower;

            if not (msg.Contains('printing')) or not (msg.Contains('impressora')) then
            begin
              OnMensagemSat(e.Message);
            end;
          end;
        end;
      finally
        if FileExists(it) then
          DeleteFile(it);

        FreeAndNil(arquivoTxt);
      end;
    end;

  end;
end;

/// <summary>
///   M�todo usado para verifica como ser� feito o envio dos dados
/// </summary>
procedure TSAT_Equipamento.VerificarSAT;
begin
  if FSat.EnvioAutomatico then
  begin
    FTmrEnviarSAT:= TTimer.CreateAnonymousTimer(
    function: Boolean
    begin
      if Assigned(FSat) and Assigned(FTmrEnviarSAT) then
        if FSat.EnvioAutomatico then
        begin
          FTmrEnviarSAT.Interval:= 2000;
          EnviarSATAutomatico();
        end
        else
          exit(false);

      result:= false;
    end,
    2000, 2000);
  end;
end;

/// <summary>
///   M�todo usado para pegar as informa��es do c�digo de ativa��o
/// <param name="chave">
///   Parametro de saida que tem o c�digo de ativa��o
/// </param>
/// </summary>
procedure TSAT_Equipamento.GetCodigoDeAtivacao(var chave: AnsiString);
begin
  chave:= AnsiString(FSAT.CodigoAtivacao);
end;

/// <summary>
///   M�todo usado para pegar as informa��es de consulta do sat
/// <returns>
///   String com as informa��es de Consulta
/// </returns>
/// </summary>
function TSAT_Equipamento.GetConsulta: string;
begin
  Result:= '';
  if _Iniciado then
  begin
      Result:= Equipamento.ConsultarSAT;
  end;
end;

/// <summary>
///   M�todo usado para adicionar produtos a venda com base na isntancia TDadosCFe
/// <param name="CFe">
///   Instancia da classe TDadosCFe
/// </param>
/// <param name="TVendaRepository">
///   Instancia da classe TVendaRepository
/// </param>
/// </summary>
procedure TSAT_Equipamento.AdicionarProdutosAVenda(const CFe: TDadosCFe; repo: TVendaRepository; idVenda: Integer);
var
  lista: System.Generics.Collections.TObjectList<TVendaProduto>;
  it: TItem;
  vendaProduto: TVendaProduto;
begin
  lista := TObjectList<TVendaProduto>.Create;
  try
    for it in CFe.Itens do
    begin
      vendaProduto := TVendaProduto.Create;
      vendaProduto.VendaId := idVenda;
      vendaProduto.Quantidade := it.Produto.Quantidade;
      vendaProduto.DescricaoProdutoAtual := it.Produto.Descricao;
      vendaProduto.ValorProdutoAtual := it.Produto.ValorUnitario;
      vendaProduto.Pis_CST := it.Imposto.PIS.CST;
      vendaProduto.Cofins_CST := it.Imposto.COFINS.CST;
      vendaProduto.CFOP := it.Produto.CFOP;
      vendaProduto.NCM := it.Produto.NCM;
      lista.Add(vendaProduto);
    end;
    repo.AddProdutosDaVenda(lista, idVenda, false);
  finally
    lista.Free;
  end;
end;

/// <summary>
///   M�todo usado para pegar as informa��es do Cliente com base no Cupom
/// <param name="CFe">
///   Instancia da classe TDadosCFe
/// </param>
/// <param name="repo">
///   Intancia da classe TVendaRepository
/// </param>
/// </summary>
procedure TSAT_Equipamento.AdicionarClienteAVenda(const CFe: TDadosCFe; repo: TVendaRepository; idVenda: Integer);
var
  cliete: TCliente;
begin
  cliete := TCliente.Create;
  try
    cliete.Nome := CFe.Cliente.Nome;
    cliete.Documento := CFe.Cliente.Documento;
    repo.AddCliente(cliete, idVenda);
  finally
    cliete.Free;
  end;
end;

/// <summary>
///   M�todo usado para Adicionar Venda no banco de dados
/// <param name="CFe">
///   Instancia da classe TDadosCFe
/// </param>
/// <param name="valorImposto">
///   Valor do imposto que ser� adicionado no banco de dados
/// </param>
/// </summary>
procedure TSAT_Equipamento.AdicionarVenda(const CFe: TDadosCFe; valorImposto: Currency; total: Currency; repo: TVendaRepository; var idVenda: Integer);
var
  venda: TVenda;
begin
  venda := TVenda.Create;
  try
    venda.DataHora := DateToISO8601(Now);
    venda.Valor := CFe.Total.Valor;
    venda.ValorTributacao := valorImposto;
    venda.Acrescimo := CFe.Total.Acrescimo;
    venda.Desconto := CFe.Total.Desconto;
    venda.OperadoraCartao := CFe.Pagamentos.First.CodigoOperadora.ToString;
    venda.Gerada := 1;
    if CFe.Pagamentos.First.TipoPagamento = 1 then
      venda.FormaPagamento := 'Dinheiro'
    else if CFe.Pagamentos.First.TipoPagamento = 2 then
      venda.FormaPagamento := 'Cheque'
    else if CFe.Pagamentos.First.TipoPagamento = 7 then
      venda.FormaPagamento := 'Cart�o de cr�dito'
    else if CFe.Pagamentos.First.TipoPagamento = 6 then
      venda.FormaPagamento := 'Cart�o de d�bito'
    else if CFe.Pagamentos.First.TipoPagamento = 4 then
      venda.FormaPagamento := 'Cr�dito em loja'
    else if CFe.Pagamentos.First.TipoPagamento = 3 then
      venda.FormaPagamento := 'Vale alimenta��o'
    else if CFe.Pagamentos.First.TipoPagamento = 5 then
      venda.FormaPagamento := 'Vale refei��o'
    else if CFe.Pagamentos.First.TipoPagamento = 86 then
      venda.FormaPagamento := 'Vale presente'
    else if CFe.Pagamentos.First.TipoPagamento = 76 then
      venda.FormaPagamento := 'Vale compust�vel'
    else if CFe.Pagamentos.First.TipoPagamento = 9 then
      venda.FormaPagamento := 'Outros';
    idVenda := repo.CriarVenda(venda);
  finally
    venda.Free;
  end;
end;

/// <summary>
///   M�todo usado para adicionar um cupom a os arquivo arquivos no banco de dados
/// <param name="idVenda">
///   Parametro usado para saber qual venda ser� adicionado as informa��es
/// </param>
/// <param name="arquivoSaida">
///   Arquivo que ser� adicionado na tabela do banco de vendas
/// </param>
/// </summary>
procedure TSAT_Equipamento.AdicionarCupomAVenda(const idVenda: integer; arquivoSaida: string; repo: TVendaRepository);
var
  cupom: TCupom;
begin
  cupom := TCupom.Create;
  try
    cupom.Nome := arquivoSaida;
    cupom.ArquivoXml := TPath.Combine(FPasta.PastaXML, arquivoSaida + '.xml');
    cupom.ArquivoPdf := TPath.Combine(Equipamento.Extrato.PathPDF, arquivoSaida + '.pdf');
    cupom.Cancelado := 0;
    cupom.Gerado := 1;
    repo.AddCupom(cupom, idVenda);
  finally
    cupom.Free;
  end;
end;

/// <summary>
///   M�todo usado para Definir o valor total do CFe
/// <param name="CFe">
///   Instancia da classe TDadosCFe
/// </param>
/// <param name="total">
///   Valor Total que ser� adicionado na tabela
/// </param>
/// </summary>
procedure TSAT_Equipamento.SetTotalEQuantidadeCFe(const CFe: TDadosCFe; var total: Currency; var quantidade: Integer);
var
  it: TItem;
begin
  total := 0;
  quantidade := 0;
  for it in CFe.Itens do
  begin
    total := total + (it.Produto.ValorUnitario * it.Produto.Quantidade);
    quantidade := quantidade + Round(it.Produto.Quantidade);
  end;
end;

/// <summary>
///   M�todo usado para pegar o LOG do equipamento
/// <returns>
///   Retorna uma string com o log
/// </returns>
/// </summary>
function TSAT_Equipamento.GetLog(): string;
var
  resultTemp: TStringList;
begin
  Result:= '';
  resultTemp:= TStringList.Create();
  try
    if _Iniciado then
    begin
      Equipamento.ExtrairLogs(resultTemp);
      Result:= resultTemp.Text;
    end;
  finally
    resultTemp.Free;
  end;

end;

/// <summary>
///   M�todo usado para Pegar o Valor Total do CFe
/// <param name="total">
///   Parametro de referencia para com as informa��es do total
/// </param>
/// </summary>
procedure TSAT_Equipamento.GetTotalCFe(var total: Currency);
var
  I: Integer;
begin
  for I := 0 to Equipamento.CFe.Pagto.Count-1 do
    total := total + Equipamento.CFe.Pagto.Items[I].vMP;
end;

/// <summary>
///   M�todo usado para Pegar o numero de s�rie do equipamento
/// <returns>
///   String com o numero de s�rie
/// </returns>
/// </summary>
function TSAT_Equipamento.GetNumSerie(): string;
begin
  Result:= '';
  if _Iniciado then
  begin
    Equipamento.ConsultarStatusOperacional;
    if Assigned(Equipamento.Resposta) and
       (Equipamento.Resposta.codigoDeRetorno = 10000) then
    begin
      Result:= Equipamento.Status.NSERIE;
    end;
  end;
end;

/// <summary>
///   M�todo usado para pegar o status do equipamento SAT
/// <returns>
///   string com o numero de s�rie
/// </returns>
/// </summary>
function TSAT_Equipamento.GetStatus(): string;
var
  resultTemp: TStringBuilder;
begin
  result := '';
  if _Iniciado then
  begin
    resultTemp := TStringBuilder.Create();
    try
     Equipamento.ConsultarStatusOperacional();

      resultTemp
        .Append('Estado Operacional:    ')
        .Append(Equipamento.status.EstadoOperacaoToStr(Equipamento.status.ESTADO_OPERACAO)).AppendLine()
        .Append('N� de S�rie:           ').Append(Equipamento.status.NSERIE).AppendLine()
        .Append('Status Lan:            ').Append(Equipamento.status.StatusLanToStr
        (Equipamento.status.STATUS_LAN)).AppendLine()
        .Append('Nivel Bateria:         ').Append(Equipamento.status.NivelBateriaToStr
        (Equipamento.status.NIVEL_BATERIA)).AppendLine()
        .Append('Data Hora S@T:         ')
        .Append(DateToStr(Equipamento.status.DH_ATUAL)).AppendLine()
        .Append('Vers�o Do XML:         ')
        .Append(Equipamento.status.VER_LAYOUT).AppendLine()
        .Append('Ultima Nota:           ')
        .Append(Equipamento.status.ULTIMO_CFe).AppendLine()
        .Append('Data/Hora Nota:        ')
        .Append(DateToStr(Equipamento.status.DH_ULTIMA)).AppendLine()
        .Append('Emiss�o Certificado:   ')
        .Append(DateToStr(Equipamento.status.CERT_EMISSAO)).AppendLine()
        .Append('Vencimento Certificado:')
        .Append(DateToStr(Equipamento.status.CERT_VENCIMENTO)).AppendLine();

      result := resultTemp.ToString;
    finally
      resultTemp.Free;
    end;
  end;

end;

/// <summary>
///   M�todo para iniciar o equipamento SAT
/// </summary>
procedure TSAT_Equipamento.IniciarSAT;
begin
  Equipamento.Inicializar;
  if Equipamento.Inicializado then
  begin
    Equipamento.ConsultarSAT;
    if Equipamento.Resposta.codigoDeRetorno = 8000 then
    begin
      Equipamento.ConsultarStatusOperacional;
      if Equipamento.Status.NSERIE <> '' then
      begin
        OnNumeroSerieSat('SAT: ON N�: '+Equipamento.Status.NSERIE);

        _Iniciado := true;
      end
      else
      begin
        OnNumeroSerieSat('SAT: OFF');
        OnErroIniciar();
      end;
    end
    else
    begin
      OnNumeroSerieSat('SAT: OFF');
      OnErroIniciar();
    end;
  end
  else
     OnErroIniciar();
end;

end.
