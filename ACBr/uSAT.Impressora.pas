/// <summary>
///   Unit com a classe de implementa��o dos m�todo com base na classe do ACBr
///   TACBrSATExtratoFortes
/// </summary>
unit uSAT.Impressora;

interface

uses
  ACBrBase,
  ACBrSATExtratoClass,
  ACBrSATExtratoReportClass,
  ACBrSATExtratoFortesFr,
  uImpressora;

type
  TSAT_Impressora = class
  private
    FImpressora: TACBrSATExtratoFortes;
  public
    constructor Create(_impressora: TImpressora);
    destructor Destroy; override;
    property Impressora: TACBrSATExtratoFortes read FImpressora write FImpressora;
  end;

implementation

{ TSAT_Impressora }

/// <summary>
///   M�todo construtor que cria uma instancia da TSAT_Impressora
/// <param name="_impressora">
///   Instancia de TImpressora
/// </param>
/// </summary>
constructor TSAT_Impressora.Create(_impressora: TImpressora);
begin
  FImpressora:= TACBrSATExtratoFortes.Create(nil);

  Impressora.ImprimeDescAcrescItem := _impressora.ImprimeDescontoAcrescimo;
  Impressora.ImprimeEmUmaLinha     := _impressora.ItemPorLinha;
  Impressora.MostraPreview         := _impressora.Preview;
  Impressora.Sistema               := 'SICLOP-SAT';
  Impressora.Impressora            := _impressora.NomeImpressora;
  Impressora.LarguraBobina         := _impressora.LarguraBobina;
  Impressora.MargemSuperior        := _impressora.MargeTopo;
  Impressora.MargemEsquerda        := _impressora.MargeEsquerda;
  Impressora.MargemInferior        := _impressora.MargeInferior;
  Impressora.MargemDireita         := _impressora.MargeDireita;
  if _impressora.Logo <> '' then
    Impressora.PictureLogo.LoadFromFile(_impressora.Logo);
end;

/// <summary>
///   M�todo destrutor usado para destruir a instancia de TSAT_Impressora
/// </summary>
destructor TSAT_Impressora.Destroy;
begin
  FImpressora.Free;
  inherited;
end;

end.
