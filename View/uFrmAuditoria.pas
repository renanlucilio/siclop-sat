unit uFrmAuditoria;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Objects, FMX.Effects, FMX.Layouts, FMX.ListBox, FMX.TabControl,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Edit, FMX.ComboEdit,
  FMX.DateTimeCtrls, uVm.Auditoria, FMX.ScrollBox, FMX.Memo;

type
//form de auditoria
  TFrmAuditoria = class(TForm)
    Rectangle1: TRectangle;
    ShadowEffect1: TShadowEffect;
    Layout1: TLayout;
    Layout2: TLayout;
    Layout3: TLayout;
    Layout4: TLayout;
    Layout5: TLayout;
    Layout6: TLayout;
    Layout7: TLayout;
    Layout8: TLayout;
    Text1: TText;
    Text2: TText;
    Line1: TLine;
    txtResumo: TText;
    Text4: TText;
    Text5: TText;
    Layout9: TLayout;
    Layout10: TLayout;
    Text6: TText;
    Text7: TText;
    Line2: TLine;
    txtDataHora: TText;
    Line3: TLine;
    txtCategoria: TText;
    Line4: TLine;
    Layout11: TLayout;
    Text10: TText;
    Rectangle2: TRectangle;
    Layout12: TLayout;
    Layout13: TLayout;
    Text11: TText;
    Button1: TButton;
    StyleBook1: TStyleBook;
    Layout14: TLayout;
    Text12: TText;
    dtData: TDateEdit;
    Layout15: TLayout;
    Text13: TText;
    cedCategoria: TComboEdit;
    lstOcorrencias: TListBox;
    mmoDetalhes: TMemo;
    //evento de clique para fechar a tela de relatorio
    procedure Rectangle2Click(Sender: TObject);
    //evneto de create para criar esse form
    procedure FormCreate(Sender: TObject);
    //m�todo para eliminar o form
    procedure FormDestroy(Sender: TObject);
    //m�todo de clique para filtar
    procedure Button1Click(Sender: TObject);
  private
    vm: TAuditoriaVm;
    { Private declarations }
  public
    { Public declarations }
  end;

  var
    frmAuditoria: TFrmAuditoria;

implementation

{$R *.fmx}


procedure TFrmAuditoria.Button1Click(Sender: TObject);
begin
  vm.Filtrar;
end;

procedure TFrmAuditoria.FormCreate(Sender: TObject);
begin
  vm:= TAuditoriaVm.Create;
end;

procedure TFrmAuditoria.FormDestroy(Sender: TObject);
begin
  vm.Free;
end;

procedure TFrmAuditoria.Rectangle2Click(Sender: TObject);
begin
  self.Close;
end;

end.
