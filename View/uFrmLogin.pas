unit uFrmLogin;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Effects, FMX.Objects, FMX.Layouts, FMX.Controls.Presentation,
  FMX.StdCtrls, FMX.Edit, uUsuario;

type
//form de login
  TfrmLogin = class(TForm)
    Rectangle1: TRectangle;
    ShadowEffect1: TShadowEffect;
    Layout1: TLayout;
    Layout2: TLayout;
    Layout3: TLayout;
    Layout4: TLayout;
    Text1: TText;
    Text2: TText;
    btnLogar: TButton;
    StyleBook1: TStyleBook;
    Layout5: TLayout;
    Layout6: TLayout;
    Text3: TText;
    Text4: TText;
    Line1: TLine;
    Line2: TLine;
    edtUsuario: TEdit;
    edtSenha: TEdit;
    txtValidacao: TText;
    Rectangle2: TRectangle;
    procedure FormCreate(Sender: TObject);
    procedure btnLogarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Rectangle2Click(Sender: TObject);
  private
    { Private declarations }

    procedure FecharModal();
    procedure AbrirSistema(user: TUsuario);
  public
    { Public declarations }
  end;

var
  frmLogin: TfrmLogin;

implementation

{$R *.fmx}

uses uSistema, Form.Principal, uVm.Principal, uUsuarioReporitory;

/// <summary>
///   M�todo para iniciar o sistema e ainda verifica se o form principal est� 
///   ativado ou n�o.
/// </summary>
/// <param name="user">
///   Parametro necess�rio para fazer a login e para verifica o login
/// </param>
procedure TfrmLogin.AbrirSistema(user: TUsuario);
begin

  if not Assigned(FormHome) then
  begin
    FormHome:= TFormHome.Create(Application);

    if not Assigned(dmVmPrincipal) then
      dmVmPrincipal:= TdmVmPrincipal.Create(Application);

    dmVmPrincipal.Autenticar(user);
    FormHome.Show();
    FecharModal();
  end
  else
  begin
    dmVmPrincipal.Autenticar(user);
    FormHome.Show();
    FecharModal();
  end;                      
end;

/// <summary>
///   M�todo usado para fazer o login pegando as informa��es do 
///   edtUsuario e edtSenha
/// </summary>
procedure TfrmLogin.btnLogarClick(Sender: TObject);
var
  repo: TUsuarioRepository;
  user: TUsuario;
begin
  user:= nil;


  if (edtUsuario.Text = 'SICLOP') and (edtSenha.Text = 'SAT787878') then
  begin
    user:= TUsuario.Create();
    user.Nome:= 'SICLOP';
    user.Senha:= 'SICLOP';
    user.Nivel:= 3;

    AbrirSistema(user);
    exit;
  end;


  repo:= TUsuarioRepository.Create();
  try
    user:= repo.Logar(edtUsuario.Text, edtSenha.Text);
    if Assigned(user) then
    begin

      AbrirSistema(user);
    end
    else
    begin
      txtValidacao.Visible:= true;
    end;
  finally
    repo.Free;
  end;
end;

/// <summary>
///   M�todo usado para fechar o form 
/// </summary>
procedure TfrmLogin.FecharModal;
begin
  Rectangle1.Visible:= false;
  edtUsuario.Text:= '';
  edtSenha.Text:= '';
  frmLogin.Hide();
end;

/// <summary>
///   Evento acionado quando o form � criado e verifica se j� esta tem um usu�rio
///   logado
/// </summary>
procedure TfrmLogin.FormCreate(Sender: TObject);
var
  sistema: TSistema;
  arquivo: string;
  usuario: TUsuario;
begin
  arquivo:= ExtractFilePath(ParamStr(0)) + 'config.ini';
  sistema:= TSistema.Create;
  try
    if not(FileExists(arquivo)) or (not(sistema.Autonamo)) then
    begin
      if not FileExists(arquivo) then
      begin
        usuario:= TUsuario.Create();
        usuario.Nome:= 'SICLOP';
        usuario.Senha:= 'SICLOP';
        usuario.Nivel:= 3;

        AbrirSistema(usuario);
        exit;
      end;

      usuario:= TUsuario.Create();
      usuario.Nome:= 'SICLOP';
      usuario.Senha:= 'SICLOP';
      usuario.Nivel:= 2;
      AbrirSistema(usuario);
    end
    else
      edtUsuario.SetFocus();

  finally

    sistema.Free;
  end;

end;


/// <summary>
///   Evento acionado quando o form aparece e define o f�cu no edt de usu�rio
/// </summary>
procedure TfrmLogin.FormShow(Sender: TObject);
begin
  edtUsuario.SetFocus;
end;

/// <summary>
///   M�todo usado para fechar a aplica��o 
/// </summary>
procedure TfrmLogin.Rectangle2Click(Sender: TObject);
begin
  Application.Terminate;
end;

end.
