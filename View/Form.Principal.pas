﻿unit Form.Principal;

interface

uses
  uGridData,
  System.SysUtils,
  System.Types,
  System.UITypes,
  System.Classes,
  System.Variants,
  System.ImageList,
  System.Actions,
  System.IOUtils,
  FMX.Types,
  FMX.Controls,
  FMX.Forms,
  FMX.Graphics,
  FMX.Dialogs,
  FMX.Layouts,
  FMX.Objects,
  FMX.Effects,
  FMX.ImgList,
  FMX.ListBox,
  FMX.TabControl,
  FMX.ActnList,
  FMX.ListView.Types,
  FMX.ListView.Appearances,
  FMX.ListView,
  FMX.Controls.Presentation,
  FMX.Edit,
  FMX.Ani,
  FMX.StdCtrls,
  FMX.platform.Win,
  FMX.ListView.Adapters.Base,
  FMX.DateTimeCtrls,
  FMX.ScrollBox,
  FMX.Memo,
  Winapi.Messages,
  Model.LibUtil,
  System.Rtti,
  System.Bindings.Outputs,
  Fmx.Bind.Editors,
  Data.Bind.EngExt,
  Fmx.Bind.DBEngExt,
  Data.Bind.Components, uVm.Principal, FMX.Grid.Style, FMX.ComboEdit,
  FMX.Grid, Fmx.Bind.Grid, Data.Bind.Grid, Data.Bind.DBScope, uVm.Produto,
  uVm.Usuario, FMX.SearchBox, uVm.CriadorCupom;

type
//formulário principal
  TFormHome = class(TForm)
    LayoutBarraSuperior: TLayout;
    LayoutBarraLateral: TLayout;
    LayoutCorpo: TLayout;
    RectBarraSuperior: TRectangle;
    RectBarraLateral: TRectangle;
    ShadowBarraSuperior: TShadowEffect;
    ShadowBarraLateral: TShadowEffect;
    LineSeparadorSuperior: TLine;
    LayoutMenuSuperior: TLayout;
    RectBtnIncio: TRectangle;
    LineSublinhado: TLine;
    TxtInicio: TText;
    RectBtnRelatorios: TRectangle;
    TxtRelatorios: TText;
    RectBtnCupons: TRectangle;
    TxtCupons: TText;
    TxtSICLOPSAT: TText;
    RectTitulo: TRectangle;
    GlyphMenuLateral: TGlyph;
    ActLst: TActionList;
    ActTrocaTab: TChangeTabAction;
    StyleBook: TStyleBook;
    ImageList: TImageList;
    RectBtnFechar: TRectangle;
    RectBtnMinimizar: TRectangle;
    GlyphMinimizar: TGlyph;
    GlyphFechar: TGlyph;
    RectBtnConfiguracoes: TRectangle;
    TxtConfiguracoes: TText;
    TabControlMenuLateral: TTabControl;
    TabMenuLateralInicio: TTabItem;
    TabMenuLateralCupons: TTabItem;
    TabMenuLateralRelatorios: TTabItem;
    TabMenuLateralConficuracoes: TTabItem;
    RectangleFundo3: TRectangle;
    RectangleFundo2: TRectangle;
    RectangleFundo1: TRectangle;
    RectangleFundo4: TRectangle;
    LstOpcoesInicio: TListBox;
    LstItemMovimentacao: TListBoxItem;
    LstItemSICLOP: TListBoxItem;
    LstOpcoesRelatorios: TListBox;
    LstItemResumo: TListBoxItem;
    LstOpcoesCupons: TListBox;
    LstItemEnviarNota: TListBoxItem;
    LstItemCancelarNota: TListBoxItem;
    LstOpcoesConfiguracoes: TListBox;
    LstItemLogs: TListBoxItem;
    LstItemSAT: TListBoxItem;
    Conectado: TText;
    GlyphConectado: TGlyph;
    RectStatusSistema: TRectangle;
    TabConfiguracoesImpressoraSAT: TListBoxItem;
    LstItemContabilidade: TListBoxItem;
    LstItemEstabelecimento: TListBoxItem;
    TabControlBody: TTabControl;
    TabBodyInicio: TTabItem;
    TabControlInicioOpcoes: TTabControl;
    TabInicioMovimentacao: TTabItem;
    LayoutMovimentacao: TLayout;
    LayoutInformacoesVenda: TLayout;
    TxtValorTotalItens: TText;
    TxtImpostosAproximados: TText;
    LayoutInformacoesSAT: TLayout;
    TxtNotasProcessadas: TText;
    TxtNotasCanceladas: TText;
    TxtNotasComErros: TText;
    TxtNotasEnviadas: TText;
    TxtMovimentacaoHoje: TText;
    TabInicioSICLOP: TTabItem;
    TxtContatoSiclop: TText;
    TxtFormasParaContato: TText;
    TxtInfoSiclop: TText;
    TxtSericosESistemas: TText;
    TxtSICLOP: TText;
    TabBodyCupons: TTabItem;
    TabControlCupom: TTabControl;
    TabCupomEnviar: TTabItem;
    RectFundoCupom: TRectangle;
    TxtNotaFiscal: TText;
    TxtInformacoesCliente: TText;
    LayoutNotasParaEnvio: TLayout;
    LsVNotasParaEnvio: TListView;
    tabCancelarCupomFiscal: TTabItem;
    TabBodyRelatorios: TTabItem;
    TabControlRelatorios: TTabControl;
    TabRelatoriosResumo: TTabItem;
    TabBodyConfiguracoes: TTabItem;
    TabControlConfiguracoes: TTabControl;
    TabConfiguracoesEquipamentoSAT: TTabItem;
    TabConfiguracoesImpressora: TTabItem;
    TabConfiguracoesContabilidade: TTabItem;
    TabConfiguracoesEstabelecimento: TTabItem;
    TxtComandas2: TText;
    LayoutNotasParaCancelar: TLayout;
    LsVNotasParaCancelar: TListView;
    TxtComandas: TText;
    TxtAvisoCancelamento: TText;
    LayoutResumoSAT: TLayout;
    LayoutDataResumo: TLayout;
    TxtDe: TText;
    TxtAte: TText;
    dtDe: TDateEdit;
    dtAte: TDateEdit;
    TxtQuantidadeNota: TText;
    TxtQuantidadeNotas: TText;
    TxtTotalVendido: TText;
    TxtValorTotalVendido: TText;
    TxtTotalImpostos: TText;
    TxtValorTotalImpostos: TText;
    TxtPreview: TText;
    swtchTSatImpressao_preview: TSwitch;
    TxtItemPorLinha: TText;
    swtchTSatImpressao_itemPorLinha: TSwitch;
    TxtImprimeDesconto: TText;
    swtchITSatImpressao_imprimeDescontoAcrecimo: TSwitch;
    EdtTSatImpressao_margeTopo: TEdit;
    EdtTSatImpressao_margeEsquerda: TEdit;
    EdtTSatImpressao_margeDireita: TEdit;
    EdtTSatImpressao_margeInferior: TEdit;
    TxtMargens: TText;
    TxtTSatImpressao_nomeImpressora: TText;
    TxtEmailsCadastrador: TText;
    EdtTContabilidade_nomeResponsavel: TEdit;
    TxtContador: TText;
    EdtTContabilidade_nomeFantasia: TEdit;
    TxtContabilidade: TText;
    LstItemSoftHouse: TListBoxItem;
    LstItemSistema: TListBoxItem;
    EdtTSatEquipamento_codigoAtivacao: TEdit;
    TxtCodigoAtivacao: TText;
    EdtTSatEquipamento_versaoXML: TEdit;
    TxtVersaoXML: TText;
    EdtTSatEquipamento_caminhoDLL: TEdit;
    TxtLocalDLL: TText;
    EdtTSatEquipamento_codPagina: TEdit;
    TxtCodigoPagina: TText;
    ChkTSatEquipamento_ehUtf: TCheckBox;
    MmoRespostaSAT: TMemo;
    cbbCbBTEstabelecimento_issqn: TComboBox;
    TxtISSQN: TText;
    EdtTEstabelecimento_nomeFantasia: TEdit;
    TxtNomeFantasiaEstabelecimento: TText;
    EdtTEstabelecimento_cnpj: TEdit;
    TxtCNPJEstabelecimento: TText;
    EdtTEstabelecimento_ie: TEdit;
    TxtInscricaoEstadual: TText;
    EdtTEstabelecimento_im: TEdit;
    TxtInscricaoMunicipal: TText;
    EdtTEstabelecimento_email: TEdit;
    TxtEmailEstabelecimento: TText;
    TabConfiguracoesSoftHouse: TTabItem;
    EdtTSoftHouse_nomeFantasia: TEdit;
    TxtNomeFantasiaSoftHouse: TText;
    EdtTSoftHouse_cnpj: TEdit;
    TxtCNPJSoftHouse: TText;
    TxtChaveAc: TText;
    EdtTEstabelecimento_numPDV: TEdit;
    TxtNumeroCaixa: TText;
    TabConfiguracoesSistema: TTabItem;
    EdtTPasta_pastaCupom: TEdit;
    TxtPastaTxt: TText;
    TxtEnvioAutomatico: TText;
    swtchTSatSistema_envioAutomatico: TSwitch;
    EdtTSatImpressao_largura: TEdit;
    TxtLargura: TText;
    TxtNomeImpressora: TText;
    EdtTModelSatImpressao_logo: TEdit;
    MmoTContabilidade_emails: TMemo;
    ChkTContabilidade_envioAutomatico: TCheckBox;
    LstDeProtudos: TListBox;
    Text1: TText;
    swtchTSatEquipamento_ehSTDCall: TSwitch;
    Text2: TText;
    SwitchTEstabelecimento_ehSimplesNacional: TSwitch;
    Text3: TText;
    SwitchTEstabelecimento_fazerRatioISSQN: TSwitch;
    bindList: TBindingsList;
    edtDiaEnvio: TEdit;
    txt2: TText;
    lst1: TListBoxItem;
    lst2: TListBoxItem;
    lst3: TListBoxItem;
    lst4: TListBoxItem;
    lst5: TListBoxItem;
    lst6: TListBoxItem;
    lst7: TListBoxItem;
    txtQtdProcessadas: TText;
    txtQtdEviadas: TText;
    txtQtdErros: TText;
    txtQtdCanceladas: TText;
    txtValorItens: TText;
    txtValorImposto: TText;
    tmrAtualizarUI: TTimer;
    txtQuantidadeCanceladasNotas: TText;
    txtQuantidadeTotalCancelada: TText;
    txtUltimoRelatorio: TText;
    txt1: TText;
    txt4: TText;
    txt5: TText;
    txt6: TText;
    txt7: TText;
    txt8: TText;
    txt9: TText;
    txt10: TText;
    txt11: TText;
    txt12: TText;
    txtVersao: TText;
    layMensagem: TLayout;
    layMsg2: TLayout;
    layMsg3: TLayout;
    layMsg1: TLayout;
    btnFechaMensagens: TLayout;
    txtMsg2: TText;
    txtMsg1: TText;
    txtMsg3: TText;
    gFechaMensagens: TGlyph;
    tmrMensagem: TTimer;
    Rectangle2: TRectangle;
    Rectangle1: TRectangle;
    Rectangle3: TRectangle;
    Layout1: TLayout;
    Layout3: TLayout;
    Layout4: TLayout;
    Layout2: TLayout;
    btnCancelarConfigSat: TSpeedButton;
    btnSalvaConfigSat: TSpeedButton;
    Layout5: TLayout;
    btnCancelarConfigImpressora: TSpeedButton;
    btnSalvarConfigImpressora: TSpeedButton;
    Layout6: TLayout;
    btnCancelarConfigCotabilidade: TSpeedButton;
    btnSalvarConfigCotabilidade: TSpeedButton;
    Layout7: TLayout;
    btnCancelarConfigEstabelecimento: TSpeedButton;
    btnSalvarConfigEstabelecimento: TSpeedButton;
    Layout8: TLayout;
    btnCancelarConfigSoftHouse: TSpeedButton;
    btnSalvarConfigSoftHouse: TSpeedButton;
    Layout9: TLayout;
    btnCancelarConfigSistema: TSpeedButton;
    btnSalvarConfigSistema: TSpeedButton;
    Image1: TImage;
    btnIniciaSat: TButton;
    btnImprimirCupom: TButton;
    Layout10: TLayout;
    txtLegProduto: TText;
    txtTotalCupom: TText;
    Rectangle4: TRectangle;
    btnCancelarCupomFiscal: TButton;
    btnMostrarCupom: TButton;
    btnCarregarRelatorioSimples: TButton;
    Layout11: TLayout;
    Text6: TText;
    Text7: TText;
    btnEnviarRelatorioViaEmail: TButton;
    Layout12: TLayout;
    Text8: TText;
    Text9: TText;
    dtDeContador: TDateEdit;
    dtAteContador: TDateEdit;
    btnSalvarRelatorio: TButton;
    btnSelecionarDllSat: TButton;
    btnConsultarSat: TButton;
    btnLogsSat: TButton;
    btnStatusSat: TButton;
    Rectangle5: TRectangle;
    Rectangle6: TRectangle;
    btnAdicionarImpressora: TButton;
    btnAdicionarEmail: TButton;
    btnLimparEmails: TButton;
    tabBodyCadastro: TTabItem;
    tabMenuCadastro: TTabItem;
    rectBtnCadastros: TRectangle;
    Text10: TText;
    Rectangle8: TRectangle;
    ListBox1: TListBox;
    mmChaveAC: TMemo;
    tbcCadastro: TTabControl;
    tabCategoria: TTabItem;
    tabProduto: TTabItem;
    tabUsuario: TTabItem;
    tbcFormCategoria: TTabControl;
    TabCategoriaLista: TTabItem;
    txtCategoriaId: TText;
    Layout13: TLayout;
    btnCategoriaNovo: TSpeedButton;
    btnCategoriaAtualizar: TSpeedButton;
    btnCategoriaApagar: TSpeedButton;
    btnCategoriaEditar: TSpeedButton;
    TabCategoriaCad: TTabItem;
    txtCategoriaTipoForm: TText;
    VertScrollBox5: TVertScrollBox;
    tbcFormProduto: TTabControl;
    tabProdutoLista: TTabItem;
    txtProdutoId: TText;
    Layout14: TLayout;
    btnProdutoNovo: TSpeedButton;
    btnProdutoAtualizar: TSpeedButton;
    btnProdutoApagar: TSpeedButton;
    btnProdutoEditar: TSpeedButton;
    tabProdutoCad: TTabItem;
    txtProdutoTipoForm: TText;
    Text11: TText;
    swtSistemaAutonomo: TSwitch;
    tbcFormUsuario: TTabControl;
    tabUsuarioLista: TTabItem;
    txtUsuarioId: TText;
    Layout15: TLayout;
    btnUsuarioNovo: TSpeedButton;
    btnUsuarioAtualizar: TSpeedButton;
    btnUsuarioApagar: TSpeedButton;
    btnUsuarioEditar: TSpeedButton;
    tabUsuarioCad: TTabItem;
    txtUsuarioTipoForm: TText;
    VertScrollBox1: TVertScrollBox;
    Rectangle10: TRectangle;
    edtUsuarioNome: TEdit;
    Text15: TText;
    Rectangle13: TRectangle;
    edtUsuarioSenha: TEdit;
    Text18: TText;
    Rectangle14: TRectangle;
    Text19: TText;
    cedUsuarioNivelAcesso: TComboEdit;
    Rectangle15: TRectangle;
    Edit4: TEdit;
    Text20: TText;
    Rectangle16: TRectangle;
    Edit5: TEdit;
    Text21: TText;
    Rectangle17: TRectangle;
    Edit6: TEdit;
    Text22: TText;
    lstITemCategoria: TListBoxItem;
    listItemProduto: TListBoxItem;
    listItemUsuario: TListBoxItem;
    VertScrollBox2: TVertScrollBox;
    Rectangle11: TRectangle;
    edtCategoriaDescricao: TEdit;
    Text47: TText;
    Rectangle12: TRectangle;
    edtCategoriaIcmsOrigem: TEdit;
    Text49: TText;
    Rectangle18: TRectangle;
    edtCategoriaIcmsCst: TEdit;
    Text50: TText;
    Rectangle19: TRectangle;
    edtCategoriaTaxaEstadual: TEdit;
    Text51: TText;
    Text52: TText;
    Rectangle20: TRectangle;
    edtCategoriaTaxaFederal: TEdit;
    Text53: TText;
    Text57: TText;
    Rectangle24: TRectangle;
    edtCategoriaPisCst: TEdit;
    Text61: TText;
    Rectangle30: TRectangle;
    edtCategoriaPisBc: TEdit;
    Text62: TText;
    Text63: TText;
    Rectangle34: TRectangle;
    edtCategoriaPisPorcentagem: TEdit;
    Text64: TText;
    Text65: TText;
    Rectangle35: TRectangle;
    edtCategoriaPisValor: TEdit;
    Text66: TText;
    Text69: TText;
    Text70: TText;
    Rectangle36: TRectangle;
    edtCategoriaIcmsPorcentagem: TEdit;
    Text72: TText;
    Text82: TText;
    Rectangle37: TRectangle;
    edtCategoriaIcmsValor: TEdit;
    Text89: TText;
    Text90: TText;
    Text92: TText;
    Rectangle38: TRectangle;
    edtCategoriaIssqnNatureza: TEdit;
    Text98: TText;
    Rectangle41: TRectangle;
    edtCategoriaIssqnBc: TEdit;
    Text105: TText;
    Text111: TText;
    Rectangle42: TRectangle;
    edtCategoriaIssqnDeducao: TEdit;
    Text112: TText;
    Text113: TText;
    Rectangle76: TRectangle;
    edtCategoriaIssqnCodigoIbge: TEdit;
    Text114: TText;
    Rectangle77: TRectangle;
    edtCategoriaIssqnBcp: TEdit;
    Text115: TText;
    Text116: TText;
    Rectangle78: TRectangle;
    edtCategoriaIssqnItemServico: TEdit;
    Text117: TText;
    Rectangle79: TRectangle;
    edtCategoriaIssqnCodigoTributario: TEdit;
    Text118: TText;
    Text119: TText;
    Text120: TText;
    Rectangle80: TRectangle;
    edtCategoriaPisBcp: TEdit;
    Text121: TText;
    Text122: TText;
    Rectangle81: TRectangle;
    edtCategoriaPisAliquota: TEdit;
    Text123: TText;
    Text124: TText;
    Text125: TText;
    Rectangle82: TRectangle;
    edtCategoriaPisstBc: TEdit;
    Text126: TText;
    Text127: TText;
    Rectangle83: TRectangle;
    edtCategoriaPisstPorcentagem: TEdit;
    Text128: TText;
    Text129: TText;
    Rectangle84: TRectangle;
    edtCategoriaPisstValor: TEdit;
    Text130: TText;
    Text131: TText;
    Rectangle85: TRectangle;
    edtCategoriaPisstBcp: TEdit;
    Text132: TText;
    Text133: TText;
    Rectangle86: TRectangle;
    edtCategoriaPisstAliquota: TEdit;
    Text134: TText;
    Text135: TText;
    Text136: TText;
    Rectangle87: TRectangle;
    edtCategoriaCofinsCst: TEdit;
    Text137: TText;
    Rectangle88: TRectangle;
    edtCategoriaCofinsBc: TEdit;
    Text138: TText;
    Text139: TText;
    Rectangle89: TRectangle;
    Text140: TText;
    edtCategoriaCofinsValor: TEdit;
    Text141: TText;
    Rectangle90: TRectangle;
    Text142: TText;
    edtCategoriaCofinsBcp: TEdit;
    Text143: TText;
    Rectangle91: TRectangle;
    Text144: TText;
    edtCategoriaCofinsAliquota: TEdit;
    Text145: TText;
    Text146: TText;
    Rectangle92: TRectangle;
    edtCategoriaCofinsstBc: TEdit;
    Text147: TText;
    Text148: TText;
    Rectangle93: TRectangle;
    edtCategoriaCofinsstPorcentagem: TEdit;
    Text149: TText;
    Text150: TText;
    Rectangle94: TRectangle;
    edtCategoriaCofinsstValor: TEdit;
    Text151: TText;
    Text152: TText;
    Rectangle95: TRectangle;
    edtCategoriaCofinsstBcp: TEdit;
    Text153: TText;
    Text154: TText;
    Rectangle96: TRectangle;
    edtCategoriaCofinsstAliquota: TEdit;
    Text155: TText;
    Text156: TText;
    Rectangle97: TRectangle;
    Text157: TText;
    edtCategoriaCofinsPorcentagem: TEdit;
    Text158: TText;
    Rectangle98: TRectangle;
    edtCategoriaNcm: TEdit;
    Text159: TText;
    Rectangle99: TRectangle;
    edtCategoriaCest: TEdit;
    Text160: TText;
    Rectangle100: TRectangle;
    edtCategoriaCfop: TEdit;
    Text161: TText;
    VertScrollBox3: TVertScrollBox;
    Rectangle22: TRectangle;
    edtProdutoIcmsOrigem: TEdit;
    Text48: TText;
    Rectangle25: TRectangle;
    edtProdutoIcmsCst: TEdit;
    Text54: TText;
    Rectangle26: TRectangle;
    edtProdutoTaxaEstadual: TEdit;
    Text55: TText;
    Text56: TText;
    Rectangle27: TRectangle;
    edtProdutoTaxaFederal: TEdit;
    Text166: TText;
    Text167: TText;
    Rectangle28: TRectangle;
    edtProdutoPisCst: TEdit;
    Text168: TText;
    Rectangle29: TRectangle;
    edtProdutoPisBc: TEdit;
    Text169: TText;
    Text170: TText;
    Rectangle103: TRectangle;
    edtProdutoPisPorcentagem: TEdit;
    Text171: TText;
    Text172: TText;
    Rectangle104: TRectangle;
    edtProdutoPisValor: TEdit;
    Text173: TText;
    Text174: TText;
    Text175: TText;
    Rectangle105: TRectangle;
    edtProdutoIcmsPorcentagem: TEdit;
    Text176: TText;
    Text177: TText;
    Rectangle106: TRectangle;
    edtProdutoIcmsValor: TEdit;
    Text178: TText;
    Text179: TText;
    Text180: TText;
    Rectangle107: TRectangle;
    edtProdutoIssqnNatureza: TEdit;
    Text181: TText;
    Rectangle108: TRectangle;
    edtProdutoIssqnBc: TEdit;
    Text182: TText;
    Text183: TText;
    Rectangle109: TRectangle;
    edtProdutoIssqnDeducao: TEdit;
    Text184: TText;
    Text185: TText;
    Rectangle110: TRectangle;
    edtProdutoIssqnCodIbge: TEdit;
    Text186: TText;
    Rectangle111: TRectangle;
    edtProdutoIssqnBcp: TEdit;
    Text187: TText;
    Text188: TText;
    Rectangle112: TRectangle;
    edtProdutoIssqnItemServico: TEdit;
    Text189: TText;
    Rectangle113: TRectangle;
    edtProdutoIssqnCodTributario: TEdit;
    Text190: TText;
    Text191: TText;
    Text192: TText;
    Rectangle114: TRectangle;
    edtProdutoPisBcp: TEdit;
    Text193: TText;
    Text194: TText;
    Rectangle115: TRectangle;
    edtProdutoPisAliquota: TEdit;
    Text195: TText;
    Text196: TText;
    Text197: TText;
    Rectangle116: TRectangle;
    edtProdutoPisstBc: TEdit;
    Text198: TText;
    Text199: TText;
    Rectangle117: TRectangle;
    edtProdutoPisstPorcentagem: TEdit;
    Text200: TText;
    Text201: TText;
    Rectangle118: TRectangle;
    edtProdutoPisstValor: TEdit;
    Text202: TText;
    Text203: TText;
    Rectangle119: TRectangle;
    edtProdutoPisstBcp: TEdit;
    Text204: TText;
    Text205: TText;
    Rectangle120: TRectangle;
    edtProdutoPisstAliquota: TEdit;
    Text206: TText;
    Text207: TText;
    Text208: TText;
    Rectangle121: TRectangle;
    edtProdutoCofinsCst: TEdit;
    Text209: TText;
    Rectangle122: TRectangle;
    edtProdutoCofinsBc: TEdit;
    Text210: TText;
    Text211: TText;
    Rectangle123: TRectangle;
    Text212: TText;
    edtProdutoCofinsValor: TEdit;
    Text213: TText;
    Rectangle124: TRectangle;
    Text214: TText;
    edtProdutoCofinsBcp: TEdit;
    Text215: TText;
    Rectangle125: TRectangle;
    Text216: TText;
    edtProdutoCofinsAliquota: TEdit;
    Text217: TText;
    Text218: TText;
    Rectangle126: TRectangle;
    edtProdutoCofinsstBc: TEdit;
    Text219: TText;
    Text220: TText;
    Rectangle127: TRectangle;
    edtProdutoCofinsstPorcentagem: TEdit;
    Text221: TText;
    Text222: TText;
    Rectangle128: TRectangle;
    edtProdutoCofinsstValor: TEdit;
    Text223: TText;
    Text224: TText;
    Rectangle129: TRectangle;
    edtProdutoCofinsstBcp: TEdit;
    Text225: TText;
    Text226: TText;
    Rectangle130: TRectangle;
    edtProdutoCofinsstAliquota: TEdit;
    Text227: TText;
    Text228: TText;
    Rectangle131: TRectangle;
    Text229: TText;
    edtProdutoCofinsPorcentagem: TEdit;
    Text230: TText;
    Rectangle132: TRectangle;
    edtProdutoNcm: TEdit;
    Text231: TText;
    Rectangle133: TRectangle;
    edtProdutoCest: TEdit;
    Text232: TText;
    Rectangle134: TRectangle;
    edtProdutoCfop: TEdit;
    Text233: TText;
    Rectangle135: TRectangle;
    edtProdutoDescricao: TEdit;
    Text234: TText;
    Rectangle136: TRectangle;
    Text235: TText;
    Rectangle137: TRectangle;
    Text236: TText;
    cedProdutoSiglaUnidade: TComboEdit;
    Rectangle138: TRectangle;
    edtProdutoValor: TEdit;
    Text237: TText;
    Text238: TText;
    tabCategoriasProduto: TTabItem;
    txtProdutoCategoria: TText;
    Text5: TText;
    Layout16: TLayout;
    Rectangle7: TRectangle;
    Text12: TText;
    TabCupom: TTabItem;
    rectCupom: TRectangle;
    Text24: TText;
    Layout18: TLayout;
    VertScrollBox4: TVertScrollBox;
    Rectangle21: TRectangle;
    Text13: TText;
    rectCupomDadosPag: TRectangle;
    rectCupomDadosCliente: TRectangle;
    Text26: TText;
    txtCupomClienteDoc: TText;
    Text28: TText;
    txtCupomClienteNome: TText;
    Text30: TText;
    rectCupomProdutos: TRectangle;
    Text14: TText;
    Text16: TText;
    txtCupomFormaPag: TText;
    Text23: TText;
    txtCupomDesconto: TText;
    txtCupomAcrescimo: TText;
    txtCupomTotal: TText;
    tbcCupom: TTabControl;
    lBtnCupons: TLayout;
    btnCupomCancelar: TSpeedButton;
    btnCupomNext: TSpeedButton;
    tabCupomCliente: TTabItem;
    tabCupomProdutos: TTabItem;
    tabCupomPagamento: TTabItem;
    Rectangle33: TRectangle;
    edtCupomClienteNome: TEdit;
    Text33: TText;
    Rectangle39: TRectangle;
    EdtCupomClienteDoc: TEdit;
    Text34: TText;
    Text35: TText;
    Text36: TText;
    Text37: TText;
    Layout19: TLayout;
    Layout20: TLayout;
    lstCupomProdutos: TListBox;
    Text38: TText;
    lstProdutosCupom: TListBox;
    lstProdutos: TListBox;
    SearchBox1: TSearchBox;
    Line1: TLine;
    Rectangle40: TRectangle;
    edtCupomPagDesconto: TEdit;
    Text39: TText;
    Rectangle43: TRectangle;
    edtCupomPagAcrescimo: TEdit;
    Text40: TText;
    Rectangle44: TRectangle;
    Text41: TText;
    cedCupomPagFormaPagamento: TComboEdit;
    rectOperadoraCartao: TRectangle;
    Text42: TText;
    cedCupomPagOperadorCartao: TComboEdit;
    lstItemEmitirCupom: TListBoxItem;
    tabDefProduto: TTabItem;
    txtProduto: TText;
    lBtnsDefProduto: TLayout;
    btnNegProduto: TSpeedButton;
    btnPosProduto: TSpeedButton;
    Rectangle23: TRectangle;
    edtProdutoQuatidade: TEdit;
    Text25: TText;
    Layout17: TLayout;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    Layout21: TLayout;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    Layout22: TLayout;
    btnUsuarioCancelar: TSpeedButton;
    btnUsuarioSalvar: TSpeedButton;
    SearchBox2: TSearchBox;
    btnExcluirProduto: TSpeedButton;
    txtValorCupom: TText;
    Text4: TText;
    Text17: TText;
    txtCupomValorTotal: TText;
    tabConfirmacao: TTabItem;
    Text27: TText;
    Rectangle9: TRectangle;
    edtCupomProdutoNome: TEdit;
    Text29: TText;
    Rectangle31: TRectangle;
    edtCupomProdutoPreco: TEdit;
    Text31: TText;
    Text32: TText;
    txtUsuario: TText;
    btnVoltar: TSpeedButton;
    Layout23: TLayout;
    Text43: TText;
    Button1: TButton;
    chkPizzas: TCheckBox;
    chkEsfihas: TCheckBox;
    chkFogazzas: TCheckBox;
    chkLanches: TCheckBox;
    chkRefeicoes: TCheckBox;
    chkRefrigerantes: TCheckBox;
    chkVinhos: TCheckBox;
    chkSucos: TCheckBox;
    chkSobremesas: TCheckBox;
    chkAgua: TCheckBox;
    chkCervejas: TCheckBox;
    Rectangle32: TRectangle;
    edtConfirmarSenha: TEdit;
    Text44: TText;
    lstUsuariosGrid: TListBox;
    SearchBox3: TSearchBox;
    lstProdutosGrid: TListBox;
    SearchBox4: TSearchBox;
    lstCategoriasGrid: TListBox;
    SearchBox5: TSearchBox;
    lstCategoriaProdutos: TListBox;
    SearchBox6: TSearchBox;
    //evento de clique para ir para inicio
    procedure RectBtnIncioClick(Sender: TObject);
    //evento de clique para ir para relatorios
    procedure RectBtnRelatoriosClick(Sender: TObject);
    //evento de clique para ir para cupons
    procedure RectBtnCuponsClick(Sender: TObject);
    //evento de clique para minizar o sistema
    procedure RectBtnMinimizarClick(Sender: TObject);
    //evento de clique para fechar o sistema
    procedure RectBtnFecharClick(Sender: TObject);
    //evento de mouse sobre o elemento para ter uma animação
    procedure RectBtnMinimizarMouseEnter(Sender: TObject);
    //evento de mouse saida do elemento para ter uma animação
    procedure RectBtnMinimizarMouseLeave(Sender: TObject);
    //evento de clique para mover a tela do sistema
    procedure RectTituloMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    //evento de clique para ir para cupons
    procedure RoundRectBtnCancelarClick(Sender: TObject);
    //evento de clique para ir para configurações
    procedure RectBtnConfiguracoesClick(Sender: TObject);
    //evento de inicialização do form
    procedure FormCreate(Sender: TObject);
    //evento de clique troca a imagem lateral
    procedure RectBtnIncioMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
      //evento de clique troca a imagem lateral
    procedure RectBtnCuponsMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
      //evento de clique troca a imagem lateral
    procedure RectBtnConfiguracoesMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Single);
      //evento de clique troca a imagem lateral
    procedure RectBtnRelatoriosMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
      //evento de clique para ir para movimento do dia
    procedure LstItemMovimentacaoClick(Sender: TObject);
    //evento de clique para ir para tela da siclop
    procedure LstItemSICLOPClick(Sender: TObject);
    //evento de clique para ir para a tela de notas para envia
    procedure LstItemEnviarNotaClick(Sender: TObject);
    //evento de clique para ir para tela de cancelar cupom
    procedure LstItemCancelarNotaClick(Sender: TObject);
    //evento de clique para ir para tela de relarios resumo
    procedure LstItemResumoClick(Sender: TObject);
    //evento de clique para ir para tela de configuração do sat
    procedure LstItemSATClick(Sender: TObject);
    //evento de clique para ir para tela de configuração de impressora
    procedure TabConfiguracoesImpressoraSATClick(Sender: TObject);
    //evento de clique para ir para tela de configuração de contabilidade
    procedure LstItemContabilidadeClick(Sender: TObject);
    //evento de clique para ir para tela de configuração de estabelcimento
    procedure LstItemEstabelecimentoClick(Sender: TObject);
        //evento de clique para ir para tela de configuração de soft house
    procedure LstItemSoftHouseClick(Sender: TObject);
        //evento de clique para ir para tela de configuração de sistema
    procedure LstItemSistemaClick(Sender: TObject);
    //evendo de show para o form
    procedure FormShow(Sender: TObject);
    //evento de clique para mostrar logs
    procedure LstItemLogsClick(Sender: TObject);
    //evento de saida do elemento para validar o código de ativação
    procedure EdtTSatEquipamento_codigoAtivacaoExit(Sender: TObject);
        //evento de saida do elemento para validar o  CNPJ do establecimento
    procedure EdtTEstabelecimento_cnpjExit(Sender: TObject);
        //evento de saida do elemento para validar o código de IE
    procedure EdtTEstabelecimento_ieExit(Sender: TObject);
        //evento de saida do elemento para validar o email do estabelecimento
    procedure EdtTEstabelecimento_emailExit(Sender: TObject);
        //evento de saida do elemento para validar o  cnpj da softhouse
    procedure EdtTSoftHouse_cnpjExit(Sender: TObject);
        //evento de saida do elemento para validar a pasta de cupom
    procedure EdtTPasta_pastaCupomExit(Sender: TObject);
        //evento de saida do elemento para validar a dll do sat
    procedure EdtTSatEquipamento_caminhoDLLExit(Sender: TObject);
        //evento de clique para mostrar um cfe de exemplo
    procedure LsVNotasParaEnvioItemClick(const Sender: TObject;
      const AItem: TListViewItem);
      //evento do timer para mostrar movimento de hoje e listar as notas nas listas
    procedure tmrAtualizarUITimer(Sender: TObject);
    //evento do timere para o elemento das mensagens
    procedure tmrMensagemTimer(Sender: TObject);
    //evento de clique para fechar as mensagem
    procedure btnFechaMensagensClick(Sender: TObject);
    //evento de clique para cancelar as configurações do sat e ir para inicio
    procedure btnCancelarConfigSatClick(Sender: TObject);
        //evento de clique para cancelar as configurações do impressora e ir para inicio
    procedure btnCancelarConfigImpressoraClick(Sender: TObject);
        //evento de clique para cancelar as configurações do contabilidade e ir para inicio
    procedure btnCancelarConfigCotabilidadeClick(Sender: TObject);
        //evento de clique para cancelar as configurações do estabelecimento e ir para inicio
    procedure btnCancelarConfigEstabelecimentoClick(Sender: TObject);
        //evento de clique para cancelar as configurações do softhouse e ir para inicio
    procedure btnCancelarConfigSoftHouseClick(Sender: TObject);
        //evento de clique para cancelar as configurações do sistema e ir para inicio
    procedure btnCancelarConfigSistemaClick(Sender: TObject);
        //evento de clique para salvar as configurações da contabilidade
    procedure btnSalvarConfigCotabilidadeClick(Sender: TObject);
        //evento de clique para salvar as configurações da establecimento
    procedure btnSalvarConfigEstabelecimentoClick(Sender: TObject);
        //evento de clique para salvar as configurações da sat
    procedure btnSalvaConfigSatClick(Sender: TObject);
            //evento de clique para salvar as configurações da impressora
    procedure btnSalvarConfigImpressoraClick(Sender: TObject);
            //evento de clique para salvar as configurações da softhouse
    procedure btnSalvarConfigSoftHouseClick(Sender: TObject);
            //evento de clique para salvar as configurações da sistema
    procedure btnSalvarConfigSistemaClick(Sender: TObject);
            //evento de clique para iniciar o sat
    procedure btnIniciaSatClick(Sender: TObject);
    //evento de clique para imprimir um cupom
    procedure btnImprimirCupomClick(Sender: TObject);
    //evento de clique para gerar o relatorio
    procedure btnCarregarRelatorioSimplesClick(Sender: TObject);
    //evento de clique para enviar o relatorio via email
    procedure btnEnviarRelatorioViaEmailClick(Sender: TObject);
    //evento de clique para enviar o relatorio para documentos
    procedure btnSalvarRelatorioClick(Sender: TObject);
    //evento de clique para selecionar uma dll do sat
    procedure btnSelecionarDllSatClick(Sender: TObject);
    //evento de clique para consultar o sat
    procedure btnConsultarSatClick(Sender: TObject);
    //evento de clique para gerar logs para o sat
    procedure btnLogsSatClick(Sender: TObject);
    //evento de clique para ver status o sat
    procedure btnStatusSatClick(Sender: TObject);
    //evento de clique para slecionar uma impressora
    procedure btnAdicionarImpressoraClick(Sender: TObject);
    //evento de clique para adicionar um email na contabilidade
    procedure btnAdicionarEmailClick(Sender: TObject);
    //evento de clique apra limpar os emails da contabilidade
    procedure btnLimparEmailsClick(Sender: TObject);
    //evento de clique para ir para cadastro
    procedure rectBtnCadastrosClick(Sender: TObject);
    //evento de clique para trocar a imagem lateral
    procedure rectBtnCadastrosMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Single);
      //evento de saida do elemento para verificar a chave AC
    procedure mmChaveACExit(Sender: TObject);
    //evento de clique para mostrar o pdf de uma venda
    procedure btnMostrarCupomClick(Sender: TObject);
    //evento de clique para ir para lista de categorias
    procedure lstITemCategoriaClick(Sender: TObject);
        //evento de clique para ir para lista de produtos
    procedure listItemProdutoClick(Sender: TObject);
        //evento de clique para ir para lista de usuarios
    procedure listItemUsuarioClick(Sender: TObject);
    //evento de clique para ir para inserção de categoria
    procedure btnCategoriaNovoClick(Sender: TObject);
    //evento de clique para atualizar a lista de categorias
    procedure btnCategoraAtualizarClick(Sender: TObject);
    //evento de clique para editar uma categoria
    procedure btnCategoriaEditarClick(Sender: TObject);
    //evento de clique para apagar uma categoria
    procedure btnCategoriaApagarClick(Sender: TObject);
    //evento de clique para cancelar a edição/adição de uma categoria
    procedure btnCancelarCategoriaClick(Sender: TObject);
    //evento de clique para salvar a edição/adição de uma categoria
    procedure btnCategoriaSalvarClick(Sender: TObject);
        //evento de clique para ir para inserção de produto
    procedure btnProdutoNovoClick(Sender: TObject);
        //evento de clique para atualizar a lista de produtos
    procedure btnProdutoAtualizarClick(Sender: TObject);
//evento de clique para editar uma produto
    procedure btnProdutoEditarClick(Sender: TObject);
    //evento de clique para apagar uma produto
    procedure btnProdutoApagarClick(Sender: TObject);
        //evento de clique para cancelar a edição/adição de um produto
    procedure btnProdutoCancelarClick(Sender: TObject);
        //evento de clique para salvar a edição/adição de um produto
    procedure btnProdutoSalvarClick(Sender: TObject);
        //evento de clique para ir para inserção de usuario
    procedure btnUsuarioNovoClick(Sender: TObject);
        //evento de clique para atualizar a lista de usuarios
    procedure btnUsuarioAtualizarClick(Sender: TObject);
    //evento de clique para editar um usuario
    procedure btnUsuarioEditarClick(Sender: TObject);
        //evento de clique para apagar uma usuario
    procedure btnUsuarioApagarClick(Sender: TObject);
            //evento de clique para cancelar a edição/adição de um usuario
    procedure btnUsuarioCancelarClick(Sender: TObject);
            //evento de clique para salvar a edição/adição de um usuario
    procedure btnUsuarioSalvarClick(Sender: TObject);
    //evento de clique para cancelar um produto
    procedure SpeedButton3Click(Sender: TObject);
    //evento de clique para salvar uma categoria
    procedure SpeedButton4Click(Sender: TObject);
    //evento de clique para cancalar um categoria
    procedure SpeedButton1Click(Sender: TObject);
    //evento de clique para salvar um produto
    procedure SpeedButton2Click(Sender: TObject);
    //evento de bind para adicionar uma mascara
    procedure LinkGridToDataSourceBindSourceDB12AssigningValue(
      Sender: TObject; AssignValueRec: TBindingAssignValueRec;
      var Value: TValue; var Handled: Boolean);
      //evento de clique para emitir um cupom
    procedure lstItemEmitirCupomClick(Sender: TObject);
    //evendo de destroy do form
    procedure FormDestroy(Sender: TObject);
    //evento de clique para cancelar um cupom fiscal
    procedure btnCancelarCupomFiscalClick(Sender: TObject);
    //evento de clique para carregar os dados de uma categoria para adicionar um produto
    procedure lsvCategoriasProdutoItemClick(const Sender: TObject;
      const AItem: TListViewItem);
    //evento de alteração do valor para mostrar a forma de pagamento que foi alterada
    procedure cedCupomPagFormaPagamentoChange(Sender: TObject);
    //evento de saida do elemento para verificação do desconto
    procedure edtCupomPagDescontoExit(Sender: TObject);
    //evento de tecla prescionada para verificar a tecla teclada
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      var KeyChar: Char; Shift: TShiftState);
      //evento de clique para executar os scripts no banco de dados
    procedure Button1Click(Sender: TObject);
    //evento de clique para definir definir sistema autonamo
    procedure swtSistemaAutonomoSwitch(Sender: TObject);
    //evento de clique para selecionar um usuario da lista
    procedure lstUsuariosGridItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
      //evento de clique para selecionar um produto da lista
    procedure lstProdutosGridItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
      //evento de clique para selecionar uma categoria da lista
    procedure lstCategoriasGridItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
      //evento de clique para selecionar um produto da lista
    procedure lstCategoriaProdutosItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
  private
    { Private declarations }

  public
    //método para mover a linha para ficar sobre os botões da nav bar
    procedure MoveLinha(Sender: TObject);
    //método para ajusstar os tab controls
    procedure AjustaTabControl();
    //método para mover a tab dos tab controls
    procedure MoveTab(pTab: TTabItem);
    //método para trocar a imagem lateral
    procedure ImgMenuLateral(pIndice: integer);
    //método para acionar uma animação em uma botão
    procedure BtnAnimacao(Sender: TObject; pWitdh, pHeigth: integer);
    //método para movimenta o form
    procedure Movimenta;
    //método para ir para inicio
    procedure VaiParaInicio;
    //método para ir para cupons
    procedure VaiParaCupons;
    //método para ir para cadastros
    procedure VaiParaCadastros;
    //método para ir para configurações
    procedure VaiParaConfiguracoes;
    //método que vai para relatórios
    procedure VaiParaRelatorios;
    //método para tirar o foco do list
    procedure tiraFocoList;

    {EVENTOS}
    //método para confirma a inicialização do equipamento
    procedure ConfirmaInicializado();
    //método para informa que houve um erro no equipamento
    procedure ErroAoIniciar();
    //método paara mostrar o numero de série do equipamento
    procedure MostraNSerieSAT(const value: string);
    //método para mostrar o retorno do equipamento do sat
    procedure MostraRetornoSAT(const value: string);
    //método para mostrar a mensagem relacionado ao email
    procedure MensagemEmail(const value: string);
    { Public declarations }

    procedure MostraMensage(mensage: string);
  end;

var
  FormHome: TFormHome;
  cupom: TCriadorCupomVM;

  { TODO : Adicionar Mascáras }

implementation

uses
  Winapi.Windows,
  FMX.platform,
  Model.PowerCMD,
  Util.Helper,
  uValidacoes,
  System.DateUtils,uTimerAnonymous, uVm.Contabilidade, uVm.Estabelecimento,
  uVm.Impressora, uVm.Pasta, uVm.Sat, uVm.SoftHouse, uDmCons,
  uVm.Categoria, uVm.Sistema, uFrmLogin, uVm.Auditoria, uFrmAuditoria,
  uAuRepository, uScriptInsercaoCategorias;

{$R *.fmx}
{ TFormHome }

procedure TFormHome.AjustaTabControl();
var
  I: Integer;
begin
  for I := 0 to ComponentCount-1 do
  begin
    if Components[I] is TTabControl then
    begin
      (Components[I] as TTabControl).TabPosition:= TTabPosition.None;
      (Components[I] as TTabControl).TabIndex:= 0;
    end;
  end;
end;

procedure TFormHome.btnAdicionarImpressoraClick(Sender: TObject);
begin
  dmVmPrincipal.SetImpressora();
end;

procedure TFormHome.BtnAnimacao(Sender: TObject; pWitdh, pHeigth: integer);
begin
  TAnimator.AnimateFloat(TRoundRect(Sender), 'Width', pWitdh);
  TAnimator.AnimateFloat(TRoundRect(Sender), 'height', pHeigth);
end;

procedure TFormHome.btnCancelarCategoriaClick(Sender: TObject);
var
  vm: TCategoriaVM;
begin
  vm:= TCategoriaVM.Create;
  try
    vm.Cancelar();
  finally
    vm.Free;
  end;
end;

procedure TFormHome.btnCancelarConfigCotabilidadeClick(Sender: TObject);
begin
  TabControlBody.ActiveTab:= TabBodyInicio;
  TContabilidadeVM.SetDadosForm;
end;

procedure TFormHome.btnCancelarConfigEstabelecimentoClick(Sender: TObject);
begin
  TabControlBody.ActiveTab:= TabBodyInicio;
  TEstabelecimentoVM.SetDadosForm;
end;

procedure TFormHome.btnCancelarConfigImpressoraClick(Sender: TObject);
begin
  TabControlBody.ActiveTab:= TabBodyInicio;
  TImpressoraVM.SetDadosForm;
end;

procedure TFormHome.btnCancelarConfigSatClick(Sender: TObject);
begin
  TabControlBody.ActiveTab:= TabBodyInicio;
  TSatVM.SetDadosForm;
end;

procedure TFormHome.btnCancelarConfigSistemaClick(Sender: TObject);
begin
  TabControlBody.ActiveTab:= TabBodyInicio;
  TPastaVm.SetDadosForm;
  TSatVM.SetDadosForm;
end;

procedure TFormHome.btnCancelarConfigSoftHouseClick(Sender: TObject);
begin
  TabControlBody.ActiveTab:= TabBodyInicio;
  TSoftHouseVM.SetDadosForm;
end;


procedure TFormHome.btnCancelarCupomFiscalClick(Sender: TObject);
begin
  dmVmPrincipal.CancelarSAT;
end;

procedure TFormHome.btnSalvaConfigSatClick(Sender: TObject);
begin
  TSatVM.GetDadosForm;
  dmVmPrincipal.ReiniciarConfiguracoes;
  dmVmPrincipal.ReiniciarSAT;

end;

procedure TFormHome.btnSalvarConfigCotabilidadeClick(Sender: TObject);
begin
  TContabilidadeVM.GetDadosForm;
  dmVmPrincipal.ReiniciarConfiguracoes;
end;

procedure TFormHome.btnSalvarConfigEstabelecimentoClick(Sender: TObject);
begin
  TEstabelecimentoVM.GetDadosForm;
  dmVmPrincipal.ReiniciarConfiguracoes;
  dmVmPrincipal.ReiniciarSAT;

end;

procedure TFormHome.btnSalvarConfigImpressoraClick(Sender: TObject);
begin
  TImpressoraVM.GetDadosForm;
  dmVmPrincipal.ReiniciarConfiguracoes;
  dmVmPrincipal.ReiniciarSAT;

end;

procedure TFormHome.btnSalvarConfigSistemaClick(Sender: TObject);
begin
  TPastaVm.GetDadosForm;
  TSatVM.GetDadosForm;
  TSistemaVM.GetDadosForm;
  dmVmPrincipal.ReiniciarConfiguracoes;
  dmVmPrincipal.ReiniciarSAT;
end;

procedure TFormHome.btnSalvarConfigSoftHouseClick(Sender: TObject);
begin
  TSoftHouseVM.GetDadosForm;
  dmVmPrincipal.ReiniciarConfiguracoes;
  dmVmPrincipal.ReiniciarSAT;
end;

procedure TFormHome.btnEnviarRelatorioViaEmailClick(Sender: TObject);
begin
  MostraMensage(Format('Será gerado o relatório do dia %s há %s',
              [FormatDateTime('dd/mm/yyyy', dtDeContador.Date), FormatDateTime('dd/mm/yyyy', dtAteContador.Date)] ));
  dmVmPrincipal.EnviarRelatorio(dtDeContador.Date, dtAteContador.Date);
end;

procedure TFormHome.btnSalvarRelatorioClick(Sender: TObject);
begin
  MostraMensage(Format('Será gerado o relatório do dia %s há %s',
              [FormatDateTime('dd/mm/yyyy', dtDeContador.Date), FormatDateTime('dd/mm/yyyy', dtAteContador.Date)] ));
  dmVmPrincipal.SalvarRelatorio(dtDeContador.Date, dtAteContador.Date);
end;

procedure TFormHome.btnSelecionarDllSatClick(Sender: TObject);
begin
  dmVmPrincipal.SetDLLSat();
end;

procedure TFormHome.btnAdicionarEmailClick(Sender: TObject);
begin
  dmVmPrincipal.AdicionarEmail();
end;

procedure TFormHome.btnConsultarSatClick(Sender: TObject);
begin
  dmVmPrincipal.ExibirConsultaSAT();
end;

procedure TFormHome.btnLogsSatClick(Sender: TObject);
begin
  dmVmPrincipal.ExibirLogSAT();
end;

procedure TFormHome.btnMostrarCupomClick(Sender: TObject);
begin
  dmVmPrincipal.MostrarPDF(LsVNotasParaCancelar.Selected.Tag);
end;

procedure TFormHome.btnProdutoApagarClick(Sender: TObject);
var
  vm: TProdutoVM;
begin
  vm:= TProdutoVM.Create;
  try
    vm.Excluir();
  finally
    vm.Free;
  end;
end;

procedure TFormHome.btnProdutoAtualizarClick(Sender: TObject);
var
  vm: TProdutoVM;
begin
  vm:= TProdutoVM.Create;
  try
    vm.Carregar();
  finally
    vm.Free;
  end;
end;


procedure TFormHome.btnProdutoCancelarClick(Sender: TObject);
var
  vm: TProdutoVM;
begin
  vm:= TProdutoVM.Create;
  try
    vm.Cancelar();
  finally
    vm.Free;
  end;
end;

procedure TFormHome.btnProdutoEditarClick(Sender: TObject);
var
  vm: TProdutoVM;
begin
  VertScrollBox3.ViewportPosition := PointF(VertScrollBox3.ViewportPosition.X, 0);
  VertScrollBox3.RealignContent;
  vm:= TProdutoVM.Create;
  try
    vm.InitEdit();
  finally
    vm.Free;
  end;
end;

procedure TFormHome.btnProdutoNovoClick(Sender: TObject);
var
  vm: TProdutoVM;
  vmCategoria: TCategoriaVM;
begin
  VertScrollBox3.ViewportPosition := PointF(VertScrollBox3.ViewportPosition.X, 0);
  VertScrollBox3.RealignContent;
  vmCategoria:= TCategoriaVM.Create;
  vm:= TProdutoVM.Create;
  try
    if vm.VerificaSeTemCategorias then
    begin
      vmCategoria.CarregarCategoriasProdutos();
      MoveTab(tabCategoriasProduto);
    end
    else
    begin
      MostraMensage('Não há categorias cadastras.');
      MostraMensage('Cadastre uma categoria, antes de cadastrar um produto.');
    end;
  finally
    vmCategoria.Free;
    vm.Free;
  end;
end;

procedure TFormHome.btnProdutoSalvarClick(Sender: TObject);
var
  vm: TProdutoVM;
begin
  vm:= TProdutoVM.Create;
  try
    vm.Salvar();
  finally
    vm.Free;
  end;
end;


procedure TFormHome.btnStatusSatClick(Sender: TObject);
begin
  dmVmPrincipal.ExibirStatusSAT();
end;

procedure TFormHome.btnUsuarioApagarClick(Sender: TObject);
var
  vm: TUsuarioVM;
begin
  vm:= TUsuarioVM.Create;
  try
    vm.Excluir();
  finally
    vm.Free;
  end;
end;

procedure TFormHome.btnUsuarioAtualizarClick(Sender: TObject);
var
  vm: TUsuarioVM;
begin
  vm:= TUsuarioVM.Create;
  try
    vm.Carregar();
  finally
    vm.Free;
  end;
end;

procedure TFormHome.btnUsuarioCancelarClick(Sender: TObject);
var
  vm: TUsuarioVM;
begin
  vm:= TUsuarioVM.Create;
  try
    vm.Cancelar();
  finally
    vm.Free;
  end;
end;

procedure TFormHome.btnUsuarioEditarClick(Sender: TObject);
var
  vm: TUsuarioVM;
begin
  VertScrollBox1.ViewportPosition := PointF(VertScrollBox1.ViewportPosition.X, 0);
  VertScrollBox1.RealignContent;
  vm:= TUsuarioVM.Create;
  try
    vm.InitEdit();
  finally
    vm.Free;
  end;
end;

procedure TFormHome.btnUsuarioNovoClick(Sender: TObject);
var
  vm: TUsuarioVM;
begin
  VertScrollBox1.ViewportPosition := PointF(VertScrollBox1.ViewportPosition.X, 0);
  VertScrollBox1.RealignContent;
  vm:= TUsuarioVM.Create;
  try
    vm.InitInserir();
  finally
    vm.Free;
  end;
end;

procedure TFormHome.btnUsuarioSalvarClick(Sender: TObject);
var
  vm: TUsuarioVM;
begin
  vm:= TUsuarioVM.Create;
  try
    vm.Salvar();
  finally
    vm.Free;
  end;
end;

procedure TFormHome.Button1Click(Sender: TObject);
var
  script: TScriptInsercaoCategorias;
begin
  script := TScriptInsercaoCategorias.Create;
  try
    script.AdicionarCategorias;
  finally
    script.Free;
  end;
end;

procedure TFormHome.cedCupomPagFormaPagamentoChange(Sender: TObject);
begin
  if cedCupomPagFormaPagamento.ItemIndex in [2, 3, 4, 10, 11, 12, 13] then
  begin
    cedCupomPagOperadorCartao.ItemIndex:= 0;
    rectOperadoraCartao.Visible:= true;
  end
  else
  begin
    cedCupomPagOperadorCartao.ItemIndex:= -1;
    rectOperadoraCartao.Visible:= False;
  end;
end;

procedure TFormHome.btnLimparEmailsClick(Sender: TObject);
var
  msg: IFMXDialogServiceSync;
  resposta: integer;
begin
  if TPlatformServices.Current.SupportsPlatformService
    (IFMXDialogServiceSync, msg) then
  begin
    resposta := msg.MessageDialogSync('Deseja mesmo apagar todos os Email?',
      TMsgDlgType.mtConfirmation, [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo],
      TMsgDlgBtn.mbNo, 0);

    if resposta = mrYes then
    begin
      dmVmPrincipal.LimparEmail();
      MmoTContabilidade_emails.Lines.Clear;
    end;
  end;
end;

procedure TFormHome.btnCarregarRelatorioSimplesClick(Sender: TObject);
begin
  dmVmPrincipal.ResumirNota(dtDe.Date, dtAte.Date);
end;

procedure TFormHome.btnCategoriaApagarClick(Sender: TObject);
var
  vm: TCategoriaVM;
begin
  vm:= TCategoriaVM.Create;
  try
    vm.Excluir();
  finally
    vm.Free;
  end;
end;

procedure TFormHome.btnCategoraAtualizarClick(Sender: TObject);
var
  vm: TCategoriaVM;
begin
  vm:= TCategoriaVM.Create;
  try
    vm.CarregarCategorias();
  finally
    vm.Free;
  end;
end;

procedure TFormHome.btnCategoriaEditarClick(Sender: TObject);
var
  vm: TCategoriaVM;
begin
  VertScrollBox2.ViewportPosition := PointF(VertScrollBox2.ViewportPosition.X, 0);
  VertScrollBox2.RealignContent;
  vm:= TCategoriaVM.Create;
  try
    vm.InitEdit();
  finally
    vm.Free;
  end;
end;

procedure TFormHome.btnCategoriaNovoClick(Sender: TObject);
var
  vm: TCategoriaVM;
begin
  VertScrollBox2.ViewportPosition := PointF(VertScrollBox2.ViewportPosition.X, 0);
  VertScrollBox2.RealignContent;
  vm:= TCategoriaVM.Create;
  try
    vm.InitInserir();
  finally
    vm.Free;
  end;
end;

procedure TFormHome.btnCategoriaSalvarClick(Sender: TObject);
var
  vm: TCategoriaVM;
begin
  vm:= TCategoriaVM.Create;
  try
    vm.Salvar();
  finally
    vm.Free;
  end;
end;

procedure TFormHome.btnImprimirCupomClick(Sender: TObject);
begin
  dmVmPrincipal.EnviarSAT();
  TxtInformacoesCliente.Text:= '';
  txtTotalCupom.Text:= '';
  txtLegProduto.Text:= '';
  LstDeProtudos.Clear;

end;

procedure TFormHome.btnIniciaSatClick(Sender: TObject);
begin
  dmVmPrincipal.IniciarSAT();
end;

procedure TFormHome.ConfirmaInicializado();
begin

  if btnIniciaSat.Visible then
    MostraMensage('S@T INICIADO!');

  btnIniciaSat.Visible := false;
end;

procedure TFormHome.edtCupomPagDescontoExit(Sender: TObject);
begin
  if StrToFloatDef(edtCupomPagDesconto.Text, 0) > txtCupomValorTotal.TagFloat then
  begin
    edtCupomPagDesconto.Text:= '0';
    edtCupomPagDesconto.SetFocus;
    MostraMensage('Valor do desconto tem que ser menor que o valor total!');
  end;
end;

procedure TFormHome.FormCreate(Sender: TObject);
begin
  AjustaTabControl();
end;

procedure TFormHome.FormDestroy(Sender: TObject);
begin
  if Assigned(cupom) then
    cupom.Free;
end;

procedure TFormHome.FormKeyDown(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  if Key = VK_F12 then
  begin
    frmLogin.Rectangle1.Visible:= true;
    frmLogin.Show();
    self.Hide();
  end;

  if Key = VK_F11 then
  begin
    if dmVmPrincipal.Usuario.Nivel.Value >= 2 then
    begin
      frmAuditoria:= TFrmAuditoria.Create(Application);
      try
        frmAuditoria.ShowModal;
      finally
        frmAuditoria.Free;
      end;
    end;
  end;

end;

procedure TFormHome.FormShow(Sender: TObject);
begin
  try
    AjustaTabControl();
    txtVersao.Text:= GetVersaoPrograma(ParamStr(0));

    TxtMovimentacaoHoje.Text := 'Movimento de Hoje - ' +
      FormatDateTime('dd/mm/yyyy', now);

    Sleep(1111);
  except
    MostraMensage('Erro ao iniciar o SISTEMA');
  end;
end;

procedure TFormHome.VaiParaCupons;
begin
  ImgMenuLateral(1);
  MoveLinha(RectBtnCupons);
  MoveTab(TabMenuLateralCupons);
  MoveTab(TabBodyCupons);
  if swtSistemaAutonomo.IsChecked then
    lstItemEmitirCupomClick(lstItemEmitirCupom)
  else
    MoveTab(TabCupomEnviar);
end;

procedure TFormHome.VaiParaCadastros;
begin
  ImgMenuLateral(7);
  MoveLinha(rectBtnCadastros);
  MoveTab(tabMenuCadastro);
  MoveTab(tabBodyCadastro);

end;

procedure TFormHome.VaiParaConfiguracoes;
begin
  ImgMenuLateral(3);
  MoveLinha(RectBtnConfiguracoes);
  MoveTab(TabMenuLateralConficuracoes);
  MoveTab(TabBodyConfiguracoes);
end;

procedure TFormHome.ImgMenuLateral(pIndice: integer);
begin
  if GlyphMenuLateral.ImageIndex <> pIndice then
  begin
    tiraFocoList();
    GlyphMenuLateral.Position.Y := -2000;
    GlyphMenuLateral.ImageIndex := pIndice;
    TAnimator.AnimateFloat(GlyphMenuLateral, 'Glyph1.Position.Y', 3, 0.5);
  end;
end;


procedure TFormHome.LinkGridToDataSourceBindSourceDB12AssigningValue(
  Sender: TObject; AssignValueRec: TBindingAssignValueRec;
  var Value: TValue; var Handled: Boolean);
var
  coluna: TGridDataObject;
begin
  if Assigned(AssignValueRec.OutObj) then
    if AssignValueRec.OutObj.ClassNameIs('TGridDataObject') then
    begin
      coluna:= TGridDataObject(AssignValueRec.OutObj);

      if coluna.Column = 3 then
      begin
        value:= 'R$ '+ value.AsString;
      end;
    end;
end;

procedure TFormHome.listItemProdutoClick(Sender: TObject);
var
  vm: TProdutoVM;
begin

  vm:= TProdutoVM.Create;
  try
    vm.Carregar();
    MoveTab(tabBodyCadastro);
    MoveTab(tabProduto);
    MoveTab(tabProdutoLista);
  finally
    vm.Free;
  end;
end;


procedure TFormHome.listItemUsuarioClick(Sender: TObject);
var
  vm: TUsuarioVM;
begin
  vm:= TUsuarioVM.Create;
  try
    VertScrollBox1.ScrollBy(0, 0);

    vm.Carregar();
    MoveTab(tabBodyCadastro);
    MoveTab(tabUsuario);
    MoveTab(tabUsuarioLista);
  finally
    vm.Free;
  end;
end;

procedure TFormHome.lstCategoriaProdutosItemClick(
  const Sender: TCustomListBox; const Item: TListBoxItem);
var
  vm: TProdutoVM;
  id: integer;
begin

  if lstCategoriaProdutos.ItemIndex > -1 then
  begin
    id:= item.Tag;
    vm:= TProdutoVM.Create;
    try
      vm.InitInserir(id);
    finally
      vm.Free;
    end;
  end;
end;

procedure TFormHome.lstCategoriasGridItemClick(
  const Sender: TCustomListBox; const Item: TListBoxItem);
begin
  txtCategoriaId.Tag:= item.Tag;
end;

procedure TFormHome.LstItemEstabelecimentoClick(Sender: TObject);
begin
  MoveTab(TabBodyConfiguracoes);
  MoveTab(TabConfiguracoesEstabelecimento);
end;

procedure TFormHome.LstItemLogsClick(Sender: TObject);
begin
  AbrePastaDeLogs();
end;

procedure TFormHome.LstItemSistemaClick(Sender: TObject);
begin
  MoveTab(TabBodyConfiguracoes);
  MoveTab(TabConfiguracoesSistema);
end;

procedure TFormHome.LstItemMovimentacaoClick(Sender: TObject);
begin
  MoveTab(TabBodyInicio);
  MoveTab(TabInicioMovimentacao);
end;

procedure TFormHome.LstItemSICLOPClick(Sender: TObject);
begin
  MoveTab(TabBodyInicio);
  MoveTab(TabInicioSICLOP);
end;

procedure TFormHome.LstItemResumoClick(Sender: TObject);
begin
  MoveTab(TabBodyRelatorios);
  MoveTab(TabRelatoriosResumo);
end;

procedure TFormHome.LstItemEnviarNotaClick(Sender: TObject);
begin
  MoveTab(TabBodyCupons);
  MoveTab(TabCupomEnviar);
end;

procedure TFormHome.btnFechaMensagensClick(Sender: TObject);
begin
  txtMsg3.Text:= '';
  txtMsg2.Text:= '';
  txtMsg1.Text:= '';
  layMensagem.Visible:= false;
  tmrMensagem.Enabled:= false;
  layMsg1.Visible:= false;
  layMsg2.Visible:= false;
  layMsg3.Visible:= false;
end;

procedure TFormHome.LstItemCancelarNotaClick(Sender: TObject);
begin
  MoveTab(TabBodyCupons);
  MoveTab(tabCancelarCupomFiscal);
end;

procedure TFormHome.lstITemCategoriaClick(Sender: TObject);
var
  vm: TCategoriaVM;
  I: Integer;
begin
  
  vm:= TCategoriaVM.Create;
  try


    vm.CarregarCategorias();
    MoveTab(tabBodyCadastro);
    MoveTab(tabCategoria);
    MoveTab(TabCategoriaLista);
  finally
    vm.Free;
  end;
end;

procedure TFormHome.LstItemSATClick(Sender: TObject);
begin
  MoveTab(TabBodyConfiguracoes);
  MoveTab(TabConfiguracoesEquipamentoSAT);
end;

procedure TFormHome.LstItemSoftHouseClick(Sender: TObject);
begin
  MoveTab(TabBodyConfiguracoes);
  MoveTab(TabConfiguracoesSoftHouse);
end;

procedure TFormHome.lstProdutosGridItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
  txtProdutoId.Tag:= item.Tag;
end;

procedure TFormHome.lstUsuariosGridItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
  txtUsuarioId.Tag:= item.Tag;
end;

procedure TFormHome.lsvCategoriasProdutoItemClick(const Sender: TObject;
  const AItem: TListViewItem);
var
  vm: TProdutoVM;
  id: integer;
begin

  if lstCategoriaProdutos.ItemIndex > -1 then
  begin
    id:= AItem.Tag;
    vm:= TProdutoVM.Create;
    try
      vm.InitInserir(id);
    finally
      vm.Free;
    end;
  end;
end;

procedure TFormHome.LsVNotasParaEnvioItemClick(const Sender: TObject;
  const AItem: TListViewItem);
begin
  dmVmPrincipal.MostrarCFeTxt();
end;

procedure TFormHome.LstItemContabilidadeClick(Sender: TObject);
begin
  MoveTab(TabBodyConfiguracoes);
  MoveTab(TabConfiguracoesContabilidade);
end;

procedure TFormHome.lstItemEmitirCupomClick(Sender: TObject);
begin
 cupom:= TCriadorCupomVM.New;
 cupom.Iniciar;
end;

procedure TFormHome.EdtTEstabelecimento_cnpjExit(Sender: TObject);
begin
   if not TValidacaoes.ValidarMascaraCNPJ(EdtTEstabelecimento_cnpj.Text)
    and not TValidacaoes.ValidarCNPJ(EdtTEstabelecimento_cnpj.Text) then
   begin
      EdtTEstabelecimento_cnpj.Text:= '';
      MostraMensage('CNPJ do estabelecimento Inválido');
   end;
end;

procedure TFormHome.EdtTEstabelecimento_emailExit(Sender: TObject);
begin
  if not TValidacaoes.ValidarEmail(EdtTEstabelecimento_email.Text) then
   begin
    EdtTEstabelecimento_email.Text:= '';
    MostraMensage('E-mail do estabelecimento está inválido');
   end;
end;

procedure TFormHome.EdtTEstabelecimento_ieExit(Sender: TObject);
begin
  if not TValidacaoes.ValidarMascaraIE(EdtTEstabelecimento_ie.Text) and
    not TValidacaoes.ValidarIE(EdtTEstabelecimento_ie.Text) then
   begin
    EdtTEstabelecimento_ie.Text:= '';
    MostraMensage('Inscrição estadual Inválido');
   end;
end;

procedure TFormHome.EdtTPasta_pastaCupomExit(Sender: TObject);
begin
  if not DirectoryExists(EdtTPasta_pastaCupom.Text) then
   begin
    EdtTPasta_pastaCupom.Text:= '';
    MostraMensage('Pasta não encontrada');
   end;
end;

procedure TFormHome.EdtTSatEquipamento_caminhoDLLExit(Sender: TObject);
begin
  if not FileExists(EdtTSatEquipamento_caminhoDLL.Text) then
  begin
    EdtTSatEquipamento_caminhoDLL.Text:= '';
    MostraMensage('Arquivo DLL do SAT não existe');
  end;
end;

procedure TFormHome.EdtTSatEquipamento_codigoAtivacaoExit(Sender: TObject);
begin
   if EdtTSatEquipamento_codigoAtivacao.Text.Length < 7 then
   begin
    EdtTSatEquipamento_codigoAtivacao.Text:= '';
    MostraMensage('Código de ativação inválido deve ser no minimo 8 caracteres');
   end;
end;

procedure TFormHome.EdtTSoftHouse_cnpjExit(Sender: TObject);
begin
  if not((EdtTSoftHouse_cnpj.Text = '19832566000108') or (EdtTSoftHouse_cnpj.Text = '11111111111111')) then
   begin
    EdtTSoftHouse_cnpj.Text:= '';
    MostraMensage('CNPJ da SoftWare House está inválido');
   end;
end;

procedure TFormHome.ErroAoIniciar;
begin
  MostraMensage('Erro ao iniciar');
end;

procedure TFormHome.MensagemEmail(const value: string);
begin
  MostraMensage(value);
end;

procedure TFormHome.mmChaveACExit(Sender: TObject);
begin
  if mmChaveAC.Lines.Text.IsEmpty then
  begin
    MostraMensage('Chave AC inválida');
  end;
end;

procedure TFormHome.MostraMensage(mensage: string);
begin
  layMensagem.Visible:= true;
  tmrMensagem.Enabled:= false;
  tmrMensagem.Enabled:= true;
  if txtMsg1.Text.IsEmpty and not(mensage.IsEmpty) then
  begin
    txtMsg1.Text:= mensage.Trim();
    layMsg1.Visible:= true;
    mensage:= '';
  end
  else if txtMsg2.Text.IsEmpty and not(mensage.IsEmpty) then
  begin
    txtMsg2.Text:= mensage;
    layMsg2.Visible:= true;
    mensage:= '';
  end
  else if txtMsg3.Text.IsEmpty and not(mensage.IsEmpty) then
  begin
    txtMsg3.Text:= mensage;
    layMsg3.Visible:= true;
    mensage:= '';
  end
  else
  begin
    txtMsg1.Text:= mensage;
    layMsg1.Visible:= true;
    mensage:= '';
  end;

end;

procedure TFormHome.MostraNSerieSAT(const value: string);
begin
  Conectado.Visible := true;
  Conectado.Text := value;

  if Conectado.Text.Contains('ON') then
    GlyphConectado.Size.Width := 26;
end;

procedure TFormHome.MostraRetornoSAT(const value: string);
begin
  MmoRespostaSAT.Lines.Add(FormatDateTime('c', now)+'------------------------------');
  MmoRespostaSAT.Lines.Add(value);
  MostraMensage(value);
end;

procedure TFormHome.MoveLinha(Sender: TObject);
begin
  TAnimator.AnimateFloat(LineSublinhado, 'Position.X', TLine(Sender).Position.X,
    0.5, TAnimationType.InOut, TInterpolationType.Cubic);
  TAnimator.AnimateFloat(LineSublinhado, 'Width', TLine(Sender).Width, 0.5,
    TAnimationType.InOut, TInterpolationType.Cubic);
end;

procedure TFormHome.MoveTab(pTab: TTabItem);
begin
  pTab.TabControl.ActiveTab:= pTab;
end;

procedure TFormHome.Movimenta;
const
  sc_DragMove = $F012;
begin
  ReleaseCapture;
  PostMessage(fmxHandleToHWND(Handle), wm_SysCommand, sc_DragMove, 0);
end;

procedure TFormHome.rectBtnCadastrosClick(Sender: TObject);
var
  vm: TCategoriaVM;
begin
  VaiParaCadastros();
  vm:= TCategoriaVM.Create;
  try
    vm.CarregarCategorias();
    MoveTab(tabBodyCadastro);
    MoveTab(tabCategoria);
    MoveTab(TabCategoriaLista);
  finally
    vm.Free;
  end;
end;

procedure TFormHome.rectBtnCadastrosMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  ImgMenuLateral(7);
end;

procedure TFormHome.RectBtnIncioClick(Sender: TObject);
begin
  VaiParaInicio;
end;

procedure TFormHome.RectBtnIncioMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  ImgMenuLateral(0);
end;

procedure TFormHome.RectTituloMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  Movimenta;
end;

procedure TFormHome.RoundRectBtnCancelarClick(Sender: TObject);
begin
  VaiParaCupons;
end;

procedure TFormHome.SpeedButton1Click(Sender: TObject);
var
  vm: TCategoriaVM;
begin
  vm:= TCategoriaVM.Create;
  try
    vm.Cancelar();
  finally
    vm.Free;
  end;
end;

procedure TFormHome.SpeedButton2Click(Sender: TObject);
var
  vm: TCategoriaVM;
begin
  vm:= TCategoriaVM.Create;
  try
    vm.Salvar();
  finally
    vm.Free;
  end;
end;

procedure TFormHome.SpeedButton3Click(Sender: TObject);
var
  vm: TProdutoVM;
begin
  vm:= TProdutoVM.Create;
  try
    vm.Cancelar();
  finally
    vm.Free;
  end;
end;

procedure TFormHome.SpeedButton4Click(Sender: TObject);
var
  vm: TProdutoVM;
begin
  vm:= TProdutoVM.Create;
  try
    vm.Salvar();
  finally
    vm.Free;
  end;
end;

procedure TFormHome.swtSistemaAutonomoSwitch(Sender: TObject);
begin
  Layout23.Visible:= swtSistemaAutonomo.IsChecked;
end;

procedure TFormHome.TabConfiguracoesImpressoraSATClick(Sender: TObject);
begin
  MoveTab(TabBodyConfiguracoes);
  MoveTab(TabConfiguracoesImpressora);
end;

procedure TFormHome.tiraFocoList;
var
  i: integer;
begin
  for i := 0 to ComponentCount - 1 do
  begin
    if Components[i] is TListBox then
    begin
      TListBox(Components[i]).ItemIndex := -1;
    end;
  end;
end;

procedure TFormHome.tmrAtualizarUITimer(Sender: TObject);
begin
    dmVmPrincipal.ExibirMovimentoDeHoje();
    dmVmPrincipal.ListarNotas();
end;

procedure TFormHome.tmrMensagemTimer(Sender: TObject);
begin
  txtMsg3.Text:= '';
  txtMsg2.Text:= '';
  txtMsg1.Text:= '';
  layMensagem.Visible:= false;
  tmrMensagem.Enabled:= false;
  layMsg1.Visible:= false;
  layMsg2.Visible:= false;
  layMsg3.Visible:= false;
end;

procedure TFormHome.VaiParaInicio;
begin
  ImgMenuLateral(0);
  MoveLinha(RectBtnIncio);
  MoveTab(TabMenuLateralInicio);
  MoveTab(TabBodyInicio);
end;

procedure TFormHome.VaiParaRelatorios;
begin
  ImgMenuLateral(2);
  MoveLinha(RectBtnRelatorios);
  MoveTab(TabMenuLateralRelatorios);
  MoveTab(TabBodyRelatorios);
end;

procedure TFormHome.RectBtnRelatoriosClick(Sender: TObject);
begin
  VaiParaRelatorios;
end;

procedure TFormHome.RectBtnRelatoriosMouseDown(Sender: TObject;
Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  ImgMenuLateral(2);
end;

procedure TFormHome.RectBtnFecharClick(Sender: TObject);
begin
  self.Hide;
  try
    self.close;
    Application.Terminate;
  except
  end;
end;

procedure TFormHome.RectBtnMinimizarClick(Sender: TObject);
begin
  self.WindowState := TWindowState.wsMinimized;
  FormHome.WindowState := TWindowState.wsMinimized;
end;

procedure TFormHome.RectBtnMinimizarMouseEnter(Sender: TObject);
begin
  BtnAnimacao(Sender, 32, 32);
end;

procedure TFormHome.RectBtnMinimizarMouseLeave(Sender: TObject);
begin
  BtnAnimacao(Sender, 26, 25);
end;

procedure TFormHome.RectBtnConfiguracoesClick(Sender: TObject);
begin
  VaiParaConfiguracoes
end;

procedure TFormHome.RectBtnConfiguracoesMouseDown(Sender: TObject;
Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  ImgMenuLateral(3);
end;

procedure TFormHome.RectBtnCuponsClick(Sender: TObject);
begin
  VaiParaCupons;
end;

procedure TFormHome.RectBtnCuponsMouseDown(Sender: TObject;
Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  ImgMenuLateral(1)
end;

end.
