unit uVm.Auditoria;

interface

uses
  uAuditoria, System.Generics.Collections, System.DateUtils;

type
//classe de view model para auditoria
  TAuditoriaVm = class
  private
    //m�todo de clique para ocorrencia
    procedure EvClickOcorrencia(Sender: TObject);


    //m�todo para listar as ocorrncia
    procedure ListarOcorrencias(auds: TObjectList<TAuditoria>);
    //m�todo para mostrar uma ocorrencia
    procedure MostrarOcorrencia();

  public
    constructor Create;
    destructor Destroy; override;

    //m�todo para carregar as ocorrencia inciais
    procedure CarregamentoInicial();
    //m�todo para filtrar as ocorrencias
    procedure Filtrar();
  end;

implementation


uses uFrmAuditoria, uAuditoriaContexto, uAuRepository, FMX.ListBox,
  FMX.Objects, FMX.Layouts, FMX.Types, System.UITypes, System.SysUtils, FMX.Forms;
{ TAuditoriaVm }

procedure TAuditoriaVm.CarregamentoInicial;
var
  repo: TAuditoriaRepository;
begin
  repo:= TAuditoriaRepository.Create;
  try
    ListarOcorrencias(repo.ListarOcorrencias(DateOf(now), 'N'));
  finally
    repo.Free;
  end;
end;

constructor TAuditoriaVm.Create;
begin

end;

destructor TAuditoriaVm.Destroy;
begin
  inherited;
end;

procedure TAuditoriaVm.EvClickOcorrencia(Sender: TObject);
begin
  MostrarOcorrencia();
end;

procedure TAuditoriaVm.Filtrar;
var
  repo: TAuditoriaRepository;
  categoria: char;
begin
  repo:= TAuditoriaRepository.Create();
  try
   categoria:= 'N';

    if frmAuditoria.cedCategoria.Text = 'Cupom Fiscal' then
      categoria:= 'N'
    else if frmAuditoria.cedCategoria.Text = 'Cadastros' then
      categoria:= 'C'
    else if frmAuditoria.cedCategoria.Text = 'Relat�rios' then
      categoria:= 'R'
    else if frmAuditoria.cedCategoria.Text = 'Configura��es' then
      categoria:= 'O';

    ListarOcorrencias(repo.ListarOcorrencias(
      frmAuditoria.dtData.Date,
      categoria));
  finally
    repo.Free;
  end;

end;

procedure TAuditoriaVm.ListarOcorrencias(auds: TObjectList<TAuditoria>);
var
  it: TAuditoria;
  listItem: TListBoxItem;
begin
  try
    frmAuditoria.lstOcorrencias.BeginUpdate;
    frmAuditoria.lstOcorrencias.Items.Clear;
    for it in auds do
    begin
      listItem:= TListBoxItem.Create(frmAuditoria.lstOcorrencias);

      listItem.Text:= it.Resumo;
      listItem.Tag:= it.Id;
      listItem.OnClick := EvClickOcorrencia;

      frmAuditoria.lstOcorrencias.AddObject(listItem);
    end;
    frmAuditoria.lstOcorrencias.EndUpdate;
  finally
    auds.Free;
  end;
end;


procedure TAuditoriaVm.MostrarOcorrencia;
var
  repo: TAuditoriaRepository;
  auditoria: TAuditoria;
begin
  auditoria:= nil;
  repo:= TAuditoriaRepository.Create;
  try
    auditoria:= repo.Get(frmAuditoria.lstOcorrencias.Selected.Tag);
    frmAuditoria.mmoDetalhes.Lines.Clear;
    if Assigned(auditoria) then
    begin
      frmAuditoria.txtResumo.Text:= auditoria.Resumo;
      frmAuditoria.txtCategoria.Text:= frmAuditoria.cedCategoria.Text;
      frmAuditoria.mmoDetalhes.Lines.Add(auditoria.Descricao);
      frmAuditoria.txtDataHora.Text:= auditoria.DataHora;
    end;
  finally
    if Assigned(auditoria) then
      auditoria.Free;

    repo.Free;
  end;
end;

end.
