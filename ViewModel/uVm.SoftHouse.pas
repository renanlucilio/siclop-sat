unit uVm.SoftHouse;

interface

uses
  uSoftwareHouse,
  Form.Principal,
  System.SysUtils;

type
    //classe de view model para softhouse
  TSoftHouseVM = class
  private
  public
            //método para pegar as informações da tela
    class procedure GetDadosForm();
                //método para definir as informações na tela
    class procedure SetDadosForm();
  end;

implementation

{ TSoftHouseVM }

uses uAuRepository;

class procedure TSoftHouseVM.GetDadosForm;
var
  soft: TSoftwareHouse;
begin
  soft := TSoftwareHouse.Create;
  try
    soft.NomeFantasia:= FormHome.EdtTSoftHouse_nomeFantasia.Text;
    soft.ChaveAC:= FormHome.mmChaveAC.Lines.Text;
    soft.CNPJ:= FormHome.EdtTSoftHouse_cnpj.Text;
    soft.NumeroCaixa:= StrToIntDef(FormHome.EdtTEstabelecimento_numPDV.Text, 0);

    soft.Salvar;

    TAuditoriaRepository.GravarLog(
          'Configuração Alterada',
           FormHome.txtUsuario.Text + #13 +
          ' Foram alteradas as configurações da "SoftHouse"' +  #13 +
          ' Horario:' + TimeToStr(Now),
          'O');
  finally
    soft.Free;
  end;
end;

class procedure TSoftHouseVM.SetDadosForm;
var
  soft: TSoftwareHouse;
begin
  soft := TSoftwareHouse.Create;
  try
    FormHome.EdtTSoftHouse_nomeFantasia.Text:= soft.NomeFantasia;
    FormHome.mmChaveAC.Lines.Add(soft.ChaveAC);
    FormHome.EdtTSoftHouse_cnpj.Text:= soft.CNPJ;
    FormHome.EdtTEstabelecimento_numPDV.Text:= IntToStr(soft.NumeroCaixa);
  finally
    soft.Free;
  end;
end;

end.
