unit uVm.Estabelecimento;

interface

uses Form.Principal, uEstabelecimento;

type
  //classe de view model para estabelecimento
  TEstabelecimentoVM = class
  private
  public
      //método para pegar as informações da tela
    class procedure GetDadosForm();
        //método para definir as informações na tela
    class procedure SetDadosForm();
  end;

implementation

{ TEstabelecimentoVM }

uses uAuRepository, System.SysUtils;

class procedure TEstabelecimentoVM.GetDadosForm;
var
  esta: TEstabelecimento;
begin
  esta := TEstabelecimento.Create;
  try
    esta.NomeFantasia:= FormHome.EdtTEstabelecimento_nomeFantasia.Text;
    esta.CNPJ:= FormHome.EdtTEstabelecimento_cnpj.Text;
    esta.IE:= FormHome.EdtTEstabelecimento_ie.Text;
    esta.IM:= FormHome.EdtTEstabelecimento_im.Text;
    esta.Email:= FormHome.EdtTEstabelecimento_email.Text;
    esta.ehSimplesNacional:= FormHome.SwitchTEstabelecimento_ehSimplesNacional.IsChecked;
    esta.FazerRateio:= FormHome.SwitchTEstabelecimento_fazerRatioISSQN.IsChecked;
    esta.SetISSQN(FormHome.cbbCbBTEstabelecimento_issqn.ItemIndex);
    esta.Salvar;

    TAuditoriaRepository.GravarLog(
          'Configuração Alterada',
           FormHome.txtUsuario.Text + #13 +
          ' Foram alteradas as configurações do "Estabelecimento"' + #13 +
          ' Horario:' + TimeToStr(Now),
          'O');
  finally
    esta.Free;
  end;
end;

class procedure TEstabelecimentoVM.SetDadosForm;
var
  esta: TEstabelecimento;
begin
  esta := TEstabelecimento.Create;
  try
    FormHome.EdtTEstabelecimento_nomeFantasia.Text:= esta.NomeFantasia;
    FormHome.EdtTEstabelecimento_cnpj.Text:= esta.CNPJ;
    FormHome.EdtTEstabelecimento_ie.Text:= esta.IE;
    FormHome.EdtTEstabelecimento_im.Text:= esta.IM;
    FormHome.EdtTEstabelecimento_email.Text:= esta.Email;
    FormHome.SwitchTEstabelecimento_ehSimplesNacional.IsChecked:= esta.ehSimplesNacional;
    FormHome.SwitchTEstabelecimento_fazerRatioISSQN.IsChecked:= esta.FazerRateio;
    FormHome.cbbCbBTEstabelecimento_issqn.ItemIndex:= Integer(esta.ISSQN);
  finally
    esta.Free;
  end;
end;
end.
