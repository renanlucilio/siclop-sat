unit uVm.Usuario;

interface

uses uUsuario, uUsuarioReporitory;

type
//classe de view model para usuario
  TUsuarioVM = class
  private
  //m�todo para limpar os dados da tela
    procedure LimparDados();
    //m�todo para definir os campos na tela bom base em um obj
    procedure SetarCampos(model: TUsuario);
    //m�todo para retorna um model
    function GetModel(): TUsuario;
  public
  //m�todo para carregar usaurios
    procedure Carregar();
    //m�todo para inicar a inser��o
    procedure InitInserir();
    //m�todo para iniciar a edi��o
    procedure InitEdit();
    //m�todo para excluir usu�rio
    procedure Excluir();
    //m�todo para salvar usu�rio
    procedure Salvar();
    //m�todo para cancelar usu�rio
    procedure Cancelar();
  end;

implementation

uses Form.Principal, FMX.Dialogs, System.UITypes, System.SysUtils;

{ TUsuarioVM }

procedure TUsuarioVM.Cancelar;
begin
  FormHome.MoveTab(FormHome.tabUsuarioLista);
  Carregar();
end;

procedure TUsuarioVM.Carregar;
var
  repo: TUsuarioRepository;
begin
  repo := TUsuarioRepository.Create;
  try
    repo.SelectAll();
  finally
    repo.Free;
  end;
end;

procedure TUsuarioVM.Excluir;
var
  repo: TUsuarioRepository;
begin
  repo := TUsuarioRepository.Create;
  try
    if FormHome.lstUsuariosGrid.ItemIndex > -1 then
    if MessageDlg('Tem certeza que deseja excluir? ',TMsgDlgType.mtConfirmation,
    [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo],0) = mrYes then
    begin
      repo.Delete(FormHome.txtUsuarioId.Tag);

      Carregar();
    end;
  finally
    repo.Free;
  end;
end;

function TUsuarioVM.GetModel: TUsuario;
var
  mensagem: TStringBuilder;
begin
  Result:= TUsuario.Create;

  if FormHome.edtUsuarioSenha.Text.Trim <> FormHome.edtConfirmarSenha.Text.Trim then
  begin
    FormHome.MostraMensage('Senhas n�o s�o Compat�veis');
    FormHome.edtUsuarioSenha.Text:= '';
    FormHome.edtConfirmarSenha.Text:= '';
  end;


  if FormHome.txtUsuarioTipoForm.Tag = 1 then
    Result.Id:= FormHome.txtUsuarioId.Tag;

  Result.Nome:= FormHome.edtUsuarioNome.Text.Trim;
  Result.Senha:= FormHome.edtUsuarioSenha.Text.Trim;
  Result.Nivel:= FormHome.cedUsuarioNivelAcesso.ItemIndex;

  mensagem:= TStringBuilder.Create();
  try

    if Result.Nome = '' then
    begin
      FormHome.edtUsuarioNome.SetFocus;
      mensagem.Append('Campo "Nome" est� inv�lido').AppendLine;
    end;

    if Result.Senha = '' then
    begin
      FormHome.edtUsuarioSenha.SetFocus;
      mensagem.Append('Campo "Senha" est� inv�lido').AppendLine;
    end;


    if mensagem.Length > 0 then
    begin
      FreeAndNil(Result);

      ShowMessage(mensagem.ToString());
    end;
  finally
    mensagem.Free;
  end;
end;

procedure TUsuarioVM.InitEdit;
var
  repo: TUsuarioRepository;
  model: TUsuario;
begin
  repo := TUsuarioRepository.Create;
  try
    model:= repo.InitEdit(FormHome.txtUsuarioId.Tag);
    if Assigned(model) then
    begin
      LimparDados();
      SetarCampos(model);

      FormHome.txtUsuarioTipoForm.Tag:= 1;
      FormHome.MoveTab(FormHome.tabUsuario);
      FormHome.MoveTab(FormHome.tabUsuarioCad);
    end;
  finally
    if Assigned(model) then
      model.Free;

    repo.Free;
  end;
end;

procedure TUsuarioVM.InitInserir;
var
  repo: TUsuarioRepository;
begin
  repo := TUsuarioRepository.Create;
  try
    LimparDados();

    FormHome.txtUsuarioTipoForm.Tag:= 0;
    FormHome.MoveTab(FormHome.tabUsuario);
    FormHome.MoveTab(FormHome.tabUsuarioCad);
  finally
    repo.Free;
  end;
end;

procedure TUsuarioVM.LimparDados;
begin
  FormHome.edtUsuarioNome.Text:= '';
  FormHome.edtUsuarioSenha.Text:= '';
  FormHome.edtConfirmarSenha.Text:= '';
  FormHome.cedUsuarioNivelAcesso.ItemIndex:= 1;
end;

procedure TUsuarioVM.Salvar;
var
  tipoForm: integer;
  repo: TUsuarioRepository;
  model: TUsuario;
begin
  tipoForm:= FormHome.txtUsuarioTipoForm.Tag;
  repo:= TUsuarioRepository.Create();
  model:= GetModel();
  if Assigned(model) then
  begin
    try
      if tipoForm = 0 then
      begin
        try
          repo.Insert(model);
          FormHome.MoveTab(FormHome.tabUsuarioLista);
        except
          FormHome.MostraMensage('Erro ao cadastrar o usu�rio');
        end;

      end
      else if tipoForm = 1 then
      begin
        try
          repo.Edit(model);
          FormHome.MoveTab(FormHome.tabUsuarioLista);
        except
          FormHome.MostraMensage('Erro ao editar o usu�rio');
        end;
      end;
      Carregar();
    finally
      repo.Free;
    end;
  end
  else
    FormHome.MostraMensage('H� dados inv�lidos para o cadastro');
end;

procedure TUsuarioVM.SetarCampos(model: TUsuario);
begin
  FormHome.edtUsuarioNome.Text:= model.Nome;
  FormHome.edtUsuarioSenha.Text:= model.Senha;
  FormHome.cedUsuarioNivelAcesso.ItemIndex:= model.Nivel;
end;

end.
