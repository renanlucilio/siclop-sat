unit ViewModel.Principal;

interface

uses
  Form.Principal,
  System.Generics.Collections,
  uSAT,
  Data.Bind.Components,
  Data.Bind.ObjectScope,
  FMX.Forms;

type
  TPrincipalViewModel = class
  private
    FView: TFormHome;
    FSAT: TObjectList<TSAT>;
    _bindSAT: TAdapterBindSource;
    procedure CriandoBindSAT(Sender: TObject; var ABindSourceAdapter: TBindSourceAdapter);
  public
    constructor Create(sat: TSAT);
    destructor Destroy; override;
    property View: TFormHome read FView;
  end;

implementation


{ TPrincipalViewModel }

constructor TPrincipalViewModel.Create(sat: TSAT);
begin
  FSAT:= TObjectList<TSAT>.Create();

  FView:= FormHome;
  FSAT.Add(sat);
  _bindSAT:= TAdapterBindSource.Create(FView);
  _bindSAT.OnCreateAdapter:= CriandoBindSAT;

end;

procedure TPrincipalViewModel.CriandoBindSAT(Sender: TObject;
  var ABindSourceAdapter: TBindSourceAdapter);
begin
  ABindSourceAdapter:= TListBindSourceAdapter<TSAT>.Create(FView, FSAT);
end;

destructor TPrincipalViewModel.Destroy;
begin
  FSAT.Clear;
  FSAT.Free;
  inherited;
end;

end.
