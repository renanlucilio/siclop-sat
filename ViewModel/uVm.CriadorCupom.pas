
unit uVm.CriadorCupom;

interface

uses uCriador.Cupom, uCliente, uVendaProduto, System.Generics.Collections,
  uVenda, FMX.Types, uProduto;

type
  //enum com as fases de cria��o de um cupom
  TFasePedido = (fpIniciado, fpAddCliente, fpProdutos, fpAddPagmento, fpFinalizado);

  //classe de view model para cria��o de um cupom
  TCriadorCupomVM = class    
  private
    //singletonda classe
    class var instacia: TCriadorCupomVM;

    fListTemp: TObjectList<TVendaProduto>;
    fase: TFasePedido;
    criadorCupom : TCriadorCupom;
    tmrFoco: TTimer;
    //construtor definitivo
    constructor CreatePrivate();

    //----Eventos
    //evento de clique para cancelar a cria��o do cupom
    procedure EvBtnCancelar(Sender: TObject);
    //evento de clique para passar de fase de cria��o do cupom
    procedure EvBtnNext(Sender: TObject);
    //evento de clique para voltar uma fase na cria��o do cupom
    procedure EvBtnVoltar(Sender: TObject);
    //evento de clique para adicionar um produto
    procedure EvProdutoClick(Sender: TObject);
    //evento de clique para editar/apagar um produto da lista
    procedure EvProdutoCupomClick(Sender: TObject);
    //evento de clique para adicionar produto no cupom
    procedure EvDefiniProdutoPositivo(Sender: TObject);
    //evento de clique para remover um produto do cupom
    procedure EvDefiniProdutoNegativo(Sender: TObject);
    //evento de clique para excluir um produto do cupom
    procedure EvDefiniProdutoExcluir(Sender: TObject);

    //----M�todos GetView
    //m�todo para pegar as informa��es do cliente da tela
    function GetCliente(): TCliente;
    //m�todo para pegar as informa��es do produto da tela
    function GetProduto(): TVendaProduto;
    //m�todo para pegar as informa��es de pagamento da tela
    function GetPagamento(): TVenda;

    //----M�todos Para trocar de fase
    //m�todo para trocar a fase atual do cupom
    procedure Next;
    //m�todo para cancelar a cria��o do cupom
    procedure Cancelar;
    //m�todo para voltar uma fase na cria��o do cupom
    procedure Voltar();

    //----M�todos das fases
    //m�todo para definir a fase de cliente
    procedure FaseCliente;
    //m�todo para definir a fase de produtos
    procedure FaseProdutos;
    //m�todo para definir a fase de pagamentos
    procedure FasePagamento;
    //m�todo para definir a fase de finalizar
    procedure FaseFinalizar;
    //m�todo para finalizar a cria��o de cupom
    procedure Fim;

    //m�todo para voltar a fase de cliente
    procedure VoltarCliente;
    //m�todo para voltar para fase de produtos
    procedure VoltarProdutos;
    //m�todo para voltar para fase de pagamento
    procedure VoltarPagamento;
    //m�todo para voltar para fase de finalizar
    procedure VoltarFinalizar;

    //----M�todos de exibi��o do cupom
    //m�todo para mostrar no cupom de exemplo as informa��es do cliente
    procedure MostrarCupomCliente(cliente: TCliente);
    //m�todo para mostrar no cupom de exemplo as informa��es de produtos
    procedure MostrarCupomProdutos();
        //m�todo para mostrar no cupom de exemplo as informa��es de pagamento
    procedure MostrarCupomPagamento(venda: TVenda);

    //----M�todos de defini��o do cupom
    //m�todo para carregar os produtos
    function CarregarProdutos(): Boolean;
    //m�todo para limpar todos os campos
    procedure LimparCampos;
    //m�todo para definir o foco dependendo da fase
    procedure DefinirTmrFoco();

    //----M�todos para crian��o de componentes em runtime
    //m�todo para criar componentes de lista de produtos
    procedure CriandoComponentesListaProdutos(listaProdutos: TObjectList<TProduto>);
    //m�todo para criar componentes de lista de produtos do cupom
    procedure CriandoComponentesListaProdutosCupom();

    //----M�todos para fase de produtos
    //m�todo para adicionar um produto da lista
    procedure SelecionarProdutoParaAdd(id: integer);
    //m�todo para editar um produto da lista
    procedure SelecionarProdutoParaEditar(indice: integer);
    //m�todo para adicionar um produto
    procedure AddProduto(produtoVenda: TVendaProduto);

  public
    //m�todo para criar uma unica instancia da classe
    class function New(): TCriadorCupomVM;
    //m�todo para inicar a cria��o do cupom
    procedure Iniciar;

    constructor Create;
    destructor Destroy; override;

  end;

implementation

uses Form.Principal, uProdutoRepository, FMX.ListBox, System.SysUtils,
  uValidacoes, uTimerAnonymous, FMX.Graphics, System.MaskUtils, FMX.Objects,
  FMX.Layouts, System.UITypes, FMX.Dialogs, System.DateUtils,
  uAuRepository;

{ TCriadorCupomVM }

procedure TCriadorCupomVM.AddProduto(produtoVenda: TVendaProduto);
var
  naoExist: Boolean;
  vndProduto: TVendaProduto;
begin
  naoExist:= true;

  for vndProduto in fListTemp do
  begin
    if (vndProduto.ProdutoId = produtoVenda.ProdutoId) and
      (vndProduto.Produto.Descricao.ToUpper().Trim() = produtoVenda.Produto.Descricao.ToUpper().Trim()) then
    begin
      vndProduto.Quantidade:= vndProduto.Quantidade + produtoVenda.Quantidade;
      naoExist:= false;
      Break;
    end
    else
      naoExist:= true;
  end;

  if naoExist then
    fListTemp.Add(produtoVenda);

  CriandoComponentesListaProdutosCupom();
end;



procedure TCriadorCupomVM.Cancelar;
begin
  if fase = fpFinalizado then
  begin
    criadorCupom.ConfirmarCupom(false);
    Fim();
    Iniciar();
  end
  else
  begin
    Fim();
    Iniciar();
    FormHome.VaiParaInicio;
  end;
end;

function TCriadorCupomVM.CarregarProdutos(): Boolean;
var
  repo: TProdutoRepository;
  listaProdutos: TObjectList<TProduto>;
  I: Integer;
begin
  repo := TProdutoRepository.Create;
  try
    listaProdutos:= repo.GetAll();

    if listaProdutos.Count <= 0 then
      exit(false);

    CriandoComponentesListaProdutos(listaProdutos);


    Result:= true;
  finally
    listaProdutos.Free;
    repo.Free;
  end;

end;

constructor TCriadorCupomVM.Create;
begin
  raise Exception.Create('');
end;

constructor TCriadorCupomVM.CreatePrivate;
begin
  DefinirTmrFoco();
  FormHome.btnCupomCancelar.OnClick:= EvBtnCancelar;
  FormHome.btnCupomNext.OnClick:= EvBtnNext;
  FormHome.btnVoltar.OnClick:= EvBtnVoltar;
  FormHome.btnNegProduto.OnClick:= EvDefiniProdutoNegativo;
  FormHome.btnPosProduto.OnClick:= EvDefiniProdutoPositivo;
  FormHome.btnExcluirProduto.OnClick:= EvDefiniProdutoExcluir;
  Iniciar();
end;

procedure TCriadorCupomVM.DefinirTmrFoco;
begin

  tmrFoco:= TTimer.CreateAnonymousTimer(
  procedure 
  begin 
    case fase of
      fpAddCliente:
      begin
        FormHome.rectCupomDadosCliente.Stroke.Kind:= TBrushKind.Solid;
        FormHome.rectCupomProdutos.Stroke.Kind:= TBrushKind.None;
        FormHome.rectCupomDadosPag.Stroke.Kind:= TBrushKind.None;
        FormHome.rectCupom.Stroke.Kind:= TBrushKind.None;
        
        if FormHome.rectCupomDadosCliente.Stroke.Dash = TStrokeDash.Solid then
          FormHome.rectCupomDadosCliente.Stroke.Dash := TStrokeDash.DashDot
        else
          FormHome.rectCupomDadosCliente.Stroke.Dash := TStrokeDash.Solid;  
      end;
      fpProdutos:
      begin
        FormHome.rectCupomDadosCliente.Stroke.Kind:= TBrushKind.None;
        FormHome.rectCupomProdutos.Stroke.Kind:= TBrushKind.Solid;
        FormHome.rectCupomDadosPag.Stroke.Kind:= TBrushKind.None;
        FormHome.rectCupom.Stroke.Kind:= TBrushKind.None;
        
        if FormHome.rectCupomProdutos.Stroke.Dash = TStrokeDash.Solid then
          FormHome.rectCupomProdutos.Stroke.Dash := TStrokeDash.DashDot
        else
          FormHome.rectCupomProdutos.Stroke.Dash := TStrokeDash.Solid;  
      end;
      fpAddPagmento:
      begin
        FormHome.rectCupomDadosCliente.Stroke.Kind:= TBrushKind.None;
        FormHome.rectCupomProdutos.Stroke.Kind:= TBrushKind.None;
        FormHome.rectCupomDadosPag.Stroke.Kind:= TBrushKind.Solid;
        FormHome.rectCupom.Stroke.Kind:= TBrushKind.None;

        if FormHome.rectCupomDadosPag.Stroke.Dash = TStrokeDash.Solid then
          FormHome.rectCupomDadosPag.Stroke.Dash := TStrokeDash.DashDot
        else
          FormHome.rectCupomDadosPag.Stroke.Dash := TStrokeDash.Solid;  
      end;
      fpFinalizado:
      begin
        FormHome.rectCupomDadosCliente.Stroke.Kind:= TBrushKind.None;
        FormHome.rectCupomProdutos.Stroke.Kind:= TBrushKind.None;
        FormHome.rectCupomDadosPag.Stroke.Kind:= TBrushKind.none;
        FormHome.rectCupom.Stroke.Kind:= TBrushKind.Solid;

        if FormHome.rectCupom.Stroke.Dash = TStrokeDash.Solid then
          FormHome.rectCupom.Stroke.Dash := TStrokeDash.DashDot
        else
          FormHome.rectCupom.Stroke.Dash := TStrokeDash.Solid;
      end;

    end;
  end, 1000, 1000);
end;

destructor TCriadorCupomVM.Destroy;
begin
  FormHome.btnCupomCancelar.OnClick:= nil;
  FormHome.btnCupomNext.OnClick:= nil;
  if Assigned(criadorCupom) then
    FreeAndNil(criadorCupom);

  if Assigned(fListTemp) then
    FreeAndNil(fListTemp);
  inherited;
end;

procedure TCriadorCupomVM.CriandoComponentesListaProdutos(listaProdutos: TObjectList<TProduto>);
var
  produto: TProduto;
  listItem: TListBoxItem;
  txtCodigo: TText;
  containerDescricaoCategoria: TLayout;
  txtDescricao: TText;
  txtCategoria: TText;
  txtValor: TText;
begin
  FormHome.lstProdutos.BeginUpdate;
  for produto in listaProdutos do
  begin
    listItem := TListBoxItem.Create(FormHome.lstProdutos);
    listItem.Height := 40;

    txtCodigo := TText.Create(listItem);
    txtCodigo.Width := 40;
    txtCodigo.Font.Family := 'Consolas';
    txtCodigo.TextSettings.HorzAlign := TTextAlign.Leading;
    txtCodigo.Font.Size := 12;
    txtCodigo.Parent := listItem;
    txtCodigo.Align := TAlignLayout.Left;
    txtCodigo.Text := produto.Id.Value.ToString;
    txtCodigo.HitTest := false;

    containerDescricaoCategoria := TLayout.Create(listItem);
    containerDescricaoCategoria.Parent := listItem;
    containerDescricaoCategoria.Align := TAlignLayout.Left;
    containerDescricaoCategoria.Width := 240;
    containerDescricaoCategoria.HitTest := false;
    containerDescricaoCategoria.Height := 40;

    txtDescricao := TText.Create(listItem);
    txtDescricao.Font.Family := 'Consolas';
    txtDescricao.TextSettings.HorzAlign := TTextAlign.Leading;
    txtDescricao.Font.Size := 14;
    txtDescricao.Height := 20;
    txtDescricao.Font.Style := [TFontStyle.fsBold];
    txtDescricao.Parent := containerDescricaoCategoria;
    txtDescricao.Align := TAlignLayout.Top;
    txtDescricao.Text := produto.Descricao;
    txtDescricao.HitTest := false;

    txtCategoria := TText.Create(listItem);
    txtCategoria.Font.Family := 'Consolas';
    txtCategoria.Font.Size := 10;
    txtCategoria.Font.Style := [TFontStyle.fsBold];
    txtCategoria.Height := 20;
    txtCategoria.TextSettings.HorzAlign := TTextAlign.Leading;
    txtCategoria.Parent := containerDescricaoCategoria;
    txtCategoria.Align := TAlignLayout.Top;
    txtCategoria.Text := produto.Categoria.Descricao;
    txtCategoria.HitTest := false;

    containerDescricaoCategoria.AddObject(txtDescricao);
    containerDescricaoCategoria.AddObject(txtCategoria);

    txtValor := TText.Create(listItem);
    txtValor.Width := 100;
    txtValor.Font.Family := 'Consolas';
    txtValor.TextSettings.HorzAlign := TTextAlign.Trailing;
    txtValor.Font.Size := 16;
    txtValor.Parent := listItem;
    txtValor.Align := TAlignLayout.Right;
    txtValor.Text := FormatFloat('R$ ###,###,##0.00', produto.Preco);
    txtValor.HitTest := false;

    listItem.AddObject(txtCodigo);
    listItem.AddObject(containerDescricaoCategoria);
    listItem.AddObject(txtValor);

    listItem.Tag := produto.Id;
    listItem.OnClick := EvProdutoClick;
    listItem.StyledSettings:= [];
    listItem.TextSettings.FontColor:= TColor($ffffff);
    listItem.TextSettings.Font.Size := 1;
    listItem.Text := produto.Id.Value.ToString + ' '
     + produto.Categoria.Descricao + ' '
      + produto.Descricao;

    FormHome.lstProdutos.AddObject(listItem);
  end;
  FormHome.lstProdutos.EndUpdate;
end;

procedure TCriadorCupomVM.CriandoComponentesListaProdutosCupom;
var
  vndProduto: TVendaProduto;
  listItem: TListBoxItem;
  txtDescricao: TText;
  txtQuantidade: TText;
  containerDescricaoQuantidade: TLayout;
  txtValor: TText;

  total: double;
begin

  FormHome.lstProdutosCupom.BeginUpdate;
  FormHome.lstProdutosCupom.Items.Clear;
  total:= 0;
  for vndProduto in fListTemp do
  begin
    listItem := TListBoxItem.Create(FormHome.lstProdutosCupom);
    listItem.Height := 40;

    containerDescricaoQuantidade := TLayout.Create(listItem);
    containerDescricaoQuantidade.Parent := listItem;
    containerDescricaoQuantidade.Align := TAlignLayout.Left;
    containerDescricaoQuantidade.Width := 150;
    containerDescricaoQuantidade.HitTest := false;
    containerDescricaoQuantidade.Height := 40;
    containerDescricaoQuantidade.HitTest:= false;

    txtDescricao := TText.Create(containerDescricaoQuantidade);
    txtDescricao.Font.Family := 'Consolas';
    txtDescricao.TextSettings.HorzAlign := TTextAlign.Leading;
    txtDescricao.Font.Size := 12;
    txtDescricao.Height := 30;
    txtDescricao.Font.Style := [TFontStyle.fsBold];
    txtDescricao.Parent := containerDescricaoQuantidade;
    txtDescricao.Align := TAlignLayout.Top;
    txtDescricao.Text := vndProduto.Produto.Descricao;
    txtDescricao.HitTest := false;

    txtQuantidade := TText.Create(containerDescricaoQuantidade);
    txtQuantidade.Font.Family := 'Consolas';
    txtQuantidade.Font.Size := 10;
    txtQuantidade.Font.Style := [TFontStyle.fsBold];
    txtQuantidade.Height := 10;
    txtQuantidade.TextSettings.HorzAlign := TTextAlign.Leading;
    txtQuantidade.Parent := containerDescricaoQuantidade;
    txtQuantidade.Align := TAlignLayout.Top;
    txtQuantidade.Text := FormatFloat('Quandidade: x###,###,##0.00', vndProduto.Quantidade);
    txtQuantidade.HitTest := false;

    containerDescricaoQuantidade.AddObject(txtDescricao);
    containerDescricaoQuantidade.AddObject(txtQuantidade);

    txtValor := TText.Create(listItem);
    txtValor.Width := 80;
    txtValor.Height := 40;
    txtValor.Font.Family := 'Consolas';
    txtValor.TextSettings.HorzAlign := TTextAlign.Trailing;
    txtValor.Font.Style := [TFontStyle.fsBold];
    txtValor.Font.Size := 12;
    txtValor.Parent := listItem;
    txtValor.Align := TAlignLayout.Left;
    total:= total + (vndProduto.Produto.Preco * vndProduto.Quantidade) ;
    txtValor.Text := FormatFloat('R$ ###,###,##0.00', (vndProduto.Produto.Preco * vndProduto.Quantidade));
    txtValor.HitTest := false;

    listItem.AddObject(containerDescricaoQuantidade);
    listItem.AddObject(txtValor);

    listItem.Tag:= fListTemp.IndexOf(vndProduto);
    listItem.OnClick := EvProdutoCupomClick;
    listItem.StyledSettings:= [];
    listItem.TextSettings.FontColor:= TColor($ffffff);
    listItem.TextSettings.Font.Size := 1;
    listItem.Text := vndProduto.Produto.Descricao;

    FormHome.lstProdutosCupom.AddObject(listItem);
  end;
  FormHome.txtValorCupom.Text:= FormatFloat('Total R$ ###,###,##0.00', total);
  FormHome.txtCupomValorTotal.Text:= FormatFloat('Total R$ ###,###,##0.00', total);
  FormHome.txtCupomValorTotal.TagFloat:= total;
  FormHome.lstProdutosCupom.EndUpdate;
end;

procedure TCriadorCupomVM.EvBtnCancelar(Sender: TObject);
begin
  Cancelar;

  TAuditoriaRepository.GravarLog(
          'Cancelado a emiss�o do cupom',
           FormHome.txtUsuario.Text + #13 +
          ' N�o foi concluido a emiss�o do cupom fiscal' + #13 +
          ' Horario:' + TimeToStr(Now),
          'N');
end;

procedure TCriadorCupomVM.EvBtnNext(Sender: TObject);
begin
  Next;
end;

procedure TCriadorCupomVM.EvBtnVoltar(Sender: TObject);
begin
  Voltar;
end;

procedure TCriadorCupomVM.EvDefiniProdutoExcluir(Sender: TObject);
begin
  if MessageDlg('Tem certeza que deseja excluir? ',TMsgDlgType.mtConfirmation,
    [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo],0) = mrYes then
  begin
    FormHome.MoveTab(FormHome.tabCupomProdutos);
    FormHome.lBtnCupons.Visible:= True;
    FormHome.btnExcluirProduto.Visible:= false;
  end;
end;

procedure TCriadorCupomVM.EvDefiniProdutoNegativo(Sender: TObject);
var
  vndProduto: TVendaProduto;
begin
  vndProduto:= GetProduto();

  if FormHome.lBtnsDefProduto.Tag = 0 then
  begin
    FormHome.MoveTab(FormHome.tabCupomProdutos);
    FormHome.lBtnCupons.Visible:= True;
    FormHome.btnExcluirProduto.Visible:= false;
  end
  else if FormHome.lBtnsDefProduto.Tag = 1 then
  begin
    AddProduto(vndProduto);
    FormHome.MoveTab(FormHome.tabCupomProdutos);
    FormHome.lBtnCupons.Visible:= True;
    FormHome.btnExcluirProduto.Visible:= false;
  end;
end;

procedure TCriadorCupomVM.EvDefiniProdutoPositivo(Sender: TObject);
var
  vndProduto: TVendaProduto;
begin
    vndProduto:= GetProduto();

    if Assigned(vndProduto) then
    begin
        AddProduto(vndProduto);
        FormHome.MoveTab(FormHome.tabCupomProdutos);
        FormHome.lBtnCupons.Visible:= True;
        FormHome.btnExcluirProduto.Visible:= false;
    end;
end;

procedure TCriadorCupomVM.EvProdutoClick(Sender: TObject);
begin
  if Sender is TListBoxItem then
  begin
    SelecionarProdutoParaAdd((Sender as TListBoxItem).Tag);
  end;
end;

procedure TCriadorCupomVM.EvProdutoCupomClick(Sender: TObject);
begin
  if Sender is TListBoxItem then
  begin
    SelecionarProdutoParaEditar((Sender as TListBoxItem).Tag);
  end;
end;

procedure TCriadorCupomVM.FaseCliente;
var
  cliente: TCliente;
begin
  if fase = fpAddCliente then
  begin
    cliente:= GetCliente();

    if not Assigned(cliente) then
      exit;

    MostrarCupomCliente(cliente);
    criadorCupom.SetarCliente(cliente);

    fase:= fpProdutos;
    FormHome.MoveTab(FormHome.tabCupomProdutos);
  end;
  
end;

procedure TCriadorCupomVM.FaseFinalizar;
var
  resposta: integer;
begin
  if fase = fpFinalizado then
  begin
    FormHome.MostraMensage('Cupom gerado com sucesso');
    criadorCupom.ConfirmarCupom(true);
    Fim();
    Iniciar();
  end;
end;

procedure TCriadorCupomVM.Iniciar;
begin
  fase:= fpIniciado;

  if Assigned(criadorCupom) then
  begin
    FreeAndNil(criadorCupom);
    criadorCupom:= TCriadorCupom.Create;
  end
  else
    criadorCupom:= TCriadorCupom.Create;

  if Assigned(fListTemp) then
  begin
    FreeAndNil(fListTemp);
    fListTemp:= TObjectList<TVendaProduto>.Create;
  end
  else
    fListTemp:= TObjectList<TVendaProduto>.Create;


  if fase = fpIniciado then
  begin
    LimparCampos;
    FormHome.btnCupomCancelar.Visible:= false;
    if CarregarProdutos then
    begin
      FormHome.btnCupomNext.Text:= 'Pr�ximo';
      FormHome.lBtnCupons.Visible:= true;
      FormHome.MoveTab(FormHome.TabCupom);
      FormHome.MoveTab(FormHome.tabCupomCliente);

    
      fase:= fpAddCliente;
    end
    else
    begin
      FormHome.MostraMensage('N�o � poss�vel emitir cupom sem produtos cadastrados');
      FormHome.MoveTab(FormHome.tabBodyCadastro);
      FormHome.MoveTab(FormHome.tabCategoria);
      FormHome.MoveTab(FormHome.TabCategoriaLista);
    end;

  end;
  
end;

procedure TCriadorCupomVM.FasePagamento;
var
  venda: TVenda;
begin
  if fase = fpAddPagmento then
  begin
    venda:= GetPagamento();

    if not Assigned(venda) then
      exit;

    MostrarCupomPagamento(venda);
    criadorCupom.SetarPagamento(venda);

    FormHome.btnCupomNext.Text:= 'Finalizar';
    FormHome.MoveTab(FormHome.tabConfirmacao);
    FormHome.btnCupomCancelar.Visible:= true;
    fase:= fpFinalizado;
  end;
end;

procedure TCriadorCupomVM.FaseProdutos;
begin
  if fase = fpProdutos then
  begin

    if fListTemp.Count <= 0 then
    begin
      FormHome.MostraMensage('O Cupom deve ter pelo menos 1 produto!');
      exit;
    end;

    MostrarCupomProdutos;
    criadorCupom.SetarProdutos(fListTemp);
    fListTemp:= nil;

    fase:= fpAddPagmento;
    FormHome.MoveTab(FormHome.tabCupomPagamento);

    if FormHome.edtCupomPagDesconto.Text.IsEmpty then
      FormHome.edtCupomPagDesconto.Text:= '0';

    if FormHome.edtCupomPagAcrescimo.Text.IsEmpty then
      FormHome.edtCupomPagAcrescimo.Text:= '0';
  end;
end;

procedure TCriadorCupomVM.Fim;
begin
  fase:= fpAddCliente;
  FreeAndNil(criadorCupom);
  criadorCupom:= TCriadorCupom.Create;
  fListTemp:= TObjectList<TVendaProduto>.Create;
  LimparCampos;
  CarregarProdutos;
  FormHome.lBtnCupons.Visible:= true;
  FormHome.MoveTab(FormHome.TabCupom);
  FormHome.MoveTab(FormHome.tabCupomCliente);
end;

function TCriadorCupomVM.GetCliente: TCliente;
var
  doc: string;
begin
  Result:= TCliente.Create;

  Result.Nome:= FormHome.edtCupomClienteNome.Text.Trim.ToUpper();
  doc:= FormHome.EdtCupomClienteDoc.Text.Trim;

  if TValidacaoes.ValidarCPF(doc) then
    Result.Documento:= doc
  else if TValidacaoes.ValidarCNPJ(doc) then
    Result.Documento:= doc
  else if doc = '' then
    Result.Documento:= doc
  else
  begin
    FormHome.MostraMensage('Documento inv�lido');
    FreeAndNil(Result);    
  end;
  
end;

function TCriadorCupomVM.GetPagamento: TVenda;
begin
  Result:= TVenda.Create;

  Result.Desconto:= StrToFloatDef(FormHome.edtCupomPagDesconto.Text, 0);
  Result.Acrescimo:= StrToFloatDef(FormHome.edtCupomPagAcrescimo.Text, 0);
  Result.DataHora:= DateToISO8601(Now());
  Result.Valor:= FormHome.txtCupomValorTotal.TagFloat;
  Result.FormaPagamento:= FormHome.cedCupomPagFormaPagamento.Text;
  Result.OperadoraCartao:= FormHome.cedCupomPagOperadorCartao.Text;

  if StrToFloatDef(FormHome.edtCupomPagDesconto.Text, 0) < FormHome.txtCupomValorTotal.TagFloat then
    Result.Desconto:= StrToFloatDef(FormHome.edtCupomPagDesconto.Text, 0)
  else
  begin
    FormHome.MostraMensage('Desconto inv�lido');
    FreeAndNil(Result);
  end;

end;

function TCriadorCupomVM.GetProduto: TVendaProduto;
var
  qtd, valor: double;
begin
  Result:= TVendaProduto.Create;

  Result.Produto.Id:= FormHome.txtProduto.Tag;
  Result.ProdutoId:= FormHome.txtProduto.Tag;
  Result.Produto.Descricao:= FormHome.edtCupomProdutoNome.Text;
  valor:= StrToFloatDef(FormHome.edtCupomProdutoPreco.text, 0);
  qtd:= StrToFloatDef(FormHome.edtProdutoQuatidade.Text, 0);

  if valor > 0 then
    Result.Produto.Preco:= valor
  else
  begin
    FormHome.MostraMensage('Valor deve ser maior que 0');
    FreeAndNil(Result);
    exit(Result);
  end;


  if qtd > 0 then
    Result.Quantidade:= qtd
  else
  begin
    FormHome.MostraMensage('Quantidade deve ser maior que 0');
    FreeAndNil(Result);
  end;

end;


class function TCriadorCupomVM.New: TCriadorCupomVM;
begin
  if Assigned(instacia) then
    exit(instacia);

  instacia:= Self.CreatePrivate();
  result:= instacia;
end;

procedure TCriadorCupomVM.LimparCampos;
begin
  FormHome.edtCupomClienteNome.Text:= '';
  FormHome.EdtCupomClienteDoc.Text:= '';
  FormHome.edtCupomPagDesconto.Text:= '';
  FormHome.edtCupomPagAcrescimo.Text:= '';
  FormHome.lstProdutosCupom.Items.Clear;
  FormHome.lstProdutos.Items.Clear;
  FormHome.lstCupomProdutos.Clear;
  FormHome.cedCupomPagFormaPagamento.ItemIndex:= -1;
  FormHome.cedCupomPagOperadorCartao.ItemIndex:= -1;
  FormHome.txtCupomClienteDoc.Text:= '';
  FormHome.txtCupomClienteNome.Text:= '';
  FormHome.txtCupomFormaPag.Text:='';
  FormHome.txtCupomDesconto.Text:= '';
  FormHome.txtCupomAcrescimo.Text:= '';
  FormHome.txtCupomTotal.Text:= '';
end;

procedure TCriadorCupomVM.MostrarCupomCliente(cliente: TCliente);
begin
  if cliente.Nome = '' then
    FormHome.txtCupomClienteNome.Text:= 'DESCONHECIDO'
  else
    FormHome.txtCupomClienteNome.Text:= cliente.Nome;    
    
  if cliente.Documento = '' then
    FormHome.txtCupomClienteDoc.Text:= 'DESCONHECIDO'
  else
  begin
    if cliente.Documento.Length = 14 then
      FormHome.txtCupomClienteDoc.Text:= FormatMaskText('99.999.999/9999-99;0', cliente.Documento)
    else if cliente.Documento.Length = 11 then
      FormHome.txtCupomClienteDoc.Text:= FormatMaskText('999.999.999-99;0', cliente.Documento)
  end;

  
end;

procedure TCriadorCupomVM.MostrarCupomPagamento(venda: TVenda);
begin
  if Assigned(venda) then
  begin
    FormHome.txtCupomFormaPag.Text:= venda.FormaPagamento;
    FormHome.txtCupomDesconto.Text:= 'Desconto: '+ FormatFloat('R$ ###,###,##0.00', venda.Desconto);
    FormHome.txtCupomAcrescimo.Text:= 'Acr�scimo: '+ FormatFloat('R$ ###,###,##0.00', venda.Acrescimo);
    FormHome.txtCupomTotal.Text:= 'Total: '+ FormatFloat('R$ ###,###,##0.00', (venda.Valor + venda.Acrescimo.Value) - venda.Desconto.Value);
  end;
end;

procedure TCriadorCupomVM.MostrarCupomProdutos;
var
  produt: TVendaProduto;
  listItem: TListBoxItem;
  linha: TStringBuilder;
begin
  FormHome.lstCupomProdutos.BeginUpdate;
  FormHome.lstCupomProdutos.Clear;
  linha:= TStringBuilder.Create;
  try
    for produt in fListTemp do
    begin

      listItem:= TListBoxItem.Create(FormHome.lstCupomProdutos);
      listItem.Height:= 30;

      linha.Append(produt.Produto.Descricao.PadRight(32)).AppendLine();

      linha.Append(FormatFloat('x###,###,##0.00', produt.Quantidade).PadRight(15));
      linha.Append(FormatFloat('R$ ###,###,##0.00', (produt.Produto.Preco * produt.Quantidade)).PadRight(17));
      listItem.StyledSettings:= [];
      listItem.Font.Family:= 'Lucida Console';
      listItem.TextSettings.Font.Size := 10;
      listItem.Text := linha.ToString;

      FormHome.lstCupomProdutos.AddObject(listItem);
    end;
  finally
    linha.Free;
  end;

  FormHome.lstCupomProdutos.EndUpdate;
end;

procedure TCriadorCupomVM.Next;
begin
  case fase of
    fpIniciado: Iniciar;
    fpAddCliente: FaseCliente;
    fpProdutos: FaseProdutos;
    fpAddPagmento: FasePagamento;
    fpFinalizado: FaseFinalizar;
  end;
end;



procedure TCriadorCupomVM.SelecionarProdutoParaAdd(id: integer);
var
  repo: TProdutoRepository;
  produto: TProduto;
begin
  repo := TProdutoRepository.Create;
  try
    produto:= repo.Get(id);

    if Assigned(produto) then
    begin

      FormHome.tbcCupom.ActiveTab:= FormHome.tabDefProduto;
      FormHome.btnNegProduto.Text:= 'Cancelar';
      FormHome.btnPosProduto.Text:= 'Adicionar';
      FormHome.lBtnsDefProduto.Tag:= 0;

      FormHome.lBtnCupons.Visible:= false;
      FormHome.txtProduto.Tag:= produto.Id;

      FormHome.edtCupomProdutoNome.Text:= produto.Descricao;
      FormHome.edtCupomProdutoPreco.Text:= FloatToStr(produto.Preco);
      FormHome.txtProduto.Text:= 'Adicionando Produto';
      FormHome.edtProdutoQuatidade.Text:= '1';
    end;

  finally
    if Assigned(produto) then
      produto.Free;

    repo.Free;
  end;

end;

procedure TCriadorCupomVM.SelecionarProdutoParaEditar(indice: integer);
var
  vndProd: TVendaProduto;
begin
  vndProd:= fListTemp.Extract(fListTemp.Items[indice]);
  CriandoComponentesListaProdutosCupom();

  try
    if Assigned(vndProd) then
    begin

      FormHome.tbcCupom.ActiveTab:= FormHome.tabDefProduto;
      FormHome.btnNegProduto.Text:= 'Cancelar';
      FormHome.btnPosProduto.Text:= 'Editar';
      FormHome.lBtnsDefProduto.Tag:= 1;
      FormHome.btnExcluirProduto.Visible:= True;

      FormHome.lBtnCupons.Visible:= false;
      FormHome.txtProduto.Tag:= vndProd.ProdutoId;

      FormHome.edtCupomProdutoNome.Text:= vndProd.produto.Descricao;;
      FormHome.edtCupomProdutoPreco.text:= FloatToStr(vndProd.produto.Preco);
      FormHome.edtProdutoQuatidade.Text:= FloatToStr(vndProd.Quantidade);
      FormHome.txtProduto.Text:= 'Editando Produto';

    end;
  finally
    if Assigned(vndProd) then
      FreeAndNil(vndProd);
  end;

end;

procedure TCriadorCupomVM.Voltar;
begin
  case fase of
    fpAddCliente: VoltarCliente;
    fpProdutos: VoltarProdutos;
    fpAddPagmento: VoltarPagamento;
    fpFinalizado: VoltarFinalizar;
  end;
end;

procedure TCriadorCupomVM.VoltarCliente;
begin
  if fase = fpAddCliente then
  begin
    Cancelar;
  end;
end;

procedure TCriadorCupomVM.VoltarFinalizar;
begin
  if fase = fpFinalizado then
  begin
    FormHome.edtCupomPagDesconto.Text:= FormatFloat('###,###,##0.00', criadorCupom.Venda.Desconto);
    FormHome.edtCupomPagAcrescimo.Text:= FormatFloat('###,###,##0.00', criadorCupom.Venda.Acrescimo);
    FormHome.txtCupomValorTotal.Text:= FormatFloat('R$ ###,###,##0.00', criadorCupom.Venda.Valor);
    FormHome.txtCupomValorTotal.TagFloat:= criadorCupom.Venda.Valor;
    FormHome.cedCupomPagFormaPagamento.Index:= FormHome.cedCupomPagFormaPagamento.Items.IndexOf(criadorCupom.Venda.FormaPagamento);
    FormHome.cedCupomPagOperadorCartao.Index:= FormHome.cedCupomPagOperadorCartao.Items.IndexOf(criadorCupom.Venda.OperadoraCartao);

    FormHome.txtCupomFormaPag.Text:= '';
    FormHome.txtCupomDesconto.Text:= '';
    FormHome.txtCupomAcrescimo.Text:= '';
    FormHome.txtCupomTotal.Text:= '';
    FormHome.btnCupomNext.Text:= 'Pr�ximo';
    FormHome.MoveTab(FormHome.tabCupomPagamento);

    fase:= fpAddPagmento;
  end;
end;


procedure TCriadorCupomVM.VoltarPagamento;
begin
  if fase = fpAddPagmento then
  begin
    fListTemp:= criadorCupom.ProdutosVendidos;
    CriandoComponentesListaProdutosCupom();
    FormHome.lstCupomProdutos.BeginUpdate;
    FormHome.lstCupomProdutos.Clear;
    FormHome.lstCupomProdutos.EndUpdate;

    FormHome.MoveTab(FormHome.tabCupomProdutos);

    fase:= fpProdutos;
  end;
end;

procedure TCriadorCupomVM.VoltarProdutos;
begin
  if fase = fpProdutos then
  begin
    FormHome.edtCupomClienteNome.Text:= criadorCupom.Cliente.Nome;
    FormHome.EdtCupomClienteDoc.Text:= criadorCupom.Cliente.Documento;

    FormHome.txtCupomClienteNome.Text:= '';
    FormHome.txtCupomClienteDoc.Text:= '';

    FormHome.MoveTab(FormHome.tabCupomCliente);
    fase:= fpAddCliente;
  end;
end;

end.
