unit uVm.Categoria;

interface

uses uCategoria, uCategoriaRepository;


type
//classe de view model para categoria
  TCategoriaVM = class
  private
    //m�todo para limpar a tela de categoria
    procedure LimparDadosCategoria();
    //m�todo para setar os campos com base na categoria
    procedure SetarCampos(categoria: TCategoria);
    //m�todo para pegar uma categoria
    function GetCategoria(): TCategoria;

  public
    //m�todo para carregar categorias para produtos
    procedure CarregarCategoriasProdutos();
    //m�todo para carregar categorias
    procedure CarregarCategorias();
    //m�todo para iniciar a inser��o
    procedure InitInserir();
    //m�todo para inicar a edi��o
    procedure InitEdit();
    //m�todo para excluir um categoria
    procedure Excluir();
    //m�todo para salvar um categoria
    procedure Salvar();
    //m�todo para cancelar uma categoria
    procedure Cancelar(); 
  end;

implementation





{ TCategoriaVM }

uses Form.Principal, FMX.Dialogs, System.UITypes, System.SysUtils
  , uValidacoes, uAuRepository, System.Generics.Collections, FMX.ListBox,
  FMX.Objects, FMX.Types;

procedure TCategoriaVM.Cancelar;
begin
  FormHome.MoveTab(FormHome.TabCategoriaLista);
  CarregarCategorias();
end;

procedure TCategoriaVM.CarregarCategorias;
var
  repo: TCategoriaRepository;
begin
  repo := TCategoriaRepository.Create;
  try
    repo.SelectAll();
  finally
    repo.Free;
  end;
end;

procedure TCategoriaVM.CarregarCategoriasProdutos;
var
  lista: TObjectList<TCategoria>;
  it: TCategoria;
  listItem: TListBoxItem;
  txtDescricao: TText;
  repo: TCategoriaRepository;
begin
  repo:= TCategoriaRepository.Create();

  try
    lista:= repo.GetAll();
    FormHome.lstCategoriaProdutos.BeginUpdate;
    FormHome.lstCategoriaProdutos.Clear;
    for it in lista do
    begin
      listItem := TListBoxItem.Create(FormHome.lstCategoriaProdutos);
      listItem.Height := 40;

      txtDescricao := TText.Create(listItem);
      txtDescricao.Font.Family := 'Consolas';
      txtDescricao.TextSettings.HorzAlign := TTextAlign.Leading;
      txtDescricao.Font.Size := 16;
      txtDescricao.Height := 20;
      txtDescricao.Font.Style := [TFontStyle.fsBold];
      txtDescricao.Parent := FormHome.lstCategoriaProdutos;
      txtDescricao.Align := TAlignLayout.Client;
      txtDescricao.Text := it.Descricao;
      txtDescricao.HitTest := false;

      listItem.AddObject(txtDescricao);
      listItem.Tag := it.Id;
      listItem.StyledSettings:= [];
      listItem.TextSettings.FontColor:= TColor($ffffff);
      listItem.TextSettings.Font.Size := 1;
      listItem.Text := it.Descricao;

      FormHome.lstCategoriaProdutos.AddObject(listItem);
    end;
    FormHome.lstCategoriaProdutos.EndUpdate;
  finally
    repo.Free;
    lista.Free;
  end;

end;

procedure TCategoriaVM.Excluir;
var
  repo: TCategoriaRepository;
begin
  repo := TCategoriaRepository.Create;
  try
    if FormHome.lstCategoriasGrid.ItemIndex > -1 then
    if MessageDlg('Tem certeza que deseja excluir? ',TMsgDlgType.mtConfirmation,
    [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo],0) = mrYes then       
    begin
      repo.Delete(FormHome.txtCategoriaId.Tag);

      CarregarCategorias();
    end;
  finally
    repo.Free;
  end;
end;

function TCategoriaVM.GetCategoria: TCategoria;
var
  mensagem: TStringBuilder;
begin
  Result:= TCategoria.Create;
  if FormHome.txtCategoriaTipoForm.Tag = 1 then
    Result.Id:= FormHome.txtCategoriaId.Tag;

  Result.Descricao:= FormHome.edtCategoriaDescricao.Text.Trim;
  Result.Ncm:= FormHome.edtCategoriaNcm.Text.Trim;
  Result.Cest:= FormHome.edtCategoriaCest.Text.Trim;
  Result.Cfop:= FormHome.edtCategoriaCfop.Text.Trim;

  Result.Pis_BaseCalculo:= StrToFloatDef(FormHome.edtCategoriaPisBc.Text, 0);
  Result.Pis_CST:= StrToIntDef(FormHome.edtCategoriaPisCst.Text, -1);
  Result.Pis_Porcento:= StrToFloatDef(FormHome.edtCategoriaPisPorcentagem.Text, 0);
  Result.Pis_Valor:= StrToFloatDef(FormHome.edtCategoriaPisValor.Text, 0);
  Result.Pis_BaseCalculoPorProduto:= StrToFloatDef(FormHome.edtCategoriaPisBcp.Text, 0);
  Result.Pis_Aliquota:= StrToFloatDef(FormHome.edtCategoriaPisAliquota.Text, 0);

  Result.PisSt_BaseCalculo:= StrToFloatDef(FormHome.edtCategoriaPisstBc.Text, 0);
  Result.PisSt_Porcento:= StrToFloatDef(FormHome.edtCategoriaPisstPorcentagem.Text, 0);
  Result.PisSt_Valor:= StrToFloatDef(FormHome.edtCategoriaPisstValor.Text, 0);
  Result.PisSt_BaseCalculoPorProduto:= StrToFloatDef(FormHome.edtCategoriaPisstBcp.Text, 0);
  Result.PisSt_Aliquota:= StrToFloatDef(FormHome.edtCategoriaPisstAliquota.Text, 0);

  Result.Cofins_CST:= StrToIntDef(FormHome.edtCategoriaCofinsCst.Text, -1);
  Result.Cofins_BaseCalculo:= StrToFloatDef(FormHome.edtCategoriaCofinsBc.Text, 0);
  Result.Cofins_Porcento:= StrToFloatDef(FormHome.edtCategoriaCofinsPorcentagem.Text, 0);
  Result.Cofins_Valor:= StrToFloatDef(FormHome.edtCategoriaCofinsValor.Text, 0);
  Result.Cofins_BaseCalculoPorProduto:= StrToFloatDef(FormHome.edtCategoriaCofinsBcp.Text, 0);
  Result.Cofins_Aliquota:= StrToFloatDef(FormHome.edtCategoriaCofinsAliquota.Text, 0);

  Result.CofinsSt_BaseCalculo:= StrToFloatDef(FormHome.edtCategoriaCofinsstBc.Text, 0);
  Result.CofinsSt_Porcento:= StrToFloatDef(FormHome.edtCategoriaCofinsstPorcentagem.Text, 0);
  Result.CofinsSt_Valor:= StrToFloatDef(FormHome.edtCategoriaCofinsstValor.Text, 0);
  Result.CofinsSt_BaseCalculoPorProduto:= StrToFloatDef(FormHome.edtCategoriaCofinsstBcp.Text, 0);
  Result.CofinsSt_Aliquota:= StrToFloatDef(FormHome.edtCategoriaCofinsstAliquota.Text, 0);

  Result.Icms_Origem:= StrToIntDef(FormHome.edtCategoriaIcmsOrigem.Text, -1);
  Result.Icms_Cst:= StrToIntDef(FormHome.edtCategoriaIcmsCst.Text, -1);
  Result.Icms_Porcento:= StrToFloatDef(FormHome.edtCategoriaIcmsPorcentagem.Text, 0);
  Result.Icms_Valor:= StrToFloatDef(FormHome.edtCategoriaIcmsValor.Text, 0);

  Result.Issqn_Natureza:= StrToIntDef(FormHome.edtCategoriaIssqnNatureza.Text, 0);
  Result.Issqn_Deducao:= StrToFloatDef(FormHome.edtCategoriaIssqnDeducao.Text, 0);
  Result.Issqn_CodigoIbge:= FormHome.edtCategoriaIssqnCodigoIbge.Text.Trim;
  Result.Issqn_BaseCalculoProduto:= StrToFloatDef(FormHome.edtCategoriaIssqnBc.Text, 0);
  Result.Issqn_ItemDaListaServico:= FormHome.edtCategoriaIssqnItemServico.Text.Trim;
  Result.Issqn_CodigoTributarioIssqn:= FormHome.edtCategoriaIssqnCodigoTributario.Text.Trim;
  Result.Issqn_BaseCalculo:= StrToFloatDef(FormHome.edtCategoriaIssqnBc.Text, 0);

  Result.TaxaEstadual:= StrToFloatDef(FormHome.edtCategoriaTaxaEstadual.Text, 0);
  Result.TaxaFederal:= StrToFloatDef(FormHome.edtCategoriaTaxaFederal.Text, 0);

  mensagem:= TStringBuilder.Create();
  try

    if Result.Descricao = '' then
    begin
      FormHome.edtCategoriaDescricao.SetFocus();
      mensagem.Append('Campo "Descri��o" est� inv�lido').AppendLine;
    end;

    if (Result.Ncm = '') or not(TValidacaoes.VerificaNcm(Result.Ncm)) then
    begin
      FormHome.edtCategoriaNcm.SetFocus();
      mensagem.Append('Campo "NCM" est� inv�lido').AppendLine;
    end;

    if (Result.Cfop = '') or not(TValidacaoes.VerificaCfop(Result.Cfop)) then
    begin
      FormHome.edtCategoriaCfop.SetFocus();
      mensagem.Append('Campo "CFOP" est� inv�lido').AppendLine;
    end;

    if (Result.Pis_CST = -1) or not(TValidacaoes.VerificaPisCst(Result.Pis_CST)) then
    begin
      FormHome.edtCategoriaPisCst.SetFocus();
      mensagem.Append('Campo "PIS CST" est� inv�lido').AppendLine;
    end;

    if (Result.Cofins_CST = -1) or not(TValidacaoes.VerificaCofinsCst(Result.Cofins_CST)) then
    begin
      FormHome.edtCategoriaCofinsCst.SetFocus();
      mensagem.Append('Campo "COFINS CST" est� inv�lido').AppendLine;
    end;

    if (Result.Icms_Origem = -1) or not(TValidacaoes.VerificaIcmsOrigem(Result.Icms_Origem)) then
    begin
      FormHome.edtCategoriaIcmsOrigem.SetFocus();
      mensagem.Append('Campo "ICMS Origem" est� inv�lido').AppendLine;
    end;

    if (Result.Icms_Cst = -1) or not(TValidacaoes.VerificaIcmsCst(Result.Icms_Cst)) then
    begin
      FormHome.edtCategoriaIcmsCst.SetFocus();
      mensagem.Append('Campo "ICMS CST" est� inv�lido').AppendLine;
    end;

    if mensagem.Length > 0 then
    begin
      FreeAndNil(Result);

      ShowMessage(mensagem.ToString());
    end;
  finally
    mensagem.Free;
  end;

end;

procedure TCategoriaVM.InitEdit;
var
  repo: TCategoriaRepository;
  categoria: TCategoria;
begin
  repo := TCategoriaRepository.Create;           
  try
    categoria:= repo.InitEdit(FormHome.txtCategoriaId.Tag);
    if Assigned(categoria) then
    begin
      LimparDadosCategoria();
      SetarCampos(categoria);

      FormHome.txtCategoriaTipoForm.Tag:= 1;
      FormHome.MoveTab(FormHome.tabCategoria);
      FormHome.MoveTab(FormHome.TabCategoriaCad);
    end;
  finally
    if Assigned(categoria) then
      categoria.Free;
      
    repo.Free;
  end;                
end;

procedure TCategoriaVM.InitInserir;
var
  repo: TCategoriaRepository;
begin
  repo := TCategoriaRepository.Create;           
  try
    FormHome.txtCategoriaTipoForm.Tag:= 0;
    LimparDadosCategoria();        

    FormHome.txtCategoriaTipoForm.Tag:= 0;
    FormHome.MoveTab(FormHome.tabCategoria);
    FormHome.MoveTab(FormHome.TabCategoriaCad);          
  finally
    repo.Free;
  end;
end;

procedure TCategoriaVM.LimparDadosCategoria;
begin

  FormHome.edtCategoriaDescricao.Text:= '';
  FormHome.edtCategoriaNcm.Text:= '';
  FormHome.edtCategoriaCest.Text:= '';
  FormHome.edtCategoriaCfop.Text:= '';

  FormHome.edtCategoriaPisBc.Text:= '';
  FormHome.edtCategoriaPisCst.Text:= '49';
  FormHome.edtCategoriaPisPorcentagem.Text:= '';
  FormHome.edtCategoriaPisValor.Text:= '';
  FormHome.edtCategoriaPisBcp.Text:= '';
  FormHome.edtCategoriaPisAliquota.Text:= '';

  FormHome.edtCategoriaPisstBc.Text:= '';
  FormHome.edtCategoriaPisstPorcentagem.Text:= '';
  FormHome.edtCategoriaPisstValor.Text:= '';
  FormHome.edtCategoriaPisstBcp.Text:= '';
  FormHome.edtCategoriaPisstAliquota.Text:= '';

  FormHome.edtCategoriaCofinsCst.Text:= '49';
  FormHome.edtCategoriaCofinsBc.Text:= '';
  FormHome.edtCategoriaCofinsPorcentagem.Text:= '';
  FormHome.edtCategoriaCofinsValor.Text:= '';
  FormHome.edtCategoriaCofinsBcp.Text:= '';
  FormHome.edtCategoriaCofinsAliquota.Text:= '';

  FormHome.edtCategoriaCofinsstBc.Text:= '';
  FormHome.edtCategoriaCofinsstPorcentagem.Text:= '';
  FormHome.edtCategoriaCofinsstValor.Text:= '';
  FormHome.edtCategoriaCofinsstBcp.Text:= '';
  FormHome.edtCategoriaCofinsstAliquota.Text:= '';

  FormHome.edtCategoriaIcmsOrigem.Text:= '0';
  FormHome.edtCategoriaIcmsCst.Text:= '102';
  FormHome.edtCategoriaIcmsPorcentagem.Text:= '';
  FormHome.edtCategoriaIcmsValor.Text:= '';

  FormHome.edtCategoriaIssqnNatureza.Text:= '';
  FormHome.edtCategoriaIssqnDeducao.Text:= '';
  FormHome.edtCategoriaIssqnCodigoIbge.Text := '';
  FormHome.edtCategoriaIssqnBc.Text:= '';
  FormHome.edtCategoriaIssqnItemServico.Text := '';
  FormHome.edtCategoriaIssqnCodigoTributario.Text:= '';

  FormHome.edtCategoriaTaxaEstadual.Text:= '';
  FormHome.edtCategoriaTaxaFederal.Text:=  '';
end;

procedure TCategoriaVM.Salvar;
var
  tipoForm: integer;
  repo: TCategoriaRepository;
  categoria: TCategoria;
begin
  tipoForm:= FormHome.txtCategoriaTipoForm.Tag;
  repo:= TCategoriaRepository.Create();
  categoria:= GetCategoria();
  try
    if Assigned(categoria) then
    begin

        if tipoForm = 0 then
        begin
          try
            repo.Insert(categoria);
            FormHome.MoveTab(FormHome.TabCategoriaLista);
          except on e: Exception do
            begin
              if e.Message.Contains('UNIQUE') then
                FormHome.MostraMensage(categoria.Descricao + ' j� est� cadastrado')
              else
                FormHome.MostraMensage('Erro ao cadastrar a categoria')
            end;
          end;

        end
        else if tipoForm = 1 then
        begin
          try
            repo.Edit(categoria);
            FormHome.MoveTab(FormHome.TabCategoriaLista);
          except on e: Exception do
            begin
              if e.Message.Contains('UNIQUE') then
                FormHome.MostraMensage(categoria.Descricao + ' j� est� cadastrado')
              else
                FormHome.MostraMensage('Erro ao cadastrar a categoria')
            end;
          end;
        end;
        CarregarCategorias();
    end
    else
      FormHome.MostraMensage('H� dados inv�lidos para o cadastro');
  finally
    repo.Free;
  end;
end;

procedure TCategoriaVM.SetarCampos(categoria: TCategoria);
begin
  if Assigned(categoria) then
  begin
    FormHome.txtCategoriaId.Tag:= categoria.id.GetValueOrDefault;
    FormHome.edtCategoriaDescricao.Text:= categoria.Descricao;
    FormHome.edtCategoriaNcm.Text:= categoria.Ncm;
    FormHome.edtCategoriaCest.Text:= categoria.Cest;
    FormHome.edtCategoriaCfop.Text:= categoria.Cfop;

    FormHome.edtCategoriaPisBc.Text:= FloatToStr(categoria.Pis_BaseCalculo);
    FormHome.edtCategoriaPisCst.Text:= IntToStr(categoria.Pis_CST);
    FormHome.edtCategoriaPisPorcentagem.Text:= FloatToStr(categoria.Pis_Porcento);
    FormHome.edtCategoriaPisValor.Text:= FloatToStr(categoria.Pis_Valor);
    FormHome.edtCategoriaPisBcp.Text:= FloatToStr(categoria.Pis_BaseCalculoPorProduto);
    FormHome.edtCategoriaPisAliquota.Text:= FloatToStr(categoria.Pis_Aliquota);

    FormHome.edtCategoriaPisstBc.Text:= FloatToStr(categoria.PisSt_BaseCalculo);
    FormHome.edtCategoriaPisstPorcentagem.Text:= FloatToStr(categoria.PisSt_Porcento);
    FormHome.edtCategoriaPisstValor.Text:= FloatToStr(categoria.PisSt_Valor);
    FormHome.edtCategoriaPisstBcp.Text:= FloatToStr(categoria.PisSt_BaseCalculoPorProduto);
    FormHome.edtCategoriaPisstAliquota.Text:= FloatToStr(categoria.PisSt_Aliquota);

    FormHome.edtCategoriaCofinsCst.Text:= IntToStr(categoria.Cofins_CST);
    FormHome.edtCategoriaCofinsBc.Text:= FloatToStr(categoria.Cofins_BaseCalculo);
    FormHome.edtCategoriaCofinsPorcentagem.Text:= FloatToStr(categoria.Cofins_Porcento);
    FormHome.edtCategoriaCofinsValor.Text:= FloatToStr(categoria.Cofins_Valor);
    FormHome.edtCategoriaCofinsBcp.Text:= FloatToStr(categoria.Cofins_BaseCalculoPorProduto);
    FormHome.edtCategoriaCofinsAliquota.Text:= FloatToStr(categoria.Cofins_Aliquota);

    FormHome.edtCategoriaCofinsstBc.Text:= FloatToStr(categoria.CofinsSt_BaseCalculo);
    FormHome.edtCategoriaCofinsstPorcentagem.Text:= FloatToStr(categoria.CofinsSt_Porcento);
    FormHome.edtCategoriaCofinsstValor.Text:= FloatToStr(categoria.CofinsSt_Valor);
    FormHome.edtCategoriaCofinsstBcp.Text:= FloatToStr(categoria.CofinsSt_BaseCalculoPorProduto);
    FormHome.edtCategoriaCofinsstAliquota.Text:= FloatToStr(categoria.CofinsSt_Aliquota);

    FormHome.edtCategoriaIcmsOrigem.Text:= IntToStr(categoria.Icms_Origem);
    FormHome.edtCategoriaIcmsCst.Text:= IntToStr(categoria.Icms_Cst);
    FormHome.edtCategoriaIcmsPorcentagem.Text:= FloatToStr(categoria.Icms_Porcento);
    FormHome.edtCategoriaIcmsValor.Text:= FloatToStr(categoria.Icms_Valor);

    FormHome.edtCategoriaIssqnNatureza.Text:= IntToStr(categoria.Issqn_Natureza);
    FormHome.edtCategoriaIssqnDeducao.Text:= FloatToStr(categoria.Issqn_Deducao);
    FormHome.edtCategoriaIssqnCodigoIbge.Text := categoria.Issqn_CodigoIbge;
    FormHome.edtCategoriaIssqnBc.Text:= FloatToStr(categoria.Issqn_BaseCalculo);
    FormHome.edtCategoriaIssqnItemServico.Text := categoria.Issqn_ItemDaListaServico;
    FormHome.edtCategoriaIssqnCodigoTributario.Text:= categoria.Issqn_CodigoTributarioIssqn;

    FormHome.edtCategoriaTaxaEstadual.Text:= FloatToStr(categoria.TaxaEstadual);
    FormHome.edtCategoriaTaxaFederal.Text:=  FloatToStr(categoria.TaxaFederal);
  end;
end;




end.
