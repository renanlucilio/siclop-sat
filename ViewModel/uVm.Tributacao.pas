unit uVm.Tributacao;

interface

uses uTributacao, uTributacaoRepository;

type
  TTributacaoVM = class
  private
    procedure LimparDados();
    procedure SetarCampos(model: TTributacao);
    function GetModel(): TTributacao;
  public
    procedure Carregar();
    procedure InitInserir();
    procedure InitEdit();
    procedure Excluir();
    procedure Salvar();
    procedure Cancelar();
  end;
implementation

uses uPis, uPisSt, uCofins, uCofinsSt, uIssqn, uIcms, uTaxasFiscais,
  Form.Principal, FMX.Dialogs, System.UITypes, System.SysUtils;

{ TTributacaoVM }

procedure TTributacaoVM.Cancelar;
begin
  FormHome.MoveTab(FormHome.tabProdutoLista);
end;

procedure TTributacaoVM.Carregar;
var
  repo: TTributacaoRepository;
begin
  repo := TTributacaoRepository.Create;
  try
    repo.SelectAll();
  finally
    repo.Free;
  end;
end;

procedure TTributacaoVM.Excluir;
var
  repo: TTributacaoRepository;
begin
  repo := TTributacaoRepository.Create;
  try
    if MessageDlg('Tem certeza que deseja excluir? ',TMsgDlgType.mtConfirmation,
    [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo],0) = mrYes then
    begin
      repo.Delete(FormHome.txtTributacaoId.Tag);

      Carregar();
    end;
  finally
    repo.Free;
  end;
end;

function TTributacaoVM.GetModel: TTributacao;
begin
  Result:= TTributacao.Create;
  Result.Pis:= TPis.Create;
  Result.PisSt:= TPisSt.Create;
  Result.Cofins:= TCofins.Create;
  Result.CofinsSt:= TCofinsSt.Create;
  Result.Issqn:= TIssqn.Create;
  Result.Icms:= TIcms.Create;
  Result.TaxasFiscais:= TTaxasFiscais.Create;

  if FormHome.txtTributacaoTipoForm.Tag = 1 then
    Result.Id:= FormHome.txtTributacaoId.Tag;
  Result.Descricao:= FormHome.edtTributacaoDescricao.Text.Trim;
  Result.Ncm:= FormHome.edtProdutoNCM.Text.Trim;
  Result.Cest:= FormHome.edtProdutoCEST.Text.Trim;
  Result.Cfop:= FormHome.edtProdutoCFOP.Text.Trim;

  if FormHome.txtTributacaoTipoForm.Tag = 1 then
  begin
    Result.Pis.Id:= FormHome.txtPisId.Tag;
    Result.PisId:= FormHome.txtPisId.Tag;
  end;
  Result.Pis.BaseCalculo:= StrToCurrDef(FormHome.edtPisBc.Text, 0);
  Result.Pis.Cst:= StrToIntDef(FormHome.edtPisCst.Text, 0);
  Result.Pis.Porcento:= StrToFloatDef(FormHome.edtPisPorcentagem.Text, 0);
  Result.Pis.Valor:= StrToFloatDef(FormHome.edtPisValor.Text, 0);
  Result.Pis.BaseCalculoPorProduto:= StrToFloatDef(FormHome.edtPisBcp.Text, 0);
  Result.Pis.Aliquota:= StrToFloatDef(FormHome.edtPisAliquota.Text, 0);

  if FormHome.txtTributacaoTipoForm.Tag = 1 then
  begin
    Result.PisSt.Id:= FormHome.txtPisstId.Tag;
    Result.PisStId:= FormHome.txtPisstId.Tag;
  end;
  Result.PisSt.BaseCalculo:= StrToCurrDef(FormHome.edtPisstBc.Text, 0);
  Result.PisSt.Porcento:= StrToFloatDef(FormHome.edtPisstPorcentagem.Text, 0);
  Result.PisSt.Valor:= StrToFloatDef(FormHome.edtPisstValor.Text, 0);
  Result.PisSt.BaseCalculoPorProduto:= StrToFloatDef(FormHome.edtPisstPorcentagem.Text, 0);
  Result.PisSt.Aliquota:= StrToFloatDef(FormHome.edtPisstAliquota.Text, 0);

  if FormHome.txtTributacaoTipoForm.Tag = 1 then
  begin
    Result.Cofins.Id:= FormHome.txtCofinsId.Tag;
    Result.CofinsId:= FormHome.txtCofinsId.Tag;
  end;
  Result.Cofins.BaseCalculo:= StrToCurrDef(FormHome.edtCofinsBc.Text, 0);
  Result.Cofins.Cst:= StrToIntDef(FormHome.edtCofinsCST.Text, 0);
  Result.Cofins.Porcento:= StrToFloatDef(FormHome.edtCofinsPorcentagem.Text, 0);
  Result.Cofins.Valor:= StrToFloatDef(FormHome.edtCofinsValor.Text, 0);
  Result.Cofins.BaseCalculoPorProduto:= StrToFloatDef(FormHome.edtCofinsBcp.Text, 0);
  Result.Cofins.Aliquota:= StrToFloatDef(FormHome.edtCofinsAliquota.Text, 0);

  if FormHome.txtTributacaoTipoForm.Tag = 1 then
  begin
    Result.CofinsSt.Id:= FormHome.txtCofinsstId.Tag;
    Result.CofinsStId:= FormHome.txtCofinsstId.Tag;
  end;
  Result.CofinsSt.BaseCalculo:= StrToCurrDef(FormHome.edtCofinsstBc.Text, 0);
  Result.CofinsSt.Porcento:= StrToFloatDef(FormHome.edtCofinsstPorcentagem.Text, 0);
  Result.CofinsSt.Valor:= StrToFloatDef(FormHome.edtCofinsstValor.Text, 0);
  Result.CofinsSt.BaseCalculoPorProduto:= StrToFloatDef(FormHome.edtCofinsstBcp.Text, 0);
  Result.CofinsSt.Aliquota:= StrToFloatDef(FormHome.edtCofinsstAliquota.Text, 0);

  if FormHome.txtTributacaoTipoForm.Tag = 1 then
  begin
    Result.Icms.Id:= FormHome.txtIcmsID.Tag;
    Result.IcmsId:= FormHome.txtIcmsID.Tag;
  end;
  Result.Icms.Origem:= StrToIntDef(FormHome.edtICMSOrigem.Text, 0);
  Result.Icms.Cst:= StrToIntDef(FormHome.edtICMSCST.Text, 0);
  Result.Icms.Porcento:= StrToFloatDef(FormHome.edtICMSPorcentagem.Text, 0);
  Result.Icms.Valor:= StrToFloatDef(FormHome.edtICMSValor.Text, 0);

  if FormHome.txtTributacaoTipoForm.Tag = 1 then
  begin
    Result.Issqn.Id:= FormHome.txtIssqnId.Tag;
    Result.IssqnId:= FormHome.txtIssqnId.Tag;
  end;
  Result.Issqn.BaseCalculo:= StrToFloatDef(FormHome.edtIssqnBC.Text, 0);
  Result.Issqn.Deducao:= StrToFloatDef(FormHome.edtIssqnDeducao.Text, 0);
  Result.Issqn.CodigoIbge:= FormHome.edtIssqnCodigoIbge.Text.Trim;
  Result.Issqn.BaseCalculoProduto:= StrToFloatDef(FormHome.edtIssqnBcp.Text, 0);
  Result.Issqn.ItemDaListaServico:= FormHome.edtIssqnItemServico.Text.Trim;
  Result.Issqn.CodigoTributarioIssqn:= FormHome.edtIssqnCodigoTributario.Text.Trim;
  Result.Issqn.Natureza:= StrToIntDef(FormHome.edtISSQNNatureza.Text, 0);

  if FormHome.txtTributacaoTipoForm.Tag = 1 then
  begin
    Result.TaxasFiscais.Id:= FormHome.txtTaxasId.Tag;
    Result.TaxasId:= FormHome.txtTaxasId.Tag;
  end;
  Result.TaxasFiscais.TaxaEstadual:= StrToFloatDef(FormHome.edtTaxaTaxaEstadual.Text, 0);
  Result.TaxasFiscais.TaxaFederal:= StrToFloatDef(FormHome.edtTaxaTaxaFederal.Text, 0);
end;

procedure TTributacaoVM.InitEdit;
var
  repo: TTributacaoRepository;
  model: TTributacao;
begin
  repo := TTributacaoRepository.Create;
  try
    model:= repo.InitEdit(FormHome.txtTributacaoId.Tag);

    LimparDados();
    SetarCampos(model);

    FormHome.txtTributacaoTipoForm.Tag:= 1;
    FormHome.MoveTab(FormHome.tabTributacao);
    FormHome.MoveTab(FormHome.tabTributacaoCad);
  finally
    if Assigned(model) then
      model.Free;

    repo.Free;
  end;
end;

procedure TTributacaoVM.InitInserir;
var
  repo: TTributacaoRepository;
begin
  repo := TTributacaoRepository.Create;
  try
    LimparDados();

    FormHome.txtTributacaoTipoForm.Tag:= 0;
    FormHome.MoveTab(FormHome.tabTributacao);
    FormHome.MoveTab(FormHome.tabTributacaoCad);
  finally
    repo.Free;
  end;
end;

procedure TTributacaoVM.LimparDados;
begin
  FormHome.edtTributacaoDescricao.Text:= '';
  FormHome.edtProdutoNCM.Text:= '';
  FormHome.edtProdutoCEST.Text:= '';
  FormHome.edtProdutoCFOP.Text:= '';

  FormHome.edtPisBc.Text:= '';
  FormHome.edtPisCst.Text:= '49';
  FormHome.edtPisPorcentagem.Text:= '';
  FormHome.edtPisValor.Text:= '';
  FormHome.edtPisBcp.Text:= '';
  FormHome.edtPisAliquota.Text:= '';

  FormHome.edtPisstBc.Text:= '';
  FormHome.edtPisstPorcentagem.Text:= '';
  FormHome.edtPisstValor.Text:= '';
  FormHome.edtPisstPorcentagem.Text:= '';
  FormHome.edtPisstAliquota.Text:= '';

  FormHome.edtCofinsBc.Text:= '';
  FormHome.edtCofinsCST.Text:= '49';
  FormHome.edtCofinsPorcentagem.Text:= '';
  FormHome.edtCofinsValor.Text:= '';
  FormHome.edtCofinsBcp.Text:= '';
  FormHome.edtCofinsAliquota.Text:= '';

  FormHome.edtCofinsstBc.Text:= '';
  FormHome.edtCofinsstPorcentagem.Text:= '';
  FormHome.edtCofinsstValor.Text:= '';
  FormHome.edtCofinsstBcp.Text:= '';
  FormHome.edtCofinsstAliquota.Text:= '';

  FormHome.edtICMSOrigem.Text:= '0';
  FormHome.edtICMSCST.Text:= '102';
  FormHome.edtICMSPorcentagem.Text:= '';
  FormHome.edtICMSValor.Text:= '';

  FormHome.edtIssqnBC.Text:= '';
  FormHome.edtIssqnDeducao.Text:= '';
  FormHome.edtIssqnItemServico.Text:= '';
  FormHome.edtIssqnBcp.Text:= '';
  FormHome.edtIssqnCodigoIbge.Text:= '';
  FormHome.edtIssqnCodigoTributario.Text:= '';
  FormHome.edtISSQNNatureza.Text:= '';

  FormHome.edtTaxaTaxaEstadual.Text:= '';
  FormHome.edtTaxaTaxaFederal.Text:= '';
end;

procedure TTributacaoVM.Salvar;
var
  tipoForm: integer;
  repo: TTributacaoRepository;
  model: TTributacao;
begin
  tipoForm:= FormHome.txtTributacaoTipoForm.Tag;
  repo:= TTributacaoRepository.Create();
  model:= GetModel();
  try
    if tipoForm = 0 then
    begin
      try
        repo.Insert(model);
        FormHome.MoveTab(FormHome.tabTributacaoLista);
      except
        FormHome.MostraMensage('Erro ao cadastrar o tributação');
      end;

    end
    else if tipoForm = 1 then
    begin
      try
        repo.Edit(model);
        FormHome.MoveTab(FormHome.tabTributacaoLista);
      except
        FormHome.MostraMensage('Erro ao editar o tributação');
      end;
    end;
    Carregar();
  finally
    repo.Free;
  end;
end;

procedure TTributacaoVM.SetarCampos(model: TTributacao);
begin

  FormHome.edtTributacaoDescricao.Text:= model.Descricao;
  FormHome.edtProdutoNCM.Text:= model.Ncm;
  FormHome.edtProdutoCEST.Text:= model.Cest;
  FormHome.edtProdutoCFOP.Text:= model.Cfop;

  FormHome.txtPisId.Tag:= model.Pis.Id.Value;
  FormHome.edtPisBc.Text:= FloatToStr(model.Pis.BaseCalculo);
  FormHome.edtPisCst.Text:= model.Pis.Cst.ToString();
  FormHome.edtPisPorcentagem.Text:= FloatToStr(model.Pis.Porcento);
  FormHome.edtPisValor.Text:= FloatToStr(model.Pis.Valor);
  FormHome.edtPisBcp.Text:= FloatToStr(model.Pis.BaseCalculoPorProduto);
  FormHome.edtPisAliquota.Text:= FloatToStr(model.Pis.Aliquota);

  FormHome.txtPisstId.Tag:= model.PisSt.Id.Value;
  FormHome.edtPisstBc.Text:= FloatToStr(model.PisSt.BaseCalculo);
  FormHome.edtPisstPorcentagem.Text:= FloatToStr(model.PisSt.Porcento);
  FormHome.edtPisstValor.Text:= FloatToStr(model.PisSt.Valor);
  FormHome.edtPisstPorcentagem.Text:= FloatToStr(model.PisSt.Porcento);
  FormHome.edtPisstAliquota.Text:= FloatToStr(model.PisSt.Aliquota);

  FormHome.txtCofinsId.Tag:= model.Cofins.Id.Value;
  FormHome.edtCofinsBc.Text:= FloatToStr(model.Cofins.BaseCalculo);
  FormHome.edtCofinsCST.Text:= model.Cofins.CST.ToString();
  FormHome.edtCofinsPorcentagem.Text:= FloatToStr(model.Cofins.Porcento);
  FormHome.edtCofinsValor.Text:= FloatToStr(model.Cofins.Valor);
  FormHome.edtCofinsBcp.Text:= FloatToStr(model.Cofins.BaseCalculoPorProduto);
  FormHome.edtCofinsAliquota.Text:= FloatToStr(model.Cofins.Aliquota);

  FormHome.txtCofinsstId.Tag:= model.CofinsSt.Id.Value;
  FormHome.edtCofinsstBc.Text:= FloatToStr(model.CofinsSt.BaseCalculo);
  FormHome.edtCofinsstPorcentagem.Text:= FloatToStr(model.CofinsSt.Porcento);
  FormHome.edtCofinsstValor.Text:= FloatToStr(model.CofinsSt.Valor);
  FormHome.edtCofinsstBcp.Text:= FloatToStr(model.CofinsSt.BaseCalculoPorProduto);
  FormHome.edtCofinsstAliquota.Text:= FloatToStr(model.CofinsSt.Aliquota);

  FormHome.txtIcmsID.Tag:= model.Icms.Id.Value;
  FormHome.edtICMSOrigem.Text:= model.Icms.Origem.ToString;
  FormHome.edtICMSCST.Text:= model.Icms.Cst.ToString;
  FormHome.edtICMSPorcentagem.Text:= FloatToStr(model.Icms.Porcento);
  FormHome.edtICMSValor.Text:= FloatToStr(model.Icms.Valor);

  FormHome.txtIssqnId.Tag:= model.Issqn.Id.Value;
  FormHome.edtIssqnBC.Text:= FloatToStr(model.Issqn.BaseCalculo);
  FormHome.edtIssqnDeducao.Text:= FloatToStr(model.Issqn.Deducao);
  FormHome.edtIssqnBcp.Text:= FloatToStr(model.Issqn.BaseCalculoProduto);
  FormHome.edtIssqnCodigoIbge.Text:= model.Issqn.CodigoIbge;
  FormHome.edtIssqnCodigoTributario.Text:= model.Issqn.CodigoTributarioIssqn;
  FormHome.edtIssqnItemServico.Text:= model.Issqn.ItemDaListaServico;
  FormHome.edtISSQNNatureza.Text:= model.Issqn.Natureza.ToString();

  FormHome.txtTaxasId.Tag:= model.TaxasFiscais.Id;
  FormHome.edtTaxaTaxaEstadual.Text:= FloatToStr(model.TaxasFiscais.TaxaEstadual);
  FormHome.edtTaxaTaxaFederal.Text:= FloatToStr(model.TaxasFiscais.TaxaFederal);
end;

end.
