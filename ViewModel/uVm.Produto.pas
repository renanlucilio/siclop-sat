unit uVm.Produto;

interface

uses uProduto, uProdutoRepository, System.SysUtils, System.Generics.Collections, uCategoria;

type
//classe de view model para produto
  TProdutoVM = class
  private
  //m�todo para limpar a tela
    procedure LimparDados();
    //m�todo para definir os dados na tela com base no obj
    procedure SetarCampos(model: TProduto);
    //m�todo para pegar um modelo da classe
    function GetModel(): TProduto;
    //m�todo para setar as tributa��es com base na categoria
    procedure SetarDadosDaCategoria(categoria: TCategoria);
    //m�todo para carregar as categorias
    procedure CarregarCategorias();
  public
  //m�todo para verificar se tem categorias cadastradas
    function VerificaSeTemCategorias: Boolean;
    //m�todo para carregar as categorias
    procedure Carregar();
    //m�todo para inicar a inser��o
    procedure InitInserir(categoriaId: integer);
    //m�todo para iniciar a edi��o
    procedure InitEdit();
    //m�todo para exluir um produto
    procedure Excluir();
    //m�todo para salvar um produto
    procedure Salvar();
    //m�todo para cancelar um produto
    procedure Cancelar();
  end;


implementation


uses Form.Principal, FMX.Dialogs, System.UITypes, uCategoriaRepository,
  uValidacoes;


{ TProdutoVM }

procedure TProdutoVM.Cancelar;
begin
  FormHome.MoveTab(FormHome.tabProdutoLista);
  Carregar();
end;

procedure TProdutoVM.Carregar;
var
  repo: TProdutoRepository;
begin
  repo := TProdutoRepository.Create;
  try
    repo.SelectAll();
  finally
    repo.Free;
  end;
end;

procedure TProdutoVM.CarregarCategorias;
begin

end;

procedure TProdutoVM.Excluir;
var
  repo: TProdutoRepository;
begin
  repo := TProdutoRepository.Create;
  try
    if FormHome.lstProdutosGrid.ItemIndex > -1 then
    if MessageDlg('Tem certeza que deseja excluir? ',TMsgDlgType.mtConfirmation,
    [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo],0) = mrYes then
    begin
        repo.Delete(FormHome.txtProdutoId.Tag);

      Carregar();
    end;
  finally
    repo.Free;
  end;
end;

function TProdutoVM.GetModel: TProduto;
var
  mensagem: TStringBuilder;
begin
  Result:= TProduto.Create;
  if FormHome.txtProdutoTipoForm.Tag = 1 then
    Result.Id:= FormHome.txtProdutoId.Tag;

  Result.CategoriaId:= FormHome.txtProdutoCategoria.Tag;
  Result.Descricao:= FormHome.edtProdutoDescricao.Text.Trim;
  Result.Preco:= StrToFloatDef(FormHome.edtProdutoValor.Text, -1);

  Result.UnidadeMedida:= FormHome.cedProdutoSiglaUnidade.Text.Trim;
  Result.Ncm:= FormHome.edtProdutoNcm.Text.Trim;
  Result.Cest:= FormHome.edtProdutoCest.Text.Trim;
  Result.Cfop:= FormHome.edtProdutoCfop.Text.Trim;

  Result.Pis_BaseCalculo:= StrToFloatDef(FormHome.edtProdutoPisBc.Text, 0);
  Result.Pis_CST:= StrToIntDef(FormHome.edtProdutoPisCst.Text, -1);
  Result.Pis_Porcento:= StrToFloatDef(FormHome.edtProdutoPisPorcentagem.Text, 0);
  Result.Pis_Valor:= StrToFloatDef(FormHome.edtProdutoPisValor.Text, 0);
  Result.Pis_BaseCalculoPorProduto:= StrToFloatDef(FormHome.edtProdutoPisBcp.Text, 0);
  Result.Pis_Aliquota:= StrToFloatDef(FormHome.edtProdutoPisAliquota.Text, 0);

  Result.PisSt_BaseCalculo:= StrToFloatDef(FormHome.edtProdutoPisstBc.Text, 0);
  Result.PisSt_Porcento:= StrToFloatDef(FormHome.edtProdutoPisstPorcentagem.Text, 0);
  Result.PisSt_Valor:= StrToFloatDef(FormHome.edtProdutoPisstValor.Text, 0);
  Result.PisSt_BaseCalculoPorProduto:= StrToFloatDef(FormHome.edtProdutoPisstBcp.Text, 0);
  Result.PisSt_Aliquota:= StrToFloatDef(FormHome.edtProdutoPisstAliquota.Text, 0);

  Result.Cofins_CST:= StrToIntDef(FormHome.edtProdutoCofinsCst.Text, -1);
  Result.Cofins_BaseCalculo:= StrToFloatDef(FormHome.edtProdutoCofinsBc.Text, 0);
  Result.Cofins_Porcento:= StrToFloatDef(FormHome.edtProdutoCofinsPorcentagem.Text, 0);
  Result.Cofins_Valor:= StrToFloatDef(FormHome.edtProdutoCofinsValor.Text, 0);
  Result.Cofins_BaseCalculoPorProduto:= StrToFloatDef(FormHome.edtProdutoCofinsBcp.Text, 0);
  Result.Cofins_Aliquota:= StrToFloatDef(FormHome.edtProdutoCofinsAliquota.Text, 0);

  Result.CofinsSt_BaseCalculo:= StrToFloatDef(FormHome.edtProdutoCofinsstBc.Text, 0);
  Result.CofinsSt_Porcento:= StrToFloatDef(FormHome.edtProdutoCofinsstPorcentagem.Text, 0);
  Result.CofinsSt_Valor:= StrToFloatDef(FormHome.edtProdutoCofinsstValor.Text, 0);
  Result.CofinsSt_BaseCalculoPorProduto:= StrToFloatDef(FormHome.edtProdutoCofinsstBcp.Text, 0);
  Result.CofinsSt_Aliquota:= StrToFloatDef(FormHome.edtProdutoCofinsstAliquota.Text, 0);

  Result.Icms_Origem:= StrToIntDef(FormHome.edtProdutoIcmsOrigem.Text, -1);
  Result.Icms_Cst:= StrToIntDef(FormHome.edtProdutoIcmsCst.Text, -1);
  Result.Icms_Porcento:= StrToFloatDef(FormHome.edtProdutoIcmsPorcentagem.Text, 0);
  Result.Icms_Valor:= StrToFloatDef(FormHome.edtProdutoIcmsValor.Text, 0);

  Result.Issqn_Natureza:= StrToIntDef(FormHome.edtProdutoIssqnNatureza.Text, 0);
  Result.Issqn_Deducao:= StrToFloatDef(FormHome.edtProdutoIssqnDeducao.Text, 0);
  Result.Issqn_CodigoIbge:= FormHome.edtProdutoIssqnCodIbge.Text.Trim;
  Result.Issqn_BaseCalculoProduto:= StrToFloatDef(FormHome.edtProdutoIssqnBc.Text, 0);
  Result.Issqn_ItemDaListaServico:= FormHome.edtProdutoIssqnItemServico.Text.Trim;
  Result.Issqn_CodigoTributarioIssqn:= FormHome.edtProdutoIssqnCodTributario.Text.Trim;
  Result.Issqn_BaseCalculo:= StrToFloatDef(FormHome.edtProdutoIssqnBc.Text, 0);

  Result.TaxaEstadual:= StrToFloatDef(FormHome.edtProdutoTaxaEstadual.Text, 0);
  Result.TaxaFederal:= StrToFloatDef(FormHome.edtProdutoTaxaFederal.Text, 0);

  mensagem:= TStringBuilder.Create();
  try

    if Result.Descricao = '' then
    begin
      FormHome.edtProdutoDescricao.SetFocus();
      mensagem.Append('Campo "Descri��o" est� inv�lido').AppendLine;
    end;

    if Result.Preco = -1 then
    begin
      FormHome.edtProdutoDescricao.SetFocus();
      mensagem.Append('Campo "Valor" est� inv�lido').AppendLine;
    end;

    if Result.UnidadeMedida = '' then
    begin
      FormHome.cedProdutoSiglaUnidade.SetFocus();
      mensagem.Append('Campo "Sigla da unidade" est� inv�lido').AppendLine;
    end;

    if (Result.Ncm = '') or not(TValidacaoes.VerificaNcm(Result.Ncm)) then
    begin
      FormHome.edtCategoriaNcm.SetFocus();
      mensagem.Append('Campo "NCM" est� inv�lido').AppendLine;
    end;

    if (Result.Cfop = '') or not(TValidacaoes.VerificaCfop(Result.Cfop)) then
    begin
      FormHome.edtCategoriaCfop.SetFocus();
      mensagem.Append('Campo "CFOP" est� inv�lido').AppendLine;
    end;

    if (Result.Pis_CST = -1) or not(TValidacaoes.VerificaPisCst(Result.Pis_CST)) then
    begin
      FormHome.edtCategoriaPisCst.SetFocus();
      mensagem.Append('Campo "PIS CST" est� inv�lido').AppendLine;
    end;

    if (Result.Cofins_CST = -1) or not(TValidacaoes.VerificaCofinsCst(Result.Cofins_CST)) then
    begin
      FormHome.edtCategoriaCofinsCst.SetFocus();
      mensagem.Append('Campo "COFINS CST" est� inv�lido').AppendLine;
    end;

    if (Result.Icms_Origem = -1) or not(TValidacaoes.VerificaIcmsOrigem(Result.Icms_Origem)) then
    begin
      FormHome.edtCategoriaIcmsOrigem.SetFocus();
      mensagem.Append('Campo "ICMS Origem" est� inv�lido').AppendLine;
    end;

    if (Result.Icms_Cst = -1) or not(TValidacaoes.VerificaIcmsCst(Result.Icms_Cst)) then
    begin
      FormHome.edtCategoriaIcmsCst.SetFocus();
      mensagem.Append('Campo "ICMS CST" est� inv�lido').AppendLine;
    end;

    if mensagem.Length > 0 then
    begin
      FreeAndNil(Result);

      ShowMessage(mensagem.ToString());
    end;
  finally
    mensagem.Free;
  end;
end;

procedure TProdutoVM.InitEdit;
var
  repo: TProdutoRepository;
  produto: TProduto;
begin
  repo := TProdutoRepository.Create;
  try
    produto:= repo.InitEdit(FormHome.txtProdutoId.Tag);
    if Assigned(produto) then
    begin
      LimparDados();
      SetarCampos(produto);

      FormHome.txtProdutoTipoForm.Tag:= 1;
      FormHome.MoveTab(FormHome.tabProduto);
      FormHome.MoveTab(FormHome.tabProdutoCad);
    end;
  finally
    if Assigned(produto) then
      produto.Free;

    repo.Free;
  end;
end;

procedure TProdutoVM.InitInserir(categoriaId: integer);
var
  repo: TProdutoRepository;
  repoCategoria: TCategoriaRepository;
  categoria: TCategoria;
begin
  repoCategoria:= TCategoriaRepository.Create;
  categoria:= repoCategoria.Get(categoriaId);
  try
    if Assigned(categoria) then
    begin
      repo := TProdutoRepository.Create;
      try
        FormHome.txtProdutoTipoForm.Tag:= 0;
        LimparDados();
        SetarDadosDaCategoria(categoria);

        FormHome.txtProdutoTipoForm.Tag:= 0;
        FormHome.MoveTab(FormHome.tabProduto);
        FormHome.MoveTab(FormHome.tabProdutoCad);

      finally
        repo.Free;
      end;
    end;
  finally
    repoCategoria.Free;
    if Assigned(categoria) then
      categoria.Free;
  end;
end;

procedure TProdutoVM.LimparDados;
begin
  FormHome.edtProdutoDescricao.Text:= '';
  FormHome.edtProdutoNcm.Text:= '';
  FormHome.edtProdutoCest.Text:= '';
  FormHome.edtProdutoCfop.Text:= '';

  FormHome.edtProdutoPisBc.Text:= '';
  FormHome.edtProdutoPisCst.Text:= '49';
  FormHome.edtProdutoPisPorcentagem.Text:= '';
  FormHome.edtProdutoPisValor.Text:= '';
  FormHome.edtProdutoPisBcp.Text:= '';
  FormHome.edtProdutoPisAliquota.Text:= '';

  FormHome.edtProdutoPisstBc.Text:= '';
  FormHome.edtProdutoPisstPorcentagem.Text:= '';
  FormHome.edtProdutoPisstValor.Text:= '';
  FormHome.edtProdutoPisstBcp.Text:= '';
  FormHome.edtProdutoPisstAliquota.Text:= '';

  FormHome.edtProdutoCofinsCst.Text:= '49';
  FormHome.edtProdutoCofinsBc.Text:= '';
  FormHome.edtProdutoCofinsPorcentagem.Text:= '';
  FormHome.edtProdutoCofinsValor.Text:= '';
  FormHome.edtProdutoCofinsBcp.Text:= '';
  FormHome.edtProdutoCofinsAliquota.Text:= '';

  FormHome.edtProdutoCofinsstBc.Text:= '';
  FormHome.edtProdutoCofinsstPorcentagem.Text:= '';
  FormHome.edtProdutoCofinsstValor.Text:= '';
  FormHome.edtProdutoCofinsstBcp.Text:= '';
  FormHome.edtProdutoCofinsstAliquota.Text:= '';

  FormHome.edtProdutoIcmsOrigem.Text:= '0';
  FormHome.edtProdutoIcmsCst.Text:= '102';
  FormHome.edtProdutoIcmsPorcentagem.Text:= '';
  FormHome.edtProdutoIcmsValor.Text:= '';

  FormHome.edtProdutoIssqnNatureza.Text:= '';
  FormHome.edtProdutoIssqnDeducao.Text:= '';
  FormHome.edtProdutoIssqnCodIbge.Text := '';
  FormHome.edtProdutoIssqnBc.Text:= '';
  FormHome.edtProdutoIssqnItemServico.Text := '';
  FormHome.edtProdutoIssqnCodTributario.Text:= '';

  FormHome.edtProdutoTaxaEstadual.Text:= '';
  FormHome.edtProdutoTaxaFederal.Text:=  '';
end;

procedure TProdutoVM.Salvar;
var
  tipoForm: integer;
  repo: TProdutoRepository;
  produto: TProduto;
begin
  tipoForm:= FormHome.txtProdutoTipoForm.Tag;
  repo:= TProdutoRepository.Create();
  produto:= GetModel();
  try
    if Assigned(produto) then
    begin

         if tipoForm = 0 then
        begin
          try
            repo.Insert(produto);
            FormHome.MoveTab(FormHome.tabProdutoLista);
          except
            FormHome.MostraMensage('Erro ao cadastrar o produto');
          end;

        end
        else if tipoForm = 1 then
        begin
          try
            repo.Edit(produto);
            FormHome.MoveTab(FormHome.tabProdutoLista);
          except
            FormHome.MostraMensage('Erro ao editar o produto');
          end;
        end;
        Carregar();

    end
    else
      FormHome.MostraMensage('H� dados inv�lidos para o cadastro');
  finally
    repo.Free;
  end;
end;

procedure TProdutoVM.SetarCampos(model: TProduto);
begin
  if Assigned(model) then
  begin
    FormHome.txtProdutoCategoria.Tag:= model.CategoriaId;
    FormHome.edtProdutoDescricao.Text:= model.Descricao;
    FormHome.edtProdutoValor.Text := CurrToStr(model.Preco);
    FormHome.cedProdutoSiglaUnidade.Text:= model.UnidadeMedida;
    FormHome.txtProdutoId.Tag:= model.Id;
    FormHome.txtProdutoCategoria.Text:= model.Categoria.Descricao;

    FormHome.edtProdutoNcm.Text:= model.Ncm;
    FormHome.edtProdutoCest.Text:= model.Cest;
    FormHome.edtProdutoCfop.Text:= model.Cfop;

    FormHome.edtProdutoPisBc.Text:= FloatToStr(model.Pis_BaseCalculo);
    FormHome.edtProdutoPisCst.Text:= IntToStr(model.Pis_CST);
    FormHome.edtProdutoPisPorcentagem.Text:= FloatToStr(model.Pis_Porcento);
    FormHome.edtProdutoPisValor.Text:= FloatToStr(model.Pis_Valor);
    FormHome.edtProdutoPisBcp.Text:= FloatToStr(model.Pis_BaseCalculoPorProduto);
    FormHome.edtProdutoPisAliquota.Text:= FloatToStr(model.Pis_Aliquota);

    FormHome.edtProdutoPisstBc.Text:= FloatToStr(model.PisSt_BaseCalculo);
    FormHome.edtProdutoPisstPorcentagem.Text:= FloatToStr(model.PisSt_Porcento);
    FormHome.edtProdutoPisstValor.Text:= FloatToStr(model.PisSt_Valor);
    FormHome.edtProdutoPisstBcp.Text:= FloatToStr(model.PisSt_BaseCalculoPorProduto);
    FormHome.edtProdutoPisstAliquota.Text:= FloatToStr(model.PisSt_Aliquota);

    FormHome.edtProdutoCofinsCst.Text:= IntToStr(model.Cofins_CST);
    FormHome.edtProdutoCofinsBc.Text:= FloatToStr(model.Cofins_BaseCalculo);
    FormHome.edtProdutoCofinsPorcentagem.Text:= FloatToStr(model.Cofins_Porcento);
    FormHome.edtProdutoCofinsValor.Text:= FloatToStr(model.Cofins_Valor);
    FormHome.edtProdutoCofinsBcp.Text:= FloatToStr(model.Cofins_BaseCalculoPorProduto);
    FormHome.edtProdutoCofinsAliquota.Text:= FloatToStr(model.Cofins_Aliquota);

    FormHome.edtProdutoCofinsstBc.Text:= FloatToStr(model.CofinsSt_BaseCalculo);
    FormHome.edtProdutoCofinsstPorcentagem.Text:= FloatToStr(model.CofinsSt_Porcento);
    FormHome.edtProdutoCofinsstValor.Text:= FloatToStr(model.CofinsSt_Valor);
    FormHome.edtProdutoCofinsstBcp.Text:= FloatToStr(model.CofinsSt_BaseCalculoPorProduto);
    FormHome.edtProdutoCofinsstAliquota.Text:= FloatToStr(model.CofinsSt_Aliquota);

    FormHome.edtProdutoIcmsOrigem.Text:= IntToStr(model.Icms_Origem);
    FormHome.edtProdutoIcmsCst.Text:= IntToStr(model.Icms_Cst);
    FormHome.edtProdutoIcmsPorcentagem.Text:= FloatToStr(model.Icms_Porcento);
    FormHome.edtProdutoIcmsValor.Text:= FloatToStr(model.Icms_Valor);

    FormHome.edtProdutoIssqnNatureza.Text:= IntToStr(model.Issqn_Natureza);
    FormHome.edtProdutoIssqnDeducao.Text:= FloatToStr(model.Issqn_Deducao);
    FormHome.edtProdutoIssqnCodIbge.Text := model.Issqn_CodigoIbge;
    FormHome.edtProdutoIssqnBc.Text:= FloatToStr(model.Issqn_BaseCalculo);
    FormHome.edtProdutoIssqnItemServico.Text := model.Issqn_ItemDaListaServico;
    FormHome.edtProdutoIssqnCodTributario.Text:= model.Issqn_CodigoTributarioIssqn;

    FormHome.edtCategoriaTaxaEstadual.Text:= FloatToStr(model.TaxaEstadual);
    FormHome.edtCategoriaTaxaFederal.Text:=  FloatToStr(model.TaxaFederal);
  end;
end;

procedure TProdutoVM.SetarDadosDaCategoria(categoria: TCategoria);
begin
  FormHome.txtProdutoCategoria.Tag:= categoria.Id;
  FormHome.txtProdutoCategoria.Text:= categoria.Descricao;
  FormHome.edtProdutoNcm.Text:= categoria.Ncm;
  FormHome.edtProdutoCest.Text:= categoria.Cest;
  FormHome.edtProdutoCfop.Text:= categoria.Cfop;

  FormHome.edtProdutoPisBc.Text:= FloatToStr(categoria.Pis_BaseCalculo);
  FormHome.edtProdutoPisCst.Text:= IntToStr(categoria.Pis_CST);
  FormHome.edtProdutoPisPorcentagem.Text:= FloatToStr(categoria.Pis_Porcento);
  FormHome.edtProdutoPisValor.Text:= FloatToStr(categoria.Pis_Valor);
  FormHome.edtProdutoPisBcp.Text:= FloatToStr(categoria.Pis_BaseCalculoPorProduto);
  FormHome.edtProdutoPisAliquota.Text:= FloatToStr(categoria.Pis_Aliquota);

  FormHome.edtProdutoPisstBc.Text:= FloatToStr(categoria.PisSt_BaseCalculo);
  FormHome.edtProdutoPisstPorcentagem.Text:= FloatToStr(categoria.PisSt_Porcento);
  FormHome.edtProdutoPisstValor.Text:= FloatToStr(categoria.PisSt_Valor);
  FormHome.edtProdutoPisstBcp.Text:= FloatToStr(categoria.PisSt_BaseCalculoPorProduto);
  FormHome.edtProdutoPisstAliquota.Text:= FloatToStr(categoria.PisSt_Aliquota);

  FormHome.edtProdutoCofinsCst.Text:= IntToStr(categoria.Cofins_CST);
  FormHome.edtProdutoCofinsBc.Text:= FloatToStr(categoria.Cofins_BaseCalculo);
  FormHome.edtProdutoCofinsPorcentagem.Text:= FloatToStr(categoria.Cofins_Porcento);
  FormHome.edtProdutoCofinsValor.Text:= FloatToStr(categoria.Cofins_Valor);
  FormHome.edtProdutoCofinsBcp.Text:= FloatToStr(categoria.Cofins_BaseCalculoPorProduto);
  FormHome.edtProdutoCofinsAliquota.Text:= FloatToStr(categoria.Cofins_Aliquota);

  FormHome.edtProdutoCofinsstBc.Text:= FloatToStr(categoria.CofinsSt_BaseCalculo);
  FormHome.edtProdutoCofinsstPorcentagem.Text:= FloatToStr(categoria.CofinsSt_Porcento);
  FormHome.edtProdutoCofinsstValor.Text:= FloatToStr(categoria.CofinsSt_Valor);
  FormHome.edtProdutoCofinsstBcp.Text:= FloatToStr(categoria.CofinsSt_BaseCalculoPorProduto);
  FormHome.edtProdutoCofinsstAliquota.Text:= FloatToStr(categoria.CofinsSt_Aliquota);

  FormHome.edtProdutoIcmsOrigem.Text:= IntToStr(categoria.Icms_Origem);
  FormHome.edtProdutoIcmsCst.Text:= IntToStr(categoria.Icms_Cst);
  FormHome.edtProdutoIcmsPorcentagem.Text:= FloatToStr(categoria.Icms_Porcento);
  FormHome.edtProdutoIcmsValor.Text:= FloatToStr(categoria.Icms_Valor);

  FormHome.edtProdutoIssqnNatureza.Text:= IntToStr(categoria.Issqn_Natureza);
  FormHome.edtProdutoIssqnDeducao.Text:= FloatToStr(categoria.Issqn_Deducao);
  FormHome.edtProdutoIssqnCodIbge.Text := categoria.Issqn_CodigoIbge;
  FormHome.edtProdutoIssqnBc.Text:= FloatToStr(categoria.Issqn_BaseCalculo);
  FormHome.edtProdutoIssqnItemServico.Text := categoria.Issqn_ItemDaListaServico;
  FormHome.edtProdutoIssqnCodTributario.Text:= categoria.Issqn_CodigoTributarioIssqn;
  FormHome.edtProdutoIssqnBcp.Text:= FloatToStr(categoria.Issqn_BaseCalculoProduto);

  FormHome.edtProdutoTaxaEstadual.Text:= FloatToStr(categoria.TaxaEstadual);
  FormHome.edtProdutoTaxaFederal.Text:=  FloatToStr(categoria.TaxaFederal);
end;

function TProdutoVM.VerificaSeTemCategorias: Boolean;
var
  repo: TCategoriaRepository;
  lista: TObjectList<TCategoria>;
begin
  repo:= TCategoriaRepository.Create;
  try
    lista:= repo.GetAll();

    Result:= lista.Count > 0;
  finally
    lista.Free;
    repo.Free;
  end;

end;

end.
