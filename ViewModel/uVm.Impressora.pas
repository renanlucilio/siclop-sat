unit uVm.Impressora;

interface

uses uImpressora, Form.Principal, System.SysUtils;

type
  //classe de view model para impressora
  TImpressoraVM = class
  private
  public
        //método para pegar as informações da tela
    class procedure GetDadosForm();
        //método para definir as informações na tela
    class procedure SetDadosForm();
  end;

implementation

{ TImpressoraVM }

uses uAuRepository;

class procedure TImpressoraVM.GetDadosForm;
var
  imp: TImpressora;
begin
  imp:= TImpressora.Create();
  try
    imp.Preview:= FormHome.swtchTSatImpressao_preview.IsChecked;
    imp.ItemPorLinha:= FormHome.swtchTSatImpressao_itemPorLinha.IsChecked;
    imp.ImprimeDescontoAcrescimo:= FormHome.swtchITSatImpressao_imprimeDescontoAcrecimo.IsChecked;
    imp.LarguraBobina:= StrToIntDef(FormHome.EdtTSatImpressao_largura.Text, 270);
    imp.MargeTopo:= StrToIntDef(FormHome.EdtTSatImpressao_margeTopo.Text, 0);
    imp.MargeInferior:= StrToIntDef(FormHome.EdtTSatImpressao_margeInferior.Text, 0);
    imp.MargeDireita:= StrToIntDef(FormHome.EdtTSatImpressao_margeDireita.Text, 0);
    imp.MargeEsquerda:= StrToIntDef(FormHome.EdtTSatImpressao_margeEsquerda.Text, 0);
    imp.NomeImpressora:= FormHome.TxtTSatImpressao_nomeImpressora.Text;

    imp.Salvar;

    TAuditoriaRepository.GravarLog(
          'Configuração Alterada',
           FormHome.txtUsuario.Text + #13 +
          ' Foram alteradas as configurações da "Impressora"' + #13 +
          ' Horario:' + TimeToStr(Now),
          'O');

  finally
    imp.Free;
  end;
end;

class procedure TImpressoraVM.SetDadosForm;
var
  imp: TImpressora;
begin
  imp:= TImpressora.Create();
  try
    FormHome.swtchTSatImpressao_preview.IsChecked:= imp.Preview;
    FormHome.swtchTSatImpressao_itemPorLinha.IsChecked:= imp.ItemPorLinha;
    FormHome.swtchITSatImpressao_imprimeDescontoAcrecimo.IsChecked:= imp.ImprimeDescontoAcrescimo;
    FormHome.EdtTSatImpressao_largura.Text:= IntToStr(imp.LarguraBobina);
    FormHome.EdtTSatImpressao_margeTopo.Text:= IntToStr(imp.MargeTopo);
    FormHome.EdtTSatImpressao_margeInferior.Text:= IntToStr(imp.MargeInferior);
    FormHome.EdtTSatImpressao_margeDireita.Text:= IntToStr(imp.MargeDireita);
    FormHome.EdtTSatImpressao_margeEsquerda.Text:= IntToStr(imp.MargeEsquerda);
    FormHome.TxtTSatImpressao_nomeImpressora.Text:= imp.NomeImpressora;
  finally
    imp.Free;
  end;
end;
end.
