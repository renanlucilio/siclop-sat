unit uVm.Sistema;

interface

uses uSistema;


type
  //classe de view model para sistema
  TSistemaVM = class
  private
  public
          //m�todo para pegar as informa��es da tela
    class procedure GetDadosForm();
                //m�todo para definir as informa��es na tela
    class procedure SetDadosForm();
  end;

implementation


{ TSistemaVM }

uses Form.Principal, uVm.Sat, uUsuarioReporitory, uAuRepository,
  System.SysUtils, FMX.Dialogs;

class procedure TSistemaVM.GetDadosForm;
var
  sistem: TSistema;
  repo: TUsuarioRepository;
begin
  sistem := TSistema.Create;
  repo:= TUsuarioRepository.Create;
  try
    if FormHome.swtSistemaAutonomo.IsChecked then
    begin
      if (repo.TemUsuariosCadastrados)  then
      begin
        sistem.Autonamo:= FormHome.swtSistemaAutonomo.IsChecked;

        if sistem.Autonamo then
        begin
          FormHome.swtchTSatSistema_envioAutomatico.IsChecked:= true;
          TSatVM.GetDadosForm;
        end;

        sistem.Salvar;


      end
      else
      begin
        FormHome.swtSistemaAutonomo.IsChecked:= false;
        ShowMessage('Deve ter pelo menos um usu�rio cadastrado');
        FormHome.MoveTab(FormHome.tabBodyCadastro);
        FormHome.MoveTab(FormHome.tabUsuario);
        FormHome.MoveTab(FormHome.tabUsuarioLista);
      end;
    end
    else
    begin
      sistem.Autonamo:= FormHome.swtSistemaAutonomo.IsChecked;

      sistem.Salvar;
    end;

    TAuditoriaRepository.GravarLog(
          'Configura��o Alterada',
           FormHome.txtUsuario.Text + #13 +
          ' Foram alteradas as configura��es do "Sistema"' + #13 +
          ' Horario:' + TimeToStr(Now),
          'O');
  finally
    repo.Free;
    sistem.Free;
  end;
end;

class procedure TSistemaVM.SetDadosForm;
var
  sistem: TSistema;
begin
  sistem := TSistema.Create;
  try
    FormHome.swtSistemaAutonomo.IsChecked:= sistem.Autonamo;
  finally
    sistem.Free;
  end;
end;

end.
