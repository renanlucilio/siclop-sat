unit uVm.Principal;

interface

uses
  System.SysUtils,
  System.Classes,
  Data.Bind.ObjectScope,
  Data.Bind.Components,
  Data.Bind.GenData,
  uImpressora,
  uContabilidade,
  uSAT,
  uSoftwareHouse,
  uEstabelecimento,
  uPastas,
  uSAT.Equipamento,
  uSAT.Impressora, Model.PowerCMD, uValidacoes, FMX.Types, uSistema,
  uUsuario;

type
//classe de view model principal
  TdmVmPrincipal = class(TDataModule)
    tmrConfiguracoes: TTimer;
    //evento de cria��o do data module para iniciar os dados
    procedure DataModuleDestroy(Sender: TObject);
    //evento de destroy do data module para eliminar as instancias
    procedure DataModuleCreate(Sender: TObject);
    //evento de timer verificar as configura��es
    procedure tmrConfiguracoesTimer(Sender: TObject);
  private
    sat: TSAT;
    estabelecimento: TEstabelecimento;
    softhouse: TSoftwareHouse;
    contabildiade: TContabilidade;
    impressora: TImpressora;
    pastas: TPasta;
    satEquipamento: TSAT_Equipamento;
    satImpressora: TSAT_Impressora;
    FUsuario: TUsuario;

    { Private declarations }
    //m�todo para listar os xml para cancelar
    procedure ListarXMLParaCancelar();
    //m�todo para listar os arquivos para emitir
    procedure ListarArquivosDisponiveis(const pastaCupom: string);
    //m�todo para encerar o sistema
    procedure EncerarSistema;
    //m�todo para iniciar o sistema
    procedure IniciarSistema;

  public
    { Public declarations }
    //m�todo para definir uma impressora
    procedure SetImpressora();
    //m�todo para definir uma dll
    procedure SetDLLSat();

    //m�todo para reiniciar as configura��es
    procedure ReiniciarConfiguracoes;

    //m�todo para verificar se o sistema � de teste
    procedure VerificarSeEhTeste();

    //m�todo para limpar os emails
    procedure LimparEmail();
    //m�todo para adicionar email
    procedure AdicionarEmail();

    //m�todo para verificar se o sistema � autononamo
    procedure VerificaSeEhStandAlon();

    //m�todo para mostrar o movimento de hoje
    procedure ExibirMovimentoDeHoje();

    //m�todo para iniciar o sat
    procedure IniciarSAT();
    //m�todo para desativar o sat
    procedure DesavativarSAT();
    //m�todo para reiniciar o sat
    procedure ReiniciarSAT();

    //m�todo para resumir uma nota
    procedure ResumirNota(dataDe, dataAte: TDate);
    //m�todo para listar as notas para envio
    procedure ListarNotas();
    //m�todo para mostrar um Cfe com base em um txt
    procedure MostrarCFeTxt();
    //m�todo para mostrar um PDF
    procedure MostrarPDF(const id: Integer);
    //m�todo para enviar relatorios
    procedure EnviarRelatorio(dataDe, dataAte: TDate);
    //m�todo para salvar relatorios
    procedure SalvarRelatorio(dataDe, dataAte: TDate);

    //m�todo para exibir status do sat
    procedure ExibirStatusSAT();
    //m�todo para exibir o log do sat
    procedure ExibirLogSAT();
    //m�todo para exibir a consulta do sat
    procedure ExibirConsultaSAT();

    //m�todo para enviar o sat
    procedure EnviarSAT();
    //m�todo para cancelar o sat
    procedure CancelarSAT();

    //m�todo para autenticar um usu�rio
    procedure Autenticar(usuario: TUsuario);

    property Usuario: TUsuario read FUsuario write FUsuario;
  end;

var
  dmVmPrincipal: TdmVmPrincipal;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}


uses
   System.DateUtils,
   Model.ViewUtil,
   Form.Principal,
   uCFe.Txt,
   System.Generics.Collections,
   FMX.ListView.Appearances,
   uCFe,
   uTimerAnonymous,
   uItem.CFe, FMX.Dialogs, System.UITypes, uVm.Contabilidade,
  uVm.Estabelecimento, uVm.Impressora, uVm.Pasta, uVm.Sat, uVm.SoftHouse,
  System.Threading, FMX.ListBox, pcnCFe, uVendaRepository,
  uVendaRelatorios, uVm.Sistema, uUsuarioReporitory, Fmx.Forms,
  uAuRepository;


var
  faltaConfigurar: Boolean;

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

procedure TdmVmPrincipal.AdicionarEmail;
var
  email: string;
begin
   try
    email:= ResultadoDaPergunta('Digite um Email', 'Digite um novo Email: ');
    if TValidacaoes.ValidarEmail(email) then
      FormHome.MmoTContabilidade_emails.Lines.Add(email)
    else
      FormHome.MostraMensage('Email inv�lido');
  except
    MostrarMsg('Email inv�lido');
  end;
end;

procedure TdmVmPrincipal.Autenticar(usuario: TUsuario);
begin
 if not Assigned(usuario) then
  Application.Terminate;

 if Assigned(self.usuario) then
  self.usuario.Free;

 self.usuario:= usuario;

 FormHome.txtUsuario.Text:= 'Usu�rio: ' + usuario.Nome;

 case usuario.Nivel.Value of
  0:
  begin
    FormHome.RectBtnConfiguracoes.Visible:= false;
    FormHome.RectBtnRelatorios.Visible:= false;
    FormHome.RectBtnCadastros.Visible:= false and (FormHome.rectBtnCadastros.Visible = true);
  end;
  1:
  begin
    FormHome.RectBtnConfiguracoes.Visible:= false;
    FormHome.RectBtnRelatorios.Visible:= true;
    FormHome.RectBtnCadastros.Visible:= true and (FormHome.rectBtnCadastros.Visible = true);
  end;
  2:
  begin
    FormHome.RectBtnConfiguracoes.Visible:= false;
    FormHome.RectBtnRelatorios.Visible:= true;
    FormHome.RectBtnCadastros.Visible:= true and (FormHome.rectBtnCadastros.Visible = true);
  end;
  3:
  begin
    FormHome.RectBtnConfiguracoes.Visible:= true;
    FormHome.RectBtnRelatorios.Visible:= true;
    FormHome.RectBtnCadastros.Visible:= true;
  end;
 end;
end;

procedure TdmVmPrincipal.CancelarSAT;
var
  infoVenda: TVendaRelatorios;
  arquivo: string;
  id: integer;
begin
  if (Assigned(satEquipamento)) then
  begin
    if (FormHome.LsVNotasParaCancelar.ItemIndex > -1)  then
    begin
      infoVenda:= TVendaRelatorios.Create();
      try
        id:=  FormHome.LsVNotasParaCancelar.Selected.Tag;
        arquivo:= infoVenda.GetArquivoXML(id);
        satEquipamento.CancelarSAT(arquivo, id);
        TAuditoriaRepository.GravarLog(
          'Cupom Fiscal Cancelado',
          'Usu�rio: ' + Usuario.Nome + #13 +
          ' Cancelou o cupom fiscal' + #13 +
          ' Horario:' + TimeToStr(Now),
          'N');
        DeleteFile(arquivo)
      finally
        infoVenda.Free;
      end;
    end;
  end
  else
    MostrarMsg('SAT Desativado');

end;

procedure TdmVmPrincipal.EncerarSistema;
begin
  if Assigned(satEquipamento) then
    satEquipamento.Free;
  if Assigned(satImpressora) then
    satImpressora.Free;
  if Assigned(sat) then
    sat.Free;
  if Assigned(estabelecimento) then
    estabelecimento.Free;
  if Assigned(softhouse) then
    softhouse.Free;
  if Assigned(contabildiade) then
    contabildiade.Free;
  if Assigned(impressora) then
    impressora.Free;
  if Assigned(usuario) then
    usuario.Free;
end;

procedure TdmVmPrincipal.DataModuleCreate(Sender: TObject);
begin
  self.usuario:= nil;
  sat:= nil;
  estabelecimento:= nil;
  softhouse:= nil;
  contabildiade:= nil;
  impressora:= nil;
  pastas:= nil;
  satEquipamento:= nil;
  satImpressora:= nil;
  faltaConfigurar:= true;
  IniciarSistema;
end;

procedure TdmVmPrincipal.DataModuleDestroy(Sender: TObject);
begin
  EncerarSistema;
end;

procedure TdmVmPrincipal.DesavativarSAT;
begin
  if Assigned(satEquipamento) then
    satEquipamento.Free;
  if Assigned(satImpressora) then
    satImpressora.Free;
end;

procedure TdmVmPrincipal.EnviarRelatorio(dataDe, dataAte: TDate);
begin
  contabildiade.EnviarRelatorioManual(dataDe, dataAte);
end;

procedure TdmVmPrincipal.EnviarSAT;
var
  CFe: TDadosCFe;
  arquivoTxt: TCFeTxt;
  arquivo, msg: string;
begin
  try
    try
      if Assigned(satEquipamento) then
      begin
          if FormHome.LsVNotasParaEnvio.ItemIndex > -1 then
          begin
            arquivoTxt:= TCFeTxt.Create();
            arquivo:= FormHome.LsVNotasParaEnvio.Selected.TagString;

            msg:= arquivoTxt.VerificarTxt(arquivo).Trim;
            if msg <> '' then
            begin
              arquivoTxt.MoverParaErro(arquivo);
              ShowMessage(msg);
              exit;
            end;

            CFe := arquivoTxt.TxtToCFe(arquivo);
            satEquipamento.EnviarSAT(CFe);
            DeleteFile(arquivo);

          end;
      end
      else
        FormHome.MostraMensage('SAT Desativado');
    except on e: Exception do
       FormHome.MostraMensage(e.Message);
    end;
  finally
    FreeAndNil(arquivoTxt);
  end;
end;

procedure TdmVmPrincipal.ExibirConsultaSAT;
begin
  if Assigned(satEquipamento) then
  begin
    FormHome.MmoRespostaSAT.Lines.Add(FormatDateTime('c', now)+'------------------------------');
    FormHome.MmoRespostaSAT.Lines.Add(satEquipamento.GetConsulta());
  end;
end;

procedure TdmVmPrincipal.ExibirLogSAT;
begin
  if Assigned(satEquipamento) then
  begin
    FormHome.MmoRespostaSAT.Lines.Clear;
    FormHome.MmoRespostaSAT.Lines.Add(
      satEquipamento.GetLog()
      );
  end;
end;

procedure TdmVmPrincipal.ExibirMovimentoDeHoje;
var
  infoVendas: TVendaRelatorios;
  processadas, erros, enviadas, canceladas: Integer;
  valorItens, valorImposto: Currency;
  dataAtual, dataAmanha: TDateTime;
begin
  dataAtual:= Now();
  dataAmanha:= IncDay(dataAtual);
  infoVendas:= TVendaRelatorios.Create();
  try
    enviadas:= infoVendas.QuantidadeVendido(
    {dataDe}DateOf(dataAtual),
    {dateAte}DateOf(dataAmanha)
    );

    valorItens:= infoVendas.ValorVendido(
    {dataDe}DateOf(dataAtual),
    {dateAte}DateOf(dataAmanha)
    );

    valorImposto:= infoVendas.ImpostosVendido(
    {dataDe}DateOf(dataAtual),
    {dateAte}DateOf(dataAmanha)
    );

    canceladas:= infoVendas.QuantidadeCancelados(
    {dataDe}DateOf(dataAtual),
    {dateAte}DateOf(dataAmanha)
    );

    erros := TCFeTxt.TotalErros();
    processadas:= TCFeTxt.TotalXML();

    FormHome.txtQtdProcessadas.Text:= processadas.ToString();
    FormHome.txtQtdEviadas.Text    := enviadas.ToString();
    FormHome.txtQtdErros.Text      := erros.ToString();
    FormHome.txtQtdCanceladas.Text := canceladas.ToString();
    FormHome.txtValorItens.Text    := FormatCurr('###,###,##0.00', valorItens);
    FormHome.txtValorImposto.Text  := FormatCurr('###,###,##0.00', valorImposto);
  finally
    FreeAndNil(infoVendas);
  end;
end;

procedure TdmVmPrincipal.ExibirStatusSAT;
begin
  if Assigned(satEquipamento) then
  begin
   FormHome.MmoRespostaSAT.Lines.Add(FormatDateTime('c', now)+'------------------------------');
   FormHome.MmoRespostaSAT.Lines.Add(satEquipamento.GetStatus());
  end;
end;

procedure TdmVmPrincipal.IniciarSAT;
var
  podeIniciar: Boolean;
  _sat: TSAT_Equipamento;
  processamento: ITask;
begin
  FormHome.MostraMensage('Verificando as configura��es antes de inicilizar SAT');
  podeIniciar:= sat.GetConfigurado and estabelecimento.GetConfigurado and
     softhouse.GetConfigurado and contabildiade.GetConfigurado and
     impressora.GetConfigurado and pastas.Create.GetConfigurado;

  if podeIniciar then
  begin
     satImpressora:= TSAT_Impressora.Create(impressora);
     satEquipamento:= TSAT_Equipamento.Create(sat, softhouse, estabelecimento, satImpressora, pastas);
     satEquipamento.OnIniciado      := FormHome.ConfirmaInicializado;
     satEquipamento.OnErroIniciar   := FormHome.ErroAoIniciar;
     satEquipamento.OnNumeroSerieSat:= FormHome.MostraNSerieSAT;
     satEquipamento.OnMensagemSat   := FormHome.MostraRetornoSAT;

    _sat:= satEquipamento;
    FormHome.MostraMensage('Iniciando S@T');
    processamento:= TTask.Create(procedure
    begin
       satEquipamento.IniciarSAT();
       if satEquipamento.Inicializado then
        FormHome.ConfirmaInicializado();
    end);

    processamento.Start();
    if not processamento.Wait(10000) then
    begin
        if Assigned(processamento) then
          processamento.Cancel();

        FormHome.MostraMensage('Erro ao iniciar o S@T');
        FormHome.MostraNSerieSAT('SAT: OFF');
      end;

  end;
end;

procedure TdmVmPrincipal.IniciarSistema;
begin
  sat:= TSAT.Create;
  estabelecimento:= TEstabelecimento.Create;
  softhouse:= TSoftwareHouse.Create;
  contabildiade:= TContabilidade.Create;
  impressora:= TImpressora.Create;
  pastas:= TPasta.Create;

  contabildiade.OnStatusEmail:= FormHome.MensagemEmail;

  TSatVM.SetDadosForm();
  TEstabelecimentoVM.SetDadosForm();
  TSoftHouseVM.SetDadosForm();
  TContabilidadeVM.SetDadosForm();
  TImpressoraVM.SetDadosForm();
  TPastaVm.SetDadosForm();
  TSistemaVM.SetDadosForm();

  VerificaSeEhStandAlon;
  VerificarSeEhTeste();
  IniciarSAT;
end;

procedure TdmVmPrincipal.limparEmail;
begin
  FormHome.MmoTContabilidade_emails.Lines.Clear;
end;

procedure TdmVmPrincipal.ListarArquivosDisponiveis(const pastaCupom: string);
var
  novoItem: TListViewItem;
  it: string;
  nomeDoArquivo: string;
  arquivosTxt: TCFeTxt;
begin
    if DirectoryExists(pastaCupom) then
    begin
    arquivosTxt:= TCFeTxt.Create();
      try
        if FormHome.LsVNotasParaEnvio.Items.Count <> Length(arquivosTxt.ListarArquivos(pastaCupom)) then
        begin
          FormHome.LsVNotasParaEnvio.BeginUpdate();
          FormHome.LsVNotasParaEnvio.Items.Clear();
          for it in arquivosTxt.ListarArquivos(pastaCupom) do
          begin

            nomeDoArquivo := it.Split(['\'])[Length(it.Split(['\'])) - 1];

            novoItem := FormHome.LsVNotasParaEnvio.Items.Add();

            novoItem.Text := nomeDoArquivo;
            novoItem.TagString:= it;
          end;
          FormHome.LsVNotasParaEnvio.EndUpdate();
        end;
      finally
        arquivosTxt.free;
      end;
    end;

end;

procedure TdmVmPrincipal.ListarNotas;
begin
  ListarXMLParaCancelar();
  try
  if Assigned(pastas) then
    ListarArquivosDisponiveis(pastas.PastaCupom);
  except
  end;
end;

procedure TdmVmPrincipal.ListarXMLParaCancelar;
var
  it: integer;
  arquivos: TDictionary<integer, string>;
  novoItem: TListViewItem;
  atualiza: Boolean;
  vendas: TVendaRelatorios;
begin
  atualiza := false;

  vendas:= TVendaRelatorios.Create();
  try
    arquivos := vendas.UltimasVendasParaCancelar30Minutos();

    if arquivos.Count <> FormHome.LsVNotasParaCancelar.items.Count then
      atualiza := true;

    if atualiza then
    begin
      FormHome.LsVNotasParaCancelar.items.Clear();
      for it in arquivos.Keys do
      begin
        novoItem := FormHome.LsVNotasParaCancelar.items.Add();
        novoItem.Tag := it;
        novoItem.Text := arquivos.items[it];
      end;
    end;
  finally
    FreeAndNil(vendas);
    FreeAndNil(arquivos);
  end;
end;

procedure TdmVmPrincipal.MostrarCFeTxt;
var
  TempCFe: TDadosCFe;
  CFe: TCFe;
  arquivoTxt: TCFeTxt;
  arquivo: string;
  linha: TStringBuilder;
  linhaItem, msg: string;
  it: TItem;
  i: integer;
  total: double;
  tmp: TListBoxItem;
begin
  CFe:= nil;
  arquivoTxt:= TCFeTxt.Create();
  arquivo:= FormHome.LsVNotasParaEnvio.Selected.TagString;

  linha := TStringBuilder.Create();
  try
    msg:= arquivoTxt.VerificarTxt(arquivo).Trim;
    if msg <> '' then
    begin
      arquivoTxt.MoverParaErro(arquivo);
      ShowMessage(msg);
      exit;
    end;

    TempCFe := arquivoTxt.TxtToCFe(arquivo);

    if Assigned(TempCFe) then
    begin
      CFe:= TempCFe.GetCFe();

      FormHome.TxtInformacoesCliente.Text := '';

      linha.Append('Nome do Cliente:').AppendLine().Append(TempCFe.Cliente.Nome)
        .AppendLine().Append('Documento do Cliente:').AppendLine()
        .Append(TempCFe.Cliente.Documento);

      FormHome.TxtInformacoesCliente.Text := linha.ToString();
      FormHome.txtLegProduto.Text:= 'Produtos';
      FormHome.LstDeProtudos.BeginUpdate();
      FormHome.LstDeProtudos.Items.Clear();
      total:= 0;
      for it in TempCFe.Itens do
      begin
        total:= total + it.Produto.GetValorProduto();

        linhaItem:= it.Produto.Descricao.PadRight(30);
        FormHome.LstDeProtudos.Items.Add(linhaItem+' R$'+
          FormatCurr('###,###,##0.00', it.Produto.ValorUnitario*it.Produto.Quantidade));
      end;
      FormHome.LstDeProtudos.EndUpdate();

      FormHome.LstDeProtudos.BeginUpdate;
       for i := 0 to FormHome.LstDeProtudos.ComponentCount-1 do
        begin
          if FormHome.LstDeProtudos.Components[i] is TListBoxItem then
          begin
            tmp := (FormHome.LstDeProtudos.Components[i] as TListBoxItem);
            tmp.TextSettings.Font.Size:= 10;
            tmp.TextSettings.Font.Family := 'Lucida Console';
            tmp.StyledSettings:=[];
          end;
        end;
       FormHome.LstDeProtudos.EndUpdate;

      total:= total + CFe.Det.Items[CFe.Det.Count - 1].Prod.vOutro;
      total:= total - CFe.Total.DescAcrEntr.vDescSubtot;
      linha.Clear();
      linha.Append('Acr�scimo: R$ '+ FormatCurr('###,###,##0.00', CFe.Det.Items[CFe.Det.Count - 1].Prod.vOutro)).AppendLine();
      linha.Append('Desconto: R$ '+ FormatCurr('###,###,##0.00', CFe.Total.DescAcrEntr.vDescSubtot)).AppendLine();
      linha.Append('Total: R$ '+ FormatCurr('###,###,##0.00', total)).AppendLine();
      FormHome.txtTotalCupom.Text:= linha.ToString();
      FormHome.txtTotalCupom.Height:= 80;
    end;
  finally
    if Assigned(CFe) then
      FreeAndNil(CFe);
    FreeAndNil(arquivoTxt);
    FreeAndNil(linha);
  end;
end;

procedure TdmVmPrincipal.MostrarPDF(const id: Integer);
var
  venda: TVendaRelatorios;
  pdf: string;
begin
  venda:= TVendaRelatorios.Create();
  try
   pdf:= venda.GetArquivoPDF(id);

    if FileExists(pdf) then
      TModelPowerCMD.New(HInstance).ExecLink(pdf)
    else
      MostrarMsg('Arquivo n�o encontrado');
  finally
    FreeAndNil(venda);
  end;
end;

procedure TdmVmPrincipal.ReiniciarConfiguracoes;
begin
  sat.Carregar;
  estabelecimento.Carregar;
  softhouse.Carregar;
  contabildiade.Carregar;
  impressora.Carregar;
  pastas.Carregar;
  VerificaSeEhStandAlon;
end;

procedure TdmVmPrincipal.ReiniciarSAT;
begin
  DesavativarSAT;
  IniciarSAT();
end;

procedure TdmVmPrincipal.ResumirNota(dataDe, dataAte: TDate);
var
  enviadas, canceladas: integer;
  valorItens: currency;
  valorImposto: currency;
  infoVendas: TVendaRelatorios;
  tmrSome: TTimer;
begin
  infoVendas:= TVendaRelatorios.Create();
  try
    enviadas    := infoVendas.QuantidadeVendido(dataDe, dataAte);
    valorItens  := infoVendas.ValorVendido(dataDe, dataAte);
    valorImposto:= infoVendas.ImpostosVendido(dataDe, dataAte);
    canceladas  := infoVendas.QuantidadeCancelados(dataDe, dataAte);

    FormHome.TxtQuantidadeNota.Visible:= true;
    FormHome.txtQuantidadeCanceladasNotas.Visible:= true;
    FormHome.TxtTotalVendido.Visible:= true;
    FormHome.TxtTotalImpostos.Visible:= true;

    FormHome.TxtQuantidadeNotas.Text         := enviadas.ToString();
    FormHome.txtQuantidadeTotalCancelada.Text:= canceladas.ToString();
    FormHome.TxtValorTotalVendido.Text       := 'R$ '+ FormatCurr('###,###,##0.00', valorItens);
    FormHome.TxtValorTotalImpostos.Text      := 'R$ '+ FormatCurr('###,###,##0.00', valorImposto);

    TTimer.CreateAnonymousTimer(procedure
    begin
      FormHome.TxtQuantidadeNota.Visible:= false;
      FormHome.txtQuantidadeCanceladasNotas.Visible:= false;
      FormHome.TxtTotalVendido.Visible:= false;
      FormHome.TxtTotalImpostos.Visible:= false;
    end, 5000);
  finally
    infoVendas.Free;
  end;
end;

procedure TdmVmPrincipal.VerificarSeEhTeste;
begin
  if not sat.EhProducao then
  begin
    FormHome.TxtSICLOPSAT.Text:= '############VERS�O DE TESTE############';
  end;
end;

procedure TdmVmPrincipal.VerificaSeEhStandAlon;
var
  sistema: TSistema;
begin
  sistema := TSistema.Create;
  try
    FormHome.rectBtnCadastros.Visible:= FormHome.swtSistemaAutonomo.IsChecked;
    FormHome.lstItemEmitirCupom.Visible:= FormHome.swtSistemaAutonomo.IsChecked;
    FormHome.LstItemEnviarNota.Visible:= not(FormHome.swtSistemaAutonomo.IsChecked);
  finally
    sistema.Free;
  end;
end;

procedure TdmVmPrincipal.SalvarRelatorio(dataDe, dataAte: TDate);
begin
  contabildiade.SalvarRelatoriosDocumentos(dataDe, dataAte);
end;

procedure TdmVmPrincipal.SetDLLSat;
var
  caminho: string;
begin
  caminho:= SelecionarCaminhoDoArquivo('Dll |*.dll','Selecione uma do SAT');

  if FileExists(caminho) then
  begin
    FormHome.EdtTSatEquipamento_caminhoDLL.Text := caminho;
    FormHome.EdtTSatEquipamento_caminhoDLL.SetFocus;
  end
  else
    FormHome.EdtTSatEquipamento_caminhoDLL.Text := '';
end;

procedure TdmVmPrincipal.SetImpressora;
begin
  FormHome.TxtTSatImpressao_nomeImpressora.Text:= selecionarImpressora();
end;



procedure TdmVmPrincipal.tmrConfiguracoesTimer(Sender: TObject);
begin
  if not(sat.GetConfigurado) or
     not(estabelecimento.GetConfigurado) or
     not(impressora.GetConfigurado) or
     not(pastas.GetConfigurado) or
     not(softhouse.GetConfigurado) or
     not(contabildiade.GetConfigurado) then
  begin
    FormHome.VaiParaConfiguracoes();
    FormHome.MoveTab(FormHome.TabBodyConfiguracoes);

    if faltaConfigurar then
    begin
      faltaConfigurar:= false;
      FormHome.MostraMensage('� necess�rio realizar as configura��es');
    end;

  end;

  if not sat.GetConfigurado() then
  begin
    FormHome.MoveTab(FormHome.TabConfiguracoesEquipamentoSAT);
    Exit();
  end;

  if not impressora.GetConfigurado() then
  begin
    FormHome.MoveTab(FormHome.TabConfiguracoesImpressora);
    Exit();
  end;

  if not contabildiade.GetConfigurado() then
  begin
    FormHome.MoveTab(FormHome.TabConfiguracoesContabilidade);
    Exit();
  end;

  if not estabelecimento.GetConfigurado() then
  begin
    FormHome.MoveTab(FormHome.TabConfiguracoesEstabelecimento);

    Exit();
  end;

  if not softhouse.GetConfigurado() then
  begin
    FormHome.MoveTab(FormHome.TabConfiguracoesSoftHouse);
    Exit();
  end;

  if not pastas.GetConfigurado() then
  begin
    FormHome.MoveTab(FormHome.TabConfiguracoesSistema);
    Exit();
  end;

  tmrConfiguracoes.Enabled:= False;
end;

end.
