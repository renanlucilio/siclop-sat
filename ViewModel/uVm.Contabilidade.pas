unit uVm.Contabilidade;

interface

uses Form.Principal, uContabilidade, System.SysUtils;

type
  //classe de view model para contabilidade
  TContabilidadeVM = class
  private
  public
    //método para pegar as informações da tela
    class procedure GetDadosForm();
    //método para definir as informações na tela
    class procedure SetDadosForm();
  end;

implementation

{ TContabilidadeVM }

uses uAuRepository;

class procedure TContabilidadeVM.GetDadosForm;
var
  conta: TContabilidade;
  it: string;
begin
  conta := TContabilidade.Create;
  try
    conta.NomeFantasia:= FormHome.EdtTContabilidade_nomeFantasia.Text;
    conta.NomeResponsavel:= FormHome.EdtTContabilidade_nomeResponsavel.Text;
    conta.EnvioAutomatico:= FormHome.ChkTContabilidade_envioAutomatico.IsChecked;
    conta.DiaEnvioAutomatico:= StrToIntDef(FormHome.edtDiaEnvio.Text, 0);
    conta.limparEmail();
    for it in FormHome.MmoTContabilidade_emails.Lines do
    begin
      conta.AdicionarEmail(it);
    end;

    conta.Salvar;

    TAuditoriaRepository.GravarLog(
          'Configuração Alterada',
           FormHome.txtUsuario.Text + #13 +
          ' Foram alteradas as configurações da "Contabilidade"' + #13 +
          ' Horario:' + TimeToStr(Now),
          'O');

  finally
    conta.Free;
  end;
end;

class procedure TContabilidadeVM.SetDadosForm;
var
  conta: TContabilidade;
  it: string;
begin
  conta := TContabilidade.Create;
  try
    FormHome.EdtTContabilidade_nomeFantasia.Text:= conta.NomeFantasia;
    FormHome.EdtTContabilidade_nomeResponsavel.Text:= conta.NomeResponsavel;
    FormHome.ChkTContabilidade_envioAutomatico.IsChecked:= conta.EnvioAutomatico;
    FormHome.edtDiaEnvio.Text := IntToStr(conta.DiaEnvioAutomatico);

    FormHome.MmoTContabilidade_emails.Lines.clear;
    for it in conta.Email do
    begin
      FormHome.MmoTContabilidade_emails.Lines.Add(it);
    end;
  finally
    conta.Free;
  end;
end;


end.
