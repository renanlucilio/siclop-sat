unit uVm.Pasta;

interface

uses uPastas, Form.Principal;

type
  //classe de view model para pasta
  TPastaVm = class
  private
  public
        //método para pegar as informações da tela
    class procedure GetDadosForm();
            //método para definir as informações na tela
    class procedure SetDadosForm();
  end;

implementation

{ TPastaVm }

uses uAuRepository, System.SysUtils;

class procedure TPastaVm.GetDadosForm;
var
  pasta: TPasta;
begin
  pasta := TPasta.Create;
  try
    pasta.PastaCupom:= FormHome.EdtTPasta_pastaCupom.Text;
    pasta.Salvar;

    TAuditoriaRepository.GravarLog(
          'Configuração Alterada',
           FormHome.txtUsuario.Text + #13 +
          ' Foram alteradas as configurações da "Pasta"' + #13 +
          ' Horario:' + TimeToStr(Now),
          'O');
  finally
    pasta.Free;
  end;
end;

class procedure TPastaVm.SetDadosForm;
var
  pasta: TPasta;
begin
  pasta := TPasta.Create;
  try
    FormHome.EdtTPasta_pastaCupom.Text:= pasta.PastaCupom;
  finally
    pasta.Free;
  end;
end;

end.
