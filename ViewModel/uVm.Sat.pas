unit uVm.Sat;

interface

uses uSAT, Form.Principal, System.SysUtils;

type
  //classe de view model para sat
  TSatVM = class
  private
  public
        //método para pegar as informações da tela
    class procedure GetDadosForm();
            //método para definir as informações na tela
    class procedure SetDadosForm();
  end;

implementation

{ TSatVM }

uses uAuRepository;

class procedure TSatVM.GetDadosForm;
var
  sat: TSAT;
begin
  sat := TSAT.Create;
  try
    sat.DLL:= FormHome.EdtTSatEquipamento_caminhoDLL.Text;
    sat.CodigoAtivacao:= FormHome.EdtTSatEquipamento_codigoAtivacao.Text;
    sat.VersaoXML:= StrToFloatDef(FormHome.EdtTSatEquipamento_versaoXML.Text, 0.07);
    sat.CodigoPagina:= StrToIntDef(FormHome.EdtTSatEquipamento_codPagina.Text, 0);
    sat.EhUTF:= FormHome.ChkTSatEquipamento_ehUtf.IsChecked;
    sat.EhSTDCall:= FormHome.swtchTSatEquipamento_ehSTDCall.IsChecked;
    sat.EnvioAutomatico:= FormHome.swtchTSatSistema_envioAutomatico.IsChecked;
    sat.Salvar;


    TAuditoriaRepository.GravarLog(
          'Configuração Alterada',
           FormHome.txtUsuario.Text + #13 +
          ' Foram alteradas as configurações do "SAT"' + #13 +
          ' Horario:' + TimeToStr(Now),
          'O');
  finally
    sat.Free;
  end;
end;

class procedure TSatVM.SetDadosForm;
var
  sat: TSAT;
begin
  sat := TSAT.Create;
  try
    FormHome.EdtTSatEquipamento_caminhoDLL.Text:= sat.DLL;
    FormHome.EdtTSatEquipamento_codigoAtivacao.Text:= sat.CodigoAtivacao;
    FormHome.EdtTSatEquipamento_versaoXML.Text:= FloatToStr(sat.VersaoXML);
    FormHome.EdtTSatEquipamento_codPagina.Text:= IntToStr(sat.CodigoPagina);
    FormHome.ChkTSatEquipamento_ehUtf.IsChecked:= sat.EhUTF;
    FormHome.swtchTSatEquipamento_ehSTDCall.IsChecked:= sat.EhSTDCall;
    FormHome.swtchTSatSistema_envioAutomatico.IsChecked:= sat.EnvioAutomatico;
  finally
    sat.Free;
  end;
end;

end.
